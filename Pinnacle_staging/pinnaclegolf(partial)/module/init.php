<?php

/* * ***************************************************************** */
/* MODULES 															 */
/* * ***************************************************************** */
/* Admin */
get_template_part('module/admin/init-settings');
get_template_part('module/admin/register-settings');
get_template_part('module/admin/custom-functions');



/* Theme Option */
get_template_part('module/theme-settings/theme-settings');


/* shortcode */
get_template_part('module/shortcodes/rd-copyright-date/rd-copyright-date');


?>