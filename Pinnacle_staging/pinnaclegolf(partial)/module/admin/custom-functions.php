<?php

/* function to return properly structured menu object */
global $mailslistarr; $mailslistarr = array();

if (!function_exists('return_wp_clean_menu_items')) {
    function return_wp_clean_menu_items($menuargs) {
        $menuarr = array();
        $submenuargs = $menuargs;

        if ($menuargs) {
            $ctr = 0;

            foreach ($menuargs as $menu):

                if ($menu->menu_item_parent == 0) {
                    $menuarr[$ctr]['menu'] = $menu;
                    $ctr1 = 0;

                    foreach ($menuargs as $submenu):

                        if ($submenu->menu_item_parent == $menu->ID) {
                            $menuarr[$ctr]['sub-menu'][$ctr1] = $submenu;
                            $ctr1++;
                        }

                    endforeach;

                    $ctr++;
                }

            endforeach;
        }

        return $menuarr;
    }
}

if (!function_exists('return_wp_menu_attr')) {
    function return_wp_menu_attr($args, $mode = 'li') {

        if ($mode == 'li') {
            $liclass = 'menu-item';
            $classattr = '';
            $liclass .=!empty($args->type) ? ' menu-item-type-' . esc_attr($args->type) : '';
            $liclass .=!empty($args->object) ? ' menu-item-object-' . esc_attr($args->object) : '';
            $liclass .=!empty($args->ID) ? ' menu-item-' . esc_attr($args->ID) : '';


            if (isset($args->classes)) {
                foreach ($args->classes as $key => $val) {
                    $classattr = explode('-slide-', $val);
                    $dataslide = ($classattr[0] === 'data') ? '" data-slide="' . end($classattr) . '' : null;
                    $liclass .= ' ' . esc_attr($val);
                }
            }

            $liclass .= ($dataslide != null) ? ' ' . $dataslide : null;
            return $liclass;
        } else if ($mode == 'link') {

            $linkattr = !empty($args->attr_title) ? ' title="' . esc_attr($args->attr_title) . '"' : '';
            $linkattr .=!empty($args->target) ? ' target="' . esc_attr($args->target) . '"' : '';
            $linkattr .=!empty($args->xfn) ? ' rel="' . esc_attr($args->xfn) . '"' : '';
            $linkattr .=!empty($args->url) ? ' href="' . esc_attr($args->url) . '"' : '';

            return $linkattr;
        }

        return '';
    }
}

/* list pages */
if (!function_exists('get_post_top_ancestor_id')) {

    /**
     * Gets the id of the topmost ancestor of the current page. Returns the current
     * page's id if there is no parent.
     * 
     * @uses object $post
     * @return int 
     */
    function get_post_top_ancestor_id() {

        global $post;

        if (is_page()) {
            if ($post->post_parent) {
                $ancestors = array_reverse(get_post_ancestors($post->ID));
                return $ancestors[0];
            }

            return $post->ID;
        }
    }

}

add_action('wp_footer', 'bh_footer_script');

if (!function_exists('bh_footer_script')) {

    function bh_footer_script() {
        $gacodecontent = get_field('google_analytics_code', 'option') ? get_field('google_analytics_code', 'option') : '';
        echo $gacodecontent;
    }
}


function wp_nav_wrap() {
    global $post;
    $postid = (have_posts()) ? $post->ID : null;
    $page_for_front_id  = get_option('page_on_front');
    $homepageclass      = ($postid == $page_for_front_id) ? ' current-menu-item' : null;
//    $shoponlinedisplay  = get_field('reach_digital_cta_toggle_display', 'option') ? get_field('reach_digital_cta_toggle_display', 'option') : false;
//    $shoponlinelink     = get_field('reach_digital_cta_link', 'option') ? get_field('reach_digital_cta_link', 'option') : '?#';
//    $shoponlinelabel    = get_field('reach_digital_cta_title', 'option') ? get_field('reach_digital_cta_title', 'option') : 'Shop online';



    $wrap = '<ul data-menu = "" id="%1$s" class="%2$s">';

    $wrap .= '<li class="menu-item menu-item-type-post_type menu-item-object-page' . $homepageclass . '">';
    $wrap .= '	<a class="home-link" href="' . home_url() . '">';
    $wrap .= '	</a>';
    $wrap .= '</li>';
    $wrap .= '%3$s';
        if (have_rows('pinnacle_social_icon_list', 'option')):
            while (have_rows('pinnacle_social_icon_list', 'option')) : the_row(); 
                      $social_link = get_sub_field('url',"option") ? get_sub_field('url',"option") : '#';
                      $name = get_sub_field("name", 'option');
                                        
                           $wrap.= '<li class="menu-item soical-icons"><a  class="social_booking" href="'.$social_link.'" target = "_blank">'
                                 .'<i class="fa fa-'.$name.'" ></i></a>';
                        
            endwhile;
        endif;
    $wrap .= '</li>';
    $wrap .= '</ul>';

    return $wrap;
}

/*
add_action('gform_post_submission_2', 'get_emails_list', 10, 2);  // contact form

function get_emails_list($entry, $form) {

    global $test_g_arr;

	 $test_g_arr = array("1","2","3");
echo "get_emails_list";
echo '<pre>';print_r($test_g_arr);
    global $mailslistarr;
    $event_type_value = $event_mails_to_receive = array();
    
    if (have_rows('contact_us_mails', 'option')):
        while (have_rows('contact_us_mails', 'option')): the_row();
            $event_type_value[] = get_sub_field("event_type_value", "option");
            $event_mails_to_receive[] = get_sub_field("event_mails_to_receive", "option");
        endwhile;
         endif;
    $dropdownSelectedVal = $entry["4"]; // get selected dropdown
    $key = array_search($dropdownSelectedVal, $event_type_value);
    
    array_push($mailslistarr, $event_mails_to_receive[$key]);
   
} */

add_action('gform_pre_submission_3', 'get_emails_contact', 10, 1);  // contact form

function get_emails_contact($form) {

    global $mailslistarr;
    $dropdownSelectedVal = $_POST['input_4']; // get selected dropdown
     $mailslistarr = explode(",",$dropdownSelectedVal);
   
}

add_filter( 'gform_pre_send_email', 'before_email' );
function before_email( $email ) {
    global $mailslistarr;
   // echo '<pre>';print_r($mailslistarr);die;
   $email['to'] = $mailslistarr;
    return $email;
  
}

add_action('gform_pre_submission_2', 'get_emails_general', 10, 2);  // General form

function get_emails_general($form) {
//echo '<pre>';print_r($_POST); die;
    global $mailslistarr;
    $dropdownSelectedVal = $_POST['input_5']; // get selected dropdown
     $mailslistarr = explode(",",$dropdownSelectedVal);
   
}



add_filter( 'gform_validation_1', 'schedule_validation' );
function schedule_validation($validation_result ) {
   // header('Location:' .$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/#teetime');
    
     // $datepickerval = str_replace("/", "-", $_POST['input_4']);
      $datepickerval = $_POST['input_4'];
      $dateExpload = explode("/",$_POST['input_4']);
      $datepickerval = $dateExpload[1].'-'.$dateExpload[0].'-'.$dateExpload[2];
        $startimearr = $_POST['input_8'];
        $startimearr["1"] = (!empty($startimearr["1"])) ? $startimearr["1"] : '00';
        $startTime = $startimearr["0"] . ':' . $startimearr["1"] . $startimearr["2"];
        $startDateTime = strtotime($datepickerval . $startTime);

        $endtimearr = $_POST['input_9'];
        $endtimearr["1"] = (!empty($endtimearr["1"])) ? $endtimearr["1"] : '00';
        $endTime = $endtimearr["0"] . ':' . $endtimearr["1"] . $endtimearr["2"];
        $endDateTime = strtotime($datepickerval . ' ' . $endTime);
        $form = $validation_result['form'];
 
        // set the form validation to false
       // $validation_result['is_valid'] = false;
 
        //finding Field with ID of 1 and marking it as failed validation
        foreach ($form['fields'] as $field) {

        //NOTE: replace 1 with the field you would like to validate

        if (!empty($startimearr["0"]) && !empty($endtimearr["0"])) {
            if ((!empty($startDateTime) && !empty($endDateTime))) {
                if ($startDateTime > $endDateTime) {

                    if ($field->id == '8' || $field->id == '9') {
                         $validation_result['is_valid'] = false;
                        $field->failed_validation = true;
                        $field->validation_message = 'Please check From time should not be greater than  To time';
                        break;
                    }
                }
                if ($startDateTime == $endDateTime) {
                    if ($field->id == '8' || $field->id == '9') {
                         $validation_result['is_valid'] = false;
                        $field->failed_validation = true;
                        $field->validation_message = 'Please check From time should not be equal to To time';
                        break;
                    }
                }
            }
        }
    }
    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
 
}
add_filter( 'gform_validation_4', 'bookteetime_validation' );
function bookteetime_validation($validation_result ) {
       // $datepickerval = str_replace("/", "-", $_POST['input_4']);
        $datepickerval = $_POST['input_4'];
      $dateExpload = explode("/",$_POST['input_4']);
      $datepickerval = $dateExpload[1].'-'.$dateExpload[0].'-'.$dateExpload[2];
        
        
        $startimearr = $_POST['input_8'];
        $startimearr["1"] = (!empty($startimearr["1"])) ? $startimearr["1"] : '00';
        $startTime = $startimearr["0"] . ':' . $startimearr["1"] . $startimearr["2"];
        $startDateTime = strtotime($datepickerval . $startTime);

        $endtimearr = $_POST['input_9'];
        $endtimearr["1"] = (!empty($endtimearr["1"])) ? $endtimearr["1"] : '00';
        $endTime = $endtimearr["0"] . ':' . $endtimearr["1"] . $endtimearr["2"];
        $endDateTime = strtotime($datepickerval . ' ' . $endTime);
        $form = $validation_result['form'];
 
        // set the form validation to false
       // $validation_result['is_valid'] = false;
 
        //finding Field with ID of 1 and marking it as failed validation
        foreach ($form['fields'] as $field) {

        //NOTE: replace 1 with the field you would like to validate

        if (!empty($startimearr["0"]) && !empty($endtimearr["0"])) {
            if ((!empty($startDateTime) && !empty($endDateTime))) {
                if ($startDateTime > $endDateTime) {

                    if ($field->id == '8' || $field->id == '9') {
                         $validation_result['is_valid'] = false;
                        $field->failed_validation = true;
                        $field->validation_message = 'Please check From time should not be greater than  To time';
                        break;
                    }
                }
                if ($startDateTime == $endDateTime) {
                    if ($field->id == '8' || $field->id == '9') {
                         $validation_result['is_valid'] = false;
                        $field->failed_validation = true;
                        $field->validation_message = 'Please check From time should not be equal to To time';
                        break;
                    }
                }
            }
        }
    }
    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
   // if($validation_result['is_valid'] == false){ header('Location:' .$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/#teetime');}
    //header('Location:' .$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'].'/#teetime');
    return $validation_result;
 
}


add_filter( 'gform_validation_2', 'general_validation' );
function general_validation($validation_result ) {
    
      //$datepickerval = str_replace("/", "-", $_POST['input_6']);
      
      $datepickerval = $_POST['input_6'];
      $dateExpload = explode("/",$_POST['input_6']);
      $datepickerval = $dateExpload[1].'-'.$dateExpload[0].'-'.$dateExpload[2];
      
        $startimearr = $_POST['input_9'];
        $startimearr["1"] = (!empty($startimearr["1"])) ? $startimearr["1"] : '00';
        $startTime = $startimearr["0"] . ':' . $startimearr["1"] . $startimearr["2"];
        $startDateTime = strtotime($datepickerval . $startTime);

        $endtimearr = $_POST['input_10'];
        $endtimearr["1"] = (!empty($endtimearr["1"])) ? $endtimearr["1"] : '00';
        $endTime = $endtimearr["0"] . ':' . $endtimearr["1"] . $endtimearr["2"];
        $endDateTime = strtotime($datepickerval . ' ' . $endTime);
        $form = $validation_result['form'];
 
        // set the form validation to false
       // $validation_result['is_valid'] = false;
 
        //finding Field with ID of 1 and marking it as failed validation
        foreach ($form['fields'] as $field) {

        //NOTE: replace 1 with the field you would like to validate

        if (!empty($startimearr["0"]) && !empty($endtimearr["0"])) {
            if ((!empty($startDateTime) && !empty($endDateTime))) {
                if ($startDateTime > $endDateTime) {

                    if ($field->id == '9' || $field->id == '10') {
                         $validation_result['is_valid'] = false;
                        $field->failed_validation = true;
                        $field->validation_message = 'Please check From time should not be greater than  To time';
                        break;
                    }
                }
                if ($startDateTime == $endDateTime) {
                    if ($field->id == '9' || $field->id == '10') {
                         $validation_result['is_valid'] = false;
                        $field->failed_validation = true;
                        $field->validation_message = 'Please check From time should not be equal to To time';
                        break;
                    }
                }
            }
        }
    }
    //Assign modified $form object back to the validation result
    $validation_result['form'] = $form;
    return $validation_result;
 
}

/* chk video type*/
//function videoType($url) {
//    if (strpos($url, 'youtube') > 0) {
//        return 'youtube';
//    } elseif (strpos($url, 'vimeo') > 0) {
//        return 'vimeo';
//    } else {
//        return 'unknown';
//    }
//}
