jQuery(document).ready(function () {
    jQuery('.custom_form.booking_form #exTab1 .nav-pills > li:first-child > a').addClass('active');
    jQuery('.datepicker').datepicker({
       // dateFormat: 'dd/mm/yy',
        minDate: 0,
    });
//    jQuery('.slider-for').slick({
//        slidesToShow: 1,
//        slidesToScroll: 1,
//        arrows: false,
//        fade: false,
//        focusOnSelect: true,
//        // dots: true,
//        infinite: true,
//        // mobileFirst: true,
//        asNavFor: '.slider-nav'
//    });
//    jQuery('.slider-nav').slick({
//       // slidesToShow: 9,
//       // slidesToScroll: 1,
//        //asNavFor: '.slider-for',
//      //  focusOnSelect: true,
//      //  arrows: false,
//      //  infinite: true,
//        // mobileFirst: true,
//    
//
//    });




    jQuery('a[data-slide]').click(function (e) {
        e.preventDefault();
        var slideno = jQuery(this).data('slide');
        // console.log(slideno);
        jQuery('.slider-nav').slick('slickGoTo', slideno - 1);
        jQuery(".nav-link").removeClass("active");
        jQuery('.nav-link[data-slide="' + slideno + '"]').addClass('active');
    });

    jQuery('.single-item').slick({
        dots: true,
        slidesToShow: 1,
        dragglable: false,
    });
    jQuery('.variable-width').slick({
        //   dots:true,
        slidesToShow: 1,
    });
    jQuery('.bdaylistclass').slick({
        //   dots:true,
        slidesToShow: 1,
    });
    jQuery(".slider").on("beforeChange", function (event, slick, currentSlide, nextSlide) {
        jQuery(".nav-link").removeClass("active");
        jQuery('.nav-link[data-slide="' + (nextSlide + 1) + '"]').addClass('active');
        jQuery('.nav-link[data-slide="' + (nextSlide + 1) + '"]').removeAttr('style');
//        if (!jQuery(jQuery.target).is('.panel-body')) {
//            jQuery('.collapse').collapse('hide');
//
//        }

    })
    jQuery(".slider").on("afterChange", function (event, slick, currentSlide, nextSlide) {
//        if (!jQuery(jQuery.target).is('.panel-body')) {
//            jQuery('.collapse').collapse('hide');
//
//        }
    })



    /*******Golf-Page-Birtday-Member-List*********/
//    var postsArr = new Array(),
//            jQuerypostsList = jQuery('.golf-memebers ul.members_list');
////Create array of all posts in lists
//    jQuerypostsList.find('li').each(function () {
//
//        postsArr.push(jQuery(this).html());
//    })
//Split the array at this point. The original array is altered.
//    var firstList = postsArr.splice(0, Math.round(postsArr.length / 2)),
//            secondList = postsArr,
//            ListHTML = '';
//    function createHTML(list) {
//        ListHTML = '';
//        for (var i = 0; i < list.length; i++) {
//            ListHTML += '<li>' + list[i] + '</li>'
//        }
//        ;
//    }
////Generate HTML for first list
//    createHTML(firstList);
//    jQuerypostsList.html(ListHTML);
////Generate HTML for second list
//    createHTML(secondList);
////Create new list after original one
//    jQuerypostsList.after('<ul class="members_list one"></ul>').next().html(ListHTML);

    var mapping = jQuery('.callout_section');
    //console.log(mapping);
    jQuery('.callout_section').click(function () {

        var popupboxId = jQuery(this).attr('id');

        if ($('.poup_msg:visible').length == 0) {
            jQuery('#popupbox' + popupboxId).css("display", "block");
            jQuery('body').css('overflow', 'hidden');
        }
    });




    jQuery('.poup_msg ._down_arrow').click(function () {
        var popupboxId = jQuery(this).attr('id');
        jQuery('#popupbox' + popupboxId).css("display", "none");
        jQuery('body').css('overflow', 'auto');
    });
    
    
    jQuery('.modal-container').on('click', function (e) {
        if (jQuery(e.target).hasClass('poup_msg') || jQuery(e.target).parents('.poup_msg').length > 0) {
            
        } else {
            jQuery('.modal-container').css("display", "none");
            jQuery('body').css('overflow', 'auto');
        }


    })



    if (jQuery(window).width() > 991) {

        jQuery(window).resize(function () {

            var left_Height = jQuery(".variable-width .slick-track").height();
            jQuery('.ameties_right_section').css('min-height', left_Height + "px");
            //console.log(left_Height);
        });
    } else {
        jQuery('.slider-mobile').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: false,
            asNavFor: '.variable-width',
            focusOnSelect: true,
            swipeToSlide: true,
            responsive: [
                {
                    breakpoint: 991,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                }


            ]

        });




    }


    //$('.sub_menu_section').css("display", "none");

    jQuery('.golf_collage_list #server_msg').hide();
    jQuery('.golf_collage_list .callout_section ._down_arrow button').on("click", function () {
        //alert(jQuery(this).text());
    });
    jQuery('.custom_form.booking_form #exTab1 .nav-pills > li:first-child > a').addClass('active');
    if (jQuery(window).width() > 992) {
        var left_Height = jQuery(".variable-width .slick-track").height();
        jQuery('.ameties_right_section').css('min-height', left_Height + "px");
    } else {

    }

    if (jQuery(window).width() < 991) {
        /* var showChar = 100;
         var ellipsestext = "...";
         var moretext = "Read More";
         var lesstext = "Read Less";
         $('.ameties_left_section .content_widht p').each(function () {
         var content = $(this).html();
         if (content.length > showChar) {
         var c = content.substr(0, showChar);
         var h = content.substr(showChar - 1, content.length - showChar);
         var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
         $(this).html(html);
         }
         });
         
         $("a.morelink").click(function () {
         if ($(this).hasClass("less")) {
         $(this).removeClass("less");
         $(this).html(moretext);
         } else {
         $(this).addClass("less");
         $(this).html(lesstext);
         }
         $(this).parent().prev().toggle();
         $(this).prev().toggle();
         return false;
         });
         
         jQuery(".slider").on("swipe", function (event, slick, currentSlide, nextSlide) {
         console.log('swipe');
         jQuery('.morecontent span').css('display', 'none');
         //jQuery('.morecontent  .morelink').html('Read More')
         jQuery('.morecontent span').css('display', 'none');
         jQuery('.morecontent  .morelink').removeClass('less');
         jQuery('.morecontent .morelink').html('Read More');
         })
         jQuery(".slider").on("afterChange", function (event, slick, currentSlide, nextSlide) {
         console.log('afterChange');
         jQuery('.morecontent span').css('display', 'none');
         jQuery('.morecontent  .morelink').removeClass('less');
         jQuery('.morecontent .morelink').html('Read More');
         
         })*/

        jQuery(".slider").on("swipe", function (event, slick, direction) {
            console.log(slick)
            if (direction == 'left') {
                jQuery('.slick-next').trigger('click');
            } else {
                jQuery('.slick-prev').trigger('click');
            }
        });

    }

    //console.log(left_Height);
    jQuery(".post-password-form input[type='submit']").val("submit");
});

jQuery(document).ready(function () {
    function swap_images() {
        var img_left = jQuery('.current_slide img').attr('src');
        var heading_left = jQuery('.current_slide .sb-heading').html();
        var desc_left = jQuery('.current_slide .sb-content').html();
        var img_title_left = jQuery('.current_slide .imgtitle-content').html();

        var img_right = jQuery('.next_slide img').attr('src');
        var heading_right = jQuery('.next_slide .sb-heading').html();
        var desc_right = jQuery('.next_slide .sb-content').html();
        var img_title_right = jQuery('.next_slide .imgtitle-content').html();


        jQuery('.current_slide img').attr('src', img_right);
        jQuery('.current_slide .sb-heading').html(heading_right);
        jQuery('.current_slide .sb-content').html(desc_right);
        jQuery('.current_slide .imgtitle-content').html(img_title_right);

        jQuery('.next_slide img').attr('src', img_left);
        jQuery('.next_slide .sb-heading').html(heading_left);
        jQuery('.next_slide .sb-content').html(desc_left);
        jQuery('.next_slide .imgtitle-content').html(img_title_left);

        jQuery("#home-carousel2-heading").html(jQuery('.current_slide .sb-heading').html());
        jQuery("#home-carousel2-content").html(jQuery('.current_slide .sb-content').html());
    }

    function calculate_dimensions() {
        jQuery('.current_slide').css('width', jQuery('#bh_slider_right_content').width() * 0.62 + 'px');
        jQuery('.next_slide').css('width', jQuery('#bh_slider_right_content').width() * 0.32 + 'px');
        var small_height = jQuery('.current_slide img').height();
        if (small_height > jQuery('.next_slide img').height()) {
            small_height = jQuery('.next_slide img').height();
        }
        jQuery('#bh_slider_right_content').css('height', small_height + 'px');
        //  jQuery('#bh_slider_right_content li,.slide-button-prev,.slide-button-next').on('click', function () {

        jQuery('.next_slide').on('click', function () {
            swap_images();
        });


    }

    jQuery(document).ready(function () {
        if (jQuery(window).width() > 767) {
            calculate_dimensions();
        }
        /*
         jQuery('#bh_slider_right_content li').on("swipeleft", function () {
         swap_images();
         });
         jQuery('#bh_slider_right_content li').on("swiperight", function () {
         swap_images();
         }); */

        jQuery(".properties-tab-img").on("click", function () {
            tab_id = jQuery(this).attr("data-tab-id");
            //console.log(tab_id);
            jQuery(".properties-tab-content").removeClass("active show");
            jQuery("#" + tab_id).addClass("active show");
        });

        jQuery(".properties_show_more").on("click", function () {
            tab_id = jQuery(this).attr("data-tab-id");
            //console.log(tab_id);
            //jQuery(".properties-mobile-content").removeClass("active show");
            jQuery("#" + tab_id).addClass("active show");
        });

        jQuery(".vertical-pagination-menu").on("click", function () {
            //jQuery(".vertical-pagination-menu").removeClass("active");
            //jQuery(this).addClass("active");
            target = jQuery(this).attr("data-section-id");
            var scroll_top = jQuery("#" + target).offset().top;
            jQuery("html, body").animate({
                scrollTop: scroll_top
            }, 800);
        });

        jQuery(".scroll_to_link").on("click", function () {
            target = jQuery(this).attr("data-section-id");
            var scroll_top = jQuery("#" + target).offset().top;
            jQuery("html, body").animate({
                scrollTop: scroll_top
            }, 800);
        });

        jQuery(window).on('resize', function () {
            if (jQuery(this).width() < 767) {
                jQuery('.current_slide').css('width', '');
                jQuery('.next_slide').css('width', '');


            } else {
                calculate_dimensions();
            }


        });



        //jQuery("#vertical-pagination").scrollspy();
    });
    
    /*
     jQuery('#bh_slider_right_content li').on("swipeleft", function () {
     swap_images();
     });
     jQuery('#bh_slider_right_content li').on("swiperight", function () {
     swap_images();
     }); */

    jQuery(".properties-tab-img").on("click", function () {
        tab_id = jQuery(this).attr("data-tab-id");
        console.log(tab_id);
        jQuery(".properties-tab-content").removeClass("active show");
        jQuery("#" + tab_id).addClass("active show");
    });

    jQuery(".properties_show_more").on("click", function () {
        tab_id = jQuery(this).attr("data-tab-id");
        console.log(tab_id);
        //jQuery(".properties-mobile-content").removeClass("active show");
        jQuery("#" + tab_id).addClass("active show");
    });

    jQuery(".vertical-pagination-menu").on("click", function () {
        //jQuery(".vertical-pagination-menu").removeClass("active");
        //jQuery(this).addClass("active");
        target = jQuery(this).attr("data-section-id");
        var scroll_top = jQuery("#" + target).offset().top;
        jQuery("html, body").animate({
            scrollTop: scroll_top
        }, 800);
    });

    jQuery(".scroll_to_link").on("click", function () {
        target = jQuery(this).attr("data-section-id");
        var scroll_top = jQuery("#" + target).offset().top;
        jQuery("html, body").animate({
            scrollTop: scroll_top
        }, 800);
    });

//    $(document).click(function (e) {
//        if (!$(e.target).is('.panel-body')) {
//            $('.collapse').collapse('hide');
//        }
//    });

    jQuery('body.page-template-golfClub-page .sub_menu_section,body.page-template-golfClub-page ul.sub-menu , body.page-template-womensClub-page .sub_menu_section,body.page-template-womensClub-page ul.sub-menu').show();

    jQuery('.blue-text-small .collapse').on('shown.bs.collapse', function () {
        jQuery(this).parent().find("span").removeClass("circle-plus").addClass("circle-minus");
    }).on('hidden.bs.collapse', function () {
        jQuery(this).parent().find("span").removeClass("circle-minus").addClass("circle-plus");
    });

    /* redirects to bokings showing tee time form*/
    if (window.location.hash) {
        var hash_val = window.location.hash;
        hash_val = hash_val.replace("#", "");
        //console.log(hash_val);
        jQuery(".generalcls").removeClass("active");
        jQuery(".teetimecls").addClass("active");
    }

    jQuery.each(jQuery.browser, function (i) {
        jQuery('body').addClass(i);
        return false;
    });


// Get OS
    var os = [
        'iphone',
        'ipad',
        'windows',
        'mac',
        'linux'
    ];

    var match = navigator.appVersion.toLowerCase().match(new RegExp(os.join('|')));
    if (match) {
        jQuery('body').addClass(match[0]);
    }
    ;


    // Smooth scrolling using jQuery easing
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: (target.offset().top - 57)
                }, 1000, "easeInOutExpo");
                return false;
            }
        }
    });

    // Closes responsive menu when a scroll trigger link is clicked
    $('.js-scroll-trigger').click(function () {
        $('.navbar-collapse').collapse('hide');
    });

    // Activate scrollspy to add active class to navbar items on scroll
    $('body').scrollspy({
        target: '#mainNav',
        offset: 57
    });
    // Collapse Navbar
    var navbarCollapse = function () {
        if ($("#mainNav").offset().top > 100) {
            $("#mainNav").addClass("navbar-shrink");
        } else {
            $("#mainNav").removeClass("navbar-shrink");
        }
    };
    // Collapse now if page is not at top
    navbarCollapse();
    // Collapse the navbar when page is scrolled
    $(window).scroll(navbarCollapse);

    jQuery('.numericvalid input').focus(function (e) {
        var fieldId = $(this).attr('id');
        $('#' + fieldId).keypress(validateNumber);

    });
    if (jQuery(window).width() < 991) {
        jQuery('.section_2 .bh-textslider .bh-slick-slider2 ul .next_slide img').on('click', function (e) {
            e.stopPropagation()
        });
    }

});
function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if (key < 48 || key > 57) {
        return false;
    } else {
        return true;
    }
}
