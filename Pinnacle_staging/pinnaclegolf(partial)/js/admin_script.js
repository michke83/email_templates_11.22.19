jQuery(document).ready(function($){

    $('.icp-auto').iconpicker();
    $('.first-icon').on('click', function() {
        $(".first-icon .iconpicker-popover").show();
        $(".first-icon .iconpicker-popover").addClass("in");
    });         
     $('.second-icon').on('click', function() {
        $(".second-icon .iconpicker-popover").show();
        $(".second-icon .iconpicker-popover").addClass("in");
    });
    
    $("#contextual-help-link").click(function () {
        $("#contextual-help-wrap").css("cssText", "display: block !important;");
    });
    $("#show-settings-link").click(function () {
        $("#screen-options-wrap").css("cssText", "display: block !important;");
    });
    
    
});