
<?php
$title = get_field("section_one_title");
$desc = get_field("section_one_description");
 if(is_front_page() || is_page_template('page-templates/about-page.php')){ $opportunityaboutcls= 'opportunityabout';} else { $opportunityaboutcls= '';}
?>
<!-- opportunity_awaits -->
<section class="spacing_bar <?php echo $opportunityaboutcls;?>">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto text-sm-center">
                <h2 class="section-heading blue-text"><?php echo $title; ?></h2>

            </div>
            <div class="col-lg-10 mx-auto text-sm-leftr"><?php echo $desc; ?></div>
            
        </div>
    </div>
</section>
<!-- opportunity_awaits -->