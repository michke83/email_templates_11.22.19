<?php 
$bannerVideo = get_field("banner_video_link");
$banner_image = get_field("banner_image")?get_field("banner_image"):get_template_directory_uri().'/images/propertiesheader.png';;
$bannerBtnTitle  = get_field("banner_title");
$bannerBtnText = get_field("banner_button_text");
$bannerBtnLink = get_field("banner_button_link")?get_field("banner_button_link"):'#'; 

 if(!is_front_page()): ?>
<!-- jumbotron -->
    <header  class="masthead text-center text-white d-flex" style="background-image:url(<?php echo $banner_image;?>)">
      <div class="container my-auto">
        <div class="row">
          <div class="col-lg-10 col-md-10 mx-auto">
            <h1 class="bottom_space bottom_top">
              <?php echo $bannerBtnTitle;?>
            </h1>
            <!-- <hr> -->
          </div>
        </div>
      </div>
    </header>
    <!-- /jumbotron -->
<?php endif;?>
