
<!-- contact accordions -->
<!--<div class="container contact-accordions ml-lg-5">-->

<div class="panel-group" id="accordion">
    <?php
    $j = 0;
    if (have_rows('main_titles')):
        while (have_rows('main_titles')): the_row();
            $j++;
            $main_title = get_sub_field('main_title');
            $sub_titles_content = get_sub_field('sub_titles_content');
            $main_title_link = get_sub_field('main_title_link') ? get_sub_field('main_title_link') : '#';
            ?>
            <!-- first accordion -->
            
<!--           <div>
                <div class="blue-text ">
                    <a class="toggle<?php //echo $j; ?> my-auto font-weight-bold blue-text" href="<?php //echo $main_title_link; ?>" data-target="#sales_expand<?php// echo $j;?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sales_expand<?php //echo $j;?>">
                        <span><img src="" class="circle-plus-<?php// echo $j;?> circle-plus"></span><span class="ml-1 font-weight-bold"><?php //echo $main_title; ?></span></a>
                </div>
                <div class="collapse ml-5" id="<?php //echo 'sales_expand'.$j;?>">
                <?php //echo $sub_titles_content; ?>
       
                </div>
            </div>-->
           
            
            
  <div class="blue-text-small">
    <div role="tab" id="heading<?php echo $j; ?>">
            <a data-toggle="collapse" data-parent="#accordion" class="my-auto accordion-toggle" href="#collapse<?php echo $j; ?>" aria-expanded="true" aria-controls="collapse<?php echo $j; ?>">
                <span class="circle-plus d-inline-block"></span><?php echo $main_title; ?>
        </a>
      </div>

    <div id="collapse<?php echo $j; ?>" class="collapse" role="tabpanel" aria-labelledby="heading<?php echo $j; ?>">
      <div class="card-block">
         <?php echo $sub_titles_content; ?>
      </div>
    </div>
  </div>
            
            
            <!-- /first accordion -->


            <!-- second accordion -->
            <!--              <div>
                            <div class="blue-text ">
                              <a class="toggle2 my-auto ml- font-weight-bold blue-text" href="#" data-target="#sales_expand2" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="sales2_expand">
                                <span><img src="" class="circle-plus-2 circle-plus"></span><span class="ml-1 font-weight-bold">SALES - Lots, Homes, Rentals</span></a>
                            </div>
                            <div class="collapse ml-5" id="sales_expand2">
                              <p>Contact Blue Heron:</p>
                              <p>blueheronproperties.com</p>
                            </div>
                          </div>
                           /second accordion 
            
                           third accordion 
                          <div>
                            <div class="blue-text ">
                              <a class="toggle3 my-auto ml- font-weight-bold blue-text" href="#" data-target="#sales_expand3" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="head_golf_expand">
                                <span><img src="" class="circle-plus-3 circle-plus"></span><span class="ml-1 font-weight-bold">HEAD GOLF PROFESSIONAL</span></a>
                            </div>
                            <div class="collapse ml-5" id="sales_expand3">
                              <p>Head Golf Professional</p>
                              <p>HGProfessional@pinnaclegolfclub.com</p>
                            </div>
                          </div>-->
            <!-- /third accordion -->
    <?php endwhile;
endif; ?>
            </div>
<!-- /contact accordions -->
