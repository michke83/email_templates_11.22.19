<?php
$ll_image1 = get_field("ll_image1") ? get_field("ll_image1") : get_template_directory_uri() . '/images/clubhouse.png';
$ll_image_title1 = get_field("ll_image_title1");
$ll_title1 = get_field("ll_title1");
$ll_description1 = get_field("ll_description1");
$ll_buttontext1 = get_field("ll_buttontext1");
$ll_buttonlink1 = get_field("ll_buttonlink1") ? get_field("ll_buttonlink1") : '#';

$ll_image2 = get_field("ll_image2") ? get_field("ll_image2") : get_template_directory_uri() . '/images/shoreline.png';
$ll_image_title2 = get_field("ll_image_title2");
$ll_title2 = get_field("ll_title2");
$ll_description2 = get_field("ll_description2");
$ll_buttontext2 = get_field("ll_buttontext2");
$ll_buttonlink2 = get_field("ll_buttonlink2") ? get_field("ll_buttonlink2") : '#';
?>


<div class="bh-textslider d-none d-sm-none d-md-none d-lg-block">
    <div class="slide-button-prev"></div>
    <div class="slide-button-next"></div>
    <div class="row">
        <div class="col-md-6" id="bh_slider2_content">
            <div class="bh-textslider_content">
                <div class="entry-header">
                    <h3 id="home-carousel2-heading"><?php echo $ll_title1; ?></h3>
                </div>
                <div class="entry-content" id="home-carousel2-content">
                    <?php echo $ll_description1; ?>
                </div>
                <div class="entry-footer bh-textslider-btn">
                    <a href="<?php echo $ll_buttonlink1; ?>"><?php echo $ll_buttontext1; ?></a>        
                </div>
            </div>
        </div>
        <div class="col-md-6 pad-left0 pad-right0 p-0" id="bh_slider_right_content">

            <div class="bh-slick-slider2">
                <ul>

                    <li class="current_slide" id="current_slide">
                        <img src="<?php echo $ll_image1; ?>">

                        <div class="sb-heading" style="display:none;"><?php echo $ll_title1; ?></div>
                        <div class="sb-content" style="display:none;">  <?php echo $ll_description1; ?>
                        </div>
                        <div class="home_side_content_div"><h2 class="imgtitle-content"><?php echo $ll_image_title1; ?></h2></div>

                    </li>

                    <li class="next_slide last_carousel" id="next_slide">
                        <img src="<?php echo $ll_image2; ?>">

                        <div class="sb-heading" style="display:none;"><?php echo $ll_title2; ?></div>
                        <div class="sb-content" style="display:none;"> <?php echo $ll_description2; ?> 
                        </div>
                        <div class="home_side_content_div" style="right: 0px;">
                            <h2 class="imgtitle-content"><?php echo $ll_image_title2; ?></h2>
                            <a class="link">Click to view</a>
                        </div>

                    </li>

                </ul>

            </div>
        </div>
    </div>
</div>

<!-- mobile section -->

<div class="bh-textslider d-block d-sm-block d-md-block d-lg-none">
    <div class="row">
        <div class="col-md-5" id = 'bh_slider2_content'>

        </div>
        <div class="col-md-12" id="bh_slider_right_content">

            <div class="bh-slick-slider2" >
                <ul>

                    <li class="current_slide" id = "current_slide">
                        <div class="mobile_home_secton">
                            <img src="<?php echo $ll_image1; ?>">
                            <h2 class="imgtitle-content"><?php echo $ll_image_title1; ?></h2>
                        </div>
                        <div class = "sb-heading" style = "display:none;"><?php echo $ll_title1; ?></div>
                        <div class = "sb-content" style = "display:none;"><?php echo $ll_description1; ?></div>
                        <div class="bh-textslider_content">
                            <div class="entry-header" >
                                <h3 id = "home-carousel2-heading"><?php echo $ll_title1; ?></h3>
                            </div>
                            <div class="entry-content" id = "home-carousel2-content">

                                <?php echo $ll_description1; ?>
                                <!--
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                                </p> -->
                            </div>
                        </div>
                    </li>

                    <li class="next_slide" id = "next_slide">
                        <div class="mobile_home_secton">
                            <img src="<?php echo $ll_image2; ?>">
                            <h2 class="imgtitle-content"><?php echo $ll_image_title2; ?></h2>
                        </div>
                        <div class = "sb-heading" style = "display:none;"><?php echo $ll_title2; ?></div>
                        <div class = "sb-content" style = "display:none;"><?php echo $ll_description2; ?></div>
                        <div class="bh-textslider_content">
                            <div class="entry-header" >
                                <h3 id = "home-carousel2-heading"><?php echo $ll_title2; ?></h3>
                            </div>
                            <div class="entry-content" id = "home-carousel2-content">

                                <?php echo $ll_description2; ?>
                                <!--
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                                </p> -->
                            </div>
                        </div>
                    </li>
                    <!--
                    <li class="next_slide canSlide" id = "next_slide" >
                        <img src="http://202.63.105.89/blueheron/wp-content/uploads/2018/01/fishingpoleondock.png" alt="home">
                    </li> -->

                </ul>

            </div>
        </div>
    </div>
</div>