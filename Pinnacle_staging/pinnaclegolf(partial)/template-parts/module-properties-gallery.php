
<?php
$galleryBackImg = array();
 if (have_rows('gallery_images')):
   while (have_rows('gallery_images')): the_row(); 
  $galleryBackImg[] = get_sub_field("image");
  $galleryBackVideo[] = get_sub_field("video");
  $galleryFieldType[] = get_sub_field("select_field_to_display");
   endwhile;
  endif;
?>
    <!-- Picture collage properties -->
<section class="picture_collage container">
      <div class="row container mx-auto">
    <?php if($galleryFieldType[0] == 'video'){
        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $galleryBackVideo[0], $match);
         $youtube_id = $match[1];
        ?>
        <div class="properties-left collage-left col-lg mr-lg-1 mb-1 mb-lg-0 mobile_pad">         
            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $youtube_id;?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>    
        </div>
          <?php } else {?>
            <div class="properties-left collage-left col-lg mr-lg-1 mb-1 mb-lg-0" style="background-image:url(<?php echo $galleryBackImg[0];?>); background-size:cover;">      
        </div>  
          <?php }
?>
        <div class="col-lg ml-lg-1 mt-1 mt-lg-0">
          <div class="row mb-2">
            <div class="properties-right-top-left collage-right-top-left col mr-1" style="background-image:url(<?php echo $galleryBackImg[1];?>)">
             </div>
            <div class="properties-right-top-right collage-right-top-right col ml-1" style="background-image:url(<?php echo $galleryBackImg[2];?>)">
            </div>
          </div>
          <div class="row mt-2">
            <div class="properties-right-bottom collage-right-bottom col" style="background-image:url(<?php echo $galleryBackImg[3];?>)">
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /Picture collage properties -->
