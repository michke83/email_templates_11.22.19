<?php
$up_events_title = get_field("up_events_title");
$up_events_description = get_field("up_events_description");
?>
<!-- Upcoming Events -->
<section>
    <div class="container my-3">
        <h2 id="schedule" class="text-center blue-text heading my-5"><?php echo $up_events_title; ?></h2>
        <p class="text-center"><a href="#" class="text-muted"></p>
        <ul  class="list-group">
            <?php
            $i = 0;
            if (have_rows('up_events_list')):
                while (have_rows('up_events_list')): the_row();
                    $i++;if($i==1) $firstchildClass = 'firstchild'; else $firstchildClass = '';
                    $event_date = date('F d - Y',strtotime(get_sub_field("event_date"))); //get_sub_field("event_date");
                    $event_name = get_sub_field("event_name");
                    $events_more_text = get_sub_field("events_more_text");
                    $events_more_content = get_sub_field("events_more_content");
                    ?>
                    <li class="list-group-item <?php echo $firstchildClass;?>">
                        <div class="d-flex justify-content-between">
                            <p class="my-3 blue-text-small"><?php echo $event_date; ?><span class="text-muted ml-5"><?php echo $event_name; ?></span></p>
                            <a class="badge my-auto ml-4 text-muted align-middle" href="#" data-target="#moreinfo<?php echo $i; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="moreinfo1"><span class="underline-blue">MORE INFO +</span></a>
                        </div>
                        <div class="collapse" id="moreinfo<?php echo $i; ?>"><?php echo $events_more_content; ?></div>
                    </li>
                <?php endwhile; ?>
            <?php endif; ?>
            <!--          <li class="list-group-item borderless">
                        <div class="d-flex justify-content-between">
                          <p class="my-3 blue-text">JANUARY 19 - 2018 <span class="text-muted ml-5">PWC Hosted Happy Hour</span></p>
                          <a class="badge my-auto ml-4 text-muted align-middle" href="#" data-target="#moreinfo2" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="moreinfo2"><span class="underline-blue">MORE INFO +</span></a>
                        </div>
                        <div class="collapse" id="moreinfo2"><p><small>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</small></p>
                        </div>
                      </li>-->



        </ul>
    </div>
</section>
<!-- /Upcoming Events -->

