<?php
$row = 0;
$frontimage = $backimage = '';
if (have_rows('common_images_overlap')):
    while (have_rows('common_images_overlap')): the_row();
        $row++;
        if ($row == 1)
            $defaultImg = get_template_directory_uri() . '/images/swapimg11.jpg';
        if ($row == 2)
            $defaultImg = get_template_directory_uri() . '/images/swapimg22.jpg';
        $imagename = get_sub_field("imagename") ? get_sub_field("imagename") : $defaultImg;
        ;
        $displaychk = get_sub_field("frontcheck");
        if ($displaychk == 'front: displayfront' ||
                $displaychk == 'back: displayback') {

            if ($displaychk == 'front: displayfront') {
                if ($row == 2 && $frontimage !=
                        '') {
                    $backimage = $imagename;
                } else {
                    $frontimage = $imagename;
                }
            }
        }
        if ($displaychk == 'back: displayback') {
            if ($row == 2 && $backimage != '') {
                $frontimage = $imagename;
            } else {
                $backimage = $imagename;
            }
        }
        ?>

    <?php endwhile; endif;?>
<div class="overlap_images_section">
    <div class="background_img" >
        <img src="<?php echo $backimage; ?>" class="img-fluid img1">
        <img class="img-fluid images img2" src="<?php echo $frontimage; ?>" alt="Mabank Overlapping Photo">  
    </div>

</div>