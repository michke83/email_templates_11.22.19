
<?php
$section_three_title = get_field("section_three_title");
$section_three_description  = get_field("section_three_description");
$section_three_button = get_field("section_three_button");
$section_three_button_link = get_field("section_three_button_link");
$row = 0;
$frontimage = $backimage = '';
if (have_rows('common_images_overlap')):
    while (have_rows('common_images_overlap')): the_row();
        $row++;
        if ($row == 1)
            $defaultImg = get_template_directory_uri() . '/images/swapimg11.jpg';
        if ($row == 2)
            $defaultImg = get_template_directory_uri() . '/images/swapimg22.jpg';
        $imagename = get_sub_field("imagename") ? get_sub_field("imagename") : $defaultImg;
        ;
        $displaychk = get_sub_field("frontcheck");
        if ($displaychk == 'front: displayfront' ||
                $displaychk == 'back: displayback') {

            if ($displaychk == 'front: displayfront') {
                if ($row == 2 && $frontimage !=
                        '') {
                    $backimage = $imagename;
                } else {
                    $frontimage = $imagename;
                }
            }
        }
        if ($displaychk == 'back: displayback') {
            if ($row == 2 && $backimage != '') {
                $frontimage = $imagename;
            } else {
                $backimage = $imagename;
            }
        }
        ?>

    <?php endwhile; endif;?>
 
            <!-- club_membership -->
    <section id="membership">
      <div class="container-fluid mt-0 pb-4">
        <div class="row align-items-center">
          <div class="col-lg-6 text-center">
            <div class="p-4 circles_seciton">
                <div class="circles_seciton_content">
                      <div class="eclips1">
                          <img class="img-fluid" src="<?php echo $backimage; ?>">
                    </div>
                    <div class="eclips2">
                        <img  class="img-fluid" src="<?php echo $frontimage; ?>">
                    </div>
                    
                </div>
              <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/images/design-icon.png" alt="">
            </div>
          </div>
          <div class="col-lg-5 align_center">
            <div class="px-4">
              <h2 class="mb-2 blue-text"><?php echo $section_three_title;?></h2>
              <?php echo $section_three_description;?>
              <span><a class="underline-blue" href="<?php echo $section_three_button_link;?>"><?php echo $section_three_button;?></a></span>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /club_membership -->
