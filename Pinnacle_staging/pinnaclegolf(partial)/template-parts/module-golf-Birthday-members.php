<?php
$bdayCount = 0;
$birthday_image = get_field("birthday_image") ? get_field("birthday_image") : get_template_directory_uri() . '/images/cake icon.png';
$birthday_month = get_field("birthday_month");
$total_birth_days = array();
if (have_rows('birthday_list')):
    while (have_rows('birthday_list')): the_row();
        $total_birth_days[] = get_sub_field("birth_each_day");
    endwhile;
    $bdayCount = count($total_birth_days);
endif;
//echo $bdayCount;
?>
<!-- blue section -->
<section id="membership_plans" class="container-fluid text-white golf-memebers" >
    <div class="container">
        <div class="row ">
            <div class="col-lg-8 order-2 text-left">
                <div class="px-4">
                    <?php if ($bdayCount <= 12) { ?>
                        <ul class="members_list">
                            <?php for ($bdayrow = 0; $bdayrow < $bdayCount; $bdayrow++) { ?>
                                <li><?php echo $total_birth_days[$bdayrow]; ?></li>
                            <?php } ?>                     
                        </ul>
                    <?php } else if ($bdayCount > 12) { ?>
                        <div class="slider bdaylistclass d-none d-sm-block d-md-block lg-d-block"> 
                            <?php
                            for ($bdayrow = 0; $bdayrow < $bdayCount; $bdayrow++) {
                                $breakrow = $bdayrow + 1;
                                if ($breakrow == 1 || $breakrow % 12 == 1) { ?>
                                    <div>
                                        <?php } 
                                        if ($breakrow == 1 || $breakrow % 6 == 1) { ?>
                                        <ul class="members_list"> 
                                        <?php } ?>
                                            
                                        <li><?php echo $total_birth_days[$bdayrow]; ?></li> 
                                        <?php if ($breakrow == $bdayCount || ($breakrow % 6) == 0) { ?>
                                        </ul>
                                        <?php } ?>
                                    <?php if ($breakrow == $bdayCount || $breakrow % 12 == 0) { ?>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div> 
                    <!-- mobile-->
                    <div class="slider bdaylistclass d-block d-sm-none d-md-none lg-d-none"> 
                            <?php
                            for ($bdayrow = 0; $bdayrow < $bdayCount; $bdayrow++) {
                                $breakrow = $bdayrow + 1;
                              
                                        if ($breakrow == 1 || $breakrow % 6 == 1) { ?>
                                      <div><ul class="members_list"> 
                                        <?php } ?>
                                            
                                        <li><?php echo $total_birth_days[$bdayrow]; ?></li> 
                                        <?php if ($breakrow == $bdayCount || ($breakrow % 6) == 0) { ?>
                                          </ul></div>
                                        <?php } ?>
                                
                            <?php } ?>
                        </div> 
                    <?php } ?>  
                                         
                </div>
            </div>
            <div class="col-lg-4 order-1 align-self-center ">
                <div class="text-center ">
                    <span>
                        <img class="img-fluid" src="<?php echo $birthday_image ?>" alt="Birtday Cake">
                        <h2 class="mb-2 "><?php echo $birthday_month; ?></h2></span>
                </div>
            </div>
        </div>

    </div>
</section>

<?php  //die(); ?>
<!-- /blue section -->
