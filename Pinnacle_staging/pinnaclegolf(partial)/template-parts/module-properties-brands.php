
<?php
$title = get_field('spectacular_title');
$address = get_field('spectacular_address');
$logoBackImg = array();
 if (have_rows('logo_images')):
   while (have_rows('logo_images')): the_row(); 
  $logoBackImg[] = get_sub_field("logoimage");
  $logoImglink[] = get_sub_field("logolink")?get_sub_field("logolink"):'#';
  endwhile;
  endif;
?>
    <!-- Spectacular Homesites -->
    <section id="opportunity">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-8 mx-auto text-sm-center">
            <h2 class="section-heading blue-text mb-4"><?php echo $title;?></h2>
<!--            <p class="my-5 text-muted">For more information or to arrange a tour contact our real estate office at <br> 1-888-340-0622 ext. 239,<br>e-mail our real estate sales department at <br><a href="#" class="blue-text"><u>ddlamb@pinnaclegolfclub.com</u></a></p>-->
           <?php echo $address;?>
          </div>
        </div>
        <div class="row text-center justify-content-center d-flex align-items-end my-5 mobile_section mobile_margin_btm">
          <div class="col-md-2 mb-4 mobile_margin_btm">
              <a class="visit_link" href="<?php echo $logoImglink[0];?>"><img class="img-fluid mb-sm-2" src="<?php echo $logoBackImg[0];?>"></a>
<!--                <span class="text-center text-uppercase underline-blue">Visit Site</span></a>-->
            <div>
            </div>
          </div>
          <div class="col-md-2 mb-4 mobile_margin_btm">
            <a class="visit_link" href="<?php echo $logoImglink[1];?>"><img class="img-fluid mb-sm-2 mb-md-4" src="<?php echo $logoBackImg[1];?>"></a>
<!--                <span class="text-center text-uppercase underline-blue">Visit Site</span></a>-->
            <div>
            </div>
          </div>
          <div class="col-md-3 mb-4 mobile_margin_btm">
            <a class="visit_link" href="<?php echo $logoImglink[2];?>"><img class="img-fluid mb-sm-2 mb-md-4" src="<?php echo $logoBackImg[2];?>"></a>
<!--                <span class="text-center text-uppercase underline-blue">Visit Site</span></a>-->
            <div>
            </div>
          </div>
          <div class="col-md-2 mb-4 mobile_margin_btm">
              <a class="visit_link" href="<?php echo $logoImglink[3];?>"><img class="img-fluid mb-sm-2 mb-md-5" src="<?php echo $logoBackImg[3];?>"></a>
<!--                <span class="text-center text-uppercase underline-blue">Visit Site</span></a>-->
            <div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /Spectacular Homesites -->
