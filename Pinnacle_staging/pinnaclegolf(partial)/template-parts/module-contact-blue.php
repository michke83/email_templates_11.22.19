
<?php
$special_event_title = get_field("special_event_title");
$special_event_description  = get_field("special_event_description");
$special_event_button = get_field("special_event_button");
$special_event_button_link = get_field("special_event_button_link")?get_field("special_event_button_link"):'#';
$special_event_image = get_field("special_event_image");
?>

 <!-- blue section -->
    <section id="planning_contact" class="container-fluid d-flex text-left text-white mt-0 py-3">
      <div class="pt-4 pb-1 container-fluid">
        <div class="row align-items-center">
          <div class="col-lg-1">
          </div>
          <div class="col-lg-4 text-left ml-5 order-3 mobile_section ">
            <div class="p-2 mobile_center">
              <img class="img-fluid" src="<?php echo $special_event_image; ?>" alt="Wedding Photo">
            </div>
          </div>
          <div class="col-lg-5 order-2 ml-5 mobile_section ">
            <div class="px-4">
              <h2 class="display-5 mb-2 white-heading"><?php echo $special_event_title;?></h2>
              <p class="mt-5 white-text"><?php echo $special_event_description;?></p>
              <p class="my-5"><a href="<?php echo $special_event_button_link;?>" class="text-white underline-white"><?php echo $special_event_button;?></a></p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /blue section -->
