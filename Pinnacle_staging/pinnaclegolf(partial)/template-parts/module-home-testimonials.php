<?php 
$TestimonialsBackImg = get_field("background_image","option")?get_field("background_image","option"): get_template_directory_uri().'/images/testimonials_background.png';?>
    <!-- testimonials -->
  <section id="testimonials" class="container-fluid d-flex text-center text-white mt-0 pt-0" style="background-image:url(<?php echo $TestimonialsBackImg;?>)">
      <div class="overlay"> </div>
      <div class="container my-auto">
          <div class="slider single-item">
           <?php
  if (have_rows('testimonials','option')):
   while (have_rows('testimonials','option')): the_row(); 
$TestimonialsTitle = get_sub_field("title_one","option");
$TestimonialsTitleTwo  = get_sub_field("title_two","option");
$TestimonialsLogo = get_sub_field("logo_image","option");
$TestimonialsBackImg = get_sub_field("background_image","option");
$TestimonialsBtnText = get_sub_field("button_text","option");
$TestimonialsBtnLink = get_sub_field("button_link","option")?get_sub_field("button_link","option"):'#'; ?>
        <div class="row">
          <div class="col-lg-12 mx-auto text-center">

            <div class="mb-5 pt-5">
              <img src="<?php echo $TestimonialsLogo; ?>" alt="D Magazine Logo">
            </div>
            <p><?php echo $TestimonialsTitle;?></p>
            <h1><?php echo $TestimonialsTitleTwo;?></h1>
            <div class="col-lg-8 mx-auto">
              <a class="text-white underline-white" href="<?php echo $TestimonialsBtnLink;?>"><?php echo $TestimonialsBtnText;?></a>
            </div>
    
          </div>
        </div>
          
  <?php endwhile; ?>
    <?php endif; ?>
          </div>
      </div>
     
    </section>

    <!-- /testimonials -->
