<?php
$title = get_field("contact_title","option");
$desc  = get_field("contact_description","option");
$form  = get_field("contact_form","option");
?>

<div class="bh_contact_sec panel">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contact_content">
                    <div class="entry-header">
                        <h2><?php echo ($title) ? $title : "Reach out and  we'll reach back.";?></h2>
                    </div>
                    <div class="entry-content">
                        <?php echo $desc;?>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-6 contact-sec">
                <?php echo do_shortcode($form);?>
            </div>
        </div>
    </div>
</div>

<!--id="bh_contact_sec" data-section-name="bh_contact_sec"  panel-->