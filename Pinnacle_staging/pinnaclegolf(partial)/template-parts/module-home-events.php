
<?php
$footerEventTitle = get_field("events_plan_title", "option");
$footerEventDesc = get_field("events_plan_decription", "option");
$footerEventImg = get_field("footer_image", "option");
$footerBtnText = get_field("events_plan_button_text", "option");
$footerBtnLink = get_field("events_plan_button_link", "option") ? get_field("events_plan_button_link", "option"): '#';

$row = 0;
$frontimage = $backimage = '';
if (have_rows('common_images_overlap','option')):
    while (have_rows('common_images_overlap','option')): the_row();
        $row++;
        if ($row == 1)
            $defaultImg = get_template_directory_uri() . '/images/swapimg11.jpg';
        if ($row == 2)
            $defaultImg = get_template_directory_uri() . '/images/swapimg22.jpg';
        $imagename = get_sub_field("imagename",'option') ? get_sub_field("imagename",'option') : $defaultImg;
        ;
        $displaychk = get_sub_field("frontcheck");
        if ($displaychk == 'front: displayfront' ||
                $displaychk == 'back: displayback') {

            if ($displaychk == 'front: displayfront') {
                if ($row == 2 && $frontimage !=
                        '') {
                    $backimage = $imagename;
                } else {
                    $frontimage = $imagename;
                }
            }
        }
        if ($displaychk == 'back: displayback') {
            if ($row == 2 && $backimage != '') {
                $frontimage = $imagename;
            } else {
                $backimage = $imagename;
            }
        }
        ?>

    <?php endwhile; endif;?>

<!-- special events -->
<section>
    <div class="py-4 container-fluid event-news">
        <div class="row align-items-center">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-4 text-center ml-2 order-3">
                <div class="p-4 circles_seciton">

                    <img class="" src="<?php echo get_template_directory_uri() ?>/images/circles2.png" alt="">

                    <div class="circles_seciton_content">
                        <div class="eclips1 right-align">
                            <img class="img-fluid" src="<?php echo $backimage; ?>">
                        </div>
                        <div class="eclips2 right-align2">
                            <img  class="img-fluid" src="<?php echo $frontimage; ?>">
                        </div>

                    </div>
                </div>
                <!--          <div class="col-lg-5 text-center">
<?php //get_template_part('template-parts/module', 'common-ovelapimages');  ?>
                          </div>-->
            </div>
            <div class="col-lg-5 order-2 ml-2">
                <div class="px-4 align_center">
                    <h2 class="display-5 mb-2 blue-text"><?php echo $footerEventTitle; ?></h2>
                    <p class="mt-5"><?php echo $footerEventDesc; ?></p>
                    <p class="mt-2"></p>
                    <a href="<?php echo $footerBtnLink; ?>" class=" underline-blue"><?php echo $footerBtnText; ?></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- special events -->
