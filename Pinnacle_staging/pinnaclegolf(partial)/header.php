<!DOCTYPE html>
<head>

    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <link href="<?php echo get_template_directory_uri() ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Domine:400,700" rel="stylesheet">
    
    <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400,700" rel="stylesheet">
    
   <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">

    <script src="https://use.fontawesome.com/a4de82742c.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?php
   // $faviconsrclink_opt = wp_get_attachment_image_src(get_field('bh_favicon', 'option'), 'full');
    //$faviconsrc = !empty($faviconsrclink_opt) ? $faviconsrclink_opt[0] : get_stylesheet_directory_uri() . '/images/favicon.ico';
   $faviconsrc = get_field("favicon", "option")? get_field("favicon", "option"):get_stylesheet_directory_uri() . '/images/favicon.ico'; 
    
    ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $faviconsrc; ?>">
    <meta name="theme-color" content="#ffffff ">
    <?php wp_head(); ?>
     <?php $header_analytics = get_field("header_analytics","option");
    if($header_analytics !=''){
        echo $header_analytics;
    }
    ?>
</head>
<?php $body_classes = array(); ?>
<body <?php body_class($body_classes); ?>  id="page-top" data-spy="scroll" data-target=".navbar .sub_menu_bar_1"> 
    <?php $body_analytics = get_field("body_analytics","option");
    if($body_analytics !=''){
        echo $body_analytics;
    }
    ?>
    <?php
    get_template_part('template-parts/module', 'menu');
    $pinnacle_logo = get_field("pinnacle_logo", "option");
    
    if(is_single()){ $closecalendar = 1;} else { $closecalendar = 2;}
    ?>

    <!-- pinnacle logo -->
    <div class="container">
        <a href="<?php echo get_home_url() ?>" class="pinnacle_logo js-scroll-trigger">
            <img class="fixed-top ml-5 pinnacle_logo mobile_section1" src="<?php echo $pinnacle_logo; ?>" style="z-index: 1031;" alt="Pinnacle Logo">
        </a>
        <?php if($closecalendar==1){ ?>
        <div class="seeicons"><a href="javascript:history.back()"><img src="http://202.63.105.89/pinnacle_wp/wp-content/themes/pinnaclegolf/images/seeicons.png'"></img></a></div>
        <?php }?>
    </div>
    <!-- /pinnacle logo -->




