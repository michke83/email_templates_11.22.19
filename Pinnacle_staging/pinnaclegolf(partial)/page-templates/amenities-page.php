<?php
/* template name: Amenities Page */
   
get_header();
$Title = get_field("title");
$Description  = get_field("description");
$BackgrndImg = get_field("image"); ?>

<div id="main">
    <div id="main-container">
 <!-- jumbotron -->

    <!-- /jumbotron -->
<?php get_template_part('template-parts/module', 'banner'); ?>
	   <!-- blue down arrow -->
    <div id="circle_down_arrow" class="container">
      <p><a href="#about_scroll_down" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png" alt="Blue Circle Arrow"></a></p>
    </div>
    <!-- /blue down arrow -->

	<div id="about_scroll_down"></div>

       <!-- resort life -->
      <section>
            <div id="resort-life-properties">
                <div class="row  container-fluid">
                    <div class="col-lg-2">
                    </div>
                    <div class="col-lg-4 text-center aminites_over_img px-xl-0">
                        <?php get_template_part('template-parts/module', 'common-ovelapimages'); ?>
                    </div>
                    <div class="col-lg-5 px-xl-0">
                        <div class="px-4 px-xl-0">
                            <h2 class="display-5 mb-2 blue-text"><?php echo $Title; ?></h2>
                            <p class="mt-4"><?php echo $Description; ?></p>
                        </div>
                    </div>
                     <div class="col-lg-1">
                    </div>
                </div>
            </div>
        </section>
    <!-- /resort life -->

<!--######### slick slider set up #############-->
<?php get_template_part('template-parts/module', 'amenities-slickslider-1'); ?>
<!--######### slick slider set up #############-->

        <!-- interested -->
     <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
    <!-- /interested -->
    
    
        <!-- testimonials -->
     <?php get_template_part('template-parts/module', 'home-testimonials'); ?>
    <!-- /testimonials -->
    
        <!-- special events -->
    <?php get_template_part('template-parts/module', 'home-events'); ?>
    <!-- special events -->
    
    <!-- special events -->
    <?php get_template_part('template-parts/module', 'amenities-mobile-slider'); ?>
    

    <!-- special events -->
       
    </div>
</div>
    

<?php get_footer(); ?>
