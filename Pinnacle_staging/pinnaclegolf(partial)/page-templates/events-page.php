<?php
/* template name: Events Page */

get_header();
?>

<div id="main">
    <div id="main-container">

        <!-- jumbotron -->
        <!--    <header id="events" class="masthead text-center text-white d-flex">
              <div class="container my-auto">
                <div class="row">
                  <div class="col-lg-8 col-md-10 mx-auto">
                    <h1 class="text-uppercase">
                      <strong>Discover Pinnacle Golf Club</strong>
                    </h1>
                     <hr> 
                  </div>
                </div>
              </div>
            </header>-->
        <!-- /jumbotron -->

        <?php get_template_part('template-parts/module', 'banner'); ?>

        <!-- blue down arrow -->
        <div id="circle_down_arrow" class="container" >
            <p><a href="#events_scroll_down" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png"></a></p>
        </div>

   
        <!-- /blue down arrow -->

        <div id="events_scroll_down"></div>


        <!-- Calendar -->
        <?php get_template_part('template-parts/module', 'events-calendar'); ?>
        <!-- Calendar -->

        <!-- Upcoming Events -->
        <?php get_template_part('template-parts/module', 'events-upcoming-events'); ?>
        <!-- Upcoming Events -->


        <!-- testimonials -->
        <?php //get_template_part('template-parts/module', 'home-testimonials'); ?>
        <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
        <!-- /testimonials -->

        <!-- special events -->
        <?php get_template_part('template-parts/module', 'home-events'); ?>
        <!-- special events -->

    </div>
</div>


<?php get_footer(); ?>
