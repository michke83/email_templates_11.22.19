<?php
/* template name: About Page */

get_header();
?>
<!--
<a href="#page-top" class="pinnacle_logo js-scroll-trigger">
            <img class="fixed-top ml-5 pinnacle_logo" src="http://202.63.105.89/pinnacle_wp/wp-content/uploads/2018/02/PinnacleLogo.png" style="z-index: 1031;" alt="Pinnacle Logo">
        </a>-->

<div id="main">
    <div id="main-container">

        <!-- jumbotron -->
        <!--    <header id="about" class="masthead text-center text-white d-flex">
              <div class="container my-auto">
                <div class="row">
                  <div class="col-lg-8 col-md-10 mx-auto">
                    <h1 class="text-uppercase">
                      <strong>Discover Pinnacle Golf Club</strong>
                    </h1>
                     <hr> 
                  </div>
                </div>
              </div>
            </header>-->
        <?php get_template_part('template-parts/module', 'banner'); ?>
        <!-- /jumbotron -->	    <!-- blue down arrow -->
        <div id="circle_down_arrow" class="container" >
            <p><a href="#about_scroll_down" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png"></a></p>
        </div>
        <!-- /blue down arrow -->
        <div id="about_scroll_down"></div>

        <!-- opportunity_awaits -->
        <?php get_template_part('template-parts/module', 'opportunity-section'); ?>
    <!--    <section>
          <div class="container">
            <div class="row">
              <div class="col-lg-8 mx-auto text-sm-center">
                <h2 class="section-heading blue-text">Opportunity Awaits.</h2>
                <p class="my-4 text-muted text-left">Welcome to the Pinnacle Golf Club, an upscale residential, recreational community of unparalleled quality and beauty located on Cedar Creek Lake. The fifth largest lake in Texas, just 70 minutes south east of Dallas. Pinnacle Club boasts properties along 7.5 miles of shoreline and the opportunity for a variety of custom-built homes. </p>
                <p class="text-left text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>
          </div>
        </section>-->
        <!-- /opportunity_awaits -->
        <?php
        $row = 1;
        if (have_rows('mabank_texas')):
            while (have_rows('mabank_texas')): the_row();
                $Title = get_sub_field("title");
                $Description = get_sub_field("description");
                // $BackgrndImg = get_sub_field("image");
                if ($row == 1) {
                    $cls1 = 'order-sm-2 order-lg-2 left-column my-5 mobile_section';
                    $cls2 = 'order-sm-1 order-lg-3 ml-5 mobile_section ';
                } else {
                    $cls1 = 'order-3 right-column my-5 mobile_section';
                    $cls2 = 'order-2 ml-5 mobile_section';
                }
                ;
                ?>


                <!-- Mabank Texas -->
                <section>
                    <div class="pb-4 container-fluid">
                        <div class="row align-items-center">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4 text-center ml-5  <?php echo $cls1; ?> mobile_section">
                                <div>
        <!--                                    <img class="img-fluid" src="<?php //echo $BackgrndImg;   ?>" alt="Mabank Overlapping Photo">-->
                                    <?php
                                    $row = 0;
                                    $frontimage = $backimage = '';
                                    if (have_rows('imagesoverlap')):
                                        while (have_rows('imagesoverlap')): the_row();
                                            $row++;
                                            if ($row == 1)
                                                    $defaultImg = get_template_directory_uri() . '/images/swapimg11.jpg';
                                            if ($row == 2)
                                                    $defaultImg = get_template_directory_uri() . '/images/swapimg22.jpg';
                                            $imagename = get_sub_field("imagename")
                                                        ? get_sub_field("imagename")
                                                        : $defaultImg;
                                            ;
                                            $displaychk = get_sub_field("frontcheck");
                                            if ($displaychk == 'front: displayfront' ||
                                                    $displaychk == 'back: displayback') {

                                                if ($displaychk == 'front: displayfront') {
                                                    if ($row == 2 && $frontimage !=
                                                            '') {
                                                        $backimage = $imagename;
                                                    } else {
                                                        $frontimage = $imagename;
                                                    }
                                                }
                                            }
                                            if ($displaychk == 'back: displayback') {
                                                if ($row == 2 && $backimage != '') {
                                                    $frontimage = $imagename;
                                                } else {
                                                    $backimage = $imagename;
                                                }
                                            }
                                            ?>

                                        <?php endwhile; ?>
                                        <?php
                                    endif;
                                    //echo $frontimage."---<br>".$backimage;
                                    //print_r($frontchecks);
                                    ?>
                                    <div class="overlap_images_section">
                                        <div class="background_img" >
                                            <img src="<?php echo $backimage; ?>" class="img-fluid img1">
                                            <img class="img-fluid images img2" src="<?php echo $frontimage; ?>" alt="Mabank Overlapping Photo">  
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-5 <?php echo $cls2; ?>">
                                <div class="px-3">
                                    <h2 class="display-5 mb-2 blue-text"><?php echo $Title; ?></h2>
                                    <p class="mt-5"><?php echo $Description; ?> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php
                $row++;
            endwhile;
            ?>
<?php endif; ?>
        <!-- /Mabank Texas -->

        <!-- Gun Barrel City Texas -->
       <!--    <section>
             <div class="pb-4 container-fluid">
               <div class="row align-items-center">
                 <div class="col-lg-1">
                 </div>
                 <div class="col-lg-4 text-center ml-5 order-3">
                   <div>
                     <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/images/GunBarrelCity.png" alt="Gun Barrel City Overlapping Photo">
                   </div>
                 </div>
                 <div class="col-lg-5 order-2 ml-5">
                   <div class="px-3">
                     <h2 class="display-5 mb-2 blue-text">Gun Barrel City, Texas</h2>
                     <p class="mt-5 text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                     tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                     quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                     consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                     cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                     proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                   </div>
                 </div>
               </div>
             </div>
           </section>-->
        <!-- /Gun Barrel City Texas -->

        <!-- interested -->
        <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
        <!-- /interested -->

        <!-- testimonials -->
        <?php get_template_part('template-parts/module', 'home-testimonials'); ?>
        <!-- /testimonials -->

        <!-- special events -->
<?php get_template_part('template-parts/module', 'home-events'); ?>
        <!-- special events -->

    </div>
</div>


<?php get_footer(); ?>
