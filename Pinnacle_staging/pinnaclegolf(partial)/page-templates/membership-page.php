<?php
/* template name: Membership Page */
   
get_header();
?>


    <!-- blue down arrow -->
    <div id="circle_down_arrow" class="container" >
      <p><a href="#membership_scroll_down" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png"></a></p>
    </div>
    <!-- /blue down arrow -->
<div id="main">
    <div id="main-container">

<?php get_template_part('template-parts/module', 'banner'); ?>
    <!-- /jumbotron -->




    <div id="membership_scroll_down"></div>

    <?php
$title = get_field("title");
$description = get_field("description");
$pdf_link = get_field("pdf_link")?get_field("pdf_link"):'#';
?>
    <!-- Member Application -->
    <section class="membership">
      <div class="container-fluid">
        <div class="row align-items-top">
          <div class="col-lg-1">
          </div>
          <div class="container col-lg-4 text-center order-3 mt-4 mx-0 px-0 member-application-imag">
            <div>
                <a href="<?php echo $pdf_link;?>" target="_blank"><img class="img-fluid member-application-image" src="<?php echo get_template_directory_uri() ?>/images/member_application.png" alt="member application image">
            </a></div>
          </div>
          <div class="col-lg-5 order-2 ml-5 mt-">
            <div class="px-2 container mt-">
              <h2 class="display-5 mb-2 blue-text mt-5"><?php echo $title;?></h2>
<!--              <h2 class="display-5 mb-2 blue-text">All year long.</h2>-->
              <p class="mt-4 text-muted"><?php echo $description;?></p>
<!--              <p class="text-muted mt-4">All levels of membership entitle Members to full use of the Restaurant and Clubhouse, access to our 24 hour Fitness Center, enjoyment of the Junior Olympic size swimming pool, use of the tennis courts and attendance to a wide variety of special Member events.</p>-->
<!--              <div>
                <a href="<?php //echo $button_link;?>" class="text-muted"><img src="<?php //echo get_template_directory_uri() ?>/arrow_right.png" class="my-auto right-arrow-image"><span class="ml-2 underline-blue"><?php echo $button_text;?></span></a>
              </div>-->
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /Member application -->


     
            </div>
          </div>
          <div class="col-lg-1">
          </div>
        </div>
      </div>
    </section>
    <!-- blue section -->
      <?php get_template_part('template-parts/module', 'membership-plan'); ?>
    <!-- /blue section -->

    <!-- blue section -->
 <section class="picture_collage container my-5">
      <?php get_template_part('template-parts/module', 'properties-gallery'); ?>
</section>
    <!-- /blue section -->
       
    </div>
</div>
    

<?php get_footer(); ?>
