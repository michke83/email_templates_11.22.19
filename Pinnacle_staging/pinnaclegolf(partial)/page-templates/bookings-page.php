<?php
/* template name: Bookings Page */

get_header();
$bookings_planning_title = get_field("bookings_planning_title");
$bookings_planning_description = get_field("bookings_planning_description");
$bookings_planning_video = get_field("bookings_planning_video");
$general_form_title = get_field("general_form_title");
$general_form_title_description = get_field("general_form_title_description");
$general_form_short_code = get_field("general_form_short_code");
$tee_form_title = get_field("tee_time_form_title");
$tee_time_form_title_description = get_field("tee_time_form_title_description");
$tee_form_short_code = get_field("tee_time_form_short_code");
//echo '<pre>';
//print_r($_POST);
if($_POST['gform_submit'] ==  4){ $tee_time_form_title_description= '';}
if($_POST['gform_submit'] ==  2){ $general_form_title_description= '';}

if(isset($_POST['input_11'])){
    if($_POST['input_11']!=''){ $bookactivecls= 'active'; $geneactivecls='';}
} else{
   $bookactivecls='';  $geneactivecls= 'active'; 
}
?>

<div id="main" class="bookings_section">
    <div id="main-container">

        <!-- Bookings -->
        <section class="bookings">
            <div class="container-fluid mt-5">
                <div class="row align-items-top">
                    <div class="col-lg-1 visible_lg">
                    </div>
                    <div class="col-lg-5 text-center order-3 mt-4 ml-5 mx-0 px-0 mobile_section">
                        <div class="px-2 container">
                            <div class="custom_form booking_form">

                                <div id="exTab1">	
                                    <ul  class="nav form_tabs">
                                        <li>
                                            <a  href="#1a" data-toggle="tab" class="generalcls <?php echo $geneactivecls;?>"><?php echo $general_form_title; ?></a>
                                        </li>
                                        <li><a href="#2a" data-toggle="tab" class="teetimecls <?php echo $bookactivecls;?>"><?php echo $tee_form_title; ?></a>
                                        </li>

                                    </ul>

                                    <div class="tab-content clearfix">
                                        <div class="tab-pane generalcls <?php echo $geneactivecls;?>" id="1a">
                                            <?php
                                            echo $general_form_title_description;
                                            echo do_shortcode($general_form_short_code);
                                            ?>
                                        </div>
                                        <div class="tab-pane teetimecls <?php echo $bookactivecls;?>" id="2a">

                                            <?php
                                            echo$tee_time_form_title_description;
                                            echo do_shortcode($tee_form_short_code);
                                            ?>
<!--                                        <img class="img-fluid bookings_image" src="<?php //echo get_template_directory_uri()        ?>/images/bookings_img.png" alt="bookings image Photo">-->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="">

                            <div class="video_section booking_mobile_video">

                                <?php
                                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $bookings_planning_video, $match);
                                $youtube_id = $match[1];
                                ?>

                                <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>     
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-5 order-2 ml-5 mt-4 mobile_section">
                        <div class="px-2 container mt-5">
                            <h2 class="display-5 mb-2 blue-text"><?php echo $bookings_planning_title; ?></h2>
                            <p class="mt-5"><?php echo $bookings_planning_description; ?></p>

                        </div>
                        <div class="video_section booking_desktop_video">

                            <?php
                            preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $bookings_planning_video, $match);
                            $youtube_id = $match[1];
                            ?>

                            <iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?php echo $youtube_id; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>     
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Bookings -->

        <!-- testimonials -->
        <?php get_template_part('template-parts/module', 'home-testimonials'); ?>
        <!-- /testimonials -->

        <!-- interested -->
        <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
        <!-- /interested -->

    </div>
</div>


<?php get_footer(); ?>
