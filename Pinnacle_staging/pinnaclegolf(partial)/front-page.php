<?php
/* template name: Front Page */
get_header();
?>
<div id="main">
    <div id="main-container">
        <div id="page-front" class="header-footer-gap">
            <?php get_template_part('template-parts/template', 'full-width'); ?>
        </div>
    </div>
</div>

 <!-- blue circle down arrow -->
    <div id="circle_down_arrow_home" class="container" >
        <p><a href="#index_scroll_down" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png" alt=""></a></p>
    </div>
    <!-- /blue circle down arrow -->

    <div id="index_scroll_down"></div>

    <!-- opportunity_awaits -->
    
    <?php get_template_part('template-parts/module', 'opportunity-section'); ?>
    <!-- /opportunity_awaits -->
    <!-- houses -->
    <?php //get_template_part('template-parts/module', 'home-clubhouse-section'); ?>
    <section class="section_2 panel" id="section_2" >
        <?php get_template_part('template-parts/module', 'home-carousel2'); ?>
    </section>
      <!-- /houses -->
    
        <!-- club_membership -->
     <?php get_template_part('template-parts/module', 'home-daydream-section'); ?>
    <!-- /club_membership -->
    
        <!-- interested -->
     <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
    <!-- /interested -->
    
    
        <!-- testimonials -->
     <?php get_template_part('template-parts/module', 'home-testimonials'); ?>
    <!-- /testimonials -->
    
        <!-- special events -->
    <?php get_template_part('template-parts/module', 'home-events'); ?>
    <!-- special events -->


<?php get_footer(); ?>
