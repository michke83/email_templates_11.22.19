<?php
get_header();
?>
<div id="main" class="main-content">
    <div id="main-container">

        <div class="inner-page-sec">
            <div class="post-content-sec">
                <div class="dialog">
                    <a href="#" class="close-classic"></a>
                </div>
                <div class="container">
                    <?php
                    if (have_posts()) :
                        while (have_posts()) : the_post();
                            $short_description = get_field('short_description');
                            ?>
                            <h2> <?php the_title(); ?></h2>
                            <p class="post-min-title"> <?php echo $short_description; ?> </p>   
                            <?php
                            the_content();
                        endwhile;
                    endif;
                    ?>
                </div>
            </div>
        </div>      
    </div>
</div>
