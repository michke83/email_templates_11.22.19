<?php
/**
*
* Template Name: Events
*
**/

  get_header();

  ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>

	          <section class="entry-content">
	            <?php the_title( '<div class="wrapper-large || ta-center"><h1 class="entry-title || templates-header">', '</h1></div>' ); ?>
	            <?php
	              the_content();
	            ?>
	          </section><!-- .entry-content -->

			<?php

			endwhile;
			?>



		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
