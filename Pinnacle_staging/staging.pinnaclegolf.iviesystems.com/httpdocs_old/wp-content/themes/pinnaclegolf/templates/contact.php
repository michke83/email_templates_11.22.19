<?php
/**
*
* Template Name: Contact
*
**/

	get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>

        <section class="entry-content">
          <?php the_title( '<div class="wrapper-large || ta-center"><h1 class="entry-title || templates-header">', '</h1></div>' ); ?>
        </section><!-- .entry-content -->

					<section class="grid || grid--large || wrapper-large || contact-form">
						<div class="flex-half" style="padding-top: 16px;">
							<div class="grid || grid--large || contact-info">
								<h5>VISIT</h5>
								<div><?php the_field('pinnacle_address', 'options'); ?></div>
							</div>
							<div class="grid || grid--large || contact-info">
								<h5>CALL</h5>
								<div><?php the_field('pinnacle_numbers', 'options'); ?></div>
							</div>
							<div class="grid || grid--large || contact-info">
								<h5>EMAIL</h5>
								<div><?php the_field('pinnacle_email', 'options'); ?></div>
							</div>
						</div>
						<div class="flex-half || contact-info">
		          <?php
		            the_content();
		          ?>
						</div>
					</section>

      <?php endwhile; ?>

    <?php endif; ?>
			<section class="wrapper-large || gmap">
				<!-- GOOGLE MAP -->
				<div id="googleMap" style="height:400px;"></div>
			</section>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
