<?php
/**
*
* Template Name: Calendar
*
**/

  get_header();

  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>

	          <section class="entry-content">
	            <?php the_title( '<div class="wrapper-large || ta-center"><h1 class="entry-title || templates-header">', '</h1></div>' ); ?>
	            <?php
	              the_content();
	            ?>
	          </section><!-- .entry-content -->

			<?php

			endwhile;
			?>
      <style>
      .month {
        color: #043062;
      }
      .month::after {
          background-color: #043062;
          content: '';
          display: block;
          height: 2px;
          width: 90%;
      }
      </style>
			<section>
			<!-- QUERY COURSES CPT -->

      <!-- current month query -->
      <?php
          $currentMonth = date('m');
          $nextMonth = date('m', strtotime('+1 months'));

          $currentMonthString = date('F');
          $nextMonthString = date('F', strtotime('+1 months'));
				  global $post;
			    $args = array(
            'post_type' => 'sc_event',
            'meta_query' => array(
            array(
                  'key'		=> 'sc_event_month',
                  'compare'	=> '=',
                  'value'		=> $currentMonth,
              )
            ),
            'orderby' => 'sc_event_date',
            'order' => 'ASC',
            'posts_per_page'=> '30',
            'no_found_rows' => true,
          );
			    $loop = new WP_Query( $args );

          if (!empty($args)) {
          ?>
            <div class="grid  || grid--medium || schedule || wrapper-large">
          <?php } ?>
          <div class="flex-half">
          <h3 class="month"><?php echo $currentMonthString; ?></h3>
          <?php

          while ( $loop->have_posts() ) : $loop->the_post();
					 ?>
              <div>

                <h5><?php echo get_the_title(); ?><span>: </span>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_day', true ) ); ?>
                <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_month', true ) ); ?>
                / <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_day_of_month', true ) ); ?>

                </h5>

                Time:
                <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_time_hour', true ) ); ?> :
                <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_time_minute', true ) ); ?>
                  -
                <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_end_time_hour', true ) ); ?> :
                <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_end_time_minute', true ) ); ?>

                <?php echo the_content(); ?>
              </div>

          <?php endwhile; // end of the loop. ?>

          <?php
          if (!empty($args)) {
          ?>
            </div>
          <?php } ?>


          <!-- next month query -->
          <?php
              $currentMonth = date('m');
              $nextMonth = date('m', strtotime('+1 months'));

              $currentMonthString = date('F');
              $nextMonthString = date('F', strtotime('+1 months'));
    				  global $post;
    			    $args = array( 'post_type' => 'sc_event',
              'meta_query' => array(
                array(
                      'key'		=> 'sc_event_month',
                      'compare'	=> '=',
                      'value'		=> $nextMonth,
                  )
                ),
                'orderby' => 'sc_event_start_date',
                'order' => 'ASC',
                'posts_per_page'=> '30',
              );
    			    $loop = new WP_Query( $args );

              if (!empty($args)) {
              ?>
              <div class="flex-half">
              <?php } ?>
              <h3 class="month"><?php echo $nextMonthString; ?></h3>
              <?php

              while ( $loop->have_posts() ) : $loop->the_post();
    					 ?>
                  <div>

                    <h5><?php echo get_the_title(); ?><span>: </span>
                    <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_day', true ) ); ?>
                    <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_month', true ) ); ?>
                    / <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_day_of_month', true ) ); ?>

                    </h5>

                    Time:
                    <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_time_hour', true ) ); ?> :
                    <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_time_minute', true ) ); ?>
                      -
                    <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_end_time_hour', true ) ); ?> :
                    <?php echo esc_html( get_post_meta( get_the_ID(), 'sc_event_end_time_minute', true ) ); ?>

                    <?php echo the_content(); ?>

              <?php endwhile; // end of the loop. ?>

              <?php
              if (!empty($args)) {
              ?>
                </div>
              <?php } ?>
            </div>
			</section>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
