<?php
/**
*
* Template Name: Ammenities
*
**/

  get_header();

  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>

            <section class="entry-content">
              <?php the_title( '<div class="wrapper-large || ta-center"><h1 class="entry-title || templates-header">', '</h1></div>' ); ?>
              <?php
                the_content();
              ?>
            </section><!-- .entry-content -->

            <section class="grid || grid--large || four-image-grid">
              <div>
                <img src="<?php the_field( 'image_grid_one' ); ?>" />
              </div>
              <div>
                <img src="<?php the_field( 'image_grid_two' ); ?>" />
              </div>
              <div>
                <img src="<?php the_field( 'image_grid_three' ); ?>" />
              </div>
              <div>
                <img src="<?php the_field( 'image_grid_four' ); ?>" />
              </div>
            </section>

            <section>
              <div class="text-one || wrapper-large">
                <?php the_field( 'image_grid_text_section' ); ?>
              </div>
            </section>

            <section>
              <div class="image-one">
                <img src="<?php the_field( 'image_section_one' ); ?>" />
              </div>
            </section>

            <section>
              <div class="text-one || wrapper-large">
                <?php the_field( 'text_section_one' ); ?>
              </div>
            </section>

            <section>
              <div class="image-two">
                <img src="<?php the_field( 'image_section_two' ); ?>" />
              </div>
            </section>

            <section class="grid">
              <div class="text-two || wrapper-large">
                <?php the_field( 'text_section_two' ); ?>
              </div>
            </section>

            <section>
              <div class="image-three">
                <img src="<?php the_field( 'image_section_three' ); ?>" />
              </div>
            </section>

            <section>
              <div class="text-three || wrapper-large">
                <?php the_field( 'text_section_three' ); ?>
              </div>
            </section>

            <section>
              <div class="image-four">
                <img src="<?php the_field( 'image_section_four' ); ?>" />
              </div>
            </section>

            <section>
              <div class="text-four || wrapper-large">
                <?php the_field( 'text_section_four' ); ?>
              </div>
            </section>
      <?php
			endwhile;
			?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
