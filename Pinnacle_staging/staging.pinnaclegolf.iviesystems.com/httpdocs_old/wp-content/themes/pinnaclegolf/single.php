<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package pinnaclegolf
 */

get_header(); ?>
<style>
.wrapper-large{
  padding-bottom:0px;
}
.entry-content{
    padding-top:0px;
    padding-bottom:20px;
}
.entry-title{
  color: #043062;
}
.sc_event_details{
  text-align:center;
  font-weight:600;
  color: #043062;
}
.nav-previous {
  float: left;
  padding-left: 10px;
}
.nav-previous > a {
  color: #043062 !important;
  font-size: 2.0rem;
  font-weight: 400;
}
.nav-next {
  float: right;
  padding-right: 10px;
}
.nav-next > a {
  color: #043062 !important;
  font-size: 2.0rem;
  font-weight: 400;
}
.nav-links {
  padding-bottom: 10px;
}

</style>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		while ( have_posts() ) : the_post();
		?>
		<section class="entry-content">
			<?php the_title( '<div class="wrapper-large || ta-center"><h1 class="entry-title || templates-header">', '</h1></div>' ); ?>
		</section><!-- .entry-content -->

		<?php

			get_template_part( 'template-parts/content', 'page' );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
