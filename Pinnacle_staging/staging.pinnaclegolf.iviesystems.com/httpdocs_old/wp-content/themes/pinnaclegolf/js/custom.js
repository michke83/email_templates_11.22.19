var hamburger = document.getElementById('js-hamburger'),
body = document.getElementById('js-body'),
container = document.getElementById('js-menu-container');

console.log(hamburger);

jQuery(hamburger).on('click', function(){
  jQuery(body).toggleClass('menu-open');
  jQuery(container).toggleClass('sr-only');
  jQuery(hamburger).toggleClass('is-active');
});
