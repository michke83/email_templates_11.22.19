<?php
/**
 * Front Page Template
 *
 * @package PinnacleGolf
 */

get_header(); ?>

	<div id="primary" class="content-area || full-width">
		<main id="main" class="site-main || full-width" role="main">
			<!-- HEADER HERO IMAGE SECTION -->
			<section class="hero-img" style="background-image: url('<?php the_field( 'header_hero_image', 'options' ); ?>')">
				<div class="hero-img--content || wrapper-large || ta-center">
          <h1 class="ta-center"><?php the_field( 'header_hero_heading', 'options' ); ?></h1>
          <h2 class="ta-center"><?php the_field( 'header_hero_subheading', 'options' ); ?></h2>
          <a class="btn" href="<?php the_field( 'header_hero_button_url', 'options' ); ?>">
						<?php the_field( 'header_hero_button_text', 'options' ); ?>
					</a>
				</div>
			</section>

			<!-- BELOW HEADER TEXT SECTION -->
			<section class="wrapper-large">
				<div>
					<?php
						$below_header_text_section = get_field( 'below_header_text_section', 'options' );
						echo $below_header_text_section;
					?>
				</div>
			</section>

			<!-- CLUB IMAGE SECTION -->
			<section class="grid || grid--medium">
				<div class="image-row">
					<img src="<?php the_field( 'club_section_image_left', 'options'); ?>" />
				</div>
				<div class="center-img || image-row">
					<img src="<?php the_field( 'club_section_image_middle', 'options'); ?>" />
				</div>
				<div class="image-row">
					<img src="<?php the_field( 'club_section_image_right', 'options'); ?>" />
				</div>
			</section>

			<!-- AMMENITIES TEXT SECTION -->
			<section class="wrapper-large">
				<div>
					<?php
						$amenities_section_text = get_field( 'amenities_section_text', 'options' );
						echo $amenities_section_text;
					?>
				</div>
			</section>

			<!-- AMMENITIES IMAGE SECTION -->
			<section class="grid || grid--large || image-grid">
				<div>
					<img src="<?php the_field( 'amenities_section_large_image_left_', 'options'); ?>" />
				</div>
				<div class="right-grid || grid || grid--vertical">
					<div class="top-grid-row || grid">
						<div>
							<img src="<?php the_field( 'amenities_section_grid_image_top_left_', 'options'); ?>" />
						</div>
						<div>
							<img src="<?php the_field( 'amenities_section_grid_image_top_right_', 'options'); ?>" />
						</div>
					</div>

					<div class="bottom-grid-row || grid">
						<div class="grid">
							<div>
								<img src="<?php the_field( 'amenities_section_grid_image_bottom_left_', 'options'); ?>" />
							</div>
						</div>
						<div class="grid">
							<div>
								<img src="<?php the_field( 'amenities_section_grid_image_bottom_right_', 'options'); ?>" />
							</div>
						</div>
					</div>
				</div>
			</section>

			<!-- MEMBERSHIP TEXT SECTION -->
			<section class="wrapper-large">
				<div>
					<?php
						$membership_text_section = get_field( 'membership_text_section', 'options' );
						echo $membership_text_section;
					?>
				</div>
			</section>

			<!-- FOOTER HERO IMAGE SECTION -->
			<section class="hero-img" style="background-image: url('<?php the_field( 'footer_hero_image', 'options' ); ?>')">
				<div class="hero-img--content || wrapper-large">
          <h3 class="ta-center || primary-header"><?php the_field( 'footer_hero_heading', 'options' ); ?></h1>
          <h4 class="ta-center || secondary-header"><?php the_field( 'footer_hero_subheading', 'options' ); ?></h2>
          <a class="btn" href="<?php the_field( 'footer_hero_button_url', 'options' ); ?>">
						<?php the_field( 'footer_hero_button_text', 'options' ); ?>
					</a>
				</div>
			</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
