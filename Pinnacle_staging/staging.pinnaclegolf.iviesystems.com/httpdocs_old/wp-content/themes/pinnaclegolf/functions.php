<?php
/**
 * pinnaclegolf functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package pinnaclegolf
 */
/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
if( !defined( 'THEME_DIRECTORY' ) ) {
 	define( 'THEME_DIRECTORY', get_template_directory_uri() );
}
if( !defined( 'THEME_DIR_PATH' ) ) {
 	define( 'THEME_DIR_PATH', get_template_directory() );
}
if(!defined('THEME_DIR_FILE')) {
	define('THEME_DIR_FILE', __FILE__);
}
if(!defined('SITE_URL')) {
  define('SITE_URL', get_site_url() );
}

if ( ! function_exists( 'pinnacle_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function pinnacle_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on pinnaclegolf, use a find and replace
	 * to change 'pinnacle' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'pinnacle', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'menu-1' => esc_html__( 'Primary', 'pinnacle' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'pinnacle_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'pinnacle_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function pinnacle_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'pinnacle_content_width', 640 );
}
add_action( 'after_setup_theme', 'pinnacle_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function pinnacle_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'pinnacle' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'pinnacle' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'pinnacle_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function pinnacle_scripts() {

	wp_enqueue_style( 'smalt-style', get_template_directory_uri() . '/css/smalt.css', '1.0', true );

	wp_enqueue_style( 'hamburger-style', get_template_directory_uri() . '/css/hamburger.min.css', '1.0', true );

	wp_enqueue_style( 'pinnacle-style', get_template_directory_uri() . '/css/styles.css', '1.0', true );

	wp_enqueue_script( 'pinnacle-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'pinnacle-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array('jquery'), '20151215', true );

	wp_enqueue_script( 'pinnacle-scripts', get_template_directory_uri() . '/js/custom.js', array('jquery'), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

  if ( is_page('calendar') ) {
   // enqueue calendar style
    wp_enqueue_style('calendar-style', get_template_directory_uri() . '/css/calendar-style.css', true );
  }

}
add_action( 'wp_enqueue_scripts', 'pinnacle_scripts' );

function pinnacle_footer_scripts() {
  // JS stuff for Google Maps API on About Page
  if (is_page('contact') )  {

    // enqueue gmaps js file
    wp_enqueue_script('pw-script', get_template_directory_uri() . '/js/gmaps.js');

    // get ACF fields from Options page settings
    $pinnlat = get_field( 'pinnacle_latitude', 'options');
    $pinnlong = get_field( 'pinnacle_longitude', 'options');
    $pinnzoom = get_field( 'google_maps_zoom_number', 'options');

    $pinapikey = get_field( 'google_maps_api_key', 'options' );

    // localize script and pass PHP variables to JS gmaps file
    wp_localize_script('pw-script', 'pw_script_vars', array(
        'latitude'   => __( $pinnlat, 'pinnaclegolf' ),
        'longitude'  => __( $pinnlong, 'pinnaclegolf' ),
        'zoom'       => __( $pinnzoom, 'pinnaclegolf' )
      )
    );

    // call Google Maps JS Library
    wp_enqueue_script( 'pinnacle-golf-gmaps-js', 'https://maps.googleapis.com/maps/api/js?key='.$pinapikey .'&callback=initMap', array( 'jquery' ), true );


  }
}
add_action( 'wp_footer', 'pinnacle_footer_scripts' );

/**
* ACF Theme Options settings
**/
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Home Page Settings',
		'menu_title'	=> 'Home Page',
		'parent_slug'	=> 'theme-general-settings',
	));

}

/* Events CPT */
// function events_post_types() {
//
// 	$labels = array(
// 		'name'                  => _x( 'Events', 'Post Type General Name', 'biznet' ),
// 		'singular_name'         => _x( 'Events', 'Post Type Singular Name', 'biznet' ),
// 		'menu_name'             => __( 'Events', 'biznet' ),
// 		'name_admin_bar'        => __( 'Events', 'biznet' ),
// 		'archives'              => __( 'Events Archives', 'biznet' ),
// 		'attributes'            => __( 'Events Attributes', 'biznet' ),
// 		'parent_item_colon'     => __( 'Parent Events:', 'biznet' ),
// 		'all_items'             => __( 'All Events', 'biznet' ),
// 		'add_new_item'          => __( 'Add New Events', 'biznet' ),
// 		'add_new'               => __( 'Add New', 'biznet' ),
// 		'new_item'              => __( 'New Events', 'biznet' ),
// 		'edit_item'             => __( 'Edit Events', 'biznet' ),
// 		'update_item'           => __( 'Update Events', 'biznet' ),
// 		'view_item'             => __( 'View Events', 'biznet' ),
// 		'view_items'            => __( 'View Events', 'biznet' ),
// 		'search_items'          => __( 'Search Events', 'biznet' ),
// 		'not_found'             => __( 'Not found', 'biznet' ),
// 		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
// 		'featured_image'        => __( 'Featured Image', 'biznet' ),
// 		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
// 		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
// 		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
// 		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
// 		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
// 		'items_list'            => __( 'Events list', 'biznet' ),
// 		'items_list_navigation' => __( 'Events list navigation', 'biznet' ),
// 		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
// 	);
// 	$args = array(
// 		'label'                 => __( 'Events', 'biznet' ),
// 		'description'           => __( 'Events Post Types.', 'biznet' ),
// 		'labels'                => $labels,
// 		'supports'              => array(),
// 		'taxonomies'            => array( 'category', 'post_tag' ),
// 		'hierarchical'          => false,
// 		'public'                => true,
// 		'show_ui'               => true,
// 		'show_in_menu'          => true,
// 		'menu_position'         => 20,
//     'menu_icon'             => 'dashicons-tickets-alt',
// 		'show_in_admin_bar'     => true,
// 		'show_in_nav_menus'     => true,
// 		'can_export'            => true,
// 		'has_archive'           => true,
// 		'exclude_from_search'   => false,
// 		'publicly_queryable'    => true,
// 		'capability_type'       => 'page',
// 	);
// 	register_post_type( 'events', $args );
//
// }
// add_action( 'init', 'events_post_types', 0 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
