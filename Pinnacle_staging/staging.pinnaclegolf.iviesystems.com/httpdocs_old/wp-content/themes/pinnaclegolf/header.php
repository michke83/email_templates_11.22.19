<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package pinnaclegolf
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!--
__________ ____ _______________________  _________ ___ ___ ._________________________
\______   \    |   \____    /\____    / /   _____//   |   \|   \_   _____/\__    ___/
 |    |  _/    |   / /     /   /     /  \_____  \/    ~    \   ||    __)    |    |
 |    |   \    |  / /     /_  /     /_  /        \    Y    /   ||     \     |    |
 |______  /______/ /_______ \/_______ \/_______  /\___|_  /|___|\___  /     |____|
				\/                 \/        \/        \/       \/          \/
-->
<?php wp_head(); ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M9KZJP6');</script>
<!-- End Google Tag Manager -->
</head>

<body id="js-body" <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M9KZJP6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'pinnacle' ); ?></a>

	<header id="masthead" class="site-header || grid" role="banner" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/menu.jpg'">

		<nav id="site-navigation" class="main-navigation || grid" role="navigation">
			<button id="js-hamburger" class="hamburger || hamburger--squeeze || menu-toggle || grid" aria-controls="primary-menu" aria-expanded="false">
				<span class="hamburger-box">
					<span class="hamburger-inner"></span>
				</span>
				<span style="margin-left: 1rem; line-height: 20px;">
					<?php esc_html_e( 'MENU', 'pinnacle' ); ?>
				</span>
      </button>
			<?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' => 'primary-menu', 'container_class' => 'sr-only menu-mainmenu-container', 'container_id' => 'js-menu-container' ) ); ?>
		</nav><!-- #site-navigation -->

		<div class="site-branding">
			<a href="<?php echo site_url(); ?>"><img src="<?php echo THEME_DIRECTORY ?>/img/logo.jpg" alt="Pinnacle Logo" /></a>
		</div><!-- .site-branding -->

		<div class="grid || header-links">
			<a href="http://67.76.232.232/TgsWeb/WebDll.DLL">MEMBER SIGN IN</a>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
