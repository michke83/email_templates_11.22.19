<?php
/**
*
* Template Name: Tee Times
*
**/

get_header();
?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php if ( have_posts() ) : ?>

      <?php
      // Start the loop.
      while ( have_posts() ) : the_post();
        ?>

        <section class="entry-content">
          <?php the_title( '<div class="wrapper-large || ta-center"><h1 class="entry-title || templates-header">', '</h1></div>' ); ?>
        </section><!-- .entry-content -->

      <?php
      // End the loop.
      endwhile;

      // End If
      endif;
      ?>
      <div class="wrapper">
        <?php
          // Custom Tee Time Bookings Form
          gravity_form( 1, $display_title = true, $display_description = true, $field_values = null, $ajax = false, $echo = true );
        ?>
      </div>

    </main><!-- .site-main -->
  </div><!-- .content-area -->

<?php get_footer(); ?>
