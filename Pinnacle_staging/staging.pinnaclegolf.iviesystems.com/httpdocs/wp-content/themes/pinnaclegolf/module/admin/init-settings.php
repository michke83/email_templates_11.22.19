<?php
/* theme setup */
add_action('after_setup_theme', 'bh_theme_setup', 15);

if (!function_exists('bh_theme_setup')) {
    function bh_theme_setup() {
        add_action('admin_enqueue_scripts', 'bh_add_admin_style');
        add_action('admin_enqueue_scripts', 'bh_add_admin_scripts');
        add_theme_support('post-thumbnails');
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support("menus");
        add_theme_support("custom-header");
        add_theme_support("custom-background");
        add_editor_style();
    }
}

show_admin_bar(false);

add_action('init', 'bh_startSession', 1);

if (!function_exists('bh_startSession')) {
    function bh_startSession() {
            session_start();
    }
}

/*** Method to allow svg files to upload ***/

if (!function_exists('bh_mime_types')) {
    function bh_mime_types($mimes) {
      $mimes['svg'] = 'image/svg+xml';
      return $mimes;
    }
}
add_filter('upload_mimes', 'bh_mime_types');

/*** Method to allow svg files to upload ***/

if (!function_exists('bh_add_admin_style')) {
    function bh_add_admin_style() {
        if (is_admin()) {
            wp_enqueue_style('admin-style', get_template_directory_uri() . '/css/admin/admin.css', false, '1.2');
            wp_enqueue_style('default-style', get_template_directory_uri() . '/css/admin/default.css', false, '1.0');
            wp_enqueue_style('thickbox');
        }
    }
}

/* ADD SCRIPT FOR ADMIN */
if (!function_exists('bh_add_admin_scripts')) {
    function bh_add_admin_scripts() {
        if (is_admin()) {
            wp_register_script('tiny_mce', includes_url() . '/js/tinymce/tinymce.min.js', array('jquery'), '1.0', true);
            wp_enqueue_script('jquery-ui-autocomplete');
            wp_enqueue_script('jquery-ui-droppable');
            wp_enqueue_script('tiny_mce');
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
        }
    }
}

/*init settings end*/