<?php
function arphabet_widgets_init() {
    register_sidebar(array(
        'name' => 'Displayed Every where',
        'id' => 'products_sidebar_cat',
        'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">', 
        'after_widget' => '<span class="seperator extralight-border"></span></section>', 
        'before_title' => '<h3 class="widgettitle">', 
        'after_title' => '</h3>'
    ));
}

add_action('widgets_init', 'arphabet_widgets_init');
?>