<?php
/* Copyright Date
- Shortcode
*/
class VC_Dgtr_Rd_Copyright_date {

    function __construct() {

        if(is_admin()) {
            $this->admin_init();
        }

        $this->init();
    }

    function admin_init() {}

    function init() {
        add_shortcode('rd-copyright-date',array($this,'dgtr_rd_copyright_date'));
    }

    function dgtr_rd_copyright_date( $atts, $content = null ) {
        return date('Y');
    }

}

$VC_Dgtr_Rd_Copyright_date = new VC_Dgtr_Rd_Copyright_date;