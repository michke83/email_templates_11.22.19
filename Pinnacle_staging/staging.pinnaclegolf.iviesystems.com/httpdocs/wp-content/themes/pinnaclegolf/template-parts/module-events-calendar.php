
<?php
$title = get_field("title");
$desc  = get_field("description");
$calenderUrl  = get_field("calender_url")?get_field("calender_url"):'#';
$calenderImg  = get_field("calender_img");
?>
    <!-- Calendar -->
    <section id="events_scroll_down">
      <div class="container-fluid">
        <div class="row align-items-center">
          <div class="col-lg-1">
          </div>
          <div class="col-lg-4 text-center ml-5 order-3 mobile_section">
           
              <a href="<?php echo $calenderUrl;?>" target="_blank"><img class="responsive-img" src="<?php echo $calenderImg;?>" alt="Calender Photo"></a>
           
          </div>
          <div class="col-lg-5 order-2 ml-5 mobile_section">
            <div class="px-2 container">
              <h2 class="display-5 mb-2 blue-text"><?php echo $title;?></h2>
              <p class="mt-5"><?php echo $desc;?></p>
              <?php 
        $row = 1;
        if (have_rows('booking_section')):
        while (have_rows('booking_section')): the_row(); 
        $button_text = get_sub_field("button_text");
        $button_link  = get_sub_field("button_link");
        if($row ==2) $button_link  = $button_link.'#teetime';
        $icon_image = get_sub_field("icon_image");
        if($row == 1){  $clas='mt-5'; } else { $clas='mt-4'; }
?>
              <div class="<?php echo $clas;?> ">
                <a href="<?php echo $button_link;?>" class="event_link"><img src="<?php echo $icon_image;?>" class="mr-2"><span class="underline-blue"><?php echo $button_text;?></span></a>
              </div>
              <?php $row++; endwhile; endif;?>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /Calendar -->
