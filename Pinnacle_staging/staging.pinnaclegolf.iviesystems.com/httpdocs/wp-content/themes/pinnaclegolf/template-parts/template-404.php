<div class="row">			
    <div class="col-lg-12">
        <div id="primary">
            <div class="entry-header">
                <h1 class="entry-title">404 Page</h1>
            </div>
            <div class="entry-content">
                <p>Sorry about this...</p>
                <p>You've followed a link for a page on our site that doesn't exist any more. We do remove content from time-to-time if it's no longer relevant, especially if it's only going to confuse readers.</p>
                <p>If there is something specific you are looking for, perhaps some info on an old blog post, please don't hesitate to contact us - we're happy to help.</p>
                <a class="light-link" href="<?php echo home_url(); ?>">Back to Home</a>
            </div>

        </div><!-- /primary -->
    </div>
</div><!-- /row -->		
