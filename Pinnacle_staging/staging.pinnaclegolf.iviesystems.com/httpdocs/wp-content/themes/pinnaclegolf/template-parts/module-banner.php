<?php
$bannerVideo = get_field("banner_video_link");
$banner_image = get_field("banner_image") ? get_field("banner_image") : get_template_directory_uri() . '/images/propertiesheader.png';
;
$bannerBtnTitle = get_field("banner_title");
$bannerBtnText = get_field("banner_button_text");
$bannerBtnLink = get_field("banner_button_link") ? get_field("banner_button_link")
            : '#';
?>
<?php if (is_front_page()):

    if ($bannerVideo != '') {
        ?>
        <!-- video background -->
        <div class="video-responsive">
            <div class="tinted">
                <iframe id="video_background" class="player mx-0" data-src="//player.vimeo.com/video/252286246" src="<?php echo $bannerVideo; ?>?autoplay=1&loop=1&background=1" frameborder="0"  webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>
            <header id="home" class="masthead text-center text-white d-flex">
                <div class="container my-auto ">
                    <div class="row">
                        <div class="col-lg-8 mx-auto">
                            <h1 class="bottom_space"><?php echo $bannerBtnTitle; ?></h1>
                            <!-- <hr> -->
                        </div>
                        <div class="col-lg-8 mx-auto">
                            <a class="text-white underline-white" href="<?php echo $bannerBtnLink; ?>"> <?php echo $bannerBtnText; ?></a>
                        </div>
                    </div>
                </div>
            </header>

           

        </div> <?php } else {
        ?>
        <!-- jumbotron -->
        <header  class="masthead text-center text-white d-flex" style="background-image:url(<?php echo $banner_image; ?>)">
            <div class="container my-auto">
                <div class="row">
                    <div class="col-lg-10 col-md-10 mx-auto">
                        <h1 class="bottom_space bottom_top">
        <?php echo $bannerBtnTitle; ?>
                        </h1>
                        <!-- <hr> -->
                    </div>
                </div>
            </div>
        </header>
        <!-- /jumbotron -->      
    <?php } ?>
    <!-- /video background -->
    <?php
endif;
if (!is_front_page()):
    ?>
    <!-- jumbotron -->
    <header  class="masthead text-center text-white d-flex" style="background-image:url(<?php echo $banner_image; ?>)">
        <div class="container my-auto">
            <div class="row">
                <div class="col-lg-10 col-md-10 mx-auto">
                    <h1 class="bottom_space">
    <?php echo $bannerBtnTitle; ?>
                    </h1>
                    <!-- <hr> -->
                </div>
            </div>
        </div>
    </header>
    <!-- /jumbotron -->
<?php endif; ?>
