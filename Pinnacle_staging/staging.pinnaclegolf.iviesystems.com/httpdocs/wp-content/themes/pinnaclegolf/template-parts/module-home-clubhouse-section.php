
<?php
$title = get_field("mid_sec_title");
$desc  = get_field("mid_sec_description");
$explore_text = get_field("mid_sec_explore_text");
$go_to_section = get_field("mid_sec_go_to_section");
?>
     <!-- houses -->
    <section id="houses_index_page">
      <div class="pt-5 d-none d-sm-none d-md-block">
        <div class="row">
          <div id="club">
            <div class="row">
              <div class="col-md-6">
                <div id="blue">
                  <div id="innertext" class="ml-5 p-5 text-white">
                    <h2 class="text-justify-left">There is life and then there is living</h2>
                    <p class="text-justify-left mt-5">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                       tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                       consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                       cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                       proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
                  </p>
                  <a href="#" class="text-white underline-white">SEE LISTINGS</a>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <div class="header2-bottom">
                  <h1 class="align-text-bottom text-white"></h1>
                </div>
              </div>

              <div class="col-md">
                <div id="shore">
                  <div class="header3-bottom">
                    <h1 class="align-text-bottom text-white"></h1>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <br/>

      <div class="bg-primar py-5 d-block d-sm-block d-md-none">
          <div class="p-0">
            <div class="row">
              <div class="col-xs-12 col-sm-12">
                <div id="bluemobile">
                  <div id="innertext" class="ml-5 p-5 pb-0 mb-0 text-white">
                    <h2 class="text-justify-left">There is life and then there is living</h2>
                    <p class="text-justify-left mt-5 mb-0 pb-0">
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </p>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12">
                <div id="clubmobile">
                </div>
                <div class="header2-bottom">
                  <h1 class="align-text-bottom text-white"></h1>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12">
                <div id="shoremobile">
                </div>
                <div class="header3-bottom">
                  <h1 class="align-text-bottom text-white"></h1>
                </div>
              </div>

            </div>
          </div>
      </div>
    </section>
    <!-- /houses -->