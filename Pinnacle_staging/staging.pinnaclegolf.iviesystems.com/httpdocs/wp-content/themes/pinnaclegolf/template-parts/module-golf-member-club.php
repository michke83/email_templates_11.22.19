<?php
$chuck_turner_title = get_field("chuck_turner_title");
$chuck_turner_description  = get_field("chuck_turner_description");?>

<!-- blue section -->

<section id="womens_club_bg" class="container-fluid text-white" >
    <div class="overlay">
        <div class="container">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="text_area">
                    <h2 class="text-white heading"><?php echo $chuck_turner_title?></h2>
                    
                    <?php echo $chuck_turner_description?>
                </div>
            </div>


        </div>
    </div>



</section>
<!-- /blue section -->
