
<?php
$up_events_title = get_field("up_events_title");
$up_events_description = get_field("up_events_description");
?>
<!-- Upcoming Events -->
<section>
    <div class="container my-5 text-center upcoming_events_section">
        <h2 id="schedule" class="text-center blue-text"><?php echo $up_events_title; ?></h2>
        <?php echo $up_events_description; ?>
<!--        <p class="text-center text-muted"><a href="#" class="text-muted"><u>Contact us</u></a> to RSVP to one of our world famous events</p>-->
        <ul  class="list-group">
            <?php
            $i = 0;
            if (have_rows('up_events_list')):
                while (have_rows('up_events_list')): the_row();
                    $i++;
                    $event_date = date('F d - Y',strtotime(get_sub_field("event_date")));
                    $event_name = get_sub_field("event_name");
                    $events_more_text = get_sub_field("events_more_text");
                    $events_more_content = get_sub_field("events_more_content");
                    ?>
                    <li class="list-group-item borderless">
                        <div class="d-flex justify-content-between">
                            <p class="my-3 blue-text-small mobile_section mobile_margin_btm"><?php echo $event_date; ?><span class="text-muted ml-5"><?php echo $event_name; ?></span></p>
                            <a class="badge my-auto ml-4  align-middle" href="#" data-target="#moreinfo<?php echo $i; ?>" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="moreinfo1"><span class="underline-blue"><?php echo $events_more_text; ?></span></a>
                        </div>
                        <div class="collapse" id="moreinfo<?php echo $i; ?>"><?php echo $events_more_content; ?></div>
                    </li>
    <?php endwhile; ?>
<?php endif; ?>
           
    
   
        </ul>
    </div>
</section>
<!-- /Upcoming Events -->

