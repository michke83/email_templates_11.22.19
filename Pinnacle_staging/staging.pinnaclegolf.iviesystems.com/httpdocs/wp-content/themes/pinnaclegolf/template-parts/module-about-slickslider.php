
<?php
$title = get_field("mid_sec_title");
$desc  = get_field("mid_sec_description");
$explore_text = get_field("mid_sec_explore_text");
$go_to_section = get_field("mid_sec_go_to_section");
?>
<!-- sub menu bar 1 -->
    <section id="sub_menu_bar_1">
      <div class="sub_menu_bar_1 pb-0 mb-0 pt-1" >
        <div class="container-fluid text-white mx-auto text-center">


          
          <!--######### slick slider set up for nav bars #############-->

          <!-- for screensize extra small only -->
          <nav id="xs-only" class="action slider text-white nav justify-content-between mx-" >
            <div class="container-fluid">
              <div class="row justify-content-between text-left">
                <div class="col-xs-12">
                  <a href="#" class="nav-link active" data-slide="1" data-target="#club_house_slide">CLUBHOUSE</a>
                  <a href="#" class="nav-link" data-slide="2">BIKE AND HIKE</a>
                  <a href="#" class="nav-link" data-slide="3">GOLF</a>
                  <a href="#" class="nav-link" data-slide="4">TENNIS</a>
                  <a href="#" class="nav-link" data-slide="5">POOL</a>
                </div>  
                <div class="col-xs-12">
                  
                  <a href="#" class="nav-link" data-slide="6">FITNESS</a>
                  <a href="#" class="nav-link" data-slide="7">DINING</a>
                  <a href="#" class="nav-link" data-slide="8">SHOPPING</a>
                  <a href="#" class="nav-link" data-slide="9">WATER ADVENTURES</a>
              <!-- <a href="#" class="text-white mx-0 px-0" data-slide=""></a> -->
                </div>
              </div>
            </div>
          </nav>
          <!-- /for screensize extra small only -->

          <!-- for screensize small and up -->
          <nav id="sm-and-up" class="action slider text-white nav justify-content-between mx-5 ">
            <div class="container-fluid">
              <div class="row justify-content-between">
                <a href="#" class="nav-link active" data-slide="1">CLUBHOUSE</a>
                <a href="#" class="nav-link" data-slide="2">BIKE AND HIKE</a>
                <a href="#" class="nav-link" data-slide="3">GOLF</a>
                <a href="#" class="nav-link" data-slide="4">TENNIS</a>
                <a href="#" class="nav-link" data-slide="5">POOL</a>
                <a href="#" class="nav-link" data-slide="6">FITNESS</a>
                <a href="#" class="nav-link" data-slide="7">DINING</a>
                <a href="#" class="nav-link" data-slide="8">SHOPPING</a>
                <a href="#" class="nav-link" data-slide="9">WATER ADVENTURES</a>
            <!-- <a href="#" class="text-white mx-0 px-0" data-slide=""></a> -->
              </div>
            </div>
          </nav>
          <!-- /for screensize small and up -->


          <!--######### /slick slider set up for nav bars #############-->
        

        </div>
      </div>
      <div class="blue_arrow_1 container " >
        <!-- <img class="align-top" src="img/blue_arrow.png"> -->
      </div>
    </section>
    <!-- /sub menu bar -->

<div class="container ssslider" >
  
  <!--######### slick slider set up #############-->

  <div class="slider slider-for">
  <!-- CLUBHOUSE SLIDER -->
    <div id="clubhouse_slide">
      <div  class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Clubhouse</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities CLUBHOUSE accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div >
              <div  class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#clubhouse1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="clubhouse1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">CLUBHOUSE 1</span></a>
              </div>
              <div class="collapse ml-5" id="clubhouse1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#clubhouse2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="clubhouse2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">CLUBHOUSE 2</span></a>
              </div>
              <div class="collapse ml-5" id="clubhouse2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#clubhouse3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="clubhouse3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">CLUBHOUSE 3</span></a>
              </div>
              <div class="collapse ml-5" id="clubhouse3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities CLUBHOUSE accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- clubhouse background image -->
        </div>
      </div>
    </div>
    <!-- /CLUBHOUSE SLIDER -->

    <!-- BIKE AND HIKE SLIDER -->
    <div id="bikeandhike_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Bike and Hike</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities BIKE AND HIKE accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#bikeandhike1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="bikeandhike1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="bikeandhike1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#bikeandhike2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="bikeandhike2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="bikeandhike2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#bikeandhike3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="bikeandhike3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="bikeandhike3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities BIKE AND HIKE accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /BIKE AND HIKE SLIDE -->

    <!-- GOLF SLIDE --> 
    <div id="golf_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Golf</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities GOLF accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#golf1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="golf1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="golf1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#golf2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="golf2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="golf2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#golf3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="golf3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="golf3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities GOLF accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
        </div>
      </div>
    </div>
    <!-- /GOLF SLIDER -->

    <!-- TENNIS SLIDE -->
    <div id="tennis_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Tennis</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities TENNIS accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#tennis1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tennis1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="tennis1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#tennis2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tennis2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="tennis2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#tennis3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="tennis3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="tennis3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities TENNIS accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /TENNIS SLIDE -->

    <!-- POOL SLIDE -->
    <div id="pool_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Pool</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities POOL accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#pool1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="pool1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="pool1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#pool2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="pool2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="pool2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#pool3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="pool3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="pool3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities POOL accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /POOL SLIDE -->

    <!-- FITNESS SLIDE -->
    <div id="fitness_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Fitness</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities FITNESS accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#fitness1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="fitness1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="fitness1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#fitness2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="fitness2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="fitness2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#fitness3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="fitness3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="fitness3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities FITNESS accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /FITNESS SLIDE -->
    
    <!-- DINING SLIDE -->
    <div id="dining_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Dining</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities DINING accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#dining1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="dining1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="dining1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#dining2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="dining2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="dining2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#dining3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="dining3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="dining3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities DINING accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /DINING SLIDE -->

    <!-- SHOPPING SLIDE -->
    <div id="shopping_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Shopping</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities SHOPPING accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#shopping1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="shopping1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="shopping1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#shopping2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="shopping2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="shopping2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#shopping3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="shopping3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="shopping3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities SHOPPING accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /SHOPPING SLIDE -->

    <!-- WATER ADVENTURES SLIDE -->
    <div id="wateradventures_slide">
      <div class="row justify-content-between">
        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1">
          <h2>Water Adventures</h2>
          <p class="text-muted">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <!--****** amenities WATER ADVENTURES accordions ******-->
          <div class="container">
            <!-- first accordion -->
            <div>
              <div class="blue-text ">
                <a class="toggle1 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#wateradventures1_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="wateradventures1_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-1 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 1</span></a>
              </div>
              <div class="collapse ml-5" id="wateradventures1_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /first accordion -->

            <!-- second accordion -->
            <div class="my-3">
              <div class="blue-text ">
                <a class="toggle2 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#wateradventures2_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="wateradventures2_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-2 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 2</span></a>
              </div>
              <div class="collapse ml-5" id="wateradventures2_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /second accordion -->

            <!-- third accordion -->
            <div>
              <div class="blue-text mb-3">
                <a class="toggle3 my-auto ml-1 font-weight-bold blue-text" href="#" data-target="#wateradventures3_expand" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="wateradventures3_expand">
                  <span><img src="#" alt="plus/minus circle" class="circle-plus-3 circle-plus d-inline-block"></span><span class="ml-1 font-weight-bold">DROP DOWN 3</span></a>
              </div>
              <div class="collapse ml-5" id="wateradventures3_expand">
                <p>Info</p>
                <p>Info</p>
              </div>
            </div>
            <!-- /third accordion -->
          </div>
          <!-- /amenities WATER ADVENTURES accordions -->
        </div>
        <div class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
          <!-- slider background image -->
        </div>
      </div>
    </div>
    <!-- /WATER ADVENTURES SLIDE -->
  </div>
  <div class="slider slider-nav">
    <div><h3></h3></div>
    <div><h3></h3></div>
    <div><h3></h3></div>
    <div><h3></h3></div>
    <div><h3></h3></div>
  </div>
</div>
<!--######### slick slider set up #############-->

    <!-- /slider -->
