
<?php
$title = get_field("plan_title");
$desc = get_field("plan_description");
?>

<!-- blue section -->
<section id="membership_plans" class="container-fluid text-white py-3 px-5" >

    <div class="pt-4 container">
        <div class="row ">
            <div class="col-lg-8 order-2 text-left ">
                <div class="row justify-content-center">
                    <?php
                    if (have_rows('events_sections')):
                        while (have_rows('events_sections')): the_row();

                            $eventplanTitle = get_sub_field("event_plan_title");
                            $eventplanLink = get_sub_field("event_plan_link");
                            ?>
                            <div class="col-md-3 col-sm-3 col-xs-6  col-centered">
                            <div class="circle">
                                <div class="circle__inner">
                                    <div class="circle__wrapper">
                                        <div class="circle__content">

                                            <?php
                                            echo $eventplanTitle;
                                            echo $eventplanLink;
                                            ?>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                        <?php
                    endwhile;
                endif;
                ?>           

<!--              <img class="img-fluid" src="<?php // echo get_template_directory_uri()         ?>/images/membership_plans.png" alt="Wedding Photo">-->
            </div>
        </div>
        <div class="col-lg-4 order-1 align-self-center">
            <div class="memeberplan_txt text-center">
              <h2 class="mb-2 heading "><?php echo $title; ?></h2>
              <?php echo $desc; ?>
            </div>
        </div>
            
    </div>
</div>
</section>
<!-- /blue section -->
