

<!-- board  committee section -->
<?php
$directors_section_heading = get_field("directors_section_heading");
$directors_section_sub_heading = get_field("directors_section_sub_heading");
$committee_section_heading = get_field("committee_section_heading");
$committee_section_sub_heading = get_field("committee_section_sub_heading");
?>
<div class="club_members_section">
    <div class="container">

        <div class="memebers_section">

            <h2 class="heading blue-text text-center"><?php echo $directors_section_heading; ?></h2>
            <span class="text-center"><?php echo $directors_section_sub_heading; ?></span>
            <div class="row">
                <?php
                if (have_rows('directors_section')):
                    while (have_rows('directors_section')): the_row();
                        $board_directors = get_sub_field("board_directors");
                        ?>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="board_members">
                                <?php echo $board_directors; ?>
                            </div>
                        </div>

                    <?php endwhile; ?>
                <?php endif; ?>
            </div>

            <h2 class="heading blue-text text-center"><?php echo $committee_section_heading; ?></h2>
            <span class="text-center"><?php echo $committee_section_sub_heading; ?></span>
            <div class="row">
                <?php
                if (have_rows('committee_section')):
                    while (have_rows('committee_section')): the_row();
                        $board_directors = get_sub_field("committee_details");
                        ?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 text-left">
                            <div class="board_members">
                                <?php echo $board_directors; ?>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<!-- /board committee section -->
