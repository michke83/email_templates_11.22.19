
<div class="ameties_content_section">
    <!-- sub menu bar 1 -->
    <section id="sub_menu_bar_1">
        <div class="sub_menu_bar_1 pb-0 mb-0 pt-1" >
            <div class="container-fluid text-white mx-auto text-center">

                <!--######### slick slider set up for nav bars #############-->

                <!-- for screensize extra small only -->
                <nav id="xs-only" class="action slider text-white nav justify-content-between mx-" >
                    <div class="container-fluid">
                        <div class="row justify-content-between text-left">
                            <div class="slick_main_mobile">
                                <div class="slider slider-mobile">

                                    <?php
                                    $menurow = 0;
                                    if (have_rows('menus_list')):
                                        while (have_rows('menus_list')): the_row();
                                            $menurow++;
                                            if ($menurow > 5) continue;
                                            if ($menurow == 1) {
                                                $activCls = 'active';
                                            } else {
                                                $activCls = '';
                                            }
                                            $menu_heading_title = get_sub_field("menu_name");
                                            ?>
                                            <a href="#" class="nav-link <?php echo $activCls; ?>" data-slide="<?php echo $menurow; ?>"><?php echo $menu_heading_title; ?></a>
                                        <?php endwhile; ?>
                                    <?php endif; ?>

                                    <?php
                                    $menurow = 0;
                                    if (have_rows('menus_list')):
                                        while (have_rows('menus_list')): the_row();
                                            $menurow++;
                                            if ($menurow < 6) continue;
                                            if ($menurow == 1) {
                                                $activCls = 'active';
                                            } else {
                                                $activCls = '';
                                            }
                                            $menu_heading_title = get_sub_field("menu_name");
                                            ?>
                                            <a href="#" class="nav-link <?php echo $activCls; ?>" data-slide="<?php echo $menurow; ?>"><?php echo $menu_heading_title; ?></a>
                                        <?php endwhile; ?>
                                    <?php endif; ?>

                                </div>

                            </div>

                        </div>
                    </div>
           
            </nav>
                 </div>
            <!-- /for screensize extra small only -->

            <!-- for screensize small and up -->
            <nav id="sm-and-up" class="action slider text-white nav justify-content-between mx-5 mobile_section despktop_slider_navigation">

                <div class="container-fluid">

                    <div class="row justify-content-between">
                        <?php
                        $menurow = 0;
                        if (have_rows('menus_list')):
                            while (have_rows('menus_list')): the_row();
                                $menurow++;
                                if ($menurow == 1) {
                                    $activCls = 'active';
                                } else {
                                    $activCls = '';
                                }
                                $menu_heading_title = get_sub_field("menu_name");
                                ?>
                                <a href="#" class="nav-link <?php echo $activCls; ?>" data-slide="<?php echo $menurow; ?>"><?php echo $menu_heading_title; ?></a>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        <!--                        <a href="#" class="text-white mx-0 px-0" data-slide=""></a> -->
                    </div>
                </div>
            </nav>
            <!-- /for screensize small and up -->
            <!--######### /slick slider set up for nav bars #############-->
        </div>
</div>
<!--      <div class="blue_arrow_1 container " >
         <img class="align-top" src="img/blue_arrow.png"> 
      </div>-->
</section>
<!-- /sub menu bar -->
<span class="slick_down_arrow"></span>
<div class="container ssslider">
    <div class="slider variable-width slider-nav">
        <?php
        $club = 0;
        $club2 = 0;
        if (have_rows('amenities_carousel')):
            while (have_rows('amenities_carousel')): the_row();
                $club++;
                $menu_title = get_sub_field("menu_title");
                $menu_titleAccordion = str_replace(' ', '_', $menu_title);
                $menu_description = get_sub_field("menu_description");
                $slide_image = get_sub_field("slide_image");
                ?>			
                <div>

                    <div  class="row justify-content-center">
                        <div class="col-md-6 slider_content_left mt-2 mt-md-5 ml-1 ameties_left_section">
                            <h2 class="blue-text spacing_bar_bottom"><?php echo $menu_title; ?></h2>
                            <div class="col-md-12 content_widht p-sm-0">
                                <?php echo $menu_description; ?>
                            </div>
                            <!--****** amenities CLUBHOUSE accordions ******-->

                <!--                            <div class="panel-group" id="accordion<?php // echo $club;   ?>">
                                                 first accordion 
                            <?php
//                                if (have_rows('sub_section_accordion')):
//                                    while (have_rows('sub_section_accordion')): the_row();
//                                        $club2++;
//                                        $sub_title = get_sub_field("sub_title");
//                                        $sub_description = get_sub_field("sub_description");
                            ?>	
                                                        <div class="blue-text-small">
                                                                            <div role="tab" id="heading<?php // echo $club;   ?>">
                                                            <a data-toggle="collapse" data-parent="#accordion<?php //echo $club;   ?>" class="my-auto accordion-toggle" href="#collapse<?php
                            // echo $club + $club2;
                            ?>">
                                                                <span class="circle-plus d-inline-block"></span><?php //echo $sub_title;   ?>
                                                            </a>
                                                                                                        </div>

                                                            <div id="collapse<?php //echo $club + $club2;   ?>" class="collapse">
                                                                <div class="card-block">
                            <?php //echo $sub_description; ?>
                                                                </div>
                                                            </div>
                                                        </div>

                            <?php // endwhile; ?>
                            <?php //endif; ?>
                                            </div>-->

                            <!--****** amenities CLUBHOUSE accordions ******-->

                        </div>

                        <!--                    <div id="outer" class="slider_clubhouse_bg col-10 col-md-5 slider_bg_images p-0">
                                                
                                            </div>-->
                        <div class="slider_clubhouse_bg col-10 col-md-5 p-0 ameties_right_section" >

                            <div id="inner" style="background-image:url(<?php echo $slide_image; ?>)"></div>

                        </div>
                        <!-- clubhouse background image -->

                    </div>
                </div>
                <!-- /CLUBHOUSE SLIDER -->

            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
</div>