<?php
/* template name: Calender Page */

get_header();
?>

<div id="main" class = "header-footer-gap bcb_error_page">
    <div id="main-container">
        <div class="main-content">
            <div id="page-404" <?php post_class(); ?>>
                <section class="cal-entry-content">
                    <div class="wrapper-large || ta-center">
                        <h1 class="entry-title || templates-header">Calendar</h1>
                    </div>
                    <?php get_template_part('template-parts/content'); ?>	
                </section>
            </div>
        </div>
    </div>

</div>


<?php //get_footer(); ?>
