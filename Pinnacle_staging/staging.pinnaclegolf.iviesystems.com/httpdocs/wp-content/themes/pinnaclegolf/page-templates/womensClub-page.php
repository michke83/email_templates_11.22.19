<?php
/* template name: Women'sClub Page */

get_header();
?>
<div id="main">
    <div id="maincontainer">

        <?php get_template_part('template-parts/module', 'banner_membership'); ?>
        <!-- blue down arrow -->
        <div id="circle_down_arrow" class="container" >
            <p><a href="#membership_scroll_down" class="js-scroll-trigger" ><img src=" <?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png"></a></p>
        </div>
        <!-- /blue down arrow -->

        <div id="membership_scroll_down"></div>
<?php     the_content(); if ( ! post_password_required() ) {?>
        <!-- Member Application -->
        <section class="golf_collage_list container ">
            <div class="row  mx-auto">

                <div class="col-lg ml-lg-1 mt-1 ">
                    <div class="row mb-12">

                        <?php
                        $pwcrow = 0;
                        if (have_rows('pwc_section')):
                            while (have_rows('pwc_section')): the_row();
                                $pwcrow++;
                                $pwc_event = get_sub_field("pwc_event");
                                $pwc_event_date_text = get_sub_field("pwc_event_date_text");
                                $pwc_event_data = get_sub_field("pwc_event_data");
                                $pwc_event_details = get_sub_field("pwc_event_details");
                                ?>	

                                <!-- small box-->
                                <div class=" col-lg-4 col-sm-6 col-xs-12 my-3 last-callout-box">
                                    <div class="callout_section" id="<?php echo $pwcrow; ?>">

                                        <h2 class="blue-text text-center heading3"><?php echo $pwc_event; ?></h2>

                                        <hr id="blue">

                                        <div class=" mx-auto">
                                            <a href="#" class="days-date">
                                                <span class="text-uppercase text-center"><?php echo $pwc_event_date_text; ?></span>
                                                <hr id="blue">
                                            </a>
                                        </div>
                                        <?php echo $pwc_event_data; ?>
                                        <div class="_down_arrow">
                                            <button></button>
                                        </div>
                                    </div>
                                </div>
                                <!-- small box ends-->
                                <!-- popup box starts -->
                                <div  class="modal-container" id="popupbox<?php echo $pwcrow; ?>" style="display:none">
                                    <div  class="poup_msg">
                                        <?php //echo $pwc_event_details;  ?>
                                        <h2 class=" blue-text text-center heading3"><?php echo $pwc_event; ?></h2>

                                        <hr id="blue">

                                        <div class=" mx-auto">
                                            <a href="#">
                                                <span class="text-uppercase text-center"><?php echo $pwc_event_date_text; ?></span>
                                                <hr id="blue">
                                            </a>
                                        </div>
                                        <?php echo $pwc_event_details; ?>
                                        <div class="_down_arrow" id="<?php echo $pwcrow; ?>">
                                            <button ></button>
                                        </div>
                                    </div>
                                </div>
                                <!--                                <div id="server_msg">
                                <?php //echo $pwc_event_details;  ?>
                                                                    <h2 class="section-heading blue-text  text-center">Pwc January Luncheon</h2>
                                
                                                                    <hr id="blue">
                                
                                                                    <div class=" mx-auto">
                                                                        <a href="#" class="text-muted">
                                                                            <span class="text-uppercase text-center">Thursday, January 11 , 2018</span>
                                                                            <hr id="blue">
                                                                        </a>
                                                                    </div>
                                                                    <p>Devin will be speaking about a few simple and practical things anyone and everyone can benefit</p>
                                                                    <p><strong>11:15 AM (SOCIAL), 11:30 AM (LUNCH) | GENERAL MEETING TO FALLOW</strong></p>
                                                                    <p>Our guest speaker</p>
                                                                    <h2 class="section-heading blue-text  text-center">Devin Austin</h2>
                                                                    <p>(Certified Personal Trainer, Corrective)</p>
                                                                    <p>Exercise  Specialist and Golf Fitness Specialist</p>
                                                                    <p>Devin will be speaking about a few simple and practical things anyone and everyone can benefit from regardless of age, size or physical limitations. 
                                                                        Learn how to eliminate chronic pain, move more efficiently and sustain your active lifestyles.</p>
                                
                                                                    <h3 class="section-heading blue-text  text-center">Lunch Menu</h3>
                                                                    <p><small>(Loaded Potato Soup, Spring Mix Salad with strawberries, crumbled feta, chopped pecans, served with raspberry vinaigrette. Dessert : Vanilla ice creams with chocolate sauce.)</small>
                                                                        <b>$14.95</b></p>
                                                                    <span class="text-muted  text-center">Make your reservations NOW by email to:</span><br>
                                                                    <a href="mailTO:Pinnaclereservations3@gmail" class="text-muted text-uppercase text-center">
                                                                        Pinnaclereservations3@gmail.com
                                                                        <hr id="blue">
                                                                    </a>
                                                                    <div class="_down_arrow">
                                                                        <button ></button>
                                                                    </div>
                                                                </div>-->
                                <!-- popup box ends -->
                                <?php
                            endwhile;
                        endif;
                        ?>                     
                    </div>
                </div>
            </div>

        </section>

        <!-- events section -->
        <?php get_template_part('template-parts/module', 'golf-events '); ?>
        <!-- /events section -->

        <!-- blue section -->
        <?php get_template_part('template-parts/module', 'golf-Birthday-members'); ?>


        <!-- /blue section -->
        <!-- group section starts here -->
        <?php
        $pwc_group_event = get_field("pwc_group_title");
        $pwc_group_event_details = get_field("pwc_group_description");
        ?>	

        <div class="container">
            <div class="group my-5 mobile_margin_btm">
                <div class="row">
                    <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                        <div class="pwc_groups_txt ">
                            <h2 class="section-heading blue-text text-uppercase"><?php echo $pwc_group_event; ?></h2>
                            <?php echo $pwc_group_event_details; ?>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg ml-lg-1 mt-1 ">
                         
                                <section class="golf_collage_list bottom_list ">
                                    <div class="row mobile_section">
                                        <?php
                                        $eventrow = 0;
                                        if (have_rows('pwc_group_section')):
                                            while (have_rows('pwc_group_section')): the_row();
                                                $eventrow++;
                                                $pwc_group_event = get_sub_field("pwc_group_event");
                                                $pwc_group_event_date_text = get_sub_field("pwc_group_event_date_text");
                                                $pwc_group_event_data = get_sub_field("pwc_group_event_data");
                                                $pwc_group_event_details = get_sub_field("pwc_group_event_details");
                                                if ($eventrow == 1) {
                                                    $aligncls = 'col-sm-6';
                                                } else {
                                                    $aligncls = 'col-sm-12';
                                                }
                                                ?>
                                                <!-- small box-->                                   
                                                <div class=" col-lg-6 col-md-6  col-sm-6 col-xs-12 my-4">
                                                    <div class="callout_section" id="<?php
                                                    echo $eventrow + 3;
                                                    ?>">
                                                        <h2 class="section-heading blue-text  text-center heading3"><?php echo $pwc_group_event; ?></h2>
                                                        <hr id="blue">
                                                        <div class=" mx-auto">
                                                            <a href="#" class="days-date">
                                                                <span class="text-uppercase text-center"><?php echo $pwc_group_event_date_text; ?></span>
                                                                <hr id="blue">
                                                            </a>
                                                        </div>
                                                        <?php echo $pwc_group_event_data; ?>

                                                        <div class="_down_arrow" >
                                                            <button></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- small box ends-->                                                      
                                                <!-- group popup box starts -->
                                                <div  class="modal-container" id="popupbox<?php
                                                echo $eventrow + 3;
                                                ?>" style="display:none">
                                                    <div  class="poup_msg">
                                                        <h2 class="section-heading blue-text  text-center"><?php echo $pwc_group_event; ?></h2>

                                                        <hr id="blue">

                                                        <div class=" mx-auto">
                                                            <a href="#" class="text-muted">
                                                                <span class="text-uppercase text-center"><?php echo $pwc_group_event_date_text; ?></span>
                                                                <hr id="blue">
                                                            </a>
                                                        </div>
                                                        <?php echo $pwc_group_event_details; ?>
                                                        <div class="_down_arrow" id="<?php
                                                             echo $eventrow + 3;
                                                             ?>">
                                                            <button ></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                            endwhile;
                                        endif;
                                        ?>  
                                    </div>
                                </section>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <!-- interested -->
        <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
        <!-- /interested -->

        <!-- events section -->
        <?php get_template_part('template-parts/module', 'golf-member-club'); ?>
        <!-- /events section -->

        <!-- board commitee section -->
        <?php get_template_part('template-parts/module', 'golf-board-comittee'); ?>
        <!-- /board commitee section -->

<?php } ?>
    </div>
</div>

</div>
</div>

<?php get_footer(); ?>
