<?php
/* template name: Properties Page */

get_header();
?>

<div id="main">
    <div id="main-container">
        <?php get_template_part('template-parts/module', 'banner'); ?>
        <!-- blue down arrow -->
        <div id="circle_down_arrow" class="container" >
            <p><a href="#properties_scroll_down" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png"></a></p>
        </div>
        <!-- /blue down arrow -->

        <div id="properties_scroll_down"></div>
        <?php
        $Title = get_field("title");
        $Description = get_field("description");
        $BackgrndImg = get_field("image");
        ?>

        <!-- resort life -->
        <section>
            <div id="resort-life-properties">
                <div class="row  container-fluid mobile_section">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5 text-center resortlife-img">
                        <?php get_template_part('template-parts/module', 'common-ovelapimages'); ?>
                    </div>
                    <div class="col-lg-5 px-xl-0 mobile_content">
                        <div class="px-4 px-xl-0 mobile_pad">
                            <h2 class="display-5 mb-2 blue-text"><?php echo $Title; ?></h2>
                            <p class="mt-4"><?php echo $Description; ?></p>
                        </div>
                    </div>
                     <div class="col-lg-1">
                    </div>
                </div>
            </div>
        </section>
        <!-- /resort life -->

        <?php
        $feature_home_title = get_field("feature_home_title");
        $feature_home_description = get_field("feature_home_description");
        $form_title = get_field("form_title");
        $title_description = get_field("title_description");
        $tour_shortcode = get_field("form_short_code");
        if($_POST['gform_submit'] ==  1){ $title_description= '';}
        ?>

        <!-- Feature Homesites -->
        <section id="featurehomesites-properties">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-1">
                    </div>
                    <div class="col-lg-5 text-center ml-5 order-3 mobile_section">
                        <div class="custom_form">
                           <h2 class="blue-text"> <?php echo $form_title;?></h2>
                            <?php
                            echo $title_description;
                            echo do_shortcode($tour_shortcode);
                            ?>
          <!--              <img class="img-fluid align-top" src="<?php //echo get_template_directory_uri()    ?>/images/schedule_tour.png" alt="Wedding Photo">-->
                        </div>
                    </div>
                    <div class="col-lg-5 order-2 ml-4">
                        <div class="pr-3">
                            <h2 class="display-5 blue-text mb-2"><?php echo $feature_home_title; ?></h2>
                            <p class="mt-4 "><?php echo $feature_home_description; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /Feature Homesites -->


        <!-- Picture collage properties -->
        <?php get_template_part('template-parts/module', 'properties-gallery'); ?>
        <!-- /Picture collage properties -->

        <!-- Spectacular Homesites -->
        <?php get_template_part('template-parts/module', 'properties-brands'); ?>
        <!-- /Spectacular Homesites -->


        <!-- interested -->
        <?php get_template_part('template-parts/module', 'interested-estate-section'); ?>
        <!-- /interested -->


        <!-- testimonials -->
        <?php get_template_part('template-parts/module', 'home-testimonials'); ?>
        <!-- /testimonials -->

        <!-- special events -->
        <?php get_template_part('template-parts/module', 'home-events'); ?>
        <!-- special events -->

    </div>
</div>


<?php get_footer(); ?>
