<?php get_header(); ?>

<div id="main" class = "header-footer-gap bcb_error_page">
    <?php get_template_part('template-parts/module', 'banner'); ?>

    <div id="circle_down_arrow" class="container" >
        <p><a href="#page_content_section" class="js-scroll-trigger" ><img src="<?php echo get_template_directory_uri() ?>/images/blue_circle_arrow.png"></a></p>
    </div>
    <!-- /blue down arrow -->
    <div id="page_content_section"></div>

    <div id="main-container" class="container">
        <div class="spacing_bar my-5">
            <div class="main-content">
                <div id="page-404" <?php post_class(); ?>>
                    <?php get_template_part('template-parts/content'); ?>	
                </div>
            </div>
        </div>
    </div>

</div>

<?php get_footer(); ?>