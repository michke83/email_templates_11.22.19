// $(document).ready(function(){
(function($) {
  "use strict"; // Start of use strict


  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 57)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 57
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

  // Scroll reveal calls
  window.sr = ScrollReveal();
  sr.reveal('.sr-icons', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 200);
  sr.reveal('.sr-button', {
    duration: 1000,
    delay: 200
  });
  sr.reveal('.sr-contact', {
    duration: 600,
    scale: 0.3,
    distance: '0px'
  }, 300);

  // Magnific popup calls
  $('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Loading image #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0, 1]
    },
    image: {
      tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
    }
  });

  //contact page accordion toggles 
  $('.contact .toggle1').click(function(){
    $('.contact .circle-plus-1').toggleClass("circle-plus circle-minus");
  });
  $('.contact .toggle2').click(function(){
    $(".contact .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('.contact .toggle3').click(function(){
    $(".contact .circle-plus-3").toggleClass("circle-plus circle-minus");
  });


  //amenities page sub menu bar nav color changing
  $('.sub_menu_bar_1 a').click(function(){
    $('.sub_menu_bar_1 a').css('color','#6986a9');
    $(this).css('color','white');
  });

  $('#sub_menu_bar_2 a').click(function(){
    $('#sub_menu_bar_2 a').css('color','#6986a9');
    $(this).css('color','white');
  });


//#############################################################
  // amenities page clubhouse accordion toggle
  $('#clubhouse_slide .toggle1').click(function(){
    $('#clubhouse_slide .circle-plus-1').toggleClass("circle-plus circle-minus");
  });
  $('#clubhouse_slide .toggle2').click(function(){
    $("#clubhouse_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#clubhouse_slide .toggle3').click(function(){
    $("#clubhouse_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

//amenities page bike and hike accordion toggle
  $('#bikeandhike_slide .toggle1').click(function(){
    $("#bikeandhike_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#bikeandhike_slide .toggle2').click(function(){
    $("#bikeandhike_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#bikeandhike_slide .toggle3').click(function(){
    $("#bikeandhike_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

//amenities page golf accordion toggle
  $('#golf_slide .toggle1').click(function(){
    $("#golf_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#golf_slide .toggle2').click(function(){
    $("#golf_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#golf_slide .toggle3').click(function(){
    $("#golf_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

// //amenities page tennis accordion toggle
  $('#tennis_slide .toggle1').click(function(){
    $("#tennis_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#tennis_slide .toggle2').click(function(){
    $("#tennis_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#tennis_slide .toggle3').click(function(){
    $("#tennis_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

// //amenities page pool accordion toggle
  $('#pool_slide .toggle1').click(function(){
    $("#pool_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#pool_slide .toggle2').click(function(){
    $("#pool_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#pool_slide .toggle3').click(function(){
    $("#pool_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

// //amenities page fitness accordion toggle
  $('#fitness_slide .toggle1').click(function(){
    $("#fitness_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#fitness_slide .toggle2').click(function(){
    $("#fitness_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#fitness_slide .toggle3').click(function(){
    $("#fitness_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

// //amenities page dining accordion toggle
  $('#dining_slide .toggle1').click(function(){
    $("#dining_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#dining_slide .toggle2').click(function(){
    $("#dining_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#dining_slide .toggle3').click(function(){
    $("#dining_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

// //amenities page shopping accordion toggle
  $('#shopping_slide .toggle1').click(function(){
    $("#shopping_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#shopping_slide .toggle2').click(function(){
    $("#shopping_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#shopping_slide .toggle3').click(function(){
    $("#shopping_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });

// //amenities page water adventures accordion toggle
  $('#wateradventures_slide .toggle1').click(function(){
    $("#wateradventures_slide .circle-plus-1").toggleClass("circle-plus circle-minus");
  });
  $('#wateradventures_slide .toggle2').click(function(){
    $("#wateradventures_slide .circle-plus-2").toggleClass("circle-plus circle-minus");
  });
  $('#wateradventures_slide .toggle3').click(function(){
    $("#wateradventures_slide .circle-plus-3").toggleClass("circle-plus circle-minus");
  });
//end#############################################################




})(jQuery); // End of use strict
