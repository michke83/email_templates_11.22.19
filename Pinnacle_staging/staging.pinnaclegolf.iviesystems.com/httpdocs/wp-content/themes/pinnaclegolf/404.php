<?php get_header(); ?>

<header class="bh_homebanner inner_bh_banner" id="bh_headerbanner">
    <div class="inner_baner_img" style="background-image: url(<?php echo home_url();?>/wp-content/uploads/2018/01/ineer-banner.jpg);">
    <div class="overlay"></div>
    <div class="container">
        <div class="banner-title">
            <div class="entry-header">
                
                <h2 class="display-5 mb-2 blue-text">404 Page</h2>
               
            </div>
            
        </div>
    </div>
</div>
</header>
<section class="section_1 concierge_desc" id="section_1">
    
<div class="container">
    <div class="sec1-section">
        
        <div class="entry-content">
            <p>Sorry about this...</p>
            <p>You've followed a link for a page on our site that doesn't exist any more. We do remove content from time-to-time if it's no longer relevant, especially if it's only going to confuse readers.</p>
            <p>If there is something specific you are looking for, perhaps some info on an old blog post, please don't hesitate to contact us - we're happy to help.</p>
            <a class="backtohome underline-blue" href="<?php echo home_url(); ?>">Back to Home</a>
            
        </div>
    </div>
</div></section>

<?php get_footer(); ?>