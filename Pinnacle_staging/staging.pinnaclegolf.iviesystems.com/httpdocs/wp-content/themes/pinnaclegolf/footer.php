<?php
$footer_line_image = get_field("footer_line_image", "option");
$footer_title_one = get_field("footer_title_one", "option");

$footer_title_two = get_field("footer_title_two", "option");
$footer_button_text = get_field("footer_button_text", "option");
$footer_button_link = get_field("footer_button_link", "option")?get_field("footer_button_link", "option"):'#';

$pinnacle_logo = get_field("pinnacle_logo", "option");
$blue_heron_logo = get_field("blue_heron_logo", "option");
$pinnacle_address = get_field("pinnacle_address", "option");

$blue_heron_address = get_field("blue_heron_address", "option");
$copyright_content = get_field("copyright_content", "option");

?>
  <!-- footer -->
<footer>
   <section>
      <div class="container text-gray-dark text-center">
        <div class="row">
          <div class="col mb-4">
            <img class="img-fluid" src="<?php echo $footer_line_image; ?>" alt="">            
          </div>
        </div>
        <div class="row">
          <div class="col">
            <h2 class="section-heading blue-text"><?php echo $footer_title_one;?></h2>
            <p class="text-muted"><?php echo $footer_title_two;?></p>
            <a class=" underline-blue" href="<?php echo $footer_button_link;?>"><?php echo $footer_button_text;?></a>
          </div>
        </div>
        <div class="py-4 container-fluid">
          <div class="row align-items-center">
            <div class="col-lg-6 text-center">
              <div class="p-4 text-muted pinanical-address">
                <img class="img-fluid mb-3" src="<?php echo $pinnacle_logo; ?>" alt="pinnacle Logo">
                <?php echo $pinnacle_address; ?>
                <figure>
                     <?php
                    if (have_rows('pinnacle_social_icon_list', 'option')):
                        while (have_rows('pinnacle_social_icon_list', 'option')): the_row();
                            $name = get_sub_field("name", 'option');
                            $url = get_sub_field("url", 'option');
                            ?>
                  <a class="mx-3" target="_blank" href="<?php echo $url;?>"><span class="fa fa-<?php echo $name;?> text-dark" aria-hidden="true"></span></a>
                      <?php endwhile; ?>
                    <?php endif; ?>
                </figure>
              </div>
            </div>
            <div class="col-lg-6 text-center mt-3 blueheron-address">
              <div class="px-4 text-muted">
                  <img class="img-fluid mb-3" src="<?php echo $blue_heron_logo; ?>" alt="BlueHeron Logo">
                   <?php echo $blue_heron_address; ?>
                <figure>
                    <?php
                    if (have_rows('blue_heron_social_icon_list', 'option')):
                        while (have_rows('blue_heron_social_icon_list', 'option')): the_row();
                            $name = get_sub_field("name", 'option');
                            $url = get_sub_field("url", 'option');
                            ?>
                  <a class="mx-3" target="_blank" href="<?php echo $url;?>"><span class="fa fa-<?php echo $name;?> text-dark" aria-hidden="true"></span></a>                
                 <?php endwhile; ?>
                    <?php endif; ?>
                </figure>
              </div>
            </div>
          </div>
          <hr id="hrgray">
          <div class="col footer_links">
            <?php echo $copyright_content;?>
          </div>
        </div>
      </div>
    </section>
</footer>
 <!-- footer -->
 
<?php wp_footer(); ?>
<!--    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.easing.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/scrollreveal.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery.magnific-popup.min.js"></script>

    <script src="<?php echo get_template_directory_uri() ?>/js/creative.min.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/slick/slick.min.js"></script>  
    <script src="<?php echo get_template_directory_uri() ?>/js/script.js"></script>
    <script src="<?php echo get_template_directory_uri() ?>/js/jquery-ui-datepicker.js"></script>-->
  </body>