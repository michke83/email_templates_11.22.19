<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
if ($_SERVER['REMOTE_ADDR']=='127.0.0.1' || $_SERVER['REMOTE_ADDR']=='::1' ) {
    define('WP_ENV', 'development');
} elseif ($_SERVER['HTTP_HOST']=='202.63.105.89' || $_SERVER['HTTP_HOST']=='192.168.1.3') {
    define('WP_ENV', 'local');
} 
else {
    define('WP_ENV', 'production');
}

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'snapshot_pinnaclegolf');

/** MySQL database username */
define('DB_USER', 'pinnaclegolf');

/** MySQL database password */
define('DB_PASSWORD', '(3Ms0*ik(vGW');

/** MySQL hostname */
define('DB_HOST', '10.208.164.61');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'G>y][_ki-Z}/1yV2S:a>pt#]Ai:s_BJu0bP0z0~N$}19c| TnJ<xCPDj;_.bv=|M');
define('SECURE_AUTH_KEY',  'a-oeh&J?AQP>+v3){-7b2=hca%{Q;&+-MS}CdWhlO+=Oqe#.JZ7rpL#@8V<T8RYk');
define('LOGGED_IN_KEY',    '35|S*Y<?4WPcr^ePP}RT+vg?qH,OWYmq5RhPm4m6xJ3B-koN?!p.w1~:jTV+vx=$');
define('NONCE_KEY',        'Moiqp[J,-Qrgj+xJ=26T[#D!sJ,V}Hs@z.%u<Y|inm(SsL9)8jt*B6!#EXR6p_v,');
define('AUTH_SALT',        '2H+WY^1?C$V/,UkEHE#8&(_}6H*M}fV-&8qr{<e<~/0#(en7<@F|w.WqORxx:Q#I');
define('SECURE_AUTH_SALT', 'XHUHbW]k#2,Ny_B`YSg9VBdmeR!zCRE;.>EsfKoZnjgU/yl.D~;NM30F:hx@3ci%');
define('LOGGED_IN_SALT',   '8:f,,v~g Zun]Kh(##?w(p[5(ym0<WCWj$M)myPWetdZi$$FOY+B=JUiOk*J87$8');
define('NONCE_SALT',       '&]iF0y9>vqNB H4*,W&q`a% :/rRm;>2$fnY-1_w=S|A&3rr8zG-r=34K__Q=hnk');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'jgahrtsv9e_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define ( 'WPCF7_AUTOP', false );


define ( 'WP_MINIFY', false );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
