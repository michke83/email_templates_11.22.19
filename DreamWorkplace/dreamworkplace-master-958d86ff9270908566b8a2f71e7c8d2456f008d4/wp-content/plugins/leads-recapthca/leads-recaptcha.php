<?php
/**
 * Plugin Name: Leads Recaptcha
 * Plugin URI: http://ivieinc.com
 * Description: Custom Google Recaptcha for Leads Forms
 * Author: Darren Ladner
 * Author URI: http://ivieinc.com
 * Version: 1.0.0
 * Text Domain: leads-recaptcha
 *
 **/

 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/

// plugin folder url
if(!defined('LRC_PLUGIN_URL')) {
	define('LRC_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}
// plugin folder path
if(!defined('LRC_PLUGIN_DIR')) {
	define('LRC_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
}
/*
|--------------------------------------------------------------------------
| INCLUDES / REQUIRE
|--------------------------------------------------------------------------
*/
require_once( LRC_PLUGIN_DIR.'recaptchalib.php' );

// enqueue jquery and recaptcha api
function lead_recaptcha_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script('lead-recaptcha-api-scripts', 'https://www.google.com/recaptcha/api.js');
}
add_action('wp_enqueue_scripts', 'lead_recaptcha_scripts');

// create menu for recaptcha settings page
add_action( 'admin_menu', 'create_leads_recaptcha_menu' );
function create_leads_recaptcha_menu() {
  $menu = add_options_page( 'Leads Recaptcha Settings', 'Leads Google Recpatcha Settings', 'manage_options', 'leads-recaptcha-settings', 'create_leads_recaptcha_settings_page' );
}

// create options
add_action( 'admin_init', 'register_leads_recaptcha_settings' );
function register_leads_recaptcha_settings() {
	add_option('leads_recaptcha_site_key', '');
}

// create settings page
function create_leads_recaptcha_settings_page() {
	if('POST' == $_SERVER['REQUEST_METHOD'])
  	{

  		$retrieved_nonce = $_POST['_wpnonce'];

  		if (wp_verify_nonce($retrieved_nonce, 'leads_recaptcha_form' ) ) 
		{
  			$leads_recaptcha_site_key = sanitize_text_field( $_POST['leads_recaptcha_site_key'] ); 
    		update_option('leads_recaptcha_site_key', $leads_recaptcha_site_key);
    	}
    	else 
    	{
    		print 'Sorry, your nonce did not verify. Please contact the web administrator.';
			exit;
    	}

    }

    ?>
    <div class="wrap">
	    <div class="container">
	      	<form id="es_settings" name="es_settings" action="" method="post">
	            <h3><?php _e( 'Leads Google Recaptcha Settings', 'leads-recaptcha' ); ?></h3>
	            <hr>
	            <p>
	            	<?php _e( 'If you do not have Google Recaptcha API Keys, please use the link below to create your Key and get more information.', 'leads-recaptcha' ); ?>
	            </p>
	            <p>
	            	<?php _e( '<a href="https://www.google.com/recaptcha/intro/index.html">Get Google Recapthca API Keys</a>', 'leads-recaptcha' ); ?>
	            </p>
	            <hr>
	            <p>
	            	<?php _e( 'Please save your Google Recaptcha API Keys', 'leads-recaptcha' ); ?>
	            </p>
	            <br/>
	            <fieldset>	
	            	<label for="leads_recaptcha_site_key">Google Recaptcha Site Key:</label>&nbsp;&nbsp;
	            	<input style="width:400px;" type="text" id="leads_recaptcha_site_key" name="leads_recaptcha_site_key" value="<?php echo esc_attr( get_option('leads_recaptcha_site_key') ); ?>" />
	            </fieldset>
	            <br/>  	
	        	<fieldset>
	        		<?php wp_nonce_field('leads_recaptcha_form'); ?>
                	<input type="submit" value="Save Settings" class="button-primary" />
              	</fieldset>
          	</form>
	    </div><!-- end container -->
	</div><!-- end wrap -->
    <?php
}

function custom_google_recaptcha_code() {
	$sitekey = get_option( 'leads_recaptcha_site_key' );            
	$html ='';
	$html .= '<br/><div class="g-recaptcha" data-sitekey="'.$sitekey.'"></div>';
    $html .= '<span style="color: red;" id="recaptcha-error">Recaptcha Error. Please try again.</span>';
	return $html;
     
}
add_filter( 'lead_add_google_recaptcha', 'custom_google_recaptcha_code' );


// create custom fields for mapping for Contact Form
// function map_gravity_forms($lead_fields) {

// 	$lead_fields['wpleads_contact_type'] =   array(
// 	        'label' => 'Contact Type', 
// 	        'key'  => 'wpleads_contact_type', 
// 	        'priority' => 235, 
// 	        'type'  => 'dropdown' 
// 	        );

// 	return $lead_fields;
// }
// add_filter('wp_leads_add_lead_field', 'map_gravity_forms');

// custom js recaptcha validation using custom Leads hooks
function leads_form_js(){
	?>
	<script>
		jQuery('#recaptcha-error').hide();

		_inbound.add_action( 'form_before_submission', inbound_recaptcha_verify, 10);

		function inbound_recaptcha_verify( data ) {
		    var v = grecaptcha.getResponse();
		    if (v.length == 0) {
		    	jQuery('#recaptcha-error').show();
		        throw new Error(recaptcha.error);
		    }
		}
	</script>
	<?php
}
add_action('wp_footer', 'leads_form_js');

// create Custom Inbound Customizations Class to extend Leads Form Field Type
class Inbound_Customizations {
    /**
    * Initialize Class
    */
    public function __construct() {
        self::add_hooks();
    }
    /**
    * Load hooks and filters
    */
    public static function add_hooks() {
    	/* Add handler to process unit type field type */
    	add_filter( 'inbound_form_custom_field' , array( __CLASS__ , 'inbound_form_custom_field' ), 10, 3 );
        /* extend form settings  */
        add_filter('inboundnow_forms_settings' , array( __CLASS__  , 'extend_form_settings' ) );
        /* Add form processing hander */
        //add_action('inboundnow_form_submit_actions' , array( __CLASS__ , 'process_forms' ) , 10 , 2 );
       
    }
    
    /**
    * Extend Form Settings
    *
    */
    function extend_form_settings( $settings ) {
        /* Adding taxonomy dropdown field type */
        $settings['forms']['child']['options']['field_type']['options']['google_recaptcha'] = __("Recaptcha - By Google", "leads-recaptcha");
        return $settings;
    }

	/**
	*  Listens for attachment uploader field type and renders it
	*/
	public static function inbound_form_custom_field( $form, $field, $form_id) {
		/* only render field if 'attachments' type is selected. */
		if ( !$field || $field['type'] != 'google_recaptcha'){
			return;
		}
		
		/********* CUSTOM FILTER FOR GOOGLE RECAPTCHA **************/
        $form .= apply_filters('lead_add_google_recaptcha', '');
        /********* CUSTOM FILTER FOR GOOGLE RECAPTCHA **************/
		return $form;
	}

}
new Inbound_Customizations;