<?php
/*
Plugin Name: Genesis Power Hook Advisor
Plugin URI: https://hyperdrivedesigns.com
Description: Displays Genesis action hooks, Genesis filter hooks and markup inside your Genesis theme.
Version: 1.0.0
Author: Darren Ladner
Author URI: https://hyperdrivedesigns.com
License: GPLv2
Textdomain: gpha
*/

 // Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
// plugin folder url
if(!defined('GPHA_PLUGIN_URL')) {
	define('GPHA_PLUGIN_URL', plugin_dir_url( __FILE__ ));
}
// plugin folder path
if(!defined('GPHA_PLUGIN_DIR')) {
	define('GPHA_PLUGIN_DIR', plugin_dir_path( __FILE__ ));
}
// plugin root file
if(!defined('GPHA_PLUGIN_FILE')) {
	define('GPHA_PLUGIN_FILE', __FILE__);
}
// plugin version
if(!defined('GPHA_VERSION')) {
	define('GPHA_VERSION', '1.0.0');
}

/**
 * Enqueue scripts and styles.
 */
if ( !function_exists('gpha_styles_scripts' ) )
{
	function gpha_styles_scripts() {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'gpha-custom-css', GPHA_PLUGIN_URL.'css/custom.css' );
		wp_enqueue_script( 'gpha-custom-js',  GPHA_PLUGIN_URL.'js/public-custom.js' );
		wp_enqueue_style( 'gpha-font-awesome', GPHA_PLUGIN_URL.'css/font-awesome.min.css' );
		
		if ( 'show' == isset( $_GET['g_hooks'] ) ) 
		{
	 		wp_enqueue_style( 'gpha_styles', GPHA_PLUGIN_URL . 'css/styles.css' );
		}

		if ( 'show' == isset( $_GET['g_filters'] ) ) 
		{
		 	wp_enqueue_style( 'gpha_styles', GPHA_PLUGIN_URL . 'css/styles.css' );
		}

	    if ( 'show' == isset( $_GET['g_markup'] ) )
	    {
	     	wp_enqueue_style( 'gpha_markup_styles', GPHA_PLUGIN_URL . 'css/markup.css' );
	    }

	    if ( 'show' == isset( $_GET['g_sink'] ) )
	    { 	
	     	wp_enqueue_style( 'gpha_markup_styles', GPHA_PLUGIN_URL . 'css/markup.css' );
	     	wp_enqueue_style( 'gpha_styles', GPHA_PLUGIN_URL . 'css/styles.css' );
	        wp_enqueue_style( 'gpha_styles', GPHA_PLUGIN_URL . 'css/styles.css' );
	    } 		
	}
}
add_action( 'wp_enqueue_scripts', 'gpha_styles_scripts' );

/**
 * Create settings menu
 */
if ( !function_exists('gpha_create_settings_menu' ) )
{
	function gpha_create_settings_menu() {
		$menu = add_options_page( 'Genesis Power Hook Advisor', 'Genesis Power Hook Advisor', 'manage_options', 'gpha-admin.php', 'gpha_create_admin_page', 'dashicons-admin-users', 6 );

		 add_action( 'admin_print_styles-' . $menu, 'gpha_admin_custom_css' );
		 add_action( 'admin_print_scripts-' . $menu, 'gpha_admin_custom_js' );
	}
}
add_action( 'admin_menu', 'gpha_create_settings_menu' );

/**
 * Enqueue styles for admin
 */
if ( !function_exists('gpha_admin_custom_css' ) )
{
	function gpha_admin_custom_css()
	{

		wp_enqueue_style( 'rua-admin-font-awesome-css', untrailingslashit( GPHA_PLUGIN_URL.'css/font-awesome.min.css', '4.4.0' ) );
		wp_enqueue_style( 'rua-admin-bootstrap-css', untrailingslashit( GPHA_PLUGIN_URL.'css/bootstrap.min.css', '3.3.5' ) );
		wp_enqueue_style( 'wp-job-lister-lite-frontend-styles', GPHA_PLUGIN_URL.'css/jquery-ui.css' );
	}
}

/**
 * Enqueue scripts for admin
 */
if ( !function_exists('gpha_admin_custom_js' ) )
{
	function gpha_admin_custom_js()
	{
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'jquery-ui-core' );
		wp_enqueue_script( 'jquery-ui-tabs' );

		wp_enqueue_script( 'gpha-admin-bootstrap-js', untrailingslashit( GPHA_PLUGIN_URL.'js/bootstrap.min.js' ) );				
		wp_enqueue_script( 'gpha-custom-js', untrailingslashit( GPHA_PLUGIN_URL.'/js/admin-custom.js' ) );
	};
}


/**
 * Create admin dashboard page
 */
if ( !function_exists('gpha_create_admin_page' ) )
{
	function gpha_create_admin_page() {
		if('POST' == $_SERVER['REQUEST_METHOD'])
		{
			$gpha_retrieved_nonce = $_POST['_wpnonce'];
			if ( wp_verify_nonce( $gpha_retrieved_nonce, 'gpha_options_nonce' ) ) 
			{
				$enable_jquery_sliding_menu = sanitize_text_field( $_POST['enable_jquery_sliding_menu'] );
			
				update_option( 'enable_jquery_sliding_menu', $enable_jquery_sliding_menu );
			}
			else 
			{
				print 'Sorry, your nonce did not verify. Please try again.';
	   			exit;
			} 			
		}
		?>
		<div class="wrap"><!-- start wrap -->
			<div class="container"><!-- start container -->
			<h3 class="text-center"><?php _e( 'Genesis Power Hook Advisor', 'gpha' ); ?></h3>
				<div id="tabs" class="ui-tabs ui-widget ui-widget-content ui-corner-all">
					<ul class="ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all" role="tablist">
						<li class="ui-state-default ui-corner-top ui-tabs-active ui-state-active" role="tab" tabindex="0" aria-controls="tabs-1" aria-labelledby="ui-id-7" aria-selected="true" aria-expanded="true">
							<a href="#tabs-1" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-7">Settings</a>
						</li>
						<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-2" aria-labelledby="ui-id-8" aria-selected="false" aria-expanded="false">
							<a href="#tabs-2" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-8">Genesis Hooks</a>
						</li>
						<li class="ui-state-default ui-corner-top" role="tab" tabindex="-1" aria-controls="tabs-3" aria-labelledby="ui-id-9" aria-selected="false" aria-expanded="false">
							<a href="#tabs-3" class="ui-tabs-anchor" role="presentation" tabindex="-1" id="ui-id-9">Genesis Filter</a>
						</li>
					</ul>
					<div id="tabs-1" aria-labelledby="ui-id-7" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" aria-hidden="false" style="display: block;">
						<form action="" method="post">
						<?php wp_nonce_field( 'gpha_options_nonce' ); ?>
						<p>
							If you are a user that has the admin toolbar DISABLED as a user, especially when you are developing WordPress themes then ENABLE the jQuery Slidiing Genesis Power 
							Hook Advisor Option below so the the jQuery Sliding Genesis Power Hook Advisor menu will become enabled since the admin bar will not be visible.
						</p>
						<p>
							The normal operation is to the display the Genesis Power Hook Advisor in the admin bar. But many people, have the admin bar DISABLED in their User profile especially when
							they are developing themes. So the default option will not appear in the admin bar because there is NO admin bar displayed.
						</p>
						<label for="enable_jquery_sliding_menu">Enable jQuery Slidiing Genesis Power Hook Advisor Option</label>
						<select id="enable_jquery_sliding_menu" name="enable_jquery_sliding_menu">
							<option value="<?php echo get_option( 'enable_jquery_sliding_menu' ); ?>"><?php echo get_option( 'enable_jquery_sliding_menu' ); ?></option>
							<option value="enable">Enable</option>
							<option value="disable">Disable</option>
						</select>
						<input class="button-primary" type="submit" name="save_setttings" value="<?php esc_attr_e( 'Save Settings' ); ?>" />
						<hr>
						<p>If you are a user that has disabled the admin toolbar in your user profile by unchecking the option "Show Toolbar when viewing site" </p>
						</form>
					</div>
					<div id="tabs-2" aria-labelledby="ui-id-8" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" style="display: none;" aria-hidden="true">
						<?php include ( 'inc/genesis-hooks.php' ); ?>
					</div>
					<div id="tabs-3" aria-labelledby="ui-id-9" class="ui-tabs-panel ui-widget-content ui-corner-bottom" role="tabpanel" style="display: none;" aria-hidden="true">
						<?php include ( 'inc/genesis-filter.php' ); ?>
					</div>
				</div>

			</div><!-- end container -->
		</div><!-- end wrap -->
		<?php
	}
}

/**
 * Create jquery hidden sliding menu
 */
if ( !function_exists('gpha_hiding_div_menu' ) )
{
	function gpha_hiding_div_menu() {
		
		$is_jquery_slider_enabled = get_option( 'enable_jquery_sliding_menu' );

		if ( $is_jquery_slider_enabled == 'enable' ) 
		{
		   ?>
				<div id="triggerdiv"> <i class="fa fa-bullseye fa-lg" aria-hidden="true"></i> </div>
				<div class="holdingbox">
				 <span class="leftbox"></span>
				 <span class="rightbox">
				     <span class="hiddencontent">
				     <a href="<?php echo site_url(); ?>?g_hooks=show"><i class="fa fa-anchor" aria-hidden="true"></i>  Action Hooks</a><br/>
				     <a href="<?php echo site_url(); ?>?g_filters=show"><i class="fa fa-filter" aria-hidden="true"></i>  Filter Hooks</a><br/>
				     <a href="<?php echo site_url(); ?>?g_markup=show"><i class="fa fa-code" aria-hidden="true"></i>  Markup</a><br/>
				     <a href="<?php echo site_url(); ?>?g_hooks=show&g_filters=show&g_markup=show">
				     <i class="fa fa-reply-all" aria-hidden="true"></i>  Kitchen Sink</a><br/>
				     <a href="<?php echo site_url(); ?>"><i class="fa fa-eraser" aria-hidden="true"></i>  Reset</a>
				     </span>
				 </span>
				</div>

			<?php
		}

	}
}
add_action( 'wp_head', 'gpha_hiding_div_menu' );

/**
 * Activation and check for Genesis Framework
 */
if ( !function_exists('gpha_activation_genesis_check' ) )
{
	function gpha_activation_genesis_check() {

		    $gpha_theme_info = wp_get_theme();

			$gpha_genesis_themes = array(
				'genesis',
				'genesis-trunk',
			);

	        if ( ! in_array( $gpha_theme_info->Template, $gpha_genesis_themes ) ) 
	        {
	            deactivate_plugins( plugin_basename(__FILE__) );
		        wp_die('Oh No! You must have the Genesis Framework installed in order for the plugin to actiavte and work correctly.  
		        	    For more information about the Genesis Framework go to StudioPress website located <a href="http://www.studiopress.com/themes/genesis">HERE</a>');
	        }

	        add_option('enable_jquery_sliding_menu', 'disable');
	}
}
register_activation_hook(__FILE__, 'gpha_activation_genesis_check');


/**
 * Create admin bar menu
 */
if ( !function_exists('gpha_create_admin_bar_menu' ) )
{
	function gpha_create_admin_bar_menu() {
		global $wp_admin_bar;

		if ( is_admin() )
			return;

		$wp_admin_bar->add_menu(
			array(
				'id' => 'ghooks',
				'title' => __( 'GPHA Hook Advisor', 'gvisualhookguide' ),
				'href' => '',
				'position' => 0,
				'meta' => array(
					'class'	=> 'gpha-menu-text',
				),		
			)
		);
		$wp_admin_bar->add_menu(
			array(
				'id'	   => 'ghooks_action',
				'parent'   => 'ghooks',
				'title'    => __( 'Action Hooks', 'gvisualhookguide' ),
				'href'     => '?g_hooks=show',
				'position' => 10,
				'meta' => array(
					'class'	=> 'gpha-menu-text',
				),
			)
		);
		$wp_admin_bar->add_menu(
			array(
				'id'	   => 'ghooks_filter',
				'parent'   => 'ghooks',
				'title'    => __( 'Filter Hooks', 'gvisualhookguide' ),
				'href'     => '?g_filters=show',
				'position' => 10,
				'meta' => array(
					'class'	=> 'gpha-menu-text',
				),
			)
		);
		$wp_admin_bar->add_menu(
			array(
				'id'	   => 'ghooks_markup',
				'parent'   => 'ghooks',
				'title'    => __( 'Markup', 'gvisualhookguide' ),
				'href'     => '?g_markup=show',
				'position' => 10,
				'meta' => array(
					'class'	=> 'gpha-menu-text',
				),
			)
		);
		$wp_admin_bar->add_menu(
			array(
				'id'	   => 'ghooks_sink',
				'parent'   => 'ghooks',
				'title'    => __( 'Kitchen Sink', 'gvisualhookguide' ),
				'href'     => '?g_hooks=show&g_filters=show&g_markup=show',
				'position' => 10,
				'meta' => array(
					'class'	=> 'gpha-menu-text',
				),
			)
		);

		$wp_admin_bar->add_menu(
			array(
				'id'	   => 'ghooks_clear',
				'parent'   => 'ghooks',
				'title'    => __( 'Reset', 'gvisualhookguide' ),
				'href'     => remove_query_arg(
					array(
						'g_hooks',
						'g_filters',
						'g_markup',
						'g_sink',
					)
				),
				'position' => 10,
				'meta' => array(
					'class'	=> 'gpha-menu-text',
				),
			)
		);

	}
}
add_action( 'admin_bar_menu', 'gpha_create_admin_bar_menu', 100 );

/**
 * Create all of the genesis hooks data
 */
if ( !function_exists('gpha_genesis_hooks' ) )
{
	function gpha_genesis_hooks() {
		global $gpha_genesis_action_hooks;

		 if ( !('show' == isset( $_GET['g_hooks'] ) ) && !('show' == isset( $_GET['g_filters'] ) ) && !('show' == isset( $_GET['g_markup'] ) ) ) {
			 return; 
		 }

		$gpha_genesis_action_hooks = array(
				
				'genesis_pre' => array(
					'hook' => 'genesis_pre',
					'area' => 'Document Head',
					'description' => 'This is the first hook to execute within Genesis. Think of any function hooked to it as being executed before any Genesis functions have loaded.',
					'functions' => array(),
					),
				'genesis_doctype' => array(
					'hook' => 'genesis_doctype',
					'area' => 'Document Head',
					'description' => '',
					'functions' => array(),
					),
				'genesis_title' => array(
					'hook' => 'genesis_title',
					'area' => 'Document Head',
					'description' => 'This hook executes between tags and outputs the doctitle. You can find all doctitle related code in /lib/structure/header.php.',
					'functions' => array(),
					),
				'genesis_meta' => array(
					'hook' => 'genesis_meta',
					'area' => 'Document Head',
					'description' => 'This hook executes in the section of the document source. By default, things like META descriptions and keywords are output using this hook, along with the default stylesheet and the reference to the favicon.',
					'functions' => array(),
					),
				'genesis_home' => array(
					'hook' => 'genesis_home',
					'area' => 'Structural',
					'description' => '',
					'functions' => array(),
					),
				'genesis_before' => array(
					'hook' => 'genesis_before',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the opening tag in the document source.',
					'functions' => array(),
					),
				'genesis_before_header' => array(
					'hook' => 'genesis_before_header',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the header (outside the #header div).',
					'functions' => array(),
					),
				'genesis_header' => array(
					'hook' => 'genesis_header',
					'area' => 'Structural',
					'description' => 'By default, this hook outputs the header code, including the title, description, and widget area (if necessary).',
					'functions' => array(),
					),
				'genesis_header_right' => array(
					'hook' => 'genesis_header_right',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the Header Right widget area inside div.widget-area.',
					),
				'genesis_site_title' => array(
					'hook' => 'genesis_site_title',
					'area' => 'Structural',
					'description' => 'By default, this hook outputs the site title, within the header area. It uses the user-specified SEO settings to build the site title markup appropriately.',
					'functions' => array(),
					),
				'genesis_site_description' => array(
					'hook' => 'genesis_site_description',
					'area' => 'Structural',
					'description' => 'By default, this hook outputs the site description, within the header area. It uses the user-specified SEO settings to build the site description markup appropriately.',
					'functions' => array(),
					),
				'genesis_after_header' => array(
					'hook' => 'genesis_after_header',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the header (outside the #header div).',
					'functions' => array(),
					),
				'genesis_before_content_sidebar_wrap' => array(
					'hook' => 'genesis_before_content_sidebar_wrap',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the div block that wraps the content and the primary sidebar (outside the #content-sidebar-wrap div).',
					'functions' => array(),
					),
				'genesis_before_content' => array(
					'hook' => 'genesis_before_content',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the content column (outside the #content div).',
					'functions' => array(),
					),
				'genesis_before_loop' => array(
					'hook' => 'genesis_before_loop',
					'area' => 'Loop',
					'description' => 'This hook executes immediately before all loop blocks. Therefore, this hook falls outside the loop, and cannot execute functions that require loop template tags or variables.',
					'functions' => array(),
					),
				'genesis_loop' => array(
					'hook' => 'genesis_loop',
					'area' => 'Loop',
					'description' => 'This hook outputs the actual loop. See lib/structure/loop.php and lib/structure/post.php for more details.',
					'functions' => array(),
					),
				'genesis_before_entry' => array(
					'hook' => 'genesis_before_entry',
					'area' => 'Loop',
					'description' => 'This hook executes before each post in all loop blocks (outside the post_class() article).',
					'functions' => array(),
					),
				'genesis_entry_header' => array(
					'hook' => 'genesis_entry_header',
					'area' => 'Loop',
					'description' => 'This hook executes immediately inside the article element for each post within the loop.',
					'functions' => array(),
					),
				'genesis_before_entry_content' => array(
					'hook' => 'genesis_entry_content',
					'area' => 'Loop',
					'description' => 'This hook executes immediately before the post/page content is output, outside the .entry-content div.',
					'functions' => array(),
					),
				'genesis_entry_content' => array(
					'hook' => 'genesis_entry_content',
					'area' => 'Loop',
					'description' => 'This hook outputs the actual post content and if chosen, the post image (inside the #content div).',
					'functions' => array(),
					),
				'genesis_after_entry_content' => array(
					'hook' => 'genesis_entry_content',
					'area' => 'Loop',
					'description' => 'This hook executes immediately after the post/page content is output, outside the .entry-content div.',
					'functions' => array(),
					),
				'genesis_entry_footer' => array(
					'hook' => 'genesis_entry_footer',
					'area' => 'Loop',
					'description' => 'This hook executes immediately before the close of the article element for each post within the loop.',
					'functions' => array(),
					),
				'genesis_after_entry' => array(
					'hook' => 'genesis_after_entry',
					'area' => 'Loop',
					'description' => 'This hook executes after each post in all loop blocks (outside the post_class() article).',
					'functions' => array(),
					),
				'genesis_before_post' => array(
					'hook' => 'genesis_before_post',
					'area' => 'Loop',
					'description' => 'This hook executes before each post in all loop blocks (outside the post_class() div).',
					'functions' => array(),
					),
				'genesis_before_post_title' => array(
					'hook' => 'genesis_before_post_title',
					'area' => 'Loop',
					'description' => 'This hook executes immediately before each post title for each post within the loop.',
					'functions' => array(),
					),
				'genesis_post_title' => array(
					'hook' => 'genesis_post_title',
					'area' => 'Loop',
					'description' => 'This hook outputs the actual post title, contextually, based on what type of page you are viewing.',
					'functions' => array(),
					),
				'genesis_after_post_title' => array(
					'hook' => 'genesis_after_post_title',
					'area' => 'Loop',
					'description' => 'This hook executes immediately after each post title for each post within the loop.',
					'functions' => array(),
					),
				'genesis_before_post_content' => array(
					'hook' => 'genesis_before_post_content',
					'area' => 'Loop',
					'description' => 'This hook executes immediately before the post/page content is output, outside the .entry-content div.',
					'functions' => array(),
					),
				'genesis_post_content' => array(
					'hook' => 'genesis_post_content',
					'area' => 'Loop',
					'description' => 'This hook outputs the actual post content and if chosen, the post image (inside the #content div).',
					'functions' => array(),
					),
				'genesis_after_post_content' => array(
					'hook' => 'genesis_after_post_content',
					'area' => 'Loop',
					'description' => 'This hook executes immediately after the post/page content is output, outside the .entry-content div.',
					'functions' => array(),
					),
				'genesis_after_post' => array(
					'hook' => 'genesis_after_post',
					'area' => 'Loop',
					'description' => 'This hook executes after each post in all loop blocks (outside the post_class() div).',
					'functions' => array(),
					),
				'genesis_before_comments' => array(
					'hook' => 'genesis_before_comments',
					'area' => 'Comment',
					'description' => 'This hook executes immediately before the comments block (outside the #comments div).',
					'functions' => array(),
					),
				'genesis_list_comments' => array(
					'hook' => 'genesis_list_comments',
					'area' => 'Comment',
					'description' => 'This hook executes inside the comments block, inside the .comment-list OL. By default, it outputs a list of comments associated with a post via the genesis_default_list_comments() function.',
					'functions' => array(),
					),
				'genesis_before_comment' => array(
					'hook' => 'genesis_before_comment',
					'area' => 'Comment',
					'description' => 'This hook executes before the output of each individual comment (author, meta, comment text).',
					'functions' => array(),
					),
				'genesis_comment' => array(
					'hook' => 'genesis_comment',
					'area' => 'Comment',
					'description' => '',
					'functions' => array(),
					),
				'genesis_after_comment' => array(
					'hook' => 'genesis_after_comment',
					'area' => 'Comment',
					'description' => 'This hook executes after the output of each individual comment (author, meta, comment text).',
					'functions' => array(),
					),
				'genesis_after_comments' => array(
					'hook' => 'genesis_after_comments',
					'area' => 'Comment',
					'description' => 'This hook executes immediately after the comments block (outside the #comments div).',
					'functions' => array(),
					),
				'genesis_before_pings' => array(
					'hook' => 'genesis_before_pings',
					'area' => 'Comment',
					'description' => 'This hook executes immediately before the pings block (outside the #pings div).',
					'functions' => array(),
					),
				'genesis_list_pings' => array(
					'hook' => 'genesis_list_pings',
					'area' => 'Comment',
					'description' => 'This hook executes inside the pings block, inside the .ping-list OL. By default, it outputs a list of pings associated with a post via the genesis_default_list_pings() function.',
					'functions' => array(),
					),
				'genesis_after_pings' => array(
					'hook' => 'genesis_after_pings',
					'area' => 'Comment',
					'description' => 'This hook executes immediately after the pings block (outside the #pings div).',
					'functions' => array(),
					),
				'genesis_before_respond' => array(
					'hook' => 'genesis_before_respond',
					'area' => 'Comment',
					'description' => '',
					'functions' => array(),
					),
				'genesis_before_comment_form' => array(
					'hook' => 'genesis_before_comment_form',
					'area' => 'Comment',
					'description' => 'This hook executes immediately before the comment form, outside the #respond div.',
					'functions' => array(),
					),
				'genesis_comment_form' => array(
					'hook' => 'genesis_comment_form',
					'area' => 'Comment',
					'description' => 'This hook outputs the actual comment form, including the #respond div wrapper.',
					'functions' => array(),
					),
				'genesis_after_comment_form' => array(
					'hook' => 'genesis_after_comment_form',
					'area' => 'Comment',
					'description' => 'This hook executes immediately after the comment form, outside the #respond div.',
					'functions' => array(),
					),
				'genesis_after_respond' => array(
					'hook' => 'genesis_after_respond',
					'area' => 'Comment',
					'description' => '',
					'functions' => array(),
					),
				'genesis_after_endwhile' => array(
					'hook' => 'genesis_after_endwhile',
					'area' => 'Loop',
					'description' => 'This hook executes after the endwhile; statement in all loop blocks.',
					'functions' => array(),
					),
				'genesis_loop_else' => array(
					'hook' => 'genesis_loop_else',
					'area' => 'Loop',
					'description' => 'This hook executes after the else : statement in all loop blocks.',
					'functions' => array(),
					),
				'genesis_after_loop' => array(
					'hook' => 'genesis_after_loop',
					'area' => 'Loop',
					'description' => 'This hook executes immediately after all loop blocks. Therefore, this hook falls outside the loop, and cannot execute functions that require loop template tags or variables.',
					'functions' => array(),
					),
				'genesis_after_content' => array(
					'hook' => 'genesis_after_content',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the content column (outside the #content div).',
					'functions' => array(),
					),
				'genesis_before_sidebar_widget_area' => array(
					'hook' => 'genesis_before_sidebar_widget_area',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the primary sidebar widget area (inside the #sidebar div).',
					'functions' => array(),
					),
				'genesis_after_sidebar_widget_area' => array(
					'hook' => 'genesis_after_sidebar_widget_area',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the primary sidebar widget area (inside the #sidebar div).',
					'functions' => array(),
					),
				'genesis_after_content_sidebar_wrap' => array(
					'hook' => 'genesis_after_content_sidebar_wrap',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the div block that wraps the content and the primary sidebar (outside the #content-sidebar-wrap div).',
					'functions' => array(),
					),
				'genesis_before_sidebar_alt_widget_area' => array(
					'hook' => 'genesis_before_sidebar_alt_widget_area',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the alternate sidebar widget area (inside the #sidebar-alt div).',
					'functions' => array(),
					),
				'genesis_after_sidebar_alt_widget_area' => array(
					'hook' => 'genesis_after_sidebar_alt_widget_area',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the alternate sidebar widget area (inside the #sidebar-alt div).',
					'functions' => array(),
					),
				'genesis_before_footer' => array(
					'hook' => 'genesis_before_footer',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the footer, outside the #footer div.',
					'functions' => array(),
					),
				'genesis_footer' => array(
					'hook' => 'genesis_footer',
					'area' => 'Structural',
					'description' => 'This hook, by default, outputs the content of the footer, including the #footer div wrapper.',
					'functions' => array(),
					),
				'genesis_after_footer' => array(
					'hook' => 'genesis_after_footer',
					'area' => 'Structural',
					'description' => 'This hook executes immediately after the footer, outside the #footer div.',
					'functions' => array(),
					),
				'genesis_after' => array(
					'hook' => 'genesis_after',
					'area' => 'Structural',
					'description' => 'This hook executes immediately before the closing tag in the document source.',
					'functions' => array(),
					)
			);

		foreach ( $gpha_genesis_action_hooks as $action )
			add_action( $action['hook'] , 'gpha_genesis_action_hook' , 1 );
	}
}
add_action('get_header', 'gpha_genesis_hooks' );


if ( !function_exists('gpha_genesis_action_hook' ) )
{
	function gpha_genesis_action_hook () {
		global $gpha_genesis_action_hooks;

		$current_action = current_filter();

		if ( 'show' == isset( $_GET['g_hooks'] ) || ( 'show' == isset( $_GET['g_sink'] ) ) ) 
		{

			if ( 'Document Head' == $gpha_genesis_action_hooks[$current_action]['area'] ) :

				echo "<!-- ";
					echo $current_action;
				echo " -->\n";

			else :

				echo '<div class="genesis_hook" title="' . $gpha_genesis_action_hooks[$current_action]['description'] . '">' . $current_action . '</div>';

			endif;
		}

	}
}

if ( !function_exists('gpha_genesis_hook_name' ) )
{
	function gpha_genesis_hook_name( $function, $element = null, $description = null ) {
	    $function_name = str_replace( 'gpha', 'genesis', $function );
	    if ( $element ) {
	        return '<' . $element . ' class="filter" title="' . $description . '">' . $function_name . '</' . $element . '>';
	    }
	    return $function;
	}
}

if ( !function_exists('gpha_genesis_filtering' ) )
{
	function gpha_genesis_filtering() {

		if ( 'show' == isset( $_GET['g_filters'] ) ) 
		{

			add_filter( 'genesis_seo_title', 'gpha_genesis_seo_title', 10, 3 );
			add_filter( 'genesis_seo_description', 'gpha_genesis_seo_description', 10, 3 );
			add_filter( 'genesis_title_comments', 'gpha_title_comments');
			add_filter( 'genesis_comment_form_args', 'gpha_comment_form_args');
			add_filter( 'genesis_comments_closed_text', 'gpha_comments_closed_text');
			add_filter( 'comment_author_says_text', 'gpha_comment_author_says_text');
			add_filter( 'genesis_no_comments_text', 'gpha_no_comments_text');
			add_filter( 'genesis_title_pings', 'gpha_title_pings');
			add_filter( 'ping_author_says_text', 'gpha_ping_author_says_text');
			add_filter( 'genesis_no_pings_text', 'gpha_no_pings_text');
			add_filter( 'genesis_breadcrumb_args', 'gpha_breadcrumb_args');
			add_filter( 'genesis_footer_backtotop_text', 'gpha_footer_backtotop_text', 100);
			add_filter( 'genesis_footer_creds_text', 'gpha_footer_creds_text', 100);
			add_filter( 'genesis_footer_output', 'gpha_footer_output', 100, 3);
			add_filter( 'genesis_author_box_title', 'gpha_author_box_title' );
			add_filter( 'genesis_post_info', 'gpha_post_info' );
			add_filter( 'genesis_post_meta', 'gpha_post_meta' );
			add_filter( 'genesis_post_title_text', 'gpha_post_title_text');
			add_filter( 'genesis_noposts_text', 'gpha_noposts_text');
			add_filter( 'genesis_search_text', 'gpha_search_text');
			add_filter( 'genesis_search_button_text', 'gpha_search_button_text');
			add_filter( 'genesis_nav_home_text', 'gpha_nav_home_text');
			add_filter( 'genesis_favicon_url', 'gpha_favicon_url');
			add_filter( 'genesis_footer_credits', 'gpha_footer_creds_text');

		}

	}
}
add_action( 'wp', 'gpha_genesis_filtering' );


function gpha_genesis_seo_title( $title, $inside, $wrap ) {
	return sprintf('<%s id="title">' . gpha_genesis_hook_name( __FUNCTION__, 'span', 'Applied to the output of the genesis_seo_site_title function which depending on the SEO option set by the user will either wrap the title in <h1> or <p> tags. Default value: $title, $inside, $wrap' ) . '</%s>', $wrap, $wrap);
}

function gpha_genesis_seo_description( $description, $inside, $wrap ) {
	return sprintf('<%s id="title">' . gpha_genesis_hook_name( __FUNCTION__, 'span', 'Applied to the output of the genesis_seo_site_description function which depending on the SEO option set by the user will either wrap the description in <h1> or <p> tags. Default value: $description, $inside, $wrap' ) . '</%s>', $wrap, $wrap);
}

function gpha_author_box_title() {
	return '<strong>' . gpha_genesis_hook_name( __FUNCTION__, 'span' ) . '</strong>';
}

function gpha_comment_author_says_text() {
	return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}

function gpha_ping_author_says_text() {
	return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}

function gpha_footer_backtotop_text() {
    return gpha_genesis_hook_name( __FUNCTION__, 'div' );
}

function gpha_footer_creds_text() {
    return gpha_genesis_hook_name( __FUNCTION__, 'div' );
}

function gpha_footer_output($output, $backtotop_text, $creds) {
    return gpha_genesis_hook_name( __FUNCTION__, 'div' ) . $backtotop_text . $creds;
}

function gpha_breadcrumb_args($args) {
	$args['prefix'] = '<div class="breadcrumb"><span class="filter">genesis_breadcrumb_args</span> ';
    $args['suffix'] = '</div>';
	$args['home'] = __('<span class="filter">[\'home\']</span>', 'genesis');
    $args['sep'] = '<span class="filter">[\'sep\']</span>';
    $args['labels']['prefix'] = __('<span class="filter">[\'labels\'][\'prefix\']</span> ', 'genesis');
	return $args;
}

function gpha_title_pings() {
    echo gpha_genesis_hook_name( __FUNCTION__, 'h3' );
}

function gpha_no_pings_text() {
    echo gpha_genesis_hook_name( __FUNCTION__, 'p' );
}

function gpha_title_comments() {
    echo gpha_genesis_hook_name( __FUNCTION__, 'h3' );
}

function gpha_comments_closed_text() {
    echo gpha_genesis_hook_name( __FUNCTION__, 'p' );
}

function gpha_no_comments_text() {
    echo gpha_genesis_hook_name( __FUNCTION__, 'p' );
}

function gpha_comment_form_args($args) {
    $args['title_reply'] = '<span class="filter">genesis_comment_form_args [\'title_reply\']</span>';
    $args['comment_notes_before'] = '<span class="filter">genesis_comment_form_args [\'comment_notes_before\']</span>';
    $args['comment_notes_after'] = '<span class="filter">genesis_comment_form_args [\'comment_notes_after\']</span>';

    return $args;
}

function gpha_favicon_url() {
    return 'genesis_favicon_url';
}

function gpha_post_info() {
    return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}

function gpha_post_meta() {
    return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}

function gpha_post_title_text() {
	return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}

function gpha_noposts_text() {
	return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}

function gpha_search_text() {
	return esc_attr('genesis_search_text');
}

function gpha_search_button_text() {
	return esc_attr('genesis_search_button_text');
}

function gpha_nav_home_text() {
	return gpha_genesis_hook_name( __FUNCTION__, 'span' );
}
