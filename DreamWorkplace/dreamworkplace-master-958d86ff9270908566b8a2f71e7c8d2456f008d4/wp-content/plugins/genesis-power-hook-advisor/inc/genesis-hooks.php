<h3>GENESIS HOOKS</h3>

<table class="form-table">
	<tr>
		<th class="row-title"><?php esc_attr_e( 'Genesis Hooks', 'gpha' ); ?></th>
		<td class="row-title"><?php esc_attr_e( 'Description', 'gpha' ); ?></th>
		<td class="row-title"><?php esc_attr_e( 'Section', 'gpha' ); ?></th>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pre', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This is the first hook to execute within Genesis. Think of any function hooked to it as being executed before any Genesis functions have loaded.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Internal', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pre_framework', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before any of the Genesis Framework components have been loaded, but after all the constants have been defined.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Internal', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_title', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes between tags and outputs the doctitle. You can find all doctitle related code in /lib/structure/header.php.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Document Head', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_meta', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes in the section of the document source. By default, things like META descriptions and keywords are output using this hook, along with the default stylesheet and the reference to the favicon.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Document Head', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the opening tag in the document source.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Document Head', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the closing tag in the document source.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Document Head', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_header', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the header (outside the #header div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_header', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'By default, this hook outputs the header code, including the title, description, and widget area (if necessary).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_header', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the header (outside the #header div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_site_title', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'By default, this hook outputs the site title, within the header area. It uses the user-specified SEO settings to build the site title markup appropriately.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_site_description', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'By default, this hook outputs the site description, within the header area. It uses the user-specified SEO settings to build the site description markup appropriately.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_content_sidebar_wrap', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the div block that wraps the content and the primary sidebar (outside the #content-sidebar-wrap div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_content_sidebar_wrap', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the div block that wraps the content and the primary sidebar (outside the #content-sidebar-wrap div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_content', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the content column (outside the #content div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_content', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the content column (outside the #content div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_sidebar', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the content of the primary sidebar, including the widget area output.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_sidebar_widget_area', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the primary sidebar widget area (inside the #sidebar div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_sidebar_widget_area', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the primary sidebar widget area (inside the #sidebar div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_sidebar_alt', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the content of the secondary sidebar, including the widget area output.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_sidebar_alt_widget_area', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the alternate sidebar widget area (inside the #sidebar-alt div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_sidebar_alt_widget_area', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the alternate sidebar widget area (inside the #sidebar-alt div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_footer', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the footer, outside the #footer div.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook, by default, outputs the content of the footer, including the #footer div wrapper.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_footer', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the footer, outside the #footer div.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Structual', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_loop', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before all loop blocks. Therefore, this hook falls outside the loop, and cannot execute functions that require loop template tags or variables.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_loop', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the actual loop. See lib/structure/loop.php and lib/structure/post.php for more details.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_loop', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after all loop blocks. Therefore, this hook falls outside the loop, and cannot execute functions that require loop template tags or variables.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_endwhile', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after the endwhile; statement in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_loop_else', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after the else : statement in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_entry (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes before each entry in all loop blocks (outside the post_class() container).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_entry (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after each entry in all loop blocks (outside the post_class() container).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_entry_header (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes before the entry content and generates the entry header content in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_entry_content (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes before the .entry-content container in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_entry_content (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes within the .entry-content container in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_entry_content (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after the .entry-content container in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_entry_footer (HTML5)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after the entry content and generates the entry footer content in all loop blocks.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_post (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes before each post in all loop blocks (outside the post_class() div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_post (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after each post in all loop blocks (outside the post_class() div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_post_title (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before each post title for each post within the loop.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_title (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the actual post title, contextually, based on what type of page you are viewing.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_post_title (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after each post title for each post within the loop.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_post_content (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the post/page content is output, outside the .entry-content div.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_content (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the actual post content and if chosen, the post image (inside the #content div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_post_content (XHTML)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the post/page content is output, outside the .entry-content div.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_comments', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the comments block (outside the #comments div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_comments', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the entire comments block, including the section title. It also executes the genesis_list_comments hook, which outputs the comment list.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_comments', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the comments block (outside the #comments div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_list_comments', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes inside the comments block, inside the .comment-list OL. By default, it outputs a list of comments associated with a post via the genesis_default_list_comments() function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_pings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the pings block (outside the #pings div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the entire pings block, including the section title. It also executes the genesis_list_pings hook, which outputs the ping list.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_pings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the pings block (outside the #pings div).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_list_pings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes inside the pings block, inside the .ping-list OL. By default, it outputs a list of pings associated with a post via the genesis_default_list_pings() function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_comment', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes before the output of each individual comment (author, meta, comment text).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_comment', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes after the output of each individual comment (author, meta, comment text).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_before_comment_form', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately before the comment form, outside the #respond div.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_comment_form', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook outputs the actual comment form, including the #respond div wrapper.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_after_comment_form', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'This hook executes immediately after the comment form, outside the #respond div.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

</table>
