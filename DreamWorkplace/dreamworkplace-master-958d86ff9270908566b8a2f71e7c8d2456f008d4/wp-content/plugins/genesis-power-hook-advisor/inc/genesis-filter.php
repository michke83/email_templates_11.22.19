<h3>GENESIS FILTERS</h3>

<table class="form-table">
	<tr>
		<th class="row-title"><?php esc_attr_e( 'Genesis Filter', 'gpha' ); ?></th>
		<th class="row-title"><?php esc_attr_e( 'Default Value', 'gpha' ); ?></th>
		<td class="row-title"><?php esc_attr_e( 'Description', 'gpha' ); ?></th>
		<td class="row-title"><?php esc_attr_e( 'Section', 'gpha' ); ?></th>
	</tr>
	<!-- COMMENTS FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_title_comments', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '<h3>Comments</h3>', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the comments title text as well as heading tags in the genesis_do_comments function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_title_comments', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'empty', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the no comments text if commenting is enabled but there are no comments so far in the genesis_do_comments function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_comments_closed_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'empty', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the closed comment text if commenting is disabled in the genesis_do_comments function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_title_pings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '<h3>Trackbacks</h3>', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the trackbacks title text as well as heading tags in the genesis_do_pings function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_no_pings_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the no pings text if ping is enabled but there are no trackbacks so far in the genesis_do_pings function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_comment_list_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the arguments used in wp_list_comments in the genesis_default_list_comments function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_ping_list_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the comment author “says” text in the genesis_comment_callback function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'comment_author_says_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'says', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the arguments used in wp_list_comments in the genesis_default_list_pings function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_comment_form_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$args, $user_identity, $id, $commenter, $req, $aria_req', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the arguments used in comment_form in the genesis_do_comment_form function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Comments', 'gpha' ); ?></td>
	</tr>
	<!-- FOOTER FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_backtotop_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '[footer_backtotop]', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the back to top text in the genesis_do_footer function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_creds_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '[footer_copyright] [footer_childtheme_link] [footer_genesis_link] [footer_studiopress_link] [footer_wordpress_link] [footer_loginout]‘', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the credits text in the genesis_do_footer function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_output', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $backtotop_text, $creds_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to final output of genesis_do_footer including the back to top and credits text as well as div structure.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_scripts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'genesis_option(‘footer_scripts’)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the output of the footer scripts.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer', 'gpha' ); ?></td>
	</tr>
	<!-- HEADER FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_seo_title', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$title, $inside, $wrap', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the output of the genesis_seo_site_title function which depending on the SEO option set by the user will either wrap the title in <h1> or <p> tags.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Header', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_seo_description', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$description, $inside, $wrap', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the output of the genesis_seo_site_description function which depending on the SEO option set by the user will either wrap the description in <h1> or <p> tags.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Header', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pre_load_favicon', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'false', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Header', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_header_scripts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'genesis_get_option(‘header_scripts’)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the output of the header scripts.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Header', 'gpha' ); ?></td>
	</tr>
	<!-- LOOP FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_custom_loop_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'wp_parse_args($args, $defaults), $args, $defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the arguments for use in WP_Query in the genesis_custom_loop function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_title_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'get_the_title()', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the title in the genesis_do_post_title function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_title_output', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$title', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the output of the title and wrapping heading tags in the genesis_do_post_title function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_noposts_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '__(‘Sorry, no posts matched your criteria.’, ‘genesis’)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the no post text which is returned when a query is made with no results in the genesis_do_noposts function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_info', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$post_info', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the post info outputted by the genesis_post_info function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_meta', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$post_meta', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the post meta outputted by the genesis_post_meta function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_author_box_gravatar_size', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '70', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the author box gravatar image size in the genesis_author_box function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_author_box_title', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'sprintf( ‘<strong>%s %s</strong>’, __(‘About’, ‘genesis’), get_the_author() )', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the author box title in the genesis_author_box function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Loop', 'gpha' ); ?></td>
	</tr>
	<!-- SEARCH FORM FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'the_search_query', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'get_search_query()', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the search query in the genesis_search_form function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Search', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_search_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'esc_attr__(‘Search this website…’, ‘genesis’)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the search field text in the genesis_search_form function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Search', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_search_button_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'esc_attr__( ‘Search’, ‘genesis’ )’, ‘genesis’)', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the search form button text in the genesis_search_form function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Search', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_search_form', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$form, $search_text, $button_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the final output search form in the genesis_search_form function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Search', 'gpha' ); ?></td>
	</tr>
	<!-- MISC FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_breadcrumb_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the breadcrumb arguments in the genesis_breadcrumb function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Misc', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_breadcrumb_homelink', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$homelink', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the breadcrumb home link in the genesis_breadcrumb function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Misc', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_breadcrumb_bloglink', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$bloglink', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the breadcrumb blog link in the genesis_breadcrumb function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Misc', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_gravatar_sizes', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$sizes', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the sizes Small, Medium, Large, Extra Large in the Genesis User Profile Widget.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Misc', 'gpha' ); ?></td>
	</tr>
	<!-- IMAGE FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_get_image_default_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default arguments added to genesis_get_image function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Image', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pre_get_image', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'false, $args, $post', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Allows child theme to short-circuit the genesis_get_image function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Image', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_get_image', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $args, $id, $html, $url, $src', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Image', 'gpha' ); ?></td>
	</tr>
	<!-- MENU FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_nav_default_args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default arguments added to genesis_nav function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Menu', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pre_nav', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'false, $args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Allows child theme to short-circuit the genesis_nav function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Menu', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_nav_home_text', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '__(‘Home’, ‘genesis’), $args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the Home text in the genesis_nav function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Menu', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_nav_items', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$menu, $args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the nav items in the genesis_nav function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Menu', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_nav', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$menu, $args', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the final nav output in the genesis_nav function', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Menu', 'gpha' ); ?></td>
	</tr>
	<!-- OPTION FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_pre_get_option_', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$key, false, $setting', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Allows child theme to short-circuit the genesis_get_option function', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Option', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_options', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$settings_cache[$setting], $setting', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Option', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_get_option function', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'get_option($setting), $setting', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Option', 'gpha' ); ?></td>
	</tr>
	<!-- FOOTER SHORTCODE FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_backtotop_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the back to top shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_copyright_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the copyright shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_childtheme_link_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the child theme link shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_genesis_link_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the Genesis Link shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_studiopress_link_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the StudioPress link shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_wordpress_link_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the WordPress link shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_footer_loginout_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the log in/out shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Footer Shortcode', 'gpha' ); ?></td>
	</tr>
	<!-- POST SHORTCODE FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_date_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the date shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_time_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the time shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_author_link_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the author link shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_author_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the author shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_comments_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the post comments shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_tags_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the post tags shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_categories_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the post categories shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_post_edit_shortcode', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$output, $atts', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default atts and output for the post edit shortcode.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Post Shortcode', 'gpha' ); ?></td>
	</tr>
	<!-- INIT FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_settings_field', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'genesis-settings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the defined Settings Field Constants (for DB storage for genesis settings).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Init', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_seo_settings_field', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'genesis-seo-settings', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to the defined Settings Field Constants (for DB storage for genesis SEO settings).', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Init', 'gpha' ); ?></td>
	</tr>

	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_formatting_allowedtags', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '<p>, <span>, <div>, <a href="" title="">Text</a>, <b>, </i>, <em>, <strong>, <blockquote>, <br />', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Applied to allowed formatting tags, used by wp_kses().', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Init', 'gpha' ); ?></td>
	</tr>
	<!-- SEO SETTINGS FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_seo_settings_defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default values for Genesis SEO Settings called in the genesis_seo_settings_defaults function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'SEO', 'gpha' ); ?></td>
	</tr>
	<!-- THEME SETTINGS FILTERS -->
	<tr valign="top">
		<td><?php esc_attr_e( 'genesis_theme_settings_defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( '$defaults', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'applied to the default values for Genesis theme settings called in the genesis_theme_settings_defaults function.', 'gpha' ); ?></td>
		<td><?php esc_attr_e( 'Theme', 'gpha' ); ?></td>
	</tr>
</table>
