=== Genesis Power Hook Advisor ===
Contributors: dxladner
Tags: genesis, hooks, genesiswp, studiopress, filters, markup, guide
Requires at least: 4.3
Tested up to: 4.5.1
Stable tag: trunk
License: GPLv2 or later

Find Genesis hooks (action and filter hooks) quick and easily by seeing their actual locations inside your theme.

== Description ==



**Genesis Theme Framework required.**

== Installation ==
This section describes how to install the plugin and get it working.

1. Download and install the plugin from WordPress dashboard. You can also upload the entire “ggenesis-power-hook-advisor” folder to the `/wp-content/plugins/` directory

2. Or from the Plugins page of your website, search for Genesis Power Hook Advisor and install from this page.

3. Activate the plugin through the ‘Plugins’ menu in WordPress

4. You should see a Link in the Admin Menu titled GPHA Settings. Click here to go to the Genesis Power Hook Advisor Settings Page.

5. Here you would place all of your custom settings for the plugin.

6. 

7. For more Detailed Documentation, please visit our website <a target="_blank" href="https://hyperdrivedesigns">Genesis Power Hook Advisor Documentation</a>.


== Frequently Asked Questions ==



== Screenshots ==
1. settings page
2. hooks guide
3. filters guide


== Changelog ==
1.0
Initial Commit

== Upgrade Notice ==
1.0
Initial Commit
