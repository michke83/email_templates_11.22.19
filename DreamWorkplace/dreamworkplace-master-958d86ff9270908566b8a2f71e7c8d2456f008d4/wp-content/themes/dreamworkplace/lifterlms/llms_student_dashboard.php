<?php
class custom_llms_Student_Dashboard {
    
   public static function  custom_edit_account_details(){
    set_query_var( 'user', get_user_by( 'id', get_current_user_id()) );
      get_template_part( 'lifterlms/form', 'edit-account' );
   }
   
    public static function  custom_output_dashboard_content(){
        $limit = apply_filters( 'llms_dashboard_recent_courses_count', 3 );
        // ToDo Array
        set_query_var( 'current_user', get_user_by( 'id', get_current_user_id()) );
        set_query_var( 'student', new LLMS_Student() );
        set_query_var( 'courses', self::get_courses( $limit ) );
        get_template_part('lifterlms/dashboard'); 
   }
   
   public static function custom_output_courses_content() {
        	$limit = isset( $_GET['limit'] ) ? $_GET['limit'] : apply_filters( 'llms_dashboard_courses_per_page', 10 );
		$skip = isset( $_GET['skip'] ) ? $_GET['skip'] : 0;
		$courses = self::get_courses( $limit, $skip );
                set_query_var( 'student', new LLMS_Student() );
                set_query_var( 'courses', self::get_courses( $limit ) );
                set_query_var( 'pagination', $courses['more'] );
            get_template_part('lifterlms/dashboard'); 


	}
   
   
   
   public static function get_courses( $limit = 10, $skip = 0 ) {
         	// get sorting option
		$option = get_option( 'lifterlms_myaccount_courses_in_progress_sorting', 'date,DESC' );
		// parse to order & orderby
		$option = explode( ',', $option );
		$orderby = ! empty( $option[0] ) ? $option[0] : 'date';
		$order = ! empty( $option[1] ) ? $option[1] : 'DESC';
               $student = new LLMS_Student();
            return  $student->get_courses( array(
			'limit' => $limit,
			'order' => $order,
			'orderby' => $orderby,
			'skip' => $skip,
			'status' => 'enrolled',
                    
		) );
      }
      
      
      
     public static function get_tabs() {

		return apply_filters( 'llms_get_student_dashboard_tabs', array(
			'dashboard' => array(
				'content' => array( __CLASS__, 'output_dashboard_content' ),
				'endpoint' => false,
				'title' => __( 'Dashboard', 'lifterlms' ),
				'url' => llms_get_page_url( 'myaccount' ),
                                'active_type' => __( 'dashboard', 'lifterlms' ),
			),
			'view-courses' => array(
				'content' => array( __CLASS__, 'output_courses_content' ),
				'endpoint' => get_option( 'lifterlms_myaccount_courses_endpoint', 'my-courses' ),
				'title' => __( 'My Courses', 'lifterlms' ),
                                'active_type' => __( 'my-courses', 'lifterlms' ),
			),
			'edit-account' => array(
				'content' => array( __CLASS__, 'output_edit_account_content' ),
				'endpoint' => get_option( 'lifterlms_myaccount_edit_account_endpoint', 'edit-account' ),
				'title' => __( 'Edit-Account', 'lifterlms' ),
                                'active_type' => __( 'edit-account', 'lifterlms' ),
			),
			'redeem-voucher' => array(
				'content' => array( __CLASS__, 'output_redeem_voucher_content' ),
				'endpoint' => get_option( 'lifterlms_myaccount_redeem_vouchers_endpoint', 'redeem-voucher' ),
				'title' => __( 'Redeem a Voucher', 'lifterlms' ),
                                'active_type' => __( 'redeem-voucher', 'lifterlms' ),
			),
			'signout' => array(
				'endpoint' => false,
				'title' => __( 'Log Out', 'lifterlms' ),
				'url' => wp_logout_url( 'login' ),
                                'active_type' => __( 'Log Out', 'lifterlms' ),
			),
		) );

	}


}

