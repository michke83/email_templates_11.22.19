<?php
/**
 * Lesson Audio embed
 * @since    1.0.0
 * @version  3.1.1
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

global $post;

$lesson = new LLMS_Lesson( $post );
if ( ! $lesson->get( 'audio_embed' ) ) { return; }
?>
<div>
    <div class="row">
        <div class="col-sm-12">
            <div class="audio-video-container">
                 <div class="video_container videoWrapper videoWrapper169 js-videoWrapper"> 
                        <iframe width="100%" class="videoIframe js-videoIframe" height="500"  src="<?php echo $lesson->audio_embed; ?>?autoplay=0&showinfo=0&controls=0" frameborder="0" allowfullscreen></iframe> 

                         <button class="videoPoster js-videoPoster" style="background-image:url(/dreamworkplace/wp-content/themes/dreamworkplace/lifterlms/images/default-audio.png);">Play video</button>
                </div> 
              
            </div>  
        </div>
    </div>
</div>


