<?php
/**
 * Template for the Course Syllabus Displayed on individual course pages
 *
 * @author 		LifterLMS
 * @package 	LifterLMS/Templates
 * @since       1.0.0
 * @version     3.0.0 - refactored for sanity's sake
 */
if ( ! defined( 'ABSPATH' ) ) { exit; }

global $post;

$course = new LLMS_Course( $post );

// retrieve sections to use in the template
$sections = $course->get_sections( 'posts' );
?>
<div class="clear"></div>
<div class="student_lession_preview">
    <div class="baner">
        <?php
        $i = 1;
        if ( ! $sections ) : ?>

		<?php _e( 'This course does not have any sections.', 'lifterlms' ); ?>

	<?php else : ?>
        <?php foreach ( $sections as $s ) : $section = new LLMS_Section( $s->ID ); ?>
        <div class="baner_sec_1">
            <div class="row">
              <div class="col-sm-12">
                <div class="sec_title">
                <p>Section-<?php echo $i; ?></p>
                <?php if ( apply_filters( 'llms_display_outline_section_titles', true ) ) : ?>
                                <p><?php echo get_the_title( $s->ID ); ?></p>
                <?php endif; ?>
                </div>
                  <?php if ( $lessons = $section->get_children_lessons() ) : ?>

                                <?php foreach ( $lessons as $l ) : ?>
                           <?php     

                                     set_query_var( 'lesson', new LLMS_Lesson( $l->ID ) );
                                     set_query_var( 'total_lessons',  count( $lessons ) );
                                     get_template_part('lifterlms/lesson' ,'preview'); 
                            ?>
                                <?php endforeach; ?>

                        <?php else : ?>
                           <div class="sec_title">
                                <?php _e( 'This section does not have any lessons.', 'lifterlms' ); ?>
                           </div>
                       <?php endif; ?>
                 </div>
           </div>             
         </div>      
         	<?php $i++; 
                 endforeach; ?>

	<?php endif; ?>

      </div>
   </div>    
<div class="clear"></div>
   <?php //exit; ?>         
      
     