<?php
/**
 * My Account Navigation Links
 * @since  2.?.?
 * @version 3.0.4
 */
if ( ! defined( 'ABSPATH' ) ) { exit; }

$sep = apply_filters( 'lifterlms_my_account_navigation_link_separator', '&bull;' );
$current_user = wp_get_current_user(); 
$current_url  = $_SERVER['REQUEST_URI'];
$explode_current_url = explode('/',$current_url);
$base_slug = $explode_current_url[count($explode_current_url)-2];
$sub_slug = $explode_current_url[count($explode_current_url)-3];
if($sub_slug != 'my-courses')
    $fina_slug = 'dashboard';
else 
 $fina_slug = $base_slug;
?>
<div class="content_title_dashboard">  
        <div class="container">
          <?php  printf( __( '<p>Hello <strong>%1$s</strong></p>', 'lifterlms' ), $current_user->display_name ); ?>
            <div class="course-tab_view">
                <ul>
                   <?php 
                        $total_tabs =  count(custom_llms_Student_Dashboard::get_tabs());
                        foreach ( custom_llms_Student_Dashboard::get_tabs() as $var => $data ) : 
                           if ($data['active_type']  == $fina_slug )
                               $class = 'active';
                           else 
                               $class = '';
                            ?>
                             <li class="<?php echo $class;?>">
                                 <a  href="<?php echo isset( $data['url'] ) ? $data['url'] : llms_get_endpoint_url( $var, '', llms_get_page_url( 'myaccount' ) ); ?>">
                                     <?php echo $data['title']; ?>
                                 </a>
                             </li>    
                             <?php 
                             endforeach; ?>
                     </ul>
                  <?php do_action( 'lifterlms_after_my_account_navigation' ); ?>
            </div>
       </div>
 </div>