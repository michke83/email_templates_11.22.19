<?php
/**
 * My Account page
 *
 * @author 		codeBOX
 * @package 	lifterlMS/Templates
 */

llms_print_notices();
?>
<div class="llms-sd-tab dashboard">
	<?php do_action( 'lifterlms_before_student_dashboard_tab' ); ?>
	<?php echo apply_filters( 'lifterlms_account_greeting', '' ); ?>
	<?php do_action( 'lifterlms_after_student_dashboard_greeting' ); ?>
	<?php get_template_part('lifterlms/my', 'courses'); ?>
	<?php do_action( 'lifterlms_after_student_dashboard_tab' ); ?>
</div>
