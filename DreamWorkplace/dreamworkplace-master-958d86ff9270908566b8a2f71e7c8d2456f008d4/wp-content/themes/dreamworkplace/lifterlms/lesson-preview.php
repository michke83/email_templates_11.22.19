<?php
/**
 * Template for a lesson preview element
 *
 * @author 		LifterLMS
 * @package 	LifterLMS/Templates
 * @since       1.0.0
 * @version     3.2.5
 */
if ( ! defined( 'ABSPATH' ) ) { exit; }
$restrictions = llms_page_restricted( $lesson->get( 'id' ), get_current_user_id() );
$data_msg = $restrictions['is_restricted'] ? ' data-tooltip-msg="' . esc_html( strip_tags( llms_get_restriction_message( $restrictions ) ) ) . '"' : '';
?>
<div class="sec_data">
	<a class="llms-lesson-link<?php echo $restrictions['is_restricted'] ? ' llms-lesson-link-locked' : ''; ?>" href="<?php echo ( ! $restrictions['is_restricted'] ) ? get_permalink( $lesson->get( 'id' ) ) : '#llms-lesson-locked'; ?>"<?php echo $data_msg; ?>>
                <div class="sec_img">
                    <i aria-hidden="true" class="fa fa-play-circle-o"></i>
                </div>	
                <div class="sec_content">
                    <?php echo get_the_title( $lesson->get( 'id' ) ) ?>
                </div>
                <?php if ( $restrictions['is_restricted'] ) : ?>
                        <?php endif; ?>
	</a>
</div>
