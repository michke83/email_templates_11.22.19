<?php if ( apply_filters( 'lifterlms_show_page_title', true ) ) : ?>
<div class="content_title">  
  <div class="container">
    <?php lifterlms_page_title(); ?>
  </div>
</div>
<?php endif; ?>

