<?php
/**
 * Display a course progress bar and
 * a button for the next incomplete lesson in the course
 *
 * @author 		LifterLMS
 * @package 	LifterLMS/Templates
 */

if ( ! defined( 'ABSPATH' ) ) { exit; }

global $post;

if ( ! llms_is_user_enrolled( get_current_user_id(), $post->ID ) ) {
	return;
}

$student = new LLMS_Student();

$progress  = $student->get_progress( $post->ID, 'course' );
$author_id = $post->post_author;
?>
 <div class="view_student_course">
                <div class="row">
                        <div class="col-sm-3">
                              <div class="course-img">
                                  <?php echo lifterlms_get_featured_image( $post->ID ); ?>
                               </div>
                        </div>
                        <div class="col-sm-9">
                                <div class="course-content">
                                            <p><?php the_title();?></p>
                                            <p class="author">Author:
                                                <span><?php the_author_meta( 'user_nicename' , $author_id ); ?></span>
                                            </p>
                                            <div class="course-date">
                                                 <?php echo apply_filters('lifterlms_my_courses_start_date_html',
                                                                                         '<p>' . __( 'Started','lifterlms' ) . ' - ' . $student->get_enrollment_date( $post->ID, 'enrolled' ) . '</p>'
                                                                         ); ?>
                                              <div class="progress__indicator"><?php echo $progress; ?>% Completed</div>  

                                           </div>
                                    <?php if ( apply_filters( 'lifterlms_display_course_progress_bar', true ) ) : ?>
                                        <div class="course-prograss-bar">
                                                <div class="llms-progress">
                                                    <div class="llms-progress-bar">
                                                        <div class="progress-bar-complete" style="width:<?php echo $progress ?>%"></div>
                                                    </div>
                                                </div>
                                       </div>
                                   <?php endif; ?>
                                            <?php if ( 100 == $progress ) : ?>
                                            <p class="course_complete"><?php _e( 'Course Complete', 'lifterlms' ); ?></p>
                                            <?php endif; ?>
                                    <div class="view_course_signout">
                                        <a href="<?php echo wp_logout_url('login');?>">Log out</a>
                                    </div>
                              </div>
                      </div>
                </div>
  </div>



