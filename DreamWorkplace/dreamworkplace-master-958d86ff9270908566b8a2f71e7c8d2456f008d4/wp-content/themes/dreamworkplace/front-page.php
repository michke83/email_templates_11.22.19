<?php
/**
 * This file adds the Front Page Template to any Genesis Child Theme.
 */

 //* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//*  home page slider : see home-page-slider.php file to edit *//
if (is_home() || is_front_page())
{
	//*  home page slider : see home-page-slider.php file to edit *//
	add_action( 'genesis_after_header',  'home_page_slider' );

	//*  logo slider : see home-page-logo-slider.php file to edit *//
	add_action( 'genesis_after_header',  'home_page_logo_slider' );

	//*  youtube video section : see home-page-youtube.php file to edit *//
	 add_action( 'genesis_after_header', 'homepageyoutube' );

	//*  testimonials : see home-page-testimonials.php file to edit *//
	add_action( 'genesis_after_header',  'home_page_testimonials' );

	//*  testimonials : see home-page-testimonials.php file to edit *//
	add_action( 'genesis_after_header',  'home_page_services' );

	remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
}


genesis();
