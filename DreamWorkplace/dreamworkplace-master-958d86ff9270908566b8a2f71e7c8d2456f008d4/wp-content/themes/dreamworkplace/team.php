<?php
/*
*
Template Name: Team Template
*
*/
get_header();
$defaulr_img = THEME_DIRECTORY."/images/team_default.jpg";
?>
<div class="content_title">  
    <div class="container">
        <h1><?php the_title();?></h1>
    </div>
</div>

<div class="main-content" id="main">
    <div class="container" id="main-container">
        <div class="post-25 page type-page status-publish hentry" id="post-25">
            <div class="row">			
                <div id="primary">
                    <div class="team-page-wrapper">
                        <?php
                    if (have_rows('team_members')):
                            $i = 1;
                            $team_count = count(get_field('team_members'));
                        while (have_rows('team_members')): the_row();
                                $designation = get_sub_field('designation');
                                $name        = get_sub_field('title');
                                $content     = get_sub_field('description');
                                $email       = get_sub_field('email');
                                $profile_image_data = get_sub_field('profile_image');
                                $profile_image = wp_get_attachment_image_src($profile_image_data['id'], 'full')[0];
                                ?>
                            <div class="entry-content team-content">
                                <div class="<?php echo ($i % 2 == 0) ? "team-right" : "team-left"; ?>">
                                    <div class="col-cs-4">
                                        <div class="team-member-wrapper">
                                            <div class="image-container team-member-image">
                                                <div class="img-inner">
                                                    <?php if ($profile_image) { ?>
                                                        <img src="<?php echo $profile_image; ?>" class="img-responsive circle-img" alt="<?php echo $name; ?>" title="<?php echo $name; ?>"/>
                                                    <?php }  else { ?>
                                                        <img src="<?php echo $defaulr_img; ?>" class="img-responsive circle-img" alt="<?php echo $name; ?>" title="<?php echo $name; ?>"/>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div>
                                                <a class="team-member-name"><?php echo $name ?></a>
                                                <span  class="team-member-position"><?php echo $designation; ?></span>
                                                <?php if($email): ?>
                                                       <a href="mailto:<?php echo $email;?>"><?php echo $email;?></a>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($content): ?>
                                      <div class="col-cs-8">
                                            <div class="team-member-details">
                                                <div class="team-member-content">
                                                  <?php echo $content; ?>
                                                </div>
                                           </div>
                                      </div>
                                   <?php endif; ?> 
                                </div>
                                <div class="clearfix"></div>
                            </div>
                           <hr class="separator">
                        <?php
                         $i++;
                        endwhile;
                    endif;
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
get_footer(); ?>
