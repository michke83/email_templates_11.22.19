<?php
/**
 * Author: Sridhar Katakam
 * Link: https://sridharkatakam.com/
 */
remove_action( 'genesis_loop', 'genesis_do_loop' );

add_action( 'genesis_before_loop', 'genesis_do_search_title' );
/**
 * Echo the title with the search term.
 *
 * @since 1.9.0
 */
function genesis_do_search_title() {

	$title = sprintf( '<div class="archive-description"><h1 class="archive-title">%s %s</h1></div>', apply_filters( 'genesis_search_title_text', __( 'Search Results for:', 'genesis' ) ), get_search_query() );

	echo apply_filters( 'genesis_search_title_output', $title ) . "\n";

}

// customize post title
add_action( 'genesis_post_title', 'dwp_blog_post_title' );

function dwp_blog_post_title() {
	global $post;

	echo '<br/>';
	echo  '<h3>';
	echo get_the_title();
	echo '</h3>';
}

add_filter( 'excerpt_length', 'sp_excerpt_length' );
function sp_excerpt_length( $length ) {
	return 50; // pull first 50 words
}
// Add Read More Link to Excerpts
add_filter('excerpt_more', 'get_read_more_link');
add_filter( 'the_content_more_link', 'get_read_more_link' );
function get_read_more_link() {
   return ' [...]&nbsp;<br/><br/><a class="btn btn-danger" href="' . get_permalink() . '">Read&nbsp;More</a>';
}

add_action( 'genesis_loop', 'genesis_standard_loop', 5 );

genesis();
