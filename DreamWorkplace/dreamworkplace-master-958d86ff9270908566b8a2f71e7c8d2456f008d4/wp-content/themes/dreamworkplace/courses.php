<?php
/*
*
Template Name: Courses Template
*
*/
get_header();
?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

					<?php if ( have_posts() ) : ?>

        			<?php /* Start the Loop */ ?>

        			<?php while ( have_posts() ) : the_post(); ?>
                         <h1><?php echo the_title(); ?></h1>
                         <?php
                        /** get page content **/
                        the_content();

                        /** query course custom post type **/
                        $posts = get_posts(array(
                            'numberposts' => 20,
                            'post_type' => 'courses'
                        ));

                        if($posts)
                        {
                            echo '<ul id="course-list">';
                            foreach($posts as $post)
                            {
                                echo '<li><a class="course-title" href="' . get_permalink($post->ID) . '"><h3>' . get_the_title($post->ID) . '</h3></a>';
                                $mypost = get_post($post->ID);
                                echo '<p>' ;
                                echo apply_filters('the_content',$mypost->post_content);
                                echo '</p>';
                                echo'</li>';
                            }
                            echo '</ul><br><br>';
                        }
                    ?>

        			<?php endwhile; ?>

						<?php endif; ?>

    </main><!-- .site-main -->
  </div><!-- .content-area -->

<?php
get_footer();?>
