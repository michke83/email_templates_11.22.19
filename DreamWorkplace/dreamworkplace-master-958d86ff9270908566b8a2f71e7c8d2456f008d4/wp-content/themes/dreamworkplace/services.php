<?php
/*
*
Template Name: Services Template
*
*/

get_header();
global $post;
?>

<div class="content_title">  
             <div class="container"> <?php the_title();?></div>
</div>

<div class="content-sidebar-wrap container">  
	<main class="content" id="main-content-container">
            <article class="post-431 page type-page status-publish entry" itemscope="" itemtype="http://schema.org/CreativeWork">
                <header class="entry-header"></header>
                <div class="entry-content" itemprop="text">
                    <?php
                    echo apply_filters('the_content',$post->post_content);
                    
                    
                    /** query course custom post type **/
                        $posts = get_posts(array(
                            'posts_per_page' => -1 ,
                            'post_type' => 'services'
                        ));

                        if($posts)
                        {
                            echo '<ul id="course-list" class="tools_container">';
                            foreach($posts as $post)
                            {
                                echo '<li><h1 class="tools_heading">' . get_the_title($post->ID) . '</h1>';
                                $mypost = get_post($post->ID);
                                echo apply_filters('the_content',$mypost->post_content);
                                echo'</li>';
                            }
                            echo '</ul>';
                        }
                    ?>
                    
                    

                </div>
            </article>
        </main>
            
</div>
<?php get_footer(); ?>
