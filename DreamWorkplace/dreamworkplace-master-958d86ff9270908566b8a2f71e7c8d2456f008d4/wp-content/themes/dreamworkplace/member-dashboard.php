<?php
/*
*
Template Name: Member Dashboard Template
*
*/
get_header();



get_template_part('lifterlms/navigation'); 
?>

        <div id="primary" class="content-area">
            <main id="main" class="site-main" role="main">
                <div class="entry-content">  
                          <?php if ( have_posts() ) : ?>
                                          <?php /* Start the Loop */ ?>
                                          <?php while ( have_posts() ) : the_post(); ?>
                                   <?php
                                  /** get page content **/
                                 the_content();
                                  ?>
                         <?php endwhile; ?>
                  <?php endif; ?>
                </div>
            </main><!-- .site-main -->
        </div><!-- .content-area -->
<?php
get_footer(); ?>
