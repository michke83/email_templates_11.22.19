<?php
/*
*
Template Name: Testimonials Template
*
*/
get_header();
?>
<div class="content_title">  
  <div class="container">
      <h1> <?php the_title();?></h1>
  </div>
</div>

<div class="content-sidebar-wrap container">  
	<main class="content" id="main-content-container">
            <article class="post-431 page type-page status-publish entry" itemscope="" itemtype="http://schema.org/CreativeWork">
                <header class="entry-header"></header>
                <div class="entry-content" itemprop="text">
                    <?php
                        global $post;
                        $args = array( 'post_type' => 'testimonials','posts_per_page' => -1 );
                        $loop = new WP_Query( $args );
                           $i = 0;
                            while ( $loop->have_posts() ) : $loop->the_post();

                                    $test 	        = get_field( 'testimonial' );
                                    $client 	        = get_field( 'client_name' );
                                    $client_position 	= get_field('client_position' );
                                    $client_company 	= get_field( 'client_company' );
                                    $i++;
                                    ?>
                                    <div class="test">
                                            <div class="testimonial">
                                                    <h3><?php echo $test; ?></h3>
                                                    <div class="test">
                                                            <h4><?php echo $client; ?></h4>
                                                            <h4><?php echo $client_position; ?></h4>
                                                            <h4><?php echo $client_company; ?></h4>
                                                    </div>
                                            </div>
                                    </div> 
                          <hr class="separator">
                    <?php endwhile; // end of the loop. ?>
                </div>
            </article>
        </main>
</div>

<?php get_footer(); ?>
