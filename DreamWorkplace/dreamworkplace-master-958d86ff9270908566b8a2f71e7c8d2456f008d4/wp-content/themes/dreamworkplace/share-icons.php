<script type="text/javascript">
                    function share_window(width, height, network) {
                        var leftPosition, topPosition;
                        //Allow for borders.
                        leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
                        //Allow for title and status bars.
                        topPosition = (window.screen.height / 2) - ((height / 2) + 50);
                        var windowFeatures = "status=no,height=" + height + ",width=" + width + ",resizable=yes,left=" + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY=" + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no";
                        var title = "<?php echo addslashes(get_the_title());?>";
                        var link  = encodeURIComponent('<?php the_permalink();?>');

                        switch(network) {
                            case "facebook":
                                share_url = "http://www.facebook.com/sharer.php?u="+link+"&t="+title;
                                window.open(share_url,'newwindow', windowFeatures);
                                return false;
                            break;
                            case 'twitter':
                                var url = "http://twitter.com/home/?status="+title+" - "+link;
                                window.open(url,"newwindow",windowFeatures);
                                return false;
                            break;
                              case 'pinterest':
                                var url = 'https://in.pinterest.com/pin/create/button/?url='+link;
                                window.open(url,"newwindow", windowFeatures);
                                return false;
                            break;
                             case 'google':
                                var url = 'http://plus.google.com/share?url='+link;
                                window.open(url,"newwindow", windowFeatures);
                                return false;
                            break;
                              
                            default:
                                console.error('Error: network was not defined.');
                            break; 
                      }
                  }
                  
                
                </script>
                 <script type="text/javascript" src="//assets.pinterest.com/js/pinit.js"></script>
   
               <div class="social_share_link">Share:
                          <!-- Share on Facebook -->
                    <a href="#" onClick="share_window(500, 300, 'facebook');" class="hint-bottom facebook-icon" data-hint="Share on Facebook">
                        <em class="fa fa-facebook-square"></em>
                    </a>
                  <!-- Share on Twitter -->
                    <a  href="#" onClick="return share_window(500, 300, 'twitter')" target="_blank" class="hint-bottom twitter-icon" data-hint="Share on Twitter">
                        <em class="fa fa-twitter-square"></em>
                    </a>
                  <!-- Share on Pinterest -->
                   <a  href="#" onClick="return share_window(500, 300, 'pinterest')" class="hint-bottom pinterest-icon" data-hint="Share on Pinterest">
                        <em class="fa fa-pinterest-square"></em>
                    </a>
                   
                  <!-- Share on Google Plus -->
		<a  href="#" class="hint-bottom googleplus-icon" onClick="return share_window(500, 475, 'google')" data-hint="Share on Google+">
		<em class="fa fa-google-plus-square"></em>
		</a>




               </div>
