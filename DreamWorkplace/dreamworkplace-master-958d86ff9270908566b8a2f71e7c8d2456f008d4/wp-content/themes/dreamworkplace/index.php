<?php
/**
 * This file adds Custom Blog Post Template to any Genesis child theme.
 *
 */

/**
 * Blog Intro
 *
 */

// Customize Post Date
add_filter( 'genesis_post_info', 'sp_post_info_filter' );
function sp_post_info_filter($post_info) {
	$post_info = '[post_date]';
	return $post_info;
}

// Get rid of Post Categories in the entry footer
add_filter( 'genesis_post_meta', 'sp_post_meta_filter' );
function sp_post_meta_filter($post_meta) {
if ( !is_page() ) {
	$post_meta = '';
	return $post_meta;
}}

// Add Page Custom Heading

add_action( 'genesis_entry_header', 'genesis_do_post_title' );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );


 
 add_action ('genesis_entry_header','custom_genesis_post_info','10');
 function custom_genesis_post_info() {
       global $post;
       if ( ! post_type_supports( get_post_type(), 'genesis-entry-meta-before-content' ) ) {
		return;
	}
     $post_type = get_post_type( $post->ID );
        if($post_type == 'post') { 
            $filtered = apply_filters( 'genesis_post_info', '[post_date] ' . __( 'by', 'genesis' ) . ' [post_author_posts_link] [post_comments] [post_edit]' );

            if ( false == trim( $filtered ) ) {
                    return;
            }

            genesis_markup( array(
                    'open'    => '<p %s>',
                    'close'   => '</p>',
                    'content' => $filtered,
                    'context' => 'entry-meta-before-content',
            ) );
        }
}


// customize post title
add_action( 'genesis_post_title', 'dwp_blog_post_title' );

function dwp_blog_post_title() {
	global $post;

	echo '<br/>';
	echo  '<h3>';
	echo get_the_title();
	echo '</h3>';
}

add_filter( 'excerpt_length', 'sp_excerpt_length' );
function sp_excerpt_length( $length ) {
	return 50; // pull first 50 words
}
// Add Read More Link to Excerpts
add_filter('excerpt_more', 'get_read_more_link');
add_filter( 'the_content_more_link', 'get_read_more_link' );
function get_read_more_link() {
   return ' [...]&nbsp;<br/><br/><a class="btn btn-danger" href="' . get_permalink() . '">Read&nbsp;More</a>';
}
genesis();
