<?php
/***
*
* Template Name: Contact
*
**/

remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );

add_action ('genesis_entry_content', 'contact_info');
function contact_info(){
  // list of custom theme Options
  $company_phone_number = get_theme_mod( 'footer_phone_number' );
  $company_fax_number   = get_theme_mod( 'company_fax_number' );
  $company_address      = get_theme_mod( 'footer_company_name' );
  $company_city         = get_theme_mod( 'company_city' );
  $company_state        = get_theme_mod( 'company_state' );
  $company_zip          = get_theme_mod( 'company_zip' );
  ?>

    <div class="contact-info">
        <div class="contactinfo_container">
            <div class="contact_call">
              <h3>Call Us</h3>
              <div class="contact_phone">
                <p class="hidden-xs"><?php echo $company_phone_number; ?></p>
                  <a href="tel:<?php echo $company_phone_number; ?>" class="visible-xs hidden-sm hidden-md"><?php echo $company_phone_number ?>  </a>
              </div>
            </div>

            <div class="contact_letter">
              <h3>Send Us a Letter</h3>
              <div class="contact_location">
                  <p><strong><?php echo $company_address; ?></strong></p>
                <p><?php echo $company_city; ?></p>
                <p><?php echo $company_state;?></p>
                <p><?php echo $company_zip;?></p>
              </div>
            </div>
        </div>
    </div>
<?php
}
genesis();
