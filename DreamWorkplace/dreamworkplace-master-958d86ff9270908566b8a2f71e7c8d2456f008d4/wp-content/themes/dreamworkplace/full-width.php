<?php
/*
*
Template Name: Full Width Template
*
*/

 //* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );
remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );


genesis();
