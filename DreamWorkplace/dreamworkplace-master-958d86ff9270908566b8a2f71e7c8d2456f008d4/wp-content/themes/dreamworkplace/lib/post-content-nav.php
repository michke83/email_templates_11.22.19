<?php

/**
 * Apply Bootstrap Styles to Page links when a post has multiple
 * pages via the <!--nextpage--> tag.
 *
 * See https://codex.wordpress.org/Styling_Page-Links
 *
 * @since 0.4.0
 */

// remove default post_content_nav
remove_action( 'genesis_entry_content', 'genesis_do_post_content_nav', 12 );

// add custom post_content_nav
add_action( 'genesis_entry_content', 'bsg_do_post_content_nav', 12 );

// filter page links for correct bootstrap format
add_filter( 'wp_link_pages_link', 'bsg_wp_link_pages_link' );

function bsg_wp_link_pages_link( $link ) {
    if ( $link && '<' !== $link[0] ) {
        // this link is NOT an anchor tag,
        // it is the current item
        // add anchor tag and class active
        return '<li class="active"><a href="#">' . $link . '</a></li>';
    } else {
        return '<li>' . $link . '</li>';
    }
}

function bsg_do_post_content_nav( $attr ) {
    wp_link_pages( array(
        'before' => '<div class="bsg-post-content-nav">'
                . '<p>' . __( 'Pages:', 'genesis' ) . '</p>'
                . genesis_markup( array(
                    'html5'   => '<div %s><ul>',
                    'xhtml'   => '<div %s><ul>',
                    'context' => 'entry-pagination',
                    'echo'    => false,
                ) ),
        'after'  => '</ul></div></div>',
    ) );
}
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );
remove_action( 'genesis_post_content', 'genesis_do_post_content' );

add_action( 'genesis_entry_content', 'dream_do_post_content' );
add_action( 'genesis_post_content', 'dream_do_post_content' );


function dream_do_post_content() {
global $post;

	if ( is_singular() ) {
             echo apply_filters('the_content', $post->post_content); 
             if( $post->post_type == 'post' ){
               get_template_part('share', 'icons'); 
            }
		if ( is_single() && 'open' === get_option( 'default_ping_status' ) && post_type_supports( get_post_type(), 'trackbacks' ) ) {
			echo '<!--';
			trackback_rdf();
			echo '-->' . "\n";
		}

		if ( is_page() && apply_filters( 'genesis_edit_post_link', true ) ) {
			edit_post_link( __( '(Edit)', 'genesis' ), '', '' );
		}
	}
	elseif ( 'excerpts' === genesis_get_option( 'content_archive' ) ) {
		the_excerpt();
	}
	else {
		if ( genesis_get_option( 'content_archive_limit' ) ) {
			the_content_limit( (int) genesis_get_option( 'content_archive_limit' ), genesis_a11y_more_link( __( '[Read more...]', 'genesis' ) ) );
		} else {
			the_content( genesis_a11y_more_link( __( '[Read more...]', 'genesis' ) ) );
		}
	}

}

