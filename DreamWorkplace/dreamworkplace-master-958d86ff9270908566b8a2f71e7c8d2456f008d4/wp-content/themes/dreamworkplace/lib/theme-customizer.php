<?php
/**
 * Custom Genesis Customizer.
 *
 * Custom Genesis Customizer to remove all of the Standard Genesis Theme Options from the WP Theme Customizer
 *
 * The class in which settings within Genesis core are added to the Customizer.
 */
class Genesis_Customizer_DWP extends Genesis_Customizer_Base {

    /**
     * Settings field.
     *
     * @since 2.1.0
     *
     * @var string Settings field.
     */
    public $settings_field = 'genesis-settings';

    /**
     * Register new Customizer elements.
     *
     * Actual registration of settings and controls are handled in private methods.
     *
     * @since 2.1.0
     *
     * @param WP_Customize_Manager $wp_customize WP_Customize_Manager instance.
     */
    public function register( $wp_customize ) {

         if ( ! current_theme_supports( 'genesis-custom-header' ) && ! current_theme_supports( 'custom-header' ) ) {
             $this->blog_title( $wp_customize );
         }

         $this->color_scheme( $wp_customize );
         $this->layout( $wp_customize );

          if ( current_theme_supports( 'genesis-breadcrumbs' ) ) {
             $this->breadcrumbs( $wp_customize );
          }

         //$this->comments( $wp_customize );
       // $this->archives( $wp_customize );

    }

    /**
     * Remove the site title setting and control.
     *
     * @since 2.1.0
     *
     * @param WP_Customize_Manager $wp_customize WP_Customize_Manager instance.
     */
    private function blog_title( $wp_customize ) {
        $wp_customize->remove_setting( $this->get_field_name( 'blog_title' ) );
        $wp_customize->remove_control( 'blog_title' );
    }

    /**
     * Remove the color scheme setting and control.
     *
     * @since 2.1.0
     *
     * @param WP_Customize_Manager $wp_customize WP_Customize_Manager instance.
     * @return Return early if the theme does not support genesis-style-selector.
     */
    private function color_scheme( $wp_customize ) {

        if ( ! current_theme_supports( 'genesis-style-selector' ) ) {
            return;
        }

        $wp_customize->remove_section( 'genesis_color_scheme' );

        $wp_customize->remove_setting( $this->get_field_name( 'style_selection' ) );

        $genesis_style_support = get_theme_support( 'genesis-style-selector' );

        $wp_customize->remove_control( 'genesis_color_scheme' );

    }

    /**
     * Remove the layout selector setting and control.
     *
     * @since 2.1.0
     *
     * @param WP_Customize_Manager $wp_customize WP_Customize_Manager instance.
     */
    private function layout( $wp_customize ) {

        $wp_customize->remove_section( 'genesis_layout' );

        $wp_customize->remove_setting( $this->get_field_name( 'site_layout' ) );

        $wp_customize->add_control( 'genesis_layout' );
    }

    /**
     * Remove the breadcrumbs settings and controls.
     *
     * @since 2.1.0
     *
     * @param WP_Customize_Manager $wp_customize WP_Customize_Manager instance.
     */
    private function breadcrumbs( $wp_customize ) {

        $wp_customize->remove_section( 'genesis_breadcrumbs' );

        $settings = array(
            'breadcrumb_home'       => __( 'Breadcrumbs on Homepage', 'genesis' ),
            'breadcrumb_front_page' => __( 'Breadcrumbs on Front Page', 'genesis' ),
            'breadcrumb_posts_page' => __( 'Breadcrumbs on Posts Page', 'genesis' ),
            'breadcrumb_single'     => __( 'Breadcrumbs on Single Posts', 'genesis' ),
            'breadcrumb_page'       => __( 'Breadcrumbs on Pages', 'genesis' ),
            'breadcrumb_archive'    => __( 'Breadcrumbs on Archives', 'genesis' ),
            'breadcrumb_404'        => __( 'Breadcrumbs on 404 Page', 'genesis' ),
            'breadcrumb_attachment' => __( 'Breadcrumbs on Attachment/Media', 'genesis' ),
        );

        $priority = 1;

        foreach ( $settings as $setting => $label ) {

            if ( 'breadcrumb_front_page' == $setting || 'breadcrumb_posts_page' == $setting ) {
                if ( 'page' !== get_option( 'show_on_front' ) ) {
                    continue;
                }
            }

            if ( 'breadcrumb_home' == $setting ) {
                if ( 'page' === get_option( 'show_on_front' ) ) {
                    continue;
                }
            }

            $wp_customize->remove_setting( $this->get_field_name( $setting ) );

            $wp_customize->remove_control( 'genesis_' . $setting );

            $priority++;
        }

    }

    /**
     * Remove the comments and trackbacks settings and controls.
     *
     * @since 2.1.0
     *
     * @param WP_Customize_Manager $wp_customize WP_Customize_Manager instance.
     * @return null Return early if the theme does not support genesis-style-selector.
     */
    

}

add_action( 'init', 'genesis_customizer_dwp_init' );
/**
 * Create a new instance of the Custom Genesis Customizer class.
 *
 * @since 2.1.0
 */
function genesis_customizer_dwp_init() {
    new Genesis_Customizer_DWP;
}

/**
 * Added New Theme Options to the WordPress Theme Customizer
 *
 */

function dreamworkplace_theme_customizer( $wp_customize ) {





/*========= Homepage Header Settings ============*/

		// homepage header settings section
        $wp_customize->add_section( 'homepage_header_settings', array(
            'priority'          => 10,
            'capability'        => 'edit_theme_options',
            'theme_supports'    => '',
            'title'             => __( 'Homepage Header Settings ', 'dreamworkplace' ),
            'description'       => __( 'Homepage Header Settings', 'dreamworkplace' ),
            'panel'             => '',
        ) );

        // homepage featured image upload control
        $wp_customize->add_setting( 'dreamworkplace_logo', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dreamworkplace_logo', array(
                'label'         => __( 'Home Page Logo Image', 'dreamworkplace' ),
                'description'   => __( 'Use this field to upload your logo image for use in the homepage header.', 'dreamworkplace' ),
                'section'       => 'homepage_header_settings',
                'settings'      => 'dreamworkplace_logo',
                )
            )
        );
        
         // homepage featured image upload control
        $wp_customize->add_setting( 'dreamworkplace_logo', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dreamworkplace_logo', array(
                'label'         => __( 'Home Page Logo Image', 'dreamworkplace' ),
                'description'   => __( 'Use this field to upload your logo image for use in the homepage header.', 'dreamworkplace' ),
                'section'       => 'homepage_header_settings',
                'settings'      => 'dreamworkplace_logo',
                )
            )
        );
        // Fav Icon upload control
         $wp_customize->add_setting( 'dreamworkplace_fav', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'dreamworkplace_fav', array(
                'label'         => __( 'Fav Icon Image', 'dreamworkplace' ),
                'description'   => __( 'Use this field to upload your Fav Icon.', 'dreamworkplace' ),
                'section'       => 'homepage_header_settings',
                'settings'      => 'dreamworkplace_fav',
                )
            )
        );

		// homepage featured image upload control
        $wp_customize->add_setting( 'homepage_header_full_image', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
        ) );
        $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'homepage_header_full_image', array(
                'label'         => __( 'Home Page Header Full Width Image', 'dreamworkplace' ),
                'description'   => __( 'Use this field to upload your image for use in the homepage header.', 'dreamworkplace' ),
                'section'       => 'homepage_header_settings',
                'settings'      => 'homepage_header_full_image',
                )
            )
        );

        // homepage header text header control
        $wp_customize->add_setting( 'homepage_header_text_header', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'homepage_header_text_header', array(
            'label'         => __( 'Homepage Header Text Header', 'dreamworkplace' ),
            'description'   => __( 'This is the heading text that is right below the logo image.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'homepage_header_settings',
            'setting'       => 'homepage_header_text_header',
            'type'          => 'text',
        ) );

        // homepage header text header control
        $wp_customize->add_setting( 'homepage_header_paragraph', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'homepage_header_paragraph', array(
            'label'         => __( 'Homepage Header Paragraph', 'dreamworkplace' ),
            'description'   => __( 'This is the paragraph of text below logo image.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'homepage_header_settings',
            'setting'       => 'homepage_header_paragraph',
            'type'          => 'textarea',
        ) );

         // homepage button URL control
        $wp_customize->add_setting( 'dwp_logo_url', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'esc_url',
        ) );
        $wp_customize->add_control( 'dwp_logo_url', array(
            'label'         => __( 'Dreamwork Place Logo URL', 'dreamworkplace' ),
            'description'   => __( 'Enter URL for Logo Image. This URL will be the page that the user gets directed to upon clicking the logo image.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'homepage_header_settings',
            'settings'      => 'dwp_logo_url',
            'type'          => 'url',
        ) );
      $wp_customize->add_control( 'dwp_logo_test', array(
            'label'         => __( 'Dreamwork Place Logo URL', 'dreamworkplace' ),
            'description'   => __( 'Enter URL for Logo Image. .', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'homepage_header_settings',
            'settings'      => 'dwp_logo_test',
            'type'          => 'url',
        ) );


/*========= Homepage YouTube Settings ============*/

    // homepage youtube url settings section
        $wp_customize->add_section( 'homepage_youtube_settings', array(
            'priority'          => 18,
            'capability'        => 'edit_theme_options',
            'theme_supports'    => '',
            'title'             => __( 'YouTube Settings ', 'dreamworkplace' ),
            'description'       => __( 'YouTube Settings', 'dreamworkplace' ),
            'panel'             => '',
        ) );

         // homepage YouTube URL control
        $wp_customize->add_setting( 'homepage_youtube_url', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'esc_url',
        ) );
        $wp_customize->add_control( 'homepage_youtube_url', array(
            'label'         => __( 'Homepage YouTube URL', 'dreamworkplace' ),
            'description'   => __( 'Enter the YouTube embeded URL you can get from your YouTube video.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'homepage_youtube_settings',
            'settings'      => 'homepage_youtube_url',
            'type'          => 'url',
        ) );





/*========= Footer Settings ============*/

        // Footer settings section
        $wp_customize->add_section( 'footer_settings', array(
            'priority'          => 30,
            'capability'        => 'edit_theme_options',
            'theme_supports'    => '',
            'title'             => __( 'Footer Settings ', 'dreamworkplace' ),
            'description'       => __( 'Footer Settings', 'dreamworkplace' ),
            'panel'             => '',
        ) );

        // footer name control
        $wp_customize->add_setting( 'footer_company_name', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'footer_company_name', array(
            'label'         => __( 'Footer Company Name', 'dreamworkplace' ),
            'description'   => __( 'Place the name of your company or any other text that will appear in the footer.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'setting'       => 'footer_company_name',
            'type'          => 'text',
        ) );

        // footer company phone number control
        $wp_customize->add_setting( 'footer_phone_number', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'footer_phone_number', array(
            'label'         => __( 'Footer Phone Number', 'dreamworkplace' ),
            'description'   => __( 'Place the phone number of your company.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'setting'       => 'footer_phone_number',
            'type'          => 'text',
        ) );
        
           // footer Request a call back link
        $wp_customize->add_setting( 'request_callback_url', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'esc_url',
        ) );
        $wp_customize->add_control( 'request_callback_url', array(
            'label'         => __( 'Request a call back link', 'dreamworkplace' ),
            'description'   => __( 'Enter the Linked for request call back', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'settings'      => 'request_callback_url',
            'type'          => 'url',
        ) );
        
        
             // Compnay city control
        $wp_customize->add_setting( 'company_city', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'company_city', array(
            'label'         => __( 'Company city', 'dreamworkplace' ),
            'description'   => __( 'Place the city of your company.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'setting'       => 'company_city',
            'type'          => 'text',
        ) );
          // Compnay state control
        $wp_customize->add_setting( 'company_state', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'company_state', array(
            'label'         => __( 'Company state', 'dreamworkplace' ),
            'description'   => __( 'Place the state of your company.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'setting'       => 'company_state',
            'type'          => 'text',
        ) );
        
          // Compnay state control
        $wp_customize->add_setting( 'company_zip', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'sanitize_text_field',
        ) );

        $wp_customize->add_control( 'company_zip', array(
            'label'         => __( 'Company zip', 'dreamworkplace' ),
            'description'   => __( 'Place the zip code of your company.', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'setting'       => 'company_zip',
            'type'          => 'text',
        ) );

        // Facebook URL control
        $wp_customize->add_setting( 'facebook_url', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'esc_url',
        ) );
        $wp_customize->add_control( 'facebook_url', array(
            'label'         => __( 'Facebook URL', 'dreamworkplace' ),
            'description'   => __( 'Enter the Facebook URL for your Facebook accoount', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'settings'      => 'facebook_url',
            'type'          => 'url',
        ) );

        // Twitter URL control
        $wp_customize->add_setting( 'twitter_url', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'esc_url',
        ) );
        $wp_customize->add_control( 'twitter_url', array(
            'label'         => __( 'Twitter URL', 'dreamworkplace' ),
            'description'   => __( 'Enter the Twitter URL for your Twitter accoount', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'settings'      => 'twitter_url',
            'type'          => 'url',
        ) );

        // LinkedIn URL control
        $wp_customize->add_setting( 'linkedin_url', array(
            'default'           => '',
            'type'              => 'theme_mod',
            'capability'        => 'edit_theme_options',
            'transport'         => '',
            'sanitize_callback' => 'esc_url',
        ) );
        $wp_customize->add_control( 'linkedin_url', array(
            'label'         => __( 'Linked In URL', 'dreamworkplace' ),
            'description'   => __( 'Enter the Linked In URL for your Linked In accoount', 'dreamworkplace' ),
            'priority'      => 10,
            'section'       => 'footer_settings',
            'settings'      => 'linkedin_url',
            'type'          => 'url',
        ) );
}


add_action( 'customize_register', 'dreamworkplace_theme_customizer' );
