<?php

function home_page_logo_slider() { ?>
	<a class="smoothScroll" name="video"></a>
	<div class="item content">
            <ul id="content-slider" class="content-slider">
                <?php if (have_rows('brand_images_list')): ?>
                            <?php while (have_rows('brand_images_list')): the_row(); ?>
                                <?php
                                $title    = get_sub_field('title');
                                $image_id = get_sub_field('brand_images');
                               // $customlink = get_sub_field('page_link');

                                $image_src = wp_get_attachment_image_src($image_id, 'full')[0];

                                $image_data = explode("/",$image_src);
                                $image_name = explode(".",$image_data [count($image_data)-1])[0];
                                $width = wp_get_attachment_image_src($image_id, 'full')[1];
                                $title_attr = !empty($title) ? $title : $image_name;
                                $alt_attr = $title_attr;
                                            ?>
                               <li>
                                  <img src="<?php echo $image_src; ?>" alt="<?php echo (!empty($title_attr)) ? $title_attr : $alt_attr; ?>" title="<?php echo (!empty($title_attr)) ? $title_attr : $alt_attr; ?>"/>                        
                               </li>
                      <?php endwhile; ?>
                 <?php endif;?> 
            </ul>
    </div>
	<?php
}             