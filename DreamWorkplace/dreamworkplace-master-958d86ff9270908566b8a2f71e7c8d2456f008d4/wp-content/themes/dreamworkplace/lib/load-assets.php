<?php

// replace style.css - Theme Information (no css)
// with css/style.min.css -  Compressed CSS for Theme
remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
add_action( 'wp_enqueue_scripts', 'bsg_enqueue_css_js' );

function bsg_enqueue_css_js() {
    $version = wp_get_theme()->Version;

    wp_dequeue_style( 'temp-styles' );

    // wp_enqueue_style( $handle, $src, $deps, $ver, $media );

    if ( is_front_page() ) {
      wp_enqueue_style( 'slick_css', get_stylesheet_directory_uri() . '/css/slick.css', array(), $version );
      wp_enqueue_style( 'animate_css', get_stylesheet_directory_uri() . '/css/animate.min.css', array(), $version );
    }

    wp_enqueue_style( 'bsg_combined_css', get_stylesheet_directory_uri() . '/css/style.css', array(), $version );

    wp_enqueue_style( 'temp-styles' );

    wp_enqueue_style( 'bsg_custom_css', get_stylesheet_directory_uri() . '/css/custom.css', array(), $version );
    wp_enqueue_style( 'dream_custom_css', get_stylesheet_directory_uri() . '/css/dream_custom.css', array(), $version );

    wp_enqueue_style( 'lightslider-css', get_stylesheet_directory_uri() . '/css/lightslider.css', array(), $version );
    wp_enqueue_style( 'font-awesome-css', get_stylesheet_directory_uri() . '/css/font-awesome.min.css', array(), $version );

    // NOTE: this combined script is loading in the footer
    wp_enqueue_script( 'jquery' );

    if ( is_front_page() ) {
      wp_enqueue_script( 'slick_js', get_stylesheet_directory_uri() . '/js/vendor/slick.min.js', array('jquery'), $version, true );
      wp_enqueue_script( 'slick_custom_js', get_stylesheet_directory_uri() . '/js/custom/slick-custom.js', array('jquery'), $version, true );
      wp_enqueue_script( 'animate_js', get_stylesheet_directory_uri() . '/js/custom/jquery.wow.min.js', array('jquery'), $version, true );
    }

   // wp_enqueue_script( 'bsg_combined_js', get_stylesheet_directory_uri() . '/js/javascript.min.js', array('jquery'), $version, true );
    wp_enqueue_script( 'dreamworkplace_services_js', get_stylesheet_directory_uri() . '/js/custom/services.js', array('jquery'), $version, true );
    wp_enqueue_script( 'lightslider-js', get_stylesheet_directory_uri() . '/js/custom/lightslider.js', array('jquery'), $version, true );
    wp_enqueue_script( 'dreamworkplace_custom_js', get_stylesheet_directory_uri() . '/js/custom/custom.js', array('jquery'), $version, true );
    wp_enqueue_script( 'captcha', get_stylesheet_directory_uri() . '/js/captch_resize.min.js', array('jquery'), $version, true );
}

  if ( is_front_page() ) { ?>
        <script>
        jQuery(document).ready(function ($) {
            wow = new WOW();
            /*animate*/
            wow.init();
        });
        </script>
  <?php } ?>