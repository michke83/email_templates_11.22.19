<?php

function homepageyoutube() { 
            if( get_theme_mod( 'homepage_youtube_url' ) ) {
            ?>
        <div class="home_video_background">
                <div class="container">
                        <div class="embed-responsive embed-responsive-16by9">
                                        <iframe width="980" height="700" src="<?php echo get_theme_mod( 'homepage_youtube_url' ); ?>?modestbranding=1&showinfo=0" frameborder="0" allowfullscreen></iframe>
                        </div>
                </div>
        </div>
	<?php
                    } 

 }
