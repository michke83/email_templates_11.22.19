<?php 
 remove_action( 'genesis_loop_else', 'genesis_do_noposts' );

 
add_action('genesis_before_content_sidebar_wrap', 'custom_post_title');
function custom_post_title(){
        if(!is_front_page()){
             global $post;
                    $post_type = get_post_type( $post->ID );
                    $request_url = $_SERVER['REQUEST_URI'];
                    $post_title =   get_the_title();
                        if($post_type == 'page') { 
                            $post_title =   get_the_title();
                             echo  "<div class='content_title'>  
                                    <div class='container'><h1> $post_title</h1></div>
                                 </div>";
                        } ?>

                   <?php 
                   if($post_type == 'course' ){  ?>
                            <div class="content_title">  
                                    <div class="container">
                                        <?php get_template_part('lifterlms/featured','image'); ?>
                                    </div>
                            </div>

                  <?php } ?>

                     <?php  
                        if($post_type == 'lesson' ){ 
                             $post_title =   get_the_title(); ?>
                         <div class='content_title'>  
                             <div class='container'>
                                 <h1> <?php echo $post_title; ?></h1>
                             </div>
                         </div>
                          <?php } ?>
                    
                   <?php   
                   if($post_type == 'post' || (strpos($request_url, 'blog') !== false)){  ?>  
                     <?php
                 $posts_page = get_option( 'page_for_posts' );
                 if ( is_null( $posts_page ) ) {
                         return;
                 }
                 if($post_type == 'post' && (strpos($request_url, 'blog') === false)){
                         echo  "<div class='content_title content_title_blog'>  
                                    <div class='container'><h1> $post_title</h1></div>
                                 </div>";
                     
                 }
                 else {
                 printf('<div id="blog_header">');
                         $title = 'The Dream Workplace Blog';
                         $content = 'Article &#x25cf; News &#x25cf; and More';
                         $title_output = $content_output = '';
                         if ( $title ) {
                                 $title_output = sprintf( '<div class="container"><div id="blog_header_text"><h1 class="text-center archive-title">%s</h1>', $title )
                                 ;
                         }
                         if ( $content ) {
                                 //$content_output = wpautop( $content );
                                 $content_output = sprintf( '<h3 class="text-center archive-meta">%s</h3>', $content );
                         }
                         if ( $title || $content ) {
                                 printf( '<div class="archive-description">%s</div>', $title_output . $content_output );
                         }
                     printf('</div></div></div>');
                      
                   } }
                
                if($post_type != 'post' && (strpos($request_url, 'blog') !== false)):
                        echo '<div class="main_no_result"><div class="blog_not_found">No results found.</div>';
                        echo '<div class="back_blog"><a href="'.home_url().'/blog">Back to Blog</a></div></div>';
                      endif;
                
         }
}

add_action( 'genesis_entry_header', 'bsg_single_featured_image', 5 );

function bsg_single_featured_image() {
    global $post;
    $post_type = get_post_type( $post->ID );
            if($post_type == 'post' && has_post_thumbnail()) { 
         $featured_image_attr = apply_filters( 'bsg-featured-image-attr', array(
             'class' => 'single-featured-image'
         ) );

         $size = apply_filters( 'bsg-featured-image', 'bsg-featured-image' ); ?>
         <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" >
               <?php the_post_thumbnail( $size, $featured_image_attr ); ?>
        </a>
      <?php 
   } 
}