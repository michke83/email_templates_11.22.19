<?php 

// add custom post type for Contact Form Submission
function dream_work_place_post_types()
{
	// Contact Post Type
	register_post_type('contact', array( 
		'label' 			=> 'Contact Submissions',
		'description' 		=> 'Clients',
		'public' 			=> false,
		'show_ui' 			=> true,
		'show_in_menu' 		=> true,
		'menu_position' 	=> 10,
		'capability_type' 	=> 'post',
		'hierarchical' 		=> true,
		'rewrite' 			=> array(
			'slug' => ''
		),
		'query_var' 	=> true,
		'supports' 		=> array( 
			'title', 
			'editor', 
			'page-attributes' 
		),
		'exclude_from_search' => 'true', 
		'labels' => array (
	  		'name' 					=> 'Contact Submissions',
	  		'singular_name' 		=> 'Contact Submission',
	  		'menu_name' 			=> 'Contact Submissions',
	  		'add_new' 				=> 'Add New',
	  		'add_new_item' 			=> 'Add New',
	  		'edit' 					=> 'Edit',
	  		'edit_item' 			=> 'Edit',
	  		'new_item' 				=> 'New',
	  		'view' 					=> 'View',
	  		'view_item' 			=> 'View',
	  		'search_items' 			=> 'Search',
	  		'not_found' 			=> 'No Info Found',
	  		'not_found_in_trash' 	=> 'None In Trash',
	  		'parent' 				=> '',
		  ),
		) 
	);

	// Contact Post Type
	register_post_type('services', array( 
		'label' 				=> 'Services',
		'description' 			=> 'Services',
		'public' 				=> false,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'menu_position' 		=> 10,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'rewrite' 		=> array(
			'slug' 		=> ''
		),
		'query_var' 			=> true,
		'supports' 		=> array( 
			'title', 
			'editor', 
			'page-attributes' 
		),
		'exclude_from_search' 	=> 'true', 
		'labels' 		=> array (
	  		'name' 					=> 'Services',
	  		'singular_name' 		=> 'Service',
	  		'menu_name' 			=> 'Services',
	  		'add_new' 				=> 'Add New',
	  		'add_new_item' 			=> 'Add New',
	  		'edit' 					=> 'Edit',
	  		'edit_item' 			=> 'Edit',
	  		'new_item' 				=> 'New',
	  		'view' 					=> 'View',
	  		'view_item' 			=> 'View',
	  		'search_items' 			=> 'Search',
	  		'not_found' 			=> 'No Info Found',
	  		'not_found_in_trash' 	=> 'None In Trash',
	  		'parent' 				=> '',
		  ),
		) 
	);
        
        
        // Contact Post Type
	register_post_type('testimonials', array( 
		'label' 				=> 'Testimonials',
		'description' 			=> 'Testimonials',
		'public' 				=> false,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'menu_position' 		=> 10,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'rewrite' 		=> array(
			'slug' 		=> ''
		),
		'query_var' 			=> true,
		'supports' 		=> array( 
			'title', 
			'editor', 
			'page-attributes' 
		),
		'exclude_from_search' 	=> 'true', 
		'labels' 		=> array (
	  		'name' 					=> 'Testimonials',
	  		'singular_name' 		=> 'Testimonial',
	  		'menu_name' 			=> 'Testimonials',
	  		'add_new' 				=> 'Add New',
	  		'add_new_item' 			=> 'Add New',
	  		'edit' 					=> 'Edit',
	  		'edit_item' 			=> 'Edit',
	  		'new_item' 				=> 'New',
	  		'view' 					=> 'View',
	  		'view_item' 			=> 'View',
	  		'search_items' 			=> 'Search',
	  		'not_found' 			=> 'No Info Found',
	  		'not_found_in_trash' 	=> 'None In Trash',
	  		'parent' 				=> '',
		  ),
		) 
	);
        
        // Contact Post Type
        add_theme_support('post-thumbnails');
        add_post_type_support( 'home_services', 'thumbnail' );    
	register_post_type('home_services', array( 
		'label' 				=> 'Home Services',
		'description' 			=> 'Home Services',
		'public' 				=> false,
		'show_ui' 				=> true,
		'show_in_menu' 			=> true,
		'menu_position' 		=> 10,
		'capability_type' 		=> 'post',
		'hierarchical' 			=> true,
		'rewrite' 		=> array(
			'slug' 		=> ''
		),
		'query_var' 			=> true,
		'supports' 		=> array( 
			'title', 
			'editor', 
			'page-attributes' 
		),
		'exclude_from_search' 	=> 'true', 
		'labels' 		=> array (
	  		'name' 					=> 'Home Services',
	  		'singular_name' 		=> 'Home Services',
	  		'menu_name' 			=> 'Home Services',
	  		'add_new' 				=> 'Add New',
	  		'add_new_item' 			=> 'Add New',
	  		'edit' 					=> 'Edit',
	  		'edit_item' 			=> 'Edit',
	  		'new_item' 				=> 'New',
	  		'view' 					=> 'View',
	  		'view_item' 			=> 'View',
	  		'search_items' 			=> 'Search',
	  		'not_found' 			=> 'No Info Found',
	  		'not_found_in_trash' 	=> 'None In Trash',
	  		'parent' 				=> '',
		  ),
		) 
	);

}
add_action( 'init', 'dream_work_place_post_types' );
