<?php

function home_page_slider() { ?>
        <div class="slick-slider">
         <?php
            $i = 1;
            $default_image = THEME_DIRECTORY. '/images/default_banner.jpg';

            if (have_rows('page_sliders')):
               while (have_rows('page_sliders')): the_row(); 
                    $image_id_desktop = get_sub_field('image');
                    $image_desktop_src = wp_get_attachment_image_src($image_id_desktop['id'], 'full');
                    $title         = get_sub_field('title');
                    $sub_title     = get_sub_field('sub_title');
                    $button_title  = get_sub_field('button_title');
                    $button_link   = get_sub_field('button_link');
                        if (isset($image_desktop_src[0]) && $image_desktop_src[0]) {
                           $desktop_img = $image_desktop_src[0];
                        } else {
                           $desktop_img = $default_image;
                        }
                    
                ?>
            <div>
                <div class="slider_bg_img hidden-xs" style="background-image:url(<?php echo $desktop_img; ?>)">
                            <div class="overlay-slider"></div>
                                <div>
                                        <div class="info">
                                            <h1 class="slider-heading"><?php echo $title; ?></h1>
                                            <p class="slider-subheading lead"><?php echo $sub_title; ?></p>
                                            <?php if($button_link) :?>
                                            <a class="btn btn-large btn-danger" href="<?php echo $button_link; ?>"><?php echo $button_title; ?></a>
                                            <?php endif; ?>
                                        </div>
                                </div>
                 </div>
                 <div class="slider_bg_img visible-xs-block">
                            <div class="overlay-slider"></div>
                                <div>
                                <img src="<?php echo $desktop_img; ?>"/>
                                        <div class="info">
                                            <h1 class="slider-heading"><?php echo $title; ?></h1>
                                            <p class="slider-subheading lead"><?php echo $sub_title; ?></p>
                                            <?php if($button_link) :?> 
                                            <a class="btn btn-large btn-danger" href="<?php echo $button_link; ?>"><?php echo $button_title; ?></a>
                                            <?php endif; ?>
                                        </div>
                                </div>
                 </div>
            </div>
               <?php     
               $i++;
                endwhile;
          endif; ?>
        </div>
        <p class="down-arrow hidden-xs">
                        <a class="btn btn-large btn-down-arrow smoothScroll" href="#video">
                           <i class="fa fa-chevron-down fa-lg" aria-hidden="true"></i>
                        </a>
         </p>  
 <?php }