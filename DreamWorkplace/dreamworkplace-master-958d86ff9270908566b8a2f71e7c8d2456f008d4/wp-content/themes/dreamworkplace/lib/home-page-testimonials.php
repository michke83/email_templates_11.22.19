<?php

function home_page_testimonials() {

	$testbgimage = get_theme_mod( 'testimonial_background_image' );

	?>
	<div id="test-section">
            <div id="inner-tests" class="wow fadeIn animated">
			<div id="test-title" class="test-info">
				<h2>What our clients say about us</h2>
			</div>
			<?php
				global $post;
			    $args = array( 'post_type' => 'testimonials', 'posts_per_page' => 2 );
			    $loop = new WP_Query( $args );
					$i = 0;
			  	while ( $loop->have_posts() ) : $loop->the_post();
					$string 		        = get_field( 'testimonial' );
                                	$client 			= get_field( 'client_name' );
					$client_position 	        = get_field(' client_position' );
					$client_company 	        = get_field( 'client_company' );
					if (strlen($string) > 500) {
                                          	    $stringCut = substr($string, 0, 400);
                                                    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).' […]'; 
						}
						

					$i++;
					?>
					<div class="test-info || skew<?php echo $i; ?>">
						<div class="testimonial">
							<p>
								<?php echo $string; ?>
							</p>

							<div class="test-name">
								<p><?php echo $client; ?></p>
								<p><?php echo $client_position; ?></p>
								<p><?php echo $client_company; ?></p>
							</div>
						</div>

						<a href="<?php echo site_url(); ?>/testimonials/" class="test-link">Read More >></a>

					</div>

			<?php endwhile; // end of the loop. ?>
		</div>
	</div>
	<?php
}
