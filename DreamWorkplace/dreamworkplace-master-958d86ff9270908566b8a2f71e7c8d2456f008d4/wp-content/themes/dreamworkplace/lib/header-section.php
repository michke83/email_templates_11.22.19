<?php

//* Remove the site title
remove_action( 'genesis_site_title', 'genesis_seo_site_title' );

// remove page title
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
