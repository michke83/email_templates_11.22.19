<?php

if ( class_exists('UberMenuStandard') ) {
    return;
}
// remove primary & secondary nav from default position
remove_action( 'genesis_after_header', 'genesis_do_nav' );
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
// add primary & secondary nav to top of the page
add_action( 'genesis_before', 'genesis_do_nav' );
//add_action( 'genesis_before', 'genesis_do_subnav' );

// filter menu args for bootstrap walker and other settings
add_filter( 'wp_nav_menu_args', 'bsg_nav_menu_args_filter' );

// add bootstrap markup around the nav
add_filter( 'wp_nav_menu', 'bsg_nav_menu_markup_filter', 10, 2 );

// add custom fav icon function 
add_filter( 'genesis_favicon_url', 'custom_fav_icon',1,1 );
 function custom_fav_icon($default_fav_icon){
      $default_fav_icon =   get_theme_mod( 'dreamworkplace_fav' );
     return $default_fav_icon;
     
 }

function bsg_nav_menu_args_filter( $args ) {

    if (
        'primary' === $args['theme_location'] ||
        'secondary' === $args['theme_location']
    ) {
        $args['depth'] = 2;
        $args['menu_class'] = 'nav navbar-nav';
        $args['fallback_cb'] = 'wp_bootstrap_navwalker::fallback';
        $args['walker'] = new wp_bootstrap_navwalker();
    }

    return $args;
}

function bsg_nav_menu_markup_filter( $html, $args ) {
   
  // only add additional Bootstrap markup to
  // primary and secondary nav locations
  if (
      'primary'   !== $args->theme_location &&
      'secondary' !== $args->theme_location
  ) {
      return $html;
  }

  $data_target = "nav-collapse" . sanitize_html_class( '-' . $args->theme_location );

  if ( 'primary' === $args->theme_location )
  {
    $output = <<<EOT
    <div class="container container_header">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#{$data_target}">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
EOT;

      $output .= apply_filters( 'bsg_navbar_brand', bsg_navbar_brand_markup() );

      $output .= '</div>'; // .navbar-header

      $output .= "<div class=\"collapse navbar-collapse\" id=\"{$data_target}\">";

        $output .= $html;

      $output .= '</div>'; // .collapse .navbar-collapse

  $output .= '</div>'; // .container-fluid
  }

  elseif ( 'secondary' === $args->theme_location )
  { 
    $dwp_footer_date = date( 'Y' );
    $dwp_footer_name = get_theme_mod( 'footer_company_name' );
    $dwp_footer_phone_number = get_theme_mod( 'footer_phone_number' );
    $output = <<<EOT
      <div class="container">
EOT;

        $output .= '<div class="secondary-menu-wrapper">';
        $output .= $html;
        
      $output .= '</div>'; // .collapse .navbar-collapse
      
	$output .='<p class="copyright-content text-center">';
		 $output .= $dwp_footer_date;
		 $output .= '. ';
		 $output .= $dwp_footer_name;
		 $output .= ' ';
	
	         $output .= 'All Rights Reserved.';
		 $output .= ' ';
		 $output .= '<span class="footer_phone_mb">'.$dwp_footer_phone_number.'</span>';
	$output .='</p>';
    
    $output .= '</div>'; // .container
   }

   return $output;
}

function bsg_navbar_brand_markup() {
    $logo = get_theme_mod( 'dreamworkplace_logo' );
    $output = '<a class="navbar-brand" id="logo" title="' . esc_attr( get_bloginfo( 'description' ) ) . '" href="' . esc_url( home_url( '/' ) ) . '">';
    if ( empty( $logo ) )
    {
        $output .= get_bloginfo( 'name' );
    }
    else
    {
        $output .= '<img src="' . get_theme_mod( 'dreamworkplace_logo' ) . '" />';
    }
        $output .= '</a>';

    return $output;
}
