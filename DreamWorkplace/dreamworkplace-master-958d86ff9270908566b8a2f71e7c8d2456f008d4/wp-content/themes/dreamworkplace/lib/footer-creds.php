<?php

//* Customize the entire footer
remove_action( 'genesis_footer', 'genesis_do_footer' );

add_action( 'genesis_footer', 'dwp_custom_footer' );

function dwp_custom_footer() {
     $phone_number                 = get_theme_mod( 'footer_phone_number' );
     $request_callback_url         = get_theme_mod( 'request_callback_url' );
    
	?>
<div class="site_footer_clr">
    <div class="container">
    <div id='prefooter' class="row">
      <div id="pre_footer_one" class="col-md-4 text-center">
          <span>Call Us at: <?php echo $phone_number; ?></span>
      </div>
      <div id="pre_footer_two" class="col-md-4 text-center">
      <a href="<?php echo !empty($request_callback_url) ? $request_callback_url : home_url().'/contact'; ?>">    
          <img src="<?php echo THEME_DIRECTORY; ?>/images/call-us.png" />
          <p class="icon-text">Request a call back</p>
      </a> 
      </div>
      <div id="pre_footer_three" class="col-md-4 text-center">
        <span class="fa-stack fa-lg">
          <a href="<?php echo get_theme_mod('facebook_url'); ?>" target="_blank">
           <img src="<?php echo THEME_DIRECTORY; ?>/images/fb.png"  />
          </a>
        </span>
        <span class="fa-stack fa-lg">
          <a href="<?php echo get_theme_mod('twitter_url'); ?>" target="_blank">
            <img src="<?php echo THEME_DIRECTORY; ?>/images/twitter.png"  />
          </a>
        </span>
        <span class="fa-stack fa-lg">
          <a href="<?php echo get_theme_mod('linkedin_url'); ?>" target="_blank">
            <img src="<?php echo THEME_DIRECTORY; ?>/images/linkedin.png"  />
          </a>
        </span>
      </div>
    </div>
        <hr/>
    </div>
  </div>
<div class="secondary_nav_bg">
	<?php
	genesis_do_subnav(); ?>
</div>	
<?php }?>
