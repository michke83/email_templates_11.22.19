<?php

// add bootstrap classes
add_filter( 'genesis_attr_nav-primary',         'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_nav-secondary',       'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_site-header',         'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_site-inner',          'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_content-sidebar-wrap','bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_content',             'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_sidebar-primary',     'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_sidebar-secondary',   'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_archive-pagination',  'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_entry-content',       'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_entry-pagination',    'bsg_add_markup_class', 10, 2 );
add_filter( 'genesis_attr_site-footer',         'bsg_add_markup_class', 10, 2 );

function bsg_add_markup_class( $attr, $context ) {
    global $post;
    $post_type       = '';
    $post_slug       = '';
    $request_url     = '';
    $post_type       = get_post_type( $post->ID );
    $post_slug       = $post->post_name;
    $sidebar_class   = '';
    $siteinner_class = '';
    $page_content    = '';
    
    $request_url = $_SERVER['REQUEST_URI'];
        if($post_type == 'post' || (strpos($request_url, 'blog') !== false)) { 
           $sidebar_class = 'col-sm-4'; 
        }
        if($post_type == 'lesson') { 
           $siteinner_class = 'lession_full_content'; 
        }
        if(is_front_page()){
            $page_content = 'home_page_site_inner';
        }
    // default classes to add
    $classes_to_add = apply_filters ('bsg-classes-to-add',
        // default bootstrap markup values
        array(
            'nav-primary'               => 'nav-primary navbar navbar-fixed-top navbar-inverse navbar-static-top',
            'nav-secondary'             => 'navbar navbar-inverse navbar-static-top',
            'site-header'               => 'container-fluid',
            'site-inner'                => $siteinner_class . $page_content,
            //'content-sidebar-wrap'      => 'row',
            'content-sidebar-wrap'      => 'container',
            'content'                   => 'col-sm-9',
            'sidebar-primary'           => $sidebar_class,
            'archive-pagination'        => 'clearfix',
            'entry-content'             => 'clearfix page_slug_'. $post_slug,
            //'entry-content'             => 'container',
            'entry-pagination'          => 'clearfix bsg-pagination-numeric',
        ),
        $context,
        $attr
    );

    // populate $classes_array based on $classes_to_add
    $value = isset( $classes_to_add[ $context ] ) ? $classes_to_add[ $context ] : array();

    if ( is_array( $value ) ) {
        $classes_array = $value;
    } else {
        $classes_array = explode( ' ', (string) $value );
    }

    // apply any filters to modify the class
    $classes_array = apply_filters( 'bsg-add-class', $classes_array, $context, $attr );

    $classes_array = array_map( 'sanitize_html_class', $classes_array );

    // append the class(es) string (e.g. 'span9 custom-class1 custom-class2')
    $attr['class'] .= ' ' . implode( ' ', $classes_array );

    return $attr;
}
