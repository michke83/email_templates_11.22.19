<?php

function home_page_services() {
  	?>
<div id="services">
    <div class="img-container wow animated zoomIn">
            <?php
            global $post;
            $args = array( 'post_type' => 'home_services','posts_per_page' => 7,'order' => 'ASC' );
            $services = new WP_Query( $args );
            $degress = 180;
            $i = 0;
                while ( $services->have_posts() ) : $services->the_post();
                    $learn_more_title =  get_field('learn_more_title', $post->ID); 
                    $learn_more_link  =  get_field('learn_more_link', $post->ID); 
                    $feat_image       = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                   $image             = !empty($feat_image) ? $feat_image : '';
                   if( $i == 0 )
                    $text_class = 'text-1'; 
                  ?>
                        <img src="<?php echo $image; ?>" class="deg<?php echo $degress;?> || circle || js-circle" tabindex="0">
                        <div class="text-container || <?php echo $text_class; ?> || js-text">
                                <h2><?php echo the_title(); ?></h2>
                                <?php echo the_content(); ?>
                              <a class="services-button || js-button || btn" tabindex="0" href="<?php echo !empty($learn_more_link) ? $learn_more_link :  get_site_url().'/services'; ?>"><?php echo !empty($learn_more_title) ? $learn_more_title : 'Learn More' ;?></a>
                        </div>
                    <?php 
                     $degress =  $degress - 30;
                     $text_class ='';
                   $i++;
             endwhile;
            ?>
    </div>
    <img class="services-logo" alt="Dream Workplace logo" src="<?php echo THEME_DIRECTORY ?>/images/dw_logo.svg">
  </div>
<?php }