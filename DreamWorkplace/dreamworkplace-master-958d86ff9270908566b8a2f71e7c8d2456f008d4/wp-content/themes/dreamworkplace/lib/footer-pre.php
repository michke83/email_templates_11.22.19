<?php

//add_filter('genesis_before_footer', 'dwp_pre_footer', 10, 1);

function dwp_pre_footer() {
  ?>
  <div class="container-fluid">
    <div id='prefooter' class="row">
      <div id="pre_footer_one" class="col-md-5 text-center">
        Call Us at: 800-555-5555
      </div>
      <div id="pre_footer_two" class="col-md-2 text-center">
        <img src="<?php echo THEME_DIRECTORY; ?>/images/call-us.png" class="hidden-xs" />
        <a href="tel:800-555-5555" class="visible-xs hidden-sm hidden-md">
            <img src="<?php echo THEME_DIRECTORY; ?>/images/call-us.png" />
        </a> 
        <p class="icon-text">Request a call back</p>
      </div>
      <div id="pre_footer_three" class="col-md-5 text-center">
        <span class="fa-stack fa-lg">
            <a href="<?php echo get_theme_mod('facebook_url'); ?>" target="_blank">
               <img src="<?php echo THEME_DIRECTORY; ?>/images/fb.png"  />
          </a>
        </span>
        <span class="fa-stack fa-lg">
          <a href="<?php echo get_theme_mod('twitter_url'); ?>" target="_blank">
               <img src="<?php echo THEME_DIRECTORY; ?>/images/twitter.png"  />
          </a>
        </span>
        <span class="fa-stack fa-lg">
          <a href="<?php echo get_theme_mod('linkedin_url'); ?>" target="_blank">
              <img src="<?php echo THEME_DIRECTORY; ?>/images/linkedin.png"  />
          </a>
        </span>
      </div>
    </div>
      <hr/>
  </div>
  <?php } ?>
