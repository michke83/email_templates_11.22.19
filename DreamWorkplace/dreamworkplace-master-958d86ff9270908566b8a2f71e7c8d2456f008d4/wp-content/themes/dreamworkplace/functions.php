<?php
/**
 * functions.php
 *
 */
if( !defined( 'THEME_DIRECTORY' ) ) {
 	define( 'THEME_DIRECTORY', get_stylesheet_directory_uri() );
}
if( !defined( 'THEME_DIR_PATH' ) ) {
    define( 'THEME_DIR_PATH', get_template_directory() );
}
if(!defined('THEME_DIR_FILE')) {
    define('THEME_DIR_FILE', __FILE__);
}
if(!defined( 'SITE_URL' )) {
	define( 'SITE_URL', get_site_url() );
 }

add_action( 'genesis_entry_header', 'genesis_do_post_title' );
add_action('wp_head', 'add_google_lato_fonts');
function add_google_lato_fonts() {
      $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 'https://' : 'http://';
   	?>
	<link href='<?php echo $protocol;?>fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
  <link href="https://fonts.googleapis.com/css?family=Lato:900" rel="stylesheet">
	<?php
}

add_action( 'genesis_setup', 'dwp_load_lib_files', 15 );

function dwp_load_lib_files() {
    foreach ( glob( dirname( __FILE__ ) . '/lib/*.php' ) as $file ) { include $file; }
}

// custom function that saves the Inbound Contact Form to the Contact CPT using custom hooks such as 'inboundnow_form_submit_actions'
function save_contact_cpt( $form_post_data ) {

	// Create contact submssion post object
	$new_contact = array(
	  'post_title'    => $form_post_data['wpleads_first_name']. '|' . $form_post_data['wpleads_last_name'],
	  'post_content'  => $form_post_data['wpleads_notes'],
	  'post_status'   => 'publish',
	  'post_type'	  => 'contact',
	);
	$post = wp_insert_post( $new_contact );

}

add_action('inboundnow_form_submit_actions','save_contact_cpt');

add_action('admin_head', 'dwp_admin_custom_fonts');

function dwp_admin_custom_fonts() {
  echo '<style>
    #membership_courses {
		background-color: #03618d !important;
		color: #ffffff !important;
	}
	#membership_courses > .acf-label > p.description {
		color: #ffffff !important;
	}
  </style>';
}

// custom function that gets the image alt text from just the image URL
function getImageAltText($image_url)
{
	 global $wpdb;
	 $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$image_url'";
	 $img_id = $wpdb->get_var($query);
	 $attachment_meta = wp_prepare_attachment_for_js($img_id);
	 return $attachment_meta['alt'];
}

add_action( 'genesis_pre', 'getImageAltText' );


//add_action('genesis_post_title');

remove_action( 'genesis_sidebar_alt', 'genesis_do_sidebar_alt' );
remove_action( 'genesis_sidebar_alt', 'ss_do_sidebar_alt' );
// hide Leads Info on subscribers profile page
remove_filter( 'show_user_profile', array( 'Leads_User_Profile', 'add_lead_list_setup' ), 10 );
remove_filter( 'edit_user_profile', array( 'Leads_User_Profile', 'add_lead_list_setup' ), 10 );

// hide Core Dashboard Widgets from Subscribers
add_action('wp_dashboard_setup', 'dwp_remove_dashboard_widgets' );

function dwp_remove_dashboard_widgets() {
	global $wp_meta_boxes;

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_activity']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);

}

// remove the course author image and related author data
remove_action( 'lifterlms_single_course_after_summary', 'lifterlms_template_course_author', 40 );
// remove from tile on course and membership catalogs
remove_action( 'lifterlms_after_loop_item_title', 'lifterlms_template_loop_author', 10 );

/**
 * Remove tabs from the LifterLMS Student dashboard
 * @param    array     $tabs  registered tabs
 * @return   array            
 */
function my_remove_dashboard_tabs( $tabs ) {
	unset( $tabs['redeem-voucher'] );
	
	return $tabs;
}
add_filter( 'llms_get_student_dashboard_tabs', 'my_remove_dashboard_tabs' );

// try to hide admin notices from Subscriber dashboard
add_action('admin_head','dwp_hide_admin_noticies_css');

function dwp_hide_admin_noticies_css()
{
if(!current_user_can('administrator'))//not and admin
{
	?>
    <style>
    	.cpac_message,
    	.update_nag,
        .hide-notice {
        	display:none
        };
        .member_courses{
  background-color: #03618d;
}
    </style>
<?php
    }
}

// Hide CRM from all roles except administrator
add_action( 'admin_menu', 'remove_menus' );
function remove_menus() {

		global $wp_roles;
		$current_user = wp_get_current_user();
		$roles = $current_user->roles;
		$role = array_shift( $roles );

    if ( $role == 'student' || $role == 'subscriber' || $role == 'editor'  || $role == 'contributor' || $role == 'author'  )
    {
	     remove_menu_page( 'upicrm_index' );
    }
}

# Adds instruction text after the post title input
function emersonthis_edit_form_after_title() {
    $tip = '<strong>TIP:</strong> To create a single line break use SHIFT+RETURN. By default, RETURN creates a new paragraph.';
    echo '<p style="margin-bottom:0;">'.$tip.'</p>';
}
add_action(
    'edit_form_after_title',
    'emersonthis_edit_form_after_title'
);

# Changing Login menu to dashboard after user login
add_filter( 'wp_nav_menu_items', 'add_custom_menu', 10, 2 );
    function add_custom_menu( $items, $args ) {
        global $post;
        $login_class = '';
          $request_uri =  $_SERVER["REQUEST_URI"] ;
            if (strpos($request_uri,'login') !== false ){
                $login_class = 'active';
            }
              $current_user = wp_get_current_user();
               $roles = $current_user->roles;
               $role = array_shift( $roles );
            if (is_user_logged_in() && ($args->theme_location == 'primary' || $args->theme_location == 'secondary') ) {
                    if( $role == 'student' ) {
                     $items .= '<li class="current-menu-item '.$login_class.'"><a href="'.get_site_url().'/my-courses/">Dashboard</a></li>';
                    }
                    else{
                     $items .= '<li class="current-menu-item"><a href="'.get_site_url().'/wp-admin/">Dashboard</a></li>';
                    }
            } 
            else {
            $items .= '<li class="current-menu-item '.$login_class.'"><a href="'.get_site_url().'/login">Log In</a></li>';
            }
          return $items;
      }  
  
  
function custom_login_redirect( $redirect, $user_id ) {
      $user_meta  = get_userdata($user_id);
      $roles      = $user_meta->roles;
      $role       = array_shift( $roles );
            if($role == 'student'){
               $redirect = get_site_url().'/my-courses/';
            }
            elseif($role == 'editor'){
                $redirect = get_site_url().'/wp-admin/';
            }
   return $redirect;  
}
add_filter('lifterlms_login_redirect','custom_login_redirect',1,2);




/** Adding LIFTERLMS PLUGIN to user role as editor **/ 
 
add_filter('lifterlms_admin_settings_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_menu_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_reporting_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_import_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_system_report_access','teacher_lifterlms_menu',1,1);



function teacher_lifterlms_menu($user_cap){
     $current_user = wp_get_current_user();
      $user_roles = $current_user->roles;
      $user_role = array_shift( $user_roles );
      if($user_role == 'editor'){
         $user_cap = 'edit_pages';      
      }
    return $user_cap;
}

/** Adding LIFTERLMS PLUGIN to user role as editor **/
			

add_filter('lifterlms_admin_courses_access','teacher_lifterlms_menu',1,1);
//add_filter('lifterlms_admin_membership_access','teacher_lifterlms_menu',1,1);
//add_filter('lifterlms_admin_engagements_access','teacher_lifterlms_menu',1,1);
//add_filter('lifterlms_admin_achievements_access','teacher_lifterlms_menu',1,1);
//add_filter('lifterlms_admin_certificates_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_emails_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_order_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_order_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_coupons_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_vouchers_access','teacher_lifterlms_menu',1,1);
add_filter('lifterlms_admin_reviews_access','teacher_lifterlms_menu',1,1);

/* Automatic redirection to Dashboard  after login */
add_action("template_redirect", 'login_url_redirect');
    function login_url_redirect() {
        if(is_user_logged_in()){
                $current_user = wp_get_current_user();
		$roles = $current_user->roles;
            	$role = array_shift( $roles );
                $request_uri =  $_SERVER["REQUEST_URI"] ;
             if (strpos($request_uri,'login') !== false){
                 if($role == 'student'){
                      wp_redirect(get_site_url()."/my-courses/");
                            exit;               
                 }
                else {
                     wp_redirect(get_site_url()."/wp-admin/");
                        exit; 
                }
                 
             }
         
        }  
        
    }
    
/* Automatic redirection to Dashboard  after login */
    
 /* Logout redirection for student and teachers */  
    
 add_filter( 'logout_url', 'logout_redirect_url', 1,2 );
 
 function logout_redirect_url( $logout_url, $redirect ){
       if(is_user_logged_in()){
                $current_user = wp_get_current_user();
                $roles = $current_user->roles;
                $role = array_shift( $roles );
                    if( $role == 'editor' ){            
                         $logout_url =  $logout_url.'&redirect_to=login';
                    }
                  }    
   return $logout_url;
 }
 /* Logout redirection for student and teachers */  

 include_once('lifterlms/llms_student_dashboard.php');
 
add_filter('llms_get_student_dashboard_tabs', 'edit_account_details',1,1);
        function edit_account_details($data){
            $data['edit-account']['content'][0] = 'custom_llms_Student_Dashboard';
            $data['edit-account']['content'][1] = 'custom_edit_account_details';
            $data['dashboard']['content'][0]    = 'custom_llms_Student_Dashboard';
            $data['dashboard']['content'][1]    = 'custom_output_dashboard_content';
            $data['view-courses']['content'][0] = 'custom_llms_Student_Dashboard';
            $data['view-courses']['content'][1] = 'custom_output_courses_content';
         return  $data;
        }

add_action( 'lifterlms_before_student_dashboard_content', 'lifterlms_template_student_dashboard_navigation', 10 );

if ( ! function_exists( 'lifterlms_template_student_dashboard_navigation' ) ) {
	function lifterlms_template_student_dashboard_navigation() {
              include_once('lifterlms/navigation.php');
		
	}
}
add_action( 'lifterlms_single_course_after_summary', 'lifterlms_template_single_syllabus_custom',10 );
if ( ! function_exists( 'lifterlms_template_single_syllabus_custom' ) ) {
   	function lifterlms_template_single_syllabus_custom() {
               remove_action( 'lifterlms_single_course_after_summary', 'lifterlms_template_single_syllabus',90 );
            include( 'lifterlms/syllabus.php' );
	}
}

add_action( 'lifterlms_single_course_before_summary', 'lifterlms_template_single_featured_image', 10 );
if ( ! function_exists( 'lifterlms_template_single_featured_image' ) ) {
	function lifterlms_template_single_featured_image() {
		include_once( 'lifterlms/featured-image.php' );
	}
}
remove_action( 'lifterlms_single_course_after_summary', 'lifterlms_template_single_course_progress',    60 );
remove_action( 'lifterlms_single_course_after_summary', 'lifterlms_template_single_meta_wrapper_start', 5 );

/* Parent course title */ 
add_action( 'lifterlms_single_lesson_before_summary', 'lifterlms_template_single_parent_course', 10 );
if ( ! function_exists( 'lifterlms_template_single_parent_course' ) ) {
	function lifterlms_template_single_parent_course() {
		include_once( 'lifterlms/parent-course.php' );
	}
}
/* Parent course title */ 
/* Remove student dashboard title */ 
  remove_action( 'lifterlms_before_student_dashboard_content', 'lifterlms_template_student_dashboard_title', 20 );
/* Remove student dashboard title */ 

/***********************************************************************
 *
 * Main Content Wrappers
 *
 ***********************************************************************/
add_action( 'lifterlms_before_main_content', 'lifterlms_output_content_wrapper', 10 );

if ( ! function_exists( 'lifterlms_output_content_wrapper' ) ) {

	function lifterlms_output_content_wrapper() {
         include_once( 'lifterlms/wrapper-start.php' );
	}
}

/***********************************************************************
 *
 * Main Content Wrappers
 *
 ***********************************************************************/

/***********************************************************************
 *
 * Lifter LMS page title
 *
 ***********************************************************************/

add_action('lifterlms_before_main_content','lifterlmes_page_title',1);

  function  lifterlmes_page_title(){ 
      include_once( 'lifterlms/lifterlms-page-title.php' );
  }
      
   
/***********************************************************************
 *
 *Lifter LMS page title
 *
 ***********************************************************************/
   
/***********************************************************************
 *
 *Courses place holder image
 *
 ***********************************************************************/
  
add_filter('lifterlms_placeholder_img', 'course_default_img',1,1);
    function course_default_img($img){
     $img = '<img src="'.THEME_DIRECTORY.'/images/placeholder.png" alt="placeholder" class="llms-placeholder llms-featured-image wp-post-image" />';
     return $img;
    }
  
  /***********************************************************************
 *
 *Courses place holder image
 *
 ***********************************************************************/
 
 /***********************************************************************
 *
 *Lession Audio play
 *
 ***********************************************************************/  
add_action( 'lifterlms_single_lesson_before_summary', 'lifterlms_template_single_lesson_audio_custom',  20 );

if ( ! function_exists( 'lifterlms_template_single_lesson_audio_custom' ) ) {
	function lifterlms_template_single_lesson_audio_custom() {
        remove_action( 'lifterlms_single_lesson_before_summary', 'lifterlms_template_single_lesson_audio',  20 );
          include( 'lifterlms/audio.php' );
	}
}




/***********************************************************************
 *
 *Lession Audio play
 *
 ***********************************************************************/

//post page changes
add_action('genesis_before_entry','post_before_entry','1');

function post_before_entry(){
     global $post;
     $post_type = get_post_type( $post->ID );
        if($post_type == 'post') { 
            echo '<div class ="blog_article_main">';
        }
}

add_action('genesis_after_entry','post_after_entry',1);
function post_after_entry(){
      global $post;
     $post_type = get_post_type( $post->ID );
        if($post_type == 'post') { 
            echo '</div>';
        }
}
add_action('genesis_before_content','post_before_content',1);
function post_before_content(){
      global $post;
     $post_type = get_post_type( $post->ID );
        if($post_type == 'post') { 
          echo '<div class="row">';
         }
  }
  
 add_action( 'genesis_after_sidebar_widget_area', 'post_after_sidebar_widget_area',1); 
 function post_after_sidebar_widget_area(){
       global $post;
      $post_type = get_post_type( $post->ID );
        if($post_type == 'post') { 
           echo '</div>';
    }
 }
  //post page changes
 
 /***********************************************************************
 *
 *Lession video play
 *
 ***********************************************************************/
 
 add_action( 'lifterlms_single_lesson_before_summary', 'lifterlms_template_single_lesson_video_custom',  20 );

 if ( ! function_exists( 'lifterlms_template_single_lesson_video_custom' ) ) {
	function lifterlms_template_single_lesson_video_custom() {
            remove_action( 'lifterlms_single_lesson_before_summary', 'lifterlms_template_single_lesson_video',  20 );            
		include( 'lifterlms/video.php' );
	}
}
 /***********************************************************************
 *
 *Lession video play
 *
 ***********************************************************************/

/***********************************************************************
* Hiding achievements,membership,certificates menus for admin and super admin users
***********************************************************************/

add_filter('lifterlms_admin_membership_access','admin_hide_lifterlms_menu',1,1);
add_filter('lifterlms_admin_engagements_access','admin_hide_lifterlms_menu',1,1);
add_filter('lifterlms_admin_achievements_access','admin_hide_lifterlms_menu',1,1);
add_filter('lifterlms_admin_certificates_access','admin_hide_lifterlms_menu',1,1);

function admin_hide_lifterlms_menu($capability){
        $capability = '';      
    return $capability;
    
}
/***********************************************************************
* Hiding achievements,membership,certificates menus for admin and super admin users
***********************************************************************/


 

/***********************************************************************
* Adding Captcha to commnet form
***********************************************************************/

 add_filter( 'comment_form_submit_field', 'add_captcha_comment_form', 1,1); 
         
         
function add_captcha_comment_form( $submit_field )
{
       $google_captcha = get_option( 'leads_recaptcha_site_key' );
       $google_captcha = !empty($google_captcha) ? $google_captcha : ''; 
       $add_captcha  = '<p class="recpatch_set">' .
                   '<div class="g-recaptcha" data-sitekey="'.$google_captcha.'">'
                   . '</div></p>';

    return $add_captcha . $submit_field ;
}
/***********************************************************************
* Adding Captcha to commnet form
***********************************************************************/

/***********************************************************************
* Removing commnet form from courses section
***********************************************************************/

add_action( 'init', 'remove_custom_post_comment' );
function remove_custom_post_comment() {
    remove_post_type_support( 'course', 'comments' );
    remove_post_type_support( 'lesson', 'comments' );
    remove_post_type_support( 'llms_quiz', 'comments' );
 }
/***********************************************************************
* Removing commnet form from courses section
 ***********************************************************************/

/***********************************************************************
* Adding hook for lession scorllbar 
 ***********************************************************************/
 if ( ! function_exists( 'llms_get_post_content' ) ) {

	function llms_get_post_content( $content ) {

		global $post;

		if ( ! $post instanceof WP_Post ) {

			return $content;

		}

		$page_restricted = llms_page_restricted( get_the_id() );
                         
		switch ( $post->post_type ) {
                            
			case 'course':

				if ( $page_restricted['is_restricted'] ) {

					add_filter( 'the_excerpt', array( $GLOBALS['wp_embed'], 'autoembed' ), 9 );
					if ( $post->post_excerpt ) {
						$content = llms_get_excerpt( $post->ID );
					}
				}

				$template_before  = llms_get_template_part_contents( 'content', 'single-course-before' );
				$template_after  = llms_get_template_part_contents( 'content', 'single-course-after' );

				ob_start();
				load_template( $template_before, false );
				$output_before = ob_get_clean();

				ob_start();
				load_template( $template_after, false );
				$output_after = ob_get_clean();

				return do_shortcode( $output_before . $content . $output_after );

			break;

			case 'lesson':
                            
				if ( $page_restricted['is_restricted'] ) {
					$content = '';
					$template_before  = llms_get_template_part_contents( 'content', 'no-access-before' );
					$template_after  = llms_get_template_part_contents( 'content', 'no-access-after' );
				} else {
					$template_before  = llms_get_template_part_contents( 'content', 'single-lesson-before' );
					$template_after  = llms_get_template_part_contents( 'content', 'single-lesson-after' );
				}

				ob_start();
				load_template( $template_before, false );
				$output_before = ob_get_clean();

				ob_start();
				load_template( $template_after, false );
				$output_after = ob_get_clean();
                                if($content){
                                    $content_exp = array();
                                    $content_exp = explode('[gravityform id=',$content);
                                        if(!empty($content_exp[1])){
                                          $content_exp[1] = '[gravityform id='.$content_exp[1];
                                            if( strlen($content_exp[0]) > 3655 ){
                                                $content_exp[0]  = "<div class='lession_deatils_scroll'>$content_exp[0]</div>"; 
                                            }else {
                                                $content_exp[0]  = $content_exp[0]; 
                                            }    
                                         $content = $content_exp[0].$content_exp[1];
                                        }
                                        else{
                                             if( strlen($content) > 3655) {
                                               $content  = "<div class='lession_deatils_scroll'>$content</div>"; 
                                             }
                                        else {
                                               $content  = $content; 
                                            }  
                                        }
                                }       
			 return do_shortcode( $output_before . $content . $output_after );

			case 'llms_membership':
				if ( $page_restricted['is_restricted'] ) {
					add_filter( 'the_excerpt', array( $GLOBALS['wp_embed'], 'autoembed' ), 9 );
					if ( $post->post_excerpt ) {
						$content = llms_get_excerpt( $post->ID );
					}
				}
				$template_before  = llms_get_template_part_contents( 'content', 'single-membership-before' );
				$template_after  = llms_get_template_part_contents( 'content', 'single-membership-after' );

				ob_start();
				load_template( $template_before, false );
				$output_before = ob_get_clean();

				ob_start();
				load_template( $template_after, false );
				$output_after = ob_get_clean();
                         
			return do_shortcode( $output_before . $content . $output_after );

			case 'llms_quiz':
				$template_before  = llms_get_template_part_contents( 'content', 'single-quiz-before' );
				$template_after  = llms_get_template_part_contents( 'content', 'single-quiz-after' );

				ob_start();
				load_template( $template_before, false );
				$output_before = ob_get_clean();

				ob_start();
				load_template( $template_after, false );
				$output_after = ob_get_clean();

			return do_shortcode( $output_before . $content . $output_after );

			default:
				return apply_filters( 'llms_get_post_content', $content );
		}// End switch().
		if ( $page_restricted['is_restricted'] ) {

			$content = apply_filters( 'llms_get_restricted_post_content',  llms_get_notices(), $page_restricted );

		}

		return $content;
	}
}// End if().
add_filter( 'the_content', 'llms_get_post_content' );

/***********************************************************************
* Adding hook for lession scorllbar 
 ***********************************************************************/

/***********************************************************************
* Lession complete text
 ***********************************************************************/
apply_filters( 'llms_lesson_complete_text', __( 'Lesson Completed', 'lifterlms' ) ); 
add_filter('llms_lesson_complete_text', 'lession_complete_text', 1, 1 );

function lession_complete_text( $lession_complete_text ){
    return 'Lesson Completed.';
}

 /***********************************************************************
* Lession complete text
 ***********************************************************************/ 

/***********************************************************************
* Gravity form error message chaging
 ***********************************************************************/
add_filter( 'gform_validation_message', 'change_message', 10, 2 );
function change_message( $message, $form ) {
        return "<div class='validation_error'> There was a problem with your submission.</div>";
}
/***********************************************************************
* Gravity form error message chaging
 ***********************************************************************/
?>