jQuery(document).ready(function($) {
    $('html').removeClass('no-js');

	$("#content-slider").lightSlider({
        loop:true,
        keyPress:true
    });
  
});


jQuery(document).ready(function($) {
     $('form#contact,form#parent-form,form#client-form').addClass('form-inline');
     $('form#contact .inbound-field,form#parent-form .inbound-field,form#client-form .inbound-field').addClass('form-group');
     $('.inbound-input').addClass('form-control');
     
//    $('.dropdown-toggle').find('.caret').remove();
//    $('.dropdown-toggle').after('<span class="caret"></span>');

    $("ul li.menu-item-has-children > span").click(function(){
       $(this).next().slideToggle();
    });
    
    $('ul li.menu-item-has-children > span').click(function () {
        $(this).parent().siblings().find('ul').slideUp();
        $(this).prev('ul').stop(true, false, true).slideToggle();
        return false;
   });
     
 });
 
  

 
jQuery(window).on("scroll", function() {
    if(jQuery(window).scrollTop() > 50) {
        jQuery(".nav-primary").css("background-color","#fff");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
       jQuery(".nav-primary").css("background-color","background-color: rgba(252, 253, 253, 0.8);");
    }
});


// captchaScale = containerWidth / elementWidth

function scaleCaptcha(elementWidth) {
  // Width of the reCAPTCHA element, in pixels
  var reCaptchaWidth = 304;
  // Get the containing element's width
	var containerWidth = jQuery('.container').width();
  
  // Only scale the reCAPTCHA if it won't fit
  // inside the container
  if(reCaptchaWidth > containerWidth) {
    // Calculate the scale
    var captchaScale = containerWidth / reCaptchaWidth;
    // Apply the transformation
    jQuery('.g-recaptcha').css({
      'transform':'scale('+captchaScale+')'
    });
  }
}

jQuery(function() { 
 
  // Initialize scaling
  scaleCaptcha();
  
  // Update scaling on window resize
  // Uses jQuery throttle plugin to limit strain on the browser
  jQuery(window).resize( jQuery.throttle( 100, scaleCaptcha));
  
});

jQuery(document).on('click','.js-videoPoster',function(ev) {
  ev.preventDefault();
  var $poster = jQuery(this);
  var $wrapper = $poster.closest('.js-videoWrapper');
  videoPlay($wrapper);
});

// play the targeted video (and hide the poster frame)
function videoPlay($wrapper) {
  var $iframe = $wrapper.find('.js-videoIframe');
  var src = $iframe.data('src');
  // hide poster
  $wrapper.addClass('videoWrapperActive');
  // add iframe src in, starting the video
  $iframe.attr('src',src);
}
