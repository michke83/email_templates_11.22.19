jQuery(function($){
  $('.slick-slider').slick({
    accessibility: true,
    adaptiveHeight: true,
    arrows: true,
    infinite: true,
    mobileFirst: true,
    autoplay: true,
    fade: true,
    nextArrow: '<div class="chevron-container slick-right hidden-xs"><i class="fa fa-chevron-right" aria-hidden="true"></i></span><span class="sr-only">Next</span></div>',
    prevArrow: '<div class="chevron-container hidden-xs"><i class="fa fa-chevron-left" aria-hidden="true"></i></span><span class="sr-only">Previous</span></div>',
    slidesToShow: 1
  });
});

jQuery(function($) {
  // This will select everything with the class smoothScroll
  // This should prevent problems with carousel, scrollspy, etc...
  $('.smoothScroll').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000); // The number here represents the speed of the scroll in milliseconds
        return false;
      }
    }
  });
});
