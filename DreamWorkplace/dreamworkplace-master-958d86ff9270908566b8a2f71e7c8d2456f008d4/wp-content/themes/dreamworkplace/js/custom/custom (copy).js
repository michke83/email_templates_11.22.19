

jQuery(document).ready(function ($) {

    $('html').removeClass('no-js');

    $("#content-slider").lightSlider({
        loop: true,
        keyPress: true
    });

});


jQuery(function ($) {
    $("ul#menu-mainmenu .mobile_arrows").click(function () {
        $(this).next().slideToggle();
    });

    $('ul#menu-mainmenu .mobile_arrows').click(function () {
        $(this).parent().siblings().find('ul').slideUp();
        $(this).prev('ul').stop(true, false, true).slideToggle();
        return false;
    });

    $('ul#menu-mainmenu .dropdown > a').click(function () {
        location.href = this.href;
    });
   
});

jQuery(document).ready(function ($) {
    $('#wpleads_first_name').attr('value','');
    $('form#contact,form#parent-form,form#client-form').addClass('form-inline');
    $('form#contact .inbound-field,form#parent-form .inbound-field,form#client-form .inbound-field').addClass('form-group');
    $('.inbound-input').addClass('form-control');
    $('.ginput_container .medium').addClass('form-control');
    $('.llms-form-field .llms-field-input').addClass('form-control');
//contact forms js 
    //Lession video js  
    $('.center-video iframe').css("width", "100%").css("height", "500px");

    //Lession video js    
});

jQuery(window).on("scroll", function () {
    if (jQuery(window).scrollTop() > 50) {
        jQuery(".nav-primary").css("background-color", "#fff").css("box-shadow", "0 0 5px 0.5px rgba(0, 0, 0, 0.38");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
        jQuery(".nav-primary").css("background-color", "background-color: rgba(252, 253, 253, 0.8);");
    }
});


//captchaScale = containerWidth / elementWidth
function scaleCaptcha(elementWidth) {
    // Width of the reCAPTCHA element, in pixels
    var reCaptchaWidth = 380;
    // Get the containing element's width
    var containerWidth = jQuery('.container').width();

    // Only scale the reCAPTCHA if it won't fit
    // inside the container
    if (reCaptchaWidth > containerWidth) {
        // Calculate the scale
        var captchaScale = containerWidth / reCaptchaWidth;
        // Apply the transformation
        jQuery('.g-recaptcha').css({
            'transform': 'scale(' + captchaScale + ')'
        });
    }
}

jQuery(function () {

    // Initialize scaling
    scaleCaptcha();

    // Update scaling on window resize
    // Uses jQuery throttle plugin to limit strain on the browser
    jQuery(window).resize(jQuery.throttle(100, scaleCaptcha));

});

jQuery(document).on('click', '.js-videoPoster', function (ev) {
    ev.preventDefault();
    var $poster = jQuery(this);
    var $wrapper = $poster.closest('.js-videoWrapper');
    videoPlay($wrapper);
});

function setHeight() {
    var window_height = jQuery(window).height();
    var site_footer = jQuery(".site-footer ").height();
    var site_header = jQuery(".nav-primary ").height();
    var temHeight = window_height - (site_header + site_footer) - 1;
    jQuery('.site-inner').css('min-height', temHeight + 'px');
}

jQuery(document).ready(function () {
   setHeight();
    
});

jQuery(document).resize(function () {
    setHeight();
    
});


// play the targeted video (and hide the poster frame)
function videoPlay($wrapper) {
    var $iframe = $wrapper.find('.js-videoIframe');
    var src = $iframe.data('src');
    // hide poster
    $wrapper.addClass('videoWrapperActive');
    // add iframe src in, starting the video
    $iframe.attr('src', src);
}

/********************Validations*************/

//window.onload = init;



jQuery(document).ready(function(){
    jQuery("#wpleads_mobile_phone").attr("maxlength", "10");
    jQuery('.page_slug_partner-success a').click(function(){
        //jQuery('#become-partner-form').closest('form').find("input[type=text], textarea").val("");
        
    }); 

    jQuery("#wpleads_first_name").after('<div id="bcmp-first">Please enter valid first name.</div>');
    jQuery("#wpleads_last_name").after('<div id="bcmp-lastnm">Please enter valid last name.</div>');
    jQuery(".inbound-email").after('<div id="bcmp-email">Please enter valid email.</div>');
    jQuery("#wpleads_mobile_phone").after('<div id="bcmp-phne">Please enter valid phone number</div>');
    jQuery("#wpleads_professional_history").after('<div id="bcmp-pfsn">This is required.</div>');
    jQuery("#wpleads_partner_description").after('<div id="bcmp-descrip">This is required.</div>');
    jQuery(".g-recaptcha").after('<div id="bcmp-cptcha">This is required.</div>');
    jQuery('#inbound_form_submit').click(function(event){
        
        //console.log(jQuery(this));
        if(jQuery('#wpleads_first_name').val()==""){
            
            jQuery('#bcmp-first').show();
        }else {
            jQuery('#bcmp-first').hide();
        }
        if(jQuery('#wpleads_last_name').val()==""){
            
            jQuery('#bcmp-lastnm').show();
        }else {
            jQuery('#bcmp-lastnm').hide();
        }
        
        if(jQuery('.inbound-email').val()==""){
            jQuery('#bcmp-email').show();
        }else{
            jQuery('#bcmp-email').hide();
        }
        
        if(jQuery('#wpleads_mobile_phone').val()==""){
            jQuery('#bcmp-phne').show();
        }else{
            jQuery('#bcmp-phne').hide();
        }
        if(jQuery('#wpleads_professional_history').val()==""){
            jQuery('#bcmp-pfsn').show();
        }else{
            jQuery('#bcmp-pfsn').hide();
        }
         if(jQuery('#wpleads_partner_description').val()==""){
            jQuery('#bcmp-descrip').show();
        }else{
            jQuery('#bcmp-descrip').hide();
        }
        var recaptcha = jQuery("#g-recaptcha-response").val();
          if (recaptcha === "") {
             event.preventDefault();
             jQuery('#bcmp-cptcha').show("fast");
          }else{
            jQuery('#bcmp-cptcha').hide("fast");  
          }
    });
     
  jQuery("#wpleads_first_name ").on("input", function(){
        var regexp = /[^a-zA-Z]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-first').show();
        }else{
            jQuery('#bcmp-first').hide();

        }
  });
    jQuery("#wpleads_professional_history").on("input", function(){
        var regexp = /[^a-zA-Z]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-pfsn').show();
        }else{
            jQuery('#bcmp-pfsn').hide();
        }
  });
  jQuery("#wpleads_last_name").on("input", function(){
        var regexp = /[^a-zA-Z]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-lastnm').show();
        }else{
            jQuery('#bcmp-lastnm').hide();
        }
  });
  
  jQuery("#wpleads_mobile_phone").on("input", function(){
        var regexp = /[^0-9]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-phne').show();
        }else{
            jQuery('#bcmp-phne').hide();
        }
        
//        jQuery("#wpleads_mobile_phone").on("blur", function(){
//     var myval = jQuery(this).val();
// 
//     if(myval.length < 10) {
//          alert("Value must contain 10 characters.");
//          jQuery(this).focus();
//     }
//});
        
        
  });
    jQuery(".inbound-email").on("input", function(){
      var emailReg = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	var valid = emailReg.test(email);

	if(!valid) {
            jQuery('#bcmp-email').show();
        return false;
    } else {
        jQuery('#bcmp-email').hide();
    	return true;
    }
  });
  
  jQuery("#wpleads_first_name,#wpleads_mobile_phone,#wpleads_professional_history,#wpleads_partner_description,#wpleads_last_name").on('blur',function(){
      jQuery('#bcmp-first,#bcmp-phne, #bcmp-pfsn, #bcmp-descrip, #bcmp-cptcha, #bcmp-email,#bcmp-lastnm').hide();
  });
  
  

});






