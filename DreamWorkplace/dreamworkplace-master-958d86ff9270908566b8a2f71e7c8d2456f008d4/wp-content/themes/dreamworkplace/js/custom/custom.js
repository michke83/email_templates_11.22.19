jQuery(document).ready(function ($) {

    $('html').removeClass('no-js');

    $("#content-slider").lightSlider({
        loop: true,
        keyPress: true
    });

});


jQuery(function ($) {
    $("ul#menu-mainmenu .mobile_arrows").click(function () {
        $(this).next().slideToggle();
    });

    $('ul#menu-mainmenu .mobile_arrows').click(function () {
        $(this).parent().siblings().find('ul').slideUp();
        $(this).prev('ul').stop(true, false, true).slideToggle();
        return false;
    });

    $('ul#menu-mainmenu .dropdown > a').click(function () {
        location.href = this.href;
    });
   
});

jQuery(document).ready(function ($) {
    
    $('form#contact,form#parent-form,form#client-form').addClass('form-inline');
    $('form#contact .inbound-field,form#parent-form .inbound-field,form#client-form .inbound-field').addClass('form-group');
    $('.inbound-input').addClass('form-control');
    $('.ginput_container .medium').addClass('form-control');
    $('.llms-form-field .llms-field-input').addClass('form-control');
//contact forms js 
    //Lession video js  
    $('.center-video iframe').css("width", "100%").css("height", "500px");

//    //Lession video js 
//    $.fn.outerHTML = function(s) {
//   return s
//       ? this.before(s).remove()
//       : $("<p>").append(this.eq(0).clone()).html();
//};
//var dd = $('.lession_deatils_scroll .gform_wrapper').outerHTML();
//$('.lession_deatils_scroll .gform_wrapper').remove()
//$(dd).insertAfter('.lession_deatils_scroll');
   
});

jQuery(window).on("scroll", function () {
    if (jQuery(window).scrollTop() > 50) {
        jQuery(".nav-primary").css("background-color", "#fff").css("box-shadow", "0 0 5px 0.5px rgba(0, 0, 0, 0.38");
    } else {
        //remove the background property so it comes transparent again (defined in your css)
        jQuery(".nav-primary").css("background-color", "background-color: rgba(252, 253, 253, 0.8);");
    }
});


//captchaScale = containerWidth / elementWidth
function scaleCaptcha(elementWidth) {
    // Width of the reCAPTCHA element, in pixels
    var reCaptchaWidth = 380;
    // Get the containing element's width
    var containerWidth = jQuery('.container').width();

    // Only scale the reCAPTCHA if it won't fit
    // inside the container
    if (reCaptchaWidth > containerWidth) {
        // Calculate the scale
        var captchaScale = containerWidth / reCaptchaWidth;
        // Apply the transformation
        jQuery('.g-recaptcha').css({
            'transform': 'scale(' + captchaScale + ')'
        });
    }
}

jQuery(function () {

    // Initialize scaling
    scaleCaptcha();

    // Update scaling on window resize
    // Uses jQuery throttle plugin to limit strain on the browser
    jQuery(window).resize(jQuery.throttle(100, scaleCaptcha));

});

jQuery(document).on('click', '.js-videoPoster', function (ev) {
    ev.preventDefault();
    var $poster = jQuery(this);
    var $wrapper = $poster.closest('.js-videoWrapper');
    videoPlay($wrapper);
});

function setHeight() {
    var window_height = jQuery(window).height();
    var site_footer = jQuery(".site-footer ").height();
    var site_header = jQuery(".nav-primary ").height();
    var temHeight = window_height - (site_header + site_footer) - 1;
    jQuery('.site-inner').css('min-height', temHeight + 'px');
}

jQuery(document).ready(function () {
   setHeight();
    
});

jQuery(document).resize(function () {
    setHeight();
    
});


// play the targeted video (and hide the poster frame)
function videoPlay($wrapper) {
    var $iframe = $wrapper.find('.js-videoIframe');
    var src = $iframe.data('src');
    // hide poster
    $wrapper.addClass('videoWrapperActive');
    // add iframe src in, starting the video
    $iframe.attr('src', src);
}

/********************Validations*************/
function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
        return true;
    }
    else {
        return false;
    }
}

jQuery(document).ready(function(){
    var starsL =jQuery (".comment-form-comment label, .comment-form-author label, .comment-form-email label, .comment-form-url label");
    var error_mgs =jQuery (".comment-form-comment #comment, .comment-form-author #author, .comment-form-email #email, .comment-form-url #url");
    jQuery('.become-client-form #company-name').removeClass('inbound-first-name')
    jQuery('.become-client-form #name').removeAttr('placeholder');
    jQuery(".inbound-phone").attr("maxlength", "10");
    jQuery('select.custom-select option').prop('disabled', false);
  jQuery(".inbound-first-name,#first_name").after('<div id="bcmp-first">Please enter first name.</div>');
    jQuery(".inbound-last-name ,#last_name").after('<div id="bcmp-lastnm">Please enter last name.</div>');
    jQuery(".custom-select").after('<div id="bcmp-select">Please select contact type.</div>');
    jQuery(".inbound-email, #llms_login").after('<div id="bcmp-email">Please enter valid email.</div>');
    jQuery("textarea.inbound-input-textarea").after('<div id="bcm-trea">This field is required.</div>');
    jQuery("input.company").after('<div id="bcmp-company">Please enter company name.</div>');
    jQuery("#company-name").after('<div id="bcmp-company-name">Please enter company name.</div>');
    jQuery(".inbound-phone").after('<div id="bcmp-phne">Please enter phone number.</div>');
    jQuery(".inbound-phone").after('<div id="pne-err2">Please enter valid phone number.</div>');
    jQuery("input.professional-history").after('<div id="bcmp-pfsn">This field is required</div>');
    jQuery("#businesss-challenge").after('<div id="bcmp-business">This field is required.</div>');
    jQuery("#wpleads_partner_description").after('<div id="bcmp-descrip">This field is required.</div>');
    jQuery("input.website").after('<div id="bcmp-url">Please give valid url.</div>');
    jQuery(".g-recaptcha").after('<div id="bcmp-cptcha">This is required.</div>');
    jQuery("#company-size").after('<div id="bcmp-cmpny-size">This field is required.</div>');
    jQuery(".checkbox-required").after('<div id="bcm-topIns">This field is required.</div>');
    jQuery("#llms_password").after('<div id="login-pwd">Please enter password.</div>');    
    jQuery(error_mgs).after('<div id="post-cmnt">This is required</div>');
    jQuery(starsL).append('<span style="color:#f00;">*</span>');
    
    jQuery('#inbound_form_submit, #llms_login_button').click(function(event){
        
        if(jQuery('.inbound-first-name').val()==""){
            jQuery('#bcmp-first').show();
            jQuery(this).focus();            
        }else{
            jQuery('#bcmp-first').hide();
        } 
         if(jQuery('select.custom-select').val()==0){
             jQuery('#bcmp-select').show();
             jQuery(this).focus();

         }else{
             jQuery('#bcmp-select').hide();

         }

        if(jQuery('#businesss-challenge').val()==""){
            jQuery('#bcmp-business').show();
            jQuery(this).focus();
        }else{
            jQuery('#bcmp-business').hide();
        }
        if(jQuery('#llms_password').val()==""){
            jQuery('#login-pwd').show();
            jQuery(this).focus();
        }else{
            jQuery('#login-pwd').hide();
        }                      
        if(jQuery('#company-size').val()==""){
            jQuery('#bcmp-cmpny-size').show();
            jQuery(this).focus();
        }else{
            jQuery('#bcmp-cmpny-size').hide();
        }
        if(jQuery('textarea.inbound-input-textarea').val()==""){
            jQuery('#bcm-trea').show();
            jQuery(this).focus();
        }else{
            jQuery('#bcm-trea').hide();
        }
        if(jQuery('.inbound-last-name').val()==""){
            jQuery('#bcmp-lastnm').show();
            jQuery(this).focus();
        }else {
            jQuery('#bcmp-lastnm').hide();
        }
        if(jQuery('input.website').val()==""){
            jQuery('#bcmp-url').show();
            jQuery(this).focus();
        }else {
            jQuery('#bcmp-url').hide();
        }

        if(jQuery('input.company').val()==""){
            jQuery('#bcmp-company').show();
            jQuery(this).focus();
        }else {
            jQuery('#bcmp-company').hide();
        }
        if(jQuery('#company-name').val()==""){
            jQuery('#bcmp-company-name').show();
            jQuery(this).focus();
        }else {
            jQuery('#bcmp-company-name').hide();
        }

        var sEmail = jQuery('.inbound-email, #llms_login').val();
        if (jQuery.trim(sEmail).length == 0) {
            jQuery('#bcmp-email').show();
            jQuery(this).focus();
            event.preventDefault();
        }
        if (validateEmail(sEmail)) {
            
            jQuery('#bcmp-email').hide();
        }
        else {
            event.preventDefault();
            jQuery('#bcmp-email').show();
            jQuery('.inbound-email').focus()
        }


        if(jQuery('.inbound-phone').length>0){
          var phn_chk =jQuery('.inbound-phone').val();
          if(jQuery('.inbound-phone').val()==""){
              jQuery('#bcmp-phne').show();
              jQuery(this).focus();
              
          }else if(phn_chk.length<10){
            //console.log(phn_chk.length)
              event.preventDefault();
              jQuery('#pne-err2').show();
              jQuery('#bcmp-phne').hide();
              jQuery('.inbound-email').focus()
          }else{
              jQuery('#pne-err2').hide();
              jQuery('#bcmp-phne').hide();
            }
          }



        if(jQuery('input.professional-history').val()==""){
            jQuery('#bcmp-pfsn').show();
            jQuery(this).focus();
        }else{
            jQuery('#bcmp-pfsn').hide();
        }
         if(jQuery('#wpleads_partner_description').val()==""){
            jQuery('#bcmp-descrip').show();
            jQuery(this).focus();
        }else{
            jQuery('#bcmp-descrip').hide();
        }
        var recaptcha = jQuery(".g-recaptcha-response").val();
          if (recaptcha === "") {
             event.preventDefault();
             jQuery('#bcmp-cptcha').show("fast");
             jQuery(this).focus();
          }else{
            jQuery('#bcmp-cptcha').hide("fast");  
          }

          if(jQuery('.become-client-form').find('.checkbox-inbound-vertical:checked').length > 0){
            jQuery('#bcm-topIns').hide("fast");
              
          }else{
            jQuery('#bcm-topIns').show("fast");  
          }
          
          

    });
       jQuery('select.custom-select').change(function() {

       if(jQuery('select.custom-select').val()==0){
               jQuery('#bcmp-select').hide();
           }else if(jQuery('select.custom-select').val()==1){
               jQuery('#bcmp-select').hide();
           }else if(jQuery('select.custom-select').val()==2){
               jQuery('#bcmp-select').hide();
           }else{
              jQuery('#bcmp-select').hide(); 
           }
 });
  jQuery(".inbound-first-name ").on("input", function(){
        var regexp = /[^a-zA-Z .]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-first').show();
        }else{
            jQuery('#bcmp-first').hide();
        }
  });
    jQuery('.become-client-form').find('.checkbox-inbound-vertical').on('click',function(){
       if(jQuery('.become-client-form').find('.checkbox-inbound-vertical:checked').length > 0){
            jQuery('#bcm-topIns').hide("fast");
              
          }else{
            jQuery('#bcm-topIns').show("fast");  
          } 
    });
jQuery("input.professional-history").on("input", function(){
    var regexp = /[^a-zA-Z .]/g;
    if(jQuery(this).val().match(regexp)){
      jQuery(this).val( jQuery(this).val().replace(regexp,'') );
      jQuery('#bcmp-pfsn').show();
    }else{
        jQuery('#bcmp-pfsn').hide();
    }
});
  jQuery(".inbound-last-name").on("input", function(){
        var regexp = /[^a-zA-Z .]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-lastnm').show();
        }else{
            jQuery('#bcmp-lastnm').hide();
        }
  });
  jQuery(".inbound-phone").on("input", function(){
        var regexp = /[^0-9]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-phne').show();
          jQuery('#pne-err2').hide();
        }else{
            jQuery('#bcmp-phne').hide();
            jQuery('#pne-err2').hide();
        }  
  });
    jQuery("#company-size").on("input", function(){
        var regexp = /[^0-9]/g;
        if(jQuery(this).val().match(regexp)){
          jQuery(this).val( jQuery(this).val().replace(regexp,'') );
          jQuery('#bcmp-cmpny-size').show();
        }else{
            jQuery('#bcmp-cmpny-size').hide();
        }  
  });
  jQuery(".inbound-first-name").on('blur',function(){ jQuery('#bcmp-first').hide();});
  jQuery(".inbound-phone").on('blur',function(){ jQuery('#bcmp-phne').hide();});
  jQuery("input.professional-history").on('blur',function(){ jQuery('#bcmp-pfsn').hide();});
  jQuery("#wpleads_partner_description").on('blur',function(){ jQuery('#bcmp-descrip').hide();});
  jQuery(".inbound-last-name").on('blur',function(){ jQuery('#bcmp-cptcha').hide();});
  jQuery(".inbound-last-name").on('blur',function(){ jQuery('#bcmp-lastnm').hide();});
  jQuery("input.company").on('blur',function(){ jQuery('#bcmp-company').hide();});
  jQuery('#commentform #submit').click(function(e){
        var recaptcha = jQuery(".g-recaptcha-response").val();
          if (recaptcha === "") {
             event.preventDefault();
             jQuery('#bcmp-cptcha').show("fast");
          }else{
            jQuery('#bcmp-cptcha').hide("fast");  
          }


          if(jQuery('#commentform #comment').val()==""){
                jQuery('#commentform').find('textarea#comment').next('#post-cmnt').show('fast');
                e.preventDefault()
                jQuery(this).focus();
            }else{
                jQuery('#commentform').find('textarea#comment').next('#post-cmnt').hide('fast');
            }
            if(jQuery('#commentform #author').val()==""){
                jQuery('#commentform').find('#author').next('#post-cmnt').show('fast');
                e.preventDefault()
                jQuery(this).focus();
            }else{
                jQuery('#commentform').find('#author').next('#post-cmnt').hide('fast');
            }
            var sEmail = jQuery('#commentform #email').val();
            if(jQuery.trim(sEmail).length == 0){
                jQuery('#commentform').find('#email').next('#post-cmnt').show('fast');
                jQuery(this).focus();
                e.preventDefault()
            }else if (validateEmail(sEmail)){
                jQuery('#commentform').find('#email').next('#post-cmnt').hide('fast');
            }else{
                e.preventDefault();
            jQuery('#commentform').find('#email').next('#post-cmnt').show('fast');
            jQuery(this).focus();
            

            }
            if(jQuery('#commentform #url').val()==""){
                jQuery('#commentform').find('#url').next('#post-cmnt').show('fast');
                jQuery(this).focus();
                e.preventDefault()
            }else{
                jQuery('#commentform').find('#url').next('#post-cmnt').hide('fast');
            }
  });
  jQuery('#commentform #comment').keypress(function(){if(jQuery('#commentform #comment').val()==""){
jQuery('#commentform').find('textarea#comment').next('#post-cmnt').show('fast');}else{jQuery('#commentform').find('textarea#comment').next('#post-cmnt').hide('fast');}});

  jQuery('#commentform #author').keypress(function(){if(jQuery('#commentform #author').val()==""){
jQuery('#commentform').find('#author').next('#post-cmnt').show('fast');}else{jQuery('#commentform').find('#author').next('#post-cmnt').hide('fast');}});

  jQuery('#commentform #email').keypress(function(){if(jQuery('#commentform #emailt').val()==""){
jQuery('#commentform').find('#email').next('#post-cmnt').show('fast');}else{jQuery('#commentform').find('#email').next('#post-cmnt').hide('fast');}});

  jQuery('#commentform #url').keypress(function(){if(jQuery('#commentform #url').val()==""){
jQuery('#commentform').find('#url').next('#post-cmnt').show('fast');}else{jQuery('#commentform').find('#url').next('#post-cmnt').hide('fast');}});
jQuery('#commentform #url').focus(function(){
var sEmail = jQuery('#commentform #email').val();
            if(jQuery.trim(sEmail).length == 0){
                jQuery('#commentform').find('#email').next('#post-cmnt').show('fast');
                event.preventDefault()
            }else if (validateEmail(sEmail)){
                jQuery('#commentform').find('#email').next('#post-cmnt').hide('fast');
            }else{
                event.preventDefault();
            jQuery('#commentform').find('#email').next('#post-cmnt').show('fast');

            }
  });        
  jQuery('#inbound_form_submit').click(function() {
            jQuery("#businesss-challenge").on("input", function(){
                if(jQuery('#businesss-challenge').val()==""){
                jQuery('#bcmp-business').show();
                }else{
                jQuery('#bcmp-business').hide();
                }
            });
        });
        
jQuery('.edit-account #llms_update_person').click(function(){
    if(jQuery('#first_name').val()==""){
        jQuery('#bcmp-first').show();
        jQuery(this).focus();
    }else {
        jQuery('#bcmp-first').hide();
    }
    if(jQuery('#last_name').val()==""){
        jQuery('#bcmp-lastnm').show();
        jQuery(this).focus();
    }else {
        jQuery('#bcmp-lastnm').hide();
    }
    
});



});