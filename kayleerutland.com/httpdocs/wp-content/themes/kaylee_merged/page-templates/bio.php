<?php
/*
    Template Name: Bio Page
*/
?>

<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>

    <div class="container">
    	<div class="col-md-7 col-md-offset-5">
	      <div class="light-bg animated fadeInDown">
    	    	<div class="title-container">
							<h1 class="page-title"><?php the_title(); ?></h1>
						</div
	          <?php the_content(); ?>
	      </div><!--<div class="clear"></div>-->
    	</div><!-- /.row-->
    </div><!-- /.container-->

<?php endwhile; ?>
<?php get_template_part( 'footer', 'includes' ); ?>
<?php get_footer(); ?>
