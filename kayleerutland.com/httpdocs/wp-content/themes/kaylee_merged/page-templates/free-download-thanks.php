<?php
    /* 
        Template Name: Free-Download Thanks Page
    */      
    process_free_download_form(); 
    get_header(); 
    while ( have_posts() ) : the_post(); 
?>

<div class="container no-header animated fadeInDown dark-bg free-download">
    <div class="pull-right free-download-float">
        <h1> Thank you....</h1>
        <p>....check your inbox within the next few minutes for your free download! 
        <br/>In the meantime, you can check out Kaylee's brand new music video for Good Day to Get Gone below!</p>
        <iframe width="560" height="315" src="//www.youtube.com/embed/oL6yMMXpan8" frameborder="0" allowfullscreen></iframe>
    </div>
</div><!-- /content-->

<?php 
    endwhile; 
    get_template_part( 'footer', 'includes' );
    get_footer(); 
?>  
