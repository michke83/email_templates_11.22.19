<?php
/*
    Template Name: Videos
*/
?>

<?php get_header(); ?>
<!-- Preloader -->
<style>
    no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php print IMAGES; ?>/loader.gif') center no-repeat #d3d3d3;
    }
</style>
<script>
$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
});
</script>
<!-- End Preloader -->
<?php while ( have_posts() ) : the_post(); ?>
	<!-- Videos Section -->
	<div class="section" id="section0">
		<div class="content videos animated fadeInDown">
			<div class="dark-bg title-container"><h1 class="page-title">Videos</h1></div>
			<div class="video-cont animated fadeInDown text-center">

				<?php
				//Get video links meta fields,
				$videos =  rwmb_meta( 'kaylee_videos' );
				$first_video_id = ltrim(strstr($videos[0], '='), '=');
				$first_video_link = "//www.youtube.com/embed/$first_video_id?rel=0&autoplay=1";
				?>

				<iframe id="video-player" width="1100" height="619" src="<?php echo $first_video_link ;?>" frameborder="0" allowfullscreen></iframe>
        	</div>

        	<div id="ca-container" class="ca-container" id="polaroidContainer">
		    	<div class="ca-wrapper flexslider flex-videos animated fadeIn">
	                <ul class="slides photo-slides">
	                	<?php
	                	 // Display thumb and open video in fancybox
	                	$first = true;
										foreach($videos as $video) {

											// if first element, set h3 to active
											if($first) {
												$h3_class = "active";
												$first = false;
											} else {
												$h3_class = "";
											}

											$video_id =  ltrim(strstr($video, '='), '=');

											// Get title of video
											$videoTitle = file_get_contents("https://www.googleapis.com/youtube/v3/videos?id=".$video_id."&key=AIzaSyBMJzFVXyDKkxNTx-e6dz-5RzrjNGmunI0&fields=items(id,snippet(title),statistics)&part=snippet,statistics");
											if ($videoTitle) {
												$json = json_decode($videoTitle, true);
												$title = $json['items'][0]['snippet']['title'];
											} else {
												$title = "";
											}

											?>
											<li class="ca-item ca-item-1">
				                 <div class="video-container" data-video="<?php echo $video_id; ?>" style="background: url('http://img.youtube.com/vi/<?php echo $video_id; ?>/0.jpg') center center no-repeat">
				                 	<h3 class="text-center <?php echo $h3_class; ?>"><?php echo $title; ?></h3>
		                      <a href="http://www.youtube.com/embed/<?php echo $video_id; ?>?autoplay=1" data-video="<?php echo $video_id; ?>" class="btn-play-video"></a>
			                  </div>
				              </li>
										<?php } ?>
									</ul>
	        	</div><!-- /.ca-wrapper -->
    		</div><!-- /.ca-container -->
		</div> <!-- /.content.videos -->

	</div> <!-- /#section0 -->


<?php endwhile; ?>

<?php get_template_part( 'footer', 'includes' ); ?>

<script type="text/javascript" src="<?php print THEMEROOT?>/assets/scripts/flexslider/jquery.flexslider-min.js"></script>

<?php get_footer(); ?>
<script>
$(window).load(function(){

	// Carousel for video thumbnails
	$('.flexslider.flex-videos').flexslider({
	  	animation: Modernizr.touch ? "slide" : "swing",
	    animationLoop: false,
	    itemWidth: 300,
	    itemMargin: 5,
	    minItems: 1,
	    maxItems: 3,
	    mousewheel: false,
	    keyboardNav: true,
	    slideshow: false,
	    start: function(slider){
	      	jQuery('body').removeClass('loading');
	    }
	});

	// Change video in player
	$( ".btn-play-video, .video-container" ).on( "click", function(e) {
		var src = '//www.youtube.com/embed/' + $(this).data('video') + '?rel=0&autoplay=1';
	  $('#video-player').attr('src', src)
	  //Switch active classes
	  $('h3.active').removeClass('active');
	  $(this).parents('li').find('h3').addClass('active');
	  e.preventDefault();
	});


});
</script>
