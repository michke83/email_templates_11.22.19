<?php
/*
    Template Name: News Page
    Pulls News and Events custom post types with news pagination
*/
?>

<?php get_header(); ?> 
  <div class="content no-header">
      <div id="blog-container" class="light-bg">   

      <div class="dark-bg title-container">
      	<h1 class="page-title shadow"><?php the_title(); ?></h1>
    	</div>      
        <div class="main-content no-sidebar">
        
        	<div class="grid">
						<?php
						// query News posts
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$args = array(
							'posts_per_page' => 10, 
							'post_type' => 'news', 
							'orderby' => 'date', 
							'order' => 'DESC' 
						);
						$news_items = query_posts($args);
												
						if ( have_posts() ) : while (have_posts()) : the_post(); ?>
						
							<?php get_template_part( 'loop', 'news' ); ?>
							
						<?php endwhile;
						
						else : endif; wp_reset_query(); ?>
        	</div><!-- End Grid -->
        	
        	<div id="loading-spinner" class="text-center">
        		<i class="fa fa-spinner fa-pulse fa-5x"></i>
        		<p>Loading</p>
        	</div>

        </div><!-- /main-content -->

        </div><!-- /blog-container-->
        <div class="clear"></div>
    </div><!-- /content-->
    
	<?php get_template_part( 'footer', 'includes' ); ?>

	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5697efe787c673d5" async="async"></script>
	<script src="<?php print THEMEROOT?>/assets/scripts/masonry.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/3.2.0/imagesloaded.pkgd.js"></script>
	
	<script>
		var $grid = $('.grid').masonry({
		  // options
		  itemSelector: '.grid-item',
		  gutter: 10
		});
		
		$grid.imagesLoaded().progress(function() {
			$grid.masonry('layout');
		})
		
		// detect if user has scrolled to bottom of page - load more posts
		$(window).scroll(function() {
		   if($(window).scrollTop() + $(window).height() == $(document).height()) {
		   	// if loading spinner still exists - it is removed once there are no more
		   	if($('#loading-spinner').length) { 
			   	newsInfiniteScroll()
		   	}
		   }
		});
		
	</script>
	
  <?php get_footer(); ?>    

