<?php /*      Template Name: Events Page     Pulls News and Events custom post
types with news pagination */ ?>

<?php get_header(); ?>

<div class="content no-header">

<div class="title-container">
	<h1 class="page-title"><?php the_title(); ?></h1>
</div>

<?php

	// Pull all events
	$args = array(
        'posts_per_page' => -1,
		'post_type' => 'event',
		'post_status' => array('future', 'publish'),
        'orderby' => 'post_date',
        'order' => 'DESC',
    );
	$events = get_posts($args);
    
    $futureEvents = array();
    $pastEvents = array();
    $todaysDate = strtotime(date('F j, Y'));
    
    // Filter past events from future events
    foreach($events as $event ){
    	
    	$event_date = $event->post_date;
	    if(strtotime($event->post_date) > $todaysDate) {
		    $futureEvents[] = $event;
	    } else {
		    $pastEvents[] = $event;
	    }
    }
     
    // Reorder future events by ascending  
    usort($futureEvents, function($a, $b) {
	    return strtotime($b->post_date) < strtotime($a->post_date);
	});
    
?>


<div class="clearfix"></div>
<div class="panel-group" id="future-events" role="tablist" aria-multiselectable="true">
<?php    if($futureEvents) {
foreach($futureEvents as $fevent) { ?>
    <div class="panel panel-default">
        <div class="panel-heading clearfix" role="tab" id="heading<?php echo $fevent->ID ?>">
          	<div class="row event-post">
    	      	<div class="col-sm-2 col-heading">
    	    		<?php echo get_the_time('M  d, Y', $fevent->ID) ?>
    	    	</div>
    	    	<div class="col-sm-5 col-heading">
    	    		<?php echo $fevent->post_title; ?>
    	    	</div>
    	    	<div class="col-sm-3 event-place">
    	    		 <?php
                        $location = get_post_meta( $fevent->ID, 'kaylee_event_location' );
                        $venue = get_post_meta( $fevent->ID, 'kaylee_event_venue' );
                        $fb_title = get_post_meta( $pevent->ID, 'kaylee_event_facebook' );
                        $tw_title = get_post_meta( $pevent->ID, 'kaylee_event_twitter' );
                        echo '<span style="font-size: 11px;">' . $venue[0] . '</span><br/>' . $location[0];
                    ?>

    	    	</div>
    	    	<div class="col-sm-2 event-btn">
    	    		<a class="collapsed collapse-link" data-toggle="collapse" data-parent="#future-events" href="#collapse<?php echo $fevent->ID ?>" aria-expanded="false" aria-controls="collapse<?php echo $fevent->ID ?>">
    	          View Details
    	        	</a>
    	    	</div>
            </div>

        </div>
        <div id="collapse<?php echo $fevent->ID ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $fevent->ID ?>">
          <div class="panel-body">
            <?php echo $fevent->post_content; ?>


          </div>
        </div>
    </div>
<?php } }?>
</div>

<div class="panel-group" id="past-events" role="tablist" aria-multiselectable="true">
<?php    if($pastEvents) {
    foreach($pastEvents as $pevent) { ?>
    <div class="panel panel-default">
        <div class="panel-heading clearfix" role="tab" id="heading<?php echo $pevent->ID ?>">
            <div class="row event-post">
                <div class="col-sm-2 col-heading">
                    <?php echo get_the_time('M  d, Y', $pevent->ID) ?>
                </div>
                <div class="col-sm-5 col-heading">
                    <?php echo $pevent->post_title; ?>
                </div>
                <div class="col-sm-3 event-place">
                     <?php
                        $location = get_post_meta( $pevent->ID, 'kaylee_event_location' );
                        $venue = get_post_meta( $pevent->ID, 'kaylee_event_venue' );
                        $fb_title = get_post_meta( $pevent->ID, 'kaylee_event_facebook' );
                        $tw_title = get_post_meta( $pevent->ID, 'kaylee_event_twitter' );
                        echo '<span style="font-size: 11px;">' . $venue[0] . '</span><br/>' . $location[0];
                    ?>

                </div>
                <div class="col-sm-2 event-btn">
                    <a class="collapsed collapse-link" data-toggle="collapse" data-parent="#past-events" href="#collapse<?php echo $pevent->ID ?>" aria-expanded="false" aria-controls="collapse<?php echo $pevent->ID ?>">
                  View Details
                    </a>
                </div>
            </div>

        </div>

        <div id="collapse<?php echo $pevent->ID ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $pevent->ID ?>">
          <div class="panel-body">

                <?php echo $pevent->post_content; ?>


          </div>
        </div>
    </div>
<?php } } ?>
</div>

<a href="#" class="events-toggle pull-right">
		<img src="<?php print IMAGES; ?>/eventarrow.png"/>
		<span id="events">view past events</span>
</a>


</div>
<div class="clear"></div>
</div><!-- /content-->
<?php get_template_part( 'footer', 'includes' ); ?>
<script>

    $( document ).ready(function() {

        // addthis.toolbox('.post-social');
        $('.events-toggle').click(function() {
            var s = $('#events');
            $('#future-events').slideToggle('slow', function(){
                s.html(s.text() == 'view past events' ? 'back to current events' : 'view past events');
            });
            $('#past-events').slideToggle('slow');
            return false;

        });

        // $('.collapse-link').click(function() {

        //     txt = $(this).text() ==  'View Details' ? 'View Details' : 'Close';

        //     $(this).text(txt);
        //     // $('#future-events').slideToggle('slow', function(){
        //     //    s.html(s.text() == 'view past events' ? 'back to current events' : 'view past events');
        //     //});
        //     // $('#past-events').slideToggle('slow');
        //     // return false;

        // });
    });

</script>
<?php get_footer(); ?>
