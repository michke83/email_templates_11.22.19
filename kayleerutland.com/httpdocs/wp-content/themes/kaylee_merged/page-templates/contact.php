<?php
	/*
	Template Name: Contact Page
	*/
	get_header();
	while ( have_posts() ) : the_post();
?>

<div class="container dark-bg">


		<div class="title-container">
			<h1 class="page-title"><?php the_title(); ?></h1>
		</div>
		<div class="col-sm-4 contact-first-col">
			<?php the_content(); ?>

			<form class="form-content" role="form" method="POST" action="/contact-thanks" data-remote="data-remote">

				<div class="row">
					<div class="form-group col-md-6">
						<input type="text" class="form-control" required id="inputFirstName" name="contact-first-name" placeholder="First name *">
					</div>
					<div class="form-group col-md-6">
						<input type="text" class="form-control" required id="inputLastName" name="contact-last-name" placeholder="Last name *">
					</div>
				</div>

				<div class="row">
					<div class="form-group col-md-12">

							<input type="email" required class="form-control" id="inputEmail" placeholder="Enter email *" name="contact-email">
							<input type="hidden" value="true" name="form_newsletter" />

					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-offset-6 pull-right">

		         			<button type="submit" class="btn btn-primary pull-right">Sign up</button>
		         	</div>
	         	</div>

				<div class="col-sm-12 pull-right">
					<div class="form-response text-right" style="display: block;"></div>
				</div>

	        </form>

	    </div>

		<div class="col-sm-5 col-sm-offset-3">
			<form class="form-content" role="form" method="POST" action="/contact-thanks" data-remote="data-remote">
				<h2>All other inquiries:</h2>

	            <div class="form-group">
	                <label for="message_concerning">This message is concerning</label>
	                <select required class="form-control" id="contact-concerning" name="contact-concerning">
	                	<option value="" selected="selected">Select an option</option>
	                	<option value="general">General Question</option>
	                	<option value="media-kit">Media Kit Request</option>
	                	<option value="press">Press</option>
	                </select>
	            </div>


				<input type="hidden" value="true" name="form_contact" />

				<div class="form-group">
					<input type="name" required class="form-control" id="inputName" name="contact-name" placeholder="Enter first and last name">
				</div>

				<div class="form-group">
					<input type="email" required class="form-control" id="inputEmail" placeholder="Enter email" name="contact-email">
				</div>

				<div class="form-group">
					<textarea rows="" required class="form-control" placeholder="Enter your message/question" name="contact-message"></textarea>
				</div>
				<div class="col-sm-6 pull-right col-sm-offset-6  btn-sub">
					<button type="submit" class="btn btn-primary pull-right">Submit</button>
				</div>
				<div class="col-sm-6 col-sm-offset-6 pull-right">
					<div class="form-response text-right" style="display: block;"></div>
				</div>
			</form>
		</div>
</div>

<?php
	endwhile;
	get_template_part( 'footer', 'includes' );
	get_footer();
?>
<script>
$( document ).ready(function() {

    $('form[data-remote]').on('submit', function(e) {
        e.preventDefault();

        var form = $(this);

        $.ajax({
            type: 'POST',
            url: '/contact-thanks',
            data: form.serialize(),
            success: function(data) {
            	console.log(data);

                var json = JSON.parse(data);

                $(form).find('.form-response').html("").html(json);
            }
        });

    });

 });
</script>
