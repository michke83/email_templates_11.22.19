<?php
/*
    Template Name: Media Page
*/
?>
<?php get_header(); ?>
<style>
    no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php print IMAGES; ?>/loader.gif') center no-repeat #d3d3d3;
    }
</style>
<!-- Preloader -->

<div class="title-container" id="music">
  <h1 class="page-title"><?php the_title(); ?></h1>
</div>

<div id="music-container">
  
	<?php $album_args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'menu_order',
					'order'            => 'ASC',
					'post_type'        => 'album',
					);
		$albums = get_posts($album_args);
		?>
		<?php

		$i=0;


		// Loop through albums
		foreach ($albums as $album) {
		$values=get_post_meta( $album->ID, 'only_on_private_music_page', false );
		if($values[0][0]!='yes'){
		?>
	<div class="section" id="section<?php print $i ?>">

		<div class="content music dark-bg">
		
			<div class="row">
			
				<div class="col-sm-5 col-sm-offset-1">
					<div class="media-left">
						<div class="">
							<?php
							$post_thumbnail_id = get_post_thumbnail_id( $album->ID );
							$url = wp_get_attachment_thumb_url( $post_thumbnail_id);
							?>
							<?php echo get_the_post_thumbnail($album->ID, 'full', array('class' => 'img-responsive'));?>
						</div>

						<div class="buttons">
		
							<?php
								$itunesUrl = get_post_meta( $album->ID, 'kaylee_album_itunes', true );
								$amazonUrl = get_post_meta( $album->ID, 'kaylee_album_amazon', true );
								$googlePlayUrl = get_post_meta( $album->ID, 'kaylee_album_google', true );
							?>
								
							<?php if(!empty($itunesUrl)) { echo '<a href="'.$itunesUrl.'" target="_blank" id="btn-iTunes" itemprop="offer"><img src="'.IMAGES.'/btn_itunesBlack.png" alt="Buy album on iTunes"></a>'; } ?>
			
							<?php if(!empty($amazonUrl)) { echo '<a href="'.$amazonUrl.'" target="_blank" id="btn-amazon" itemprop="offer"><img src="'.IMAGES.'/btn_amazonBlack.png" alt="Buy album on Amazon"></a>'; } ?>
			
							<?php if(!empty($googlePlayUrl)) { echo '<a href="'.$googlePlayUrl.'" target="_blank" id="btn-android" itemprop="offer"><img src="'.IMAGES.'/btn-android.png" alt="Buy album on Google Play"></a>	'; } ?>
									
						</div>
					</div><!-- /.media-left -->
				</div><!-- col-md-6 -->
			
				<div class="col-sm-5">
					<div class="media-right">

						<div id="media">
							<div class="site-content" role="main">
								<div id="mediaFrame" class="windowFrame">
				    				<div class="window">
				      					<div class="panel">
											<div id="theAudio">
		
												<h3><?php echo $album->post_title; ?> </h3>
												<h4><?php echo $album->post_content; ?> </h4>
												<?php
													$songs_ids = get_post_meta( $album->ID, 'kaylee_select', false );
													$songs_array = array();
		
													foreach($songs_ids as $song_id) {
														$song = get_post($song_id);
														$song_order = get_post_meta( $song_id, 'kaylee_media_order', true);
		
														$songs_array[$song_order] = $song;
													}
													ksort($songs_array);
		
													// Loop through songs list
													foreach($songs_array as $song) {
														$lyrics = get_post_meta( $song->ID, 'kaylee_lyrics', true );
														$video_link = get_post_meta( $song->ID, 'kaylee_video_link', true );
													?>
													<div itemprop="track" class="track" itemscope="" itemtype="http://schema.org/MusicRecording">
						  							<h3 style="display: none;"><span itemprop="name"><?php echo $song->post_title; ?></span></h3>
		
						  							<!-- Lyrics Modal Content -->
						  							<?php if(!empty($lyrics)) { ?>
							  							<div class="lyrics fancybox-content" style="display: none;" id="lyrics-<?php echo $song->ID ;?>">
							  								<h2><?php echo $song->post_title; ?></h2>
							  								<small><?php echo $song->post_content; ?></small>
							  								<?php echo $lyrics; ?>
							  							</div>
						  							<?php } ?>
		
						  							<meta itemprop="url" content="https://itunes.apple.com/us/album/daddys-got-a-.45/id570854312?i=570854320&amp;uo=4">
														<meta itemprop="inAlbum" content="Kaylee Rutland">
														<meta itemprop="byArtist" content="Kaylee Rutland">
														<div class="audioplayer">
															<?php if(!empty($lyrics)) { echo '<a class="btn-lyrics btn-audioplayer" href="#lyrics-'.$song->ID.'" href="">Lyrics</a>'; } ?>
															<?php if(!empty($video_link)) { echo '<a class="btn-audioplayer btn-video various fancybox.iframe" href="'. $video_link . '" href="">Watch</a>'; } ?>
		
											      	<audio id="audio4" controls="" style="width: 0px; height: 0px; visibility: hidden;">
													    <source src="<?php echo $song->guid; ?>" itemprop="audio">
														</audio>
						  								<div class="audioplayer-playpause" title="Play"></div>
														<div id="title">
														  	<span itemprop="name"><?php echo $song->post_title; ?></span>
														</div>
														<div class="audioplayer-bar" style="display: none;">
															<div class="audioplayer-bar-loaded"></div>
															<div class="audioplayer-bar-played" style="width: 0%;"></div>
														</div>
													</div>
												</div><!-- End Track -->
												<?php } ?>
											</div>
										</div><!-- /.panel -->
									</div><!-- /.window -->
								</div>
							</div><!-- .site-content -->
						</div><!-- #media -->
					</div><!-- .mediaRight -->
				</div><!-- col-md-6 -->

			</div>
		
		
		
		
			

			
			<div class="clear"></div>
		</div><!-- /content -->
	</div> <!-- end .section -->
	<?php $i++; } } ?>

</div> <!-- /#music-container -->
<?php get_template_part( 'footer', 'includes' ); ?>
<script>
$(document).ready(function() {
	if (window.innerWidth > 600) {
		$('#music-container').fullpage({
			autoScrolling: true,
			anchors: ['goodDayToGetGone', 'intoTheCircle', 'myMan', 'whatWellDoForLove', 'kayleeRutland'],
			menu: '#menu',
			verticalCentered: true,
			scrollingSpeed: 700,
			easing: 'easeInQuart',
			loopTop: true,
			loopBottom: true,
			navigation: true,
			navigationPosition: 'right',
			resize : false,
			navigation: {
                'position': 'right',
                'tooltips': ['Page 1', 'Page 2', 'Page 3', 'Pgae 4']
            },
            afterRender: function(){
                $('#pp-nav').addClass('custom');
            },
            afterLoad: function(anchorLink, index){
                if(index>1){
                    $('#pp-nav').removeClass('custom');
                }else{
                    $('#pp-nav').addClass('custom');
                }
            }
		});
	}
});

// Modal Window for Lyrics
$(document).ready(function() {
	$(".btn-audioplayer").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '100%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers : {
        overlay : {
            locked : true
        }
    },
    beforeLoad :  function() {
    	// Disable auto scrolling of page when fancybox is open if desktop
    	if (window.innerWidth > 600) {
	      $.fn.fullpage.setAutoScrolling(false);
      }
    },
    afterClose:  function() {
    	// Reenable autoscrolling of page when fancybox is closed if desktop
    	if (window.innerWidth > 600) {
        $.fn.fullpage.setAutoScrolling(true);
      }
    }
	});
});

// Add nav arrows to fullpage navigation
$(document).ready(function() {
	var prevNavItem = "<a id='btn-fullpage-prev' class='btn-fullpage-arrow prev icon-arrow-up' href=''></a>",
			nextNavItem = "<a id='btn-fullpage-next' class='btn-fullpage-arrow next icon-arrow-down' href=''></a>";
	$('#fp-nav').prepend(prevNavItem);
	$('#fp-nav').append(nextNavItem);

	$( "#btn-fullpage-prev" ).on( "click", function() {
	  $.fn.fullpage.moveSectionUp();
	});

	$( "#btn-fullpage-next" ).on( "click", function() {
	  $.fn.fullpage.moveSectionDown();
	});
});

var image = new Image();
image.src = "<?php print IMAGES ?>/bg_music.jpg";
var path = image.src;

image.onload = function () {
   $('#section0').css('background',  "transparent").addClass('background-transition');
   $(".se-pre-con").fadeOut("slow");;
}
</script>
<script type="text/javascript" src="<?php print THEMEROOT?>/assets/scripts/audioplayer.min.js"></script>
<script type="text/javascript" src="<?php print THEMEROOT?>/assets/scripts/audioplayer.js"></script>
<?php get_footer(); ?>
