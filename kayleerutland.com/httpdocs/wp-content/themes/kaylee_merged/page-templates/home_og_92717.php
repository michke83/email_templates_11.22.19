<?php
/*
    Template Name: Home Page
*/
?>

<?php get_header(); ?>
<!-- Preloader -->
<style>
    no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php print IMAGES; ?>/loader.gif') center no-repeat #d3d3d3;
    }
</style>
<?php while ( have_posts() ) : the_post(); ?>
    <div  id="home-container" >
        <!-- <div id="flex-id" class="flexslider"> -->
            <ul class="slides">
              <li>
                <div id="section4">
                  <div id="slide4-content" class="flex-active-slide">
                    <img class="slide1" id="slide4-img1" src="<?php print IMAGES;?>/slide_3_content_1.png">
                    <img class="slide1" id="slide4-img2" src="<?php print IMAGES;?>/slide_3_content_2.png">
                    <a class="pickmeuplink" href="http://lnk.to/PickMeUp" target="_blank" onclick="trackOutboundLink('http://lnk.to/PickMeUp'); return false;">
                      <img class="slide1" id="slide4-img4" src="<?php echo IMAGES; ?>/downloadnow.png" alt="">
                    </a>
                    <a class="slide1" href="https://itunes.apple.com/us/album/pick-me-up-single/id1169190479?app=itunes&ign-mpt=uo%3D4" target="_blank" id="slide_4_itunes" itemprop="offer" onclick="trackOutboundLink('https://itunes.apple.com/us/album/pick-me-up-single/id1169190479?app=itunes&ign-mpt=uo%3D4'); return false;">
                      <img src="<?php echo IMAGES; ?>/slide_4_itunes.png" alt="">
                    </a>
                    <a class="slide1" href="https://www.amazon.com/Pick-Me-Up---Single/dp/B01M7UEX98?tag=linkfire03-20&ie=UTF8&linkCode=as2&ascsubtag=edc21a4b42c50b55e54d15c2e81c7547" id="slide_4_amazon" itemprop="offer" onclick="trackOutboundLink ('https://www.amazon.com/Pick-Me-Up---Single/dp/B01M7UEX98?tag=linkfire03-20&ie=UTF8&linkCode=as2&ascsubtag=edc21a4b42c50b55e54d15c2e81c7547'); return false;" target="_blank">
                      <img src="<?php echo IMAGES; ?>/slide_4_amazon.png" alt="">
                    </a>
                    <a class="slide1" href="https://play.google.com/store/music/album/Kaylee_Rutland_Pick_Me_Up?id=Bdsogtjqegodnt6rc44kqm22gwi&PCamRefID=LFV_edc21a4b42c50b55e54d15c2e81c7547" target="_blank" id="slide_4_google" itemprop="offer" onclick="trackOutboundLink('https://play.google.com/store/music/album/Kaylee_Rutland_Pick_Me_Up?id=Bdsogtjqegodnt6rc44kqm22gwi&PCamRefID=LFV_edc21a4b42c50b55e54d15c2e81c7547'); return false;">
                      <img src="<?php echo IMAGES; ?>/slide_4_google.png" alt="">
                    </a>
                    <a class="slide1" href="https://open.spotify.com/artist/1VeGEGCgaYtcEe0t61swGr" target="_blank" id="slide_4_spotify" itemprop="offer" onclick="trackOutboundLink('https://open.spotify.com/artist/1VeGEGCgaYtcEe0t61swGr'); return false;">
                      <img src="<?php echo IMAGES; ?>/slide_4_spotify.png" alt="">
                    </a>
                    <a class="slide1" href="http://www.maccosmetics.com/" target="_blank" id="slide_4_mac">
                      <img src="<?php echo IMAGES; ?>/maclogo2.jpg" alt="">
                    </a>
                    <span class="spotify-widget-wrap">
                        <iframe src="https://embed.spotify.com/?uri=spotify:track:0l4kymJyBHOe8ONnUPrl1X" width="300" height="80" frameborder="0" allowtransparency="true"></iframe>
                    </span>
                  </div>
                </div>
              </li>
                <!-- <li>
                    <div id="section1">
                        <div id="slide1-content">
                            <img class="slide1" id="slide1-img1" src="<?php print IMAGES;?>/content_home.png">
                            <img class="slide1" id="slide1-img2" src="<?php print IMAGES;?>/content_home_2.png">
                            <img class="slide1" id="slide1-img3" src="<?php print IMAGES;?>/content_home_3.png">
                            <img class="slide1" id="slide1-img4" src="<?php echo IMAGES; ?>/btn-available-now.png" alt="">
                            <a class="slide1" href="https://itunes.apple.com/us/album/good-day-to-get-gone-ep/id898073108?ign-mpt=uo%3D4" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo IMAGES; ?>/btn-home-itunes.png" alt=""></a>
                            <a class="slide1" href="https://play.google.com/store/music/album/Kaylee_Rutland_Good_Day_to_Get_Gone?id=Bqo2vqgmfn5uwnstmpoyjrrk3lq&hl=en" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo IMAGES; ?>/btn-home-android.png" alt=""></a>
                        </div>
                    </div>
                </li>
                <li>
                    <div id="section2">
                        <a href="/events">
                            <div class="intro">
                                <h1 class="slide1">KAYLEE<br/><span>Events</span></h1>
                                <h2 class="slide1"><span class="bullets"> &#8226; &#8226; &#8226; &#8226; &#8226;</span> COME SEE kaylee AT HER UPCOMING concerts and appearances<span class="bullets"> &#8226; &#8226; &#8226; &#8226; &#8226;</span> </h2>
                            </div>
                        </a>
                    </div>
                </li>
                <li>
                    <div id="section3">
                        <a href="/videos">
                            <div class="intro">
                                <h1 class="slide1"><span style="font-weight: 100;">Check out Kaylee's new music video</h1>
                                <h2 class="slide1">//U and the Universe// <span style="color: #e962aa;">watch now!</span></h2>
                            </div>
                        </a>
                        <video autoplay loop muted controls="false" id="myVideo">
                            <source src="<?php print IMAGES;?>/home-video-bg.mp4" type="video/mp4">
                            <source src="<?php print IMAGES;?>/home-video-bg.webm" type="video/webm">
                            <source src="<?php print IMAGES;?>/home-video-bg.mov" type="video/m4v">
                        </video>
                    </div>
                </li> -->
             </ul>
        <!-- </div> -->
        <!-- <div class="direction">
            <a href="prev" class="prev icon-arrow-up"></a>
            <a href="next" class="next icon-arrow-down"></a>
              <ol class="flex-control-nav">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
              </ol>
        </div> -->
    </div>
<?php endwhile; ?>
<?php get_template_part( 'footer', 'includes' ); ?>

<script type="text/javascript" src="<?php print THEMEROOT?>/assets/scripts/flexslider/jquery.flexslider-min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('video').get(0).play();
    if (window.innerWidth > 768) {
        $('.flexslider')
        .flexslider({
            animation: "fade",
            directionNav: false,
            useCSS: false,
            direction: "vertical",
            keyboard: true,
            mousewheel: false,
            manualControls: ".flex-control-nav li",
            after: function(slider){
                if(slider.currentSlide == 3){
                    slider.pause();
                    setTimeout(function(){
                        slider.play();
                    }, 4000);
                }
            }
        });
    } else {
         $('#flex-id').removeClass('flexslider');
    }
    $('.prev, .next').on('click', function(){
        var href = $(this).attr('href');
        $('.flexslider').flexslider(href)
        return false;
    })
});
var image = new Image();
image.src = "<?php print IMAGES ?>/bg_home_1.jpg";
var path = image.src;

image.onload = function () {
   $('#section0').css('background-image',  "url('" + path + "')").addClass('background-transition');
   $(".se-pre-con").fadeOut("slow");;
}

</script>
<script src="http://www.google.com/jsapi"></script>
<script src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js"></script>
<?php get_footer(); ?>
