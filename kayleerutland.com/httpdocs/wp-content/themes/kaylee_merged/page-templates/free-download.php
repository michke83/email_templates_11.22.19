<?php
      /* 
      Template Name: Free-Download Page
      */
      get_header();   
      while ( have_posts() ) : the_post(); 
?>

<div class="header-mobile">
      <img width="100%" src="<?php print IMAGES; ?>/bg_download.jpg"> 
</div>  
<div class="container animated fadeInDown dark-bg">

      <div><img id="title-song" class="title-mobile" src="<?php print IMAGES?>/title-song.svg"></div>

      <div class="col-sm-5 free-download-form pull-right">

            <form role="form" method="POST" class="form-content" action="/free-download-thanks">

				<div class="row">
					<div class="col-sm-6">
						<input type="text" class="form-control" required id="inputFirstName" name="contact-first-name" placeholder="First name *">
					</div>
					<div class="col-sm-6">
						<input type="text" class="form-control" required id="inputLastName" name="contact-last-name" placeholder="Last name *">
					</div>
				</div>

                <input type="hidden" value="true" required name="form_free_download" />
                <input type="email" class="form-control" id="inputEmail" placeholder="Enter your email to receive your free song! *" name="contact-email">
                <p>We will not sell or share your email address with anyone else.</p>
                  
                <p><input name="contact-newsletter" checked="checked" value="1" type="checkbox">Yes, please sign me up for your newsletter!</p>
                <button type="submit" id="" class="btn btn-primary">Send me the free download</button>

            </form>

      </div>

</div><!-- /container-->

<?php 
      endwhile; 
      get_template_part( 'footer', 'includes' );
      get_footer(); 
?>      

