<?php
/*
    Template Name: Photos
*/
?>

<?php get_header(); ?>
<!-- Preloader -->
<style>
    no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php echo IMAGES; ?>/loader.gif') center no-repeat #d3d3d3;
    }
</style>
<script>
$(window).load(function() {
    // Animate loader off screen
    $(".se-pre-con").fadeOut("slow");;
});
</script>
<!-- End Preloader -->
<?php while (have_posts()) : the_post(); ?>

	<!-- Photos Section -->
    <div class="title-container">
		    <h1 class="page-title">Photos</h1>
    </div>
	    <div class="galleries">
      	<?php
                // Get gallery posts and display featured image for each
                // Featured image triggers fancybox for gallery
                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'gallery',
                    'post_status' => 'publish',
                    'orderby' => 'menu_order',
                    'order' => 'ASC',
                );
                $galleries = get_posts($args);

                $galleries_chunk = array_chunk( $galleries, 3);
                //var_dump($galleries_chunk);

                foreach ($galleries_chunk as $galleries){ ?>
                  <div class="gallery-wrapper"> <!--//create wrapper div -->
                  <?php foreach ($galleries as $gallery) { //loop through chunk galleries
                      $default_attr = array(
                          'class' => 'trigger-gallery',
                      );
                      $thumb = get_the_post_thumbnail($gallery->ID, 'thumb', $default_attr);
                    ?>

                    <div id="<?php echo $gallery->ID; ?>" class="gallery trigger-gallery">  <!--create gallery-->
                        <div class="ca-icon">
                            <div class="photos-thumb">
                              <?php echo $thumb ?>
                              <button id="viewphotos" type="button" href="#"><span>View Photos</span><img id="noblur" src="<?php print IMAGES; ?>/photos_button.png"/>
                              </button>
                              <h3><?php echo $gallery->post_title;
                              ?></h3>
                            </div>
                        </div>
                    </div>
                  <?php } ?> <!--close foreach loop for $galleries -->
                  </div>
              <?php } ?>

        </div><!-- /Galleries -->


<?php endwhile; ?>

<?php get_template_part('footer', 'includes'); ?>

<?php get_footer(); ?>
