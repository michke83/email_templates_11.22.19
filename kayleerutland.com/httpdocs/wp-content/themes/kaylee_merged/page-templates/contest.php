<?php
/* 
Template Name: Contest Page
*/
?> 

<?php get_header(); ?> 
<style>
    no-js #loader { display: none;  }
    .js #loader { display: block; position: absolute; left: 100px; top: 0; }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('<?php print IMAGES; ?>/loader.gif') center no-repeat #d3d3d3;
    }
</style>
<script>
$(window).load(function() {
    $(".se-pre-con").fadeOut("slow");;
});
</script>
<?php while ( have_posts() ) : the_post(); ?>
    <div class="non-responsive-container full-width">

    	<?php the_content(); ?>         

    <div class="clearfix"></div>
    </div><!-- /.container -->

<?php endwhile; ?>
<?php get_template_part( 'footer', 'includes' ); ?>

<?php get_footer(); ?> 