<?php
/*
    Template Name: Private Music Catalog
*/
?>

<?php get_header(); ?>

<div class="title-container">
  <h1 class="page-title"><?php the_title(); ?></h1>
</div>


<div class="container-fluid catalog_fluid" id="music-container">

    <div class="music_catalog content">
    <?php $album_args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'menu_order',
					'order'            => 'ASC',
					'post_type'        => 'album',
					);
		$albums = get_posts($album_args);
		$newalbms=array();

		foreach($albums as $key=>$value){

			$songs_ids = get_post_meta( $value->ID, 'kaylee_select', false );
			$values=get_post_meta( $value->ID, 'only_on_private_music_page', false );
			$cat=get_post_meta( $value->ID, 'category', false );
			$itunesUrl = get_post_meta( $value->ID, 'kaylee_album_itunes', false );
			$amazonUrl = get_post_meta( $value->ID, 'kaylee_album_amazon', false );
			$googlePlayUrl = get_post_meta( $value->ID, 'kaylee_album_google', false );
			$post_thumbnail_id = get_post_thumbnail_id( $value->ID );
			$url = wp_get_attachment_thumb_url( $post_thumbnail_id);
			if($values[0][0]=='yes'){
			$songs_array=array();
			foreach($songs_ids as $song_id) {
				$song = get_post($song_id);
				$songs_array[]=$song;
			}

			$newalbms[$value->ID]=array('id'=>$value->ID,'show'=>$values[0][0],'category'=>$cat[0],'title'=>$value->post_title,'content'=>$value->post_content, 'link'=>$value->guid,'songs'=>$songs_array,'itune'=>$itunesUrl[0],'google'=>$googlePlayUrl[0],'amazon'=>$amazonUrl[0],'image'=>$url);
			}
		}
							?>
    <?php
	foreach($newalbms as $key=>$album) { ?>

      <div class="row catagery_listsec">
        <div class="col-sm-5 col-sm-offset-1">
			<?php echo get_the_post_thumbnail($album['id'], 'full', array('class' => 'img-responsive'));?>
			<div class="buttons">

				<?php
				if(!empty($album['itune'])) { echo '<a href="'.$album['itune'].'" target="_blank" id="btn-iTunes" itemprop="offer"><img src="'.IMAGES.'/btn_itunesBlack.png" alt="Buy album on iTunes"></a>'; } ?>

				<?php if(!empty($album['amazon'])) { echo '<a href="'.$album['amazon'].'" target="_blank" id="btn-amazon" itemprop="offer"><img src="'.IMAGES.'/btn_amazonBlack.png" alt="Buy album on Amazon"></a>'; } ?>

				<?php if(!empty($album['google'])) { echo '<a href="'.$album['google'].'" target="_blank" id="btn-android" itemprop="offer"><img src="'.IMAGES.'/btn-android.png" alt="Buy album on Google Play"></a>	'; }
				?>


			</div>
		</div>
        <div class="col-sm-5 audio_list">
          <h3 class="text-left"><?php echo $album['title'];?></h3>
          <h4 class="text-left"><?php echo $album['content']; ?> </h4>
          <ul>
          <?php
			foreach($album['songs'] as $newkey=>$newsong) {

				$ts=get_the_time('U', $newsong->ID);
				$lyrics = get_post_meta( $newsong->ID, 'kaylee_lyrics', true );
				$video_link = get_post_meta( $newsong->ID, 'kaylee_video_link', true );
				?>
	          	<li class="audioplayer" >
	             	<?php if(!empty($lyrics)) { echo '<a class="btn-lyrics btn-audioplayer" href="#lyrics-'.$newsong->ID.'" href="">Lyrics</a>'; } ?>
															<?php if(!empty($video_link)) { echo '<a class="btn-audioplayer btn-video various fancybox.iframe" href="'. $video_link . '" href="">Watch</a>'; } ?>

					<!-- Lyrics Modal Content -->
					<?php if(!empty($lyrics)) { ?>
						<div class="lyrics fancybox-content" style="display: none;" id="lyrics-<?php echo $newsong->ID ;?>">
							<h2><?php echo $newsong->post_title; ?></h2>
							<small><?php echo $newsong->post_content; ?></small>
							<?php echo $lyrics; ?>
						</div>
					<?php } ?>

					<audio preload="none" id="player<?php echo ($album['id'] + $ts);?>" src="<?php echo $newsong->guid; ?>"></audio>
					<div class="audioplayer-playpause" lang='player<?php echo ($album['id'] + $ts);?>' title="Play">
						<a href=""></a>
					</div>
          <div id="title">
            <span itemprop="name"><?php echo $newsong->post_title; ?></span>
          </div>
	            </li>
            <?php $i++;
			} ?>
          </ul>
        </div>
      </div>
    <?php
  } // End for loop ?>
    </div>
</div>
</div>

<?php get_template_part('footer', 'includes'); ?>

<script type="text/javascript">

var this_lang = '';
	$('.audioplayer-playpause').click(function(){
		if(this_lang!= ''){
			document.getElementById(this_lang).pause();
		  	$("#"+ this_lang).prop("currentTime",0);
		}
		if($(this).parent().hasClass('audioplayer-playing')){
			console.log(this_lang);
			document.getElementById(this_lang).pause();
		  	$("#"+ this_lang).prop("currentTime",0);
			$(this).parent().removeClass('audioplayer-playing');
		} else {
			$('.audioplayer').removeClass('audioplayer-playing');
			$(this).parent().addClass('audioplayer-playing');
			this_lang = $(this).attr('lang');
			console.log(this_lang);
			document.getElementById(this_lang).play();
		}



		//$('#'+this_lang).play;
	});
 $('.audioplayer-playpause a').click(function(){
	 $(this).parent('.audioplayer-playpause').trigger('click');
	return false;
 });

// Modal Window for Lyrics
$(document).ready(function() {
	$(".btn-audioplayer").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '100%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none',
		helpers : {
        overlay : {
            locked : false
        }
    }
	});
});
</script>

<?php get_footer(); ?>
