<?php
/* 
    Template Name: Contact Thanks Page
*/ 
    process_contact_form(); 
    get_header();  
    while ( have_posts() ) : the_post(); 

?>

<div class="container no-header dark-bg">  
    <?php the_content(); ?> 
</div><!-- /container-->

<?php 
    endwhile;
    get_template_part( 'footer', 'includes' );
    get_footer(); 
?>      

    
