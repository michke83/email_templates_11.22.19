<?php get_header(); ?> 
<?php if (have_posts()) : while(have_posts()) : the_post(); ?> 
    <div class="content">
        <div id="blog-container" class="light-bg">   
              
              <div class="dark-bg title-container">
				      	<h1 class="page-title shadow">News</h1>
				    	</div>
            
	            <div id="main-content">

                   <article  class="blog-post">

                        <ul class="article-meta">
                            <li class="article-date highlighted"><?php the_time(get_option('date_format')); ?></li>
                        </ul>
                        <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>  

                        <div class="post-start clear">
                        
                        	<?php the_post_thumbnail( 'full', array('class' => 'img-responsive') ); ?> 
                        
                        	<?php the_content(); ?>
                        	
                        	<?php comments_template( '', true ); ?>
                        	
                        	
                        </div>

                    </article> 

                <?php endwhile; else : ?> 
                    <h1><?php _e('No posts were found!', 'kaylee'); ?></h1> 
                <?php endif; ?> 
                                
                <nav class="nav-single">
                	<a href="<?php echo blogInfo('url'); ?>/news-events">View All News</a>
								</nav><!-- .nav-single -->

            </div><!-- /main-content -->

            
            <div class="clear"></div>
            
        </div><!-- /blog-container-->
    </div><!-- /content-->
    <?php get_template_part( 'footer', 'includes' ); ?>

    <script>
        
        var temp= "<?php echo IMAGES; ?>";
        $.backstretch(temp + "/bg_events.jpg");

    </script>
    
    <?php get_footer(); ?> 
