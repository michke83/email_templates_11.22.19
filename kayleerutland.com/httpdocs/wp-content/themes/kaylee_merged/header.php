<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <script src="//use.typekit.net/chw3vjk.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta property="og:image" content="http://kayleerutland.com/wp-content/uploads/2015/01/Capture.jpg" />
    <meta name="Resource-type" content="Document" />
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php wp_title('|', true, 'right');?><?php bloginfo('name'); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="google-site-verification" content="aRXXV2qA0sfLF27NFaWCDjx8u6rHgAFMOF05Xvz9Vp0" />
    <meta name="google-site-verification" content="CoR0TwfVXp452pA4m0mAf_dSyRSURK4h1q1uTWt29lg" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo(stylesheet_url); ?>" />

    <link href='https://fonts.googleapis.com/css?family=Flamenco:300,400|Rokkitt:400,700|Oswald:400,300,700|Shadows+Into+Light+Two|Sacramento|Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo THEMEROOT; ?>/assets/scripts/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?php echo THEMEROOT; ?>/favicon.ico" type="image/x-icon">


    <?php
    wp_deregister_script('jquery');
    wp_register_script('jquery', ('https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js'), false, '1.11.1');
    wp_enqueue_script('jquery'); ?>

    <?php wp_head(); ?>

    <script src="<?php echo THEMEROOT; ?>/assets/javascripts/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-PKVF3K7');</script>
    <!-- End Google Tag Manager -->

    <script>
        var _gaq=[['_setAccount','UA-8044346-12'],['_trackPageview']];
        (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g,s)}(document,'script'));
    </script>

    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '953227798076951');
    fbq('track', "PageView");</script>
    <noscript><img height="1" width="1" style="display:none"
     src="https://www.facebook.com/tr?id=953227798076951&ev=PageView&noscript=1"/>
     </noscript>
    <!-- End Facebook Pixel Code -->

    <script>
    var trackOutboundLink = function(url) {
       ga('send', 'event', 'outbound', 'click', url, {'hitCallback':
         function () {
         document.location = url;
         }
       });
    }
    </script>
</head>
<body <?php body_class(); ?>>
  <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PKVF3K7"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="se-pre-con"></div>

<header>

	<div class="container">
		<nav class="navbar navbar-default" role="navigation">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
		            <span class="sr-only">Toggle navigation</span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		        </button>
			    <a class="navbar-brand" href="/"><img src="<?php echo IMAGES;?>/kheader.png" alt="Kaylee Rutland Icon" width="80"></a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse navbar-ex1-collapse">
		        <?php
		        wp_nav_menu( array(
		            'theme_location' => 'header-menu',
		            'depth' => 2,
		            'container' => false,
		            'menu_class' => 'nav navbar-nav',
		            'fallback_cb' => 'wp_page_menu',
		            //Process nav menu using our custom nav walker
		            'walker' => new wp_bootstrap_navwalker())
		        );
		        ?>
		    </div><!-- /.navbar-collapse -->
		</nav>
	</div>

</header>
