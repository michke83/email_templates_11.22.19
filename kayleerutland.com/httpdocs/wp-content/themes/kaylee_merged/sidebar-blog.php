<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div id="blog-aside">
    <div class="blog-widget">
        <div class="posts-widget" id="recent-posts">
            <h1>Recent Posts</h1>
            <ul>
            	<?php
            	$args = array( 'numberposts' => '3' );
				$recent_posts = wp_get_recent_posts( $args );
				foreach( $recent_posts as $recent ){
					echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
				}
            	?>
            </ul>   
        </div> 

        <div class="posts-widget" id="recent-comments">
            <h1>Recent Comments</h1>
            <?php 
        	//Get most recent comment
        	$args = array( 'number' => '1' );
			$comments = get_comments( array(
                'status'    => 'approve'
            ) );
			if($comments) {
				echo '<p><i>' . $comments[0]->comment_author . ': </i>' . $comments[0]->comment_content . ' <a href="' . get_comment_link($comments[0]) . '">[...]</a></p>';
			} else {
				echo '<p>no recent comments</p>';
			}
        	?>
        </div>

        <div class="posts-widget" id="archives">
            <h1>Archives</h1>
                <ul>
                    <?php wp_get_archives( array( 'type' => 'monthly', 'limit' => 12 ) ); ?>
                </ul>  
            </div> 

            <div class="posts-widget" id="categories">
                <h1>Categories</h1>
                <ul>
                	<?php 
                	$args = array(
                		'title_li'           => __( '' ),
                		'number'             => 3,
                	);
                	
                	wp_list_categories($args); ?>
                </ul>  
            </div>  
        </div>
</div><!-- /blog-aside -->