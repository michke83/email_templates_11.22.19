<?php get_header(); ?> 

<div class="content no-header">
    <div id="blog-container" class="light-bg">   

        <div class="dark-bg blog-title"><h1 class="shadow"><a href="/blog/"/>blog</a></h1></div>
        
        <div id="main-content">
         
            <?php if (have_posts()) : while(have_posts()) : the_post(); ?> 

               <article  class="blog-post">

                    <header> 
                        <ul class="article-meta">
                            <li class="article-meta-categories"><?php the_category('&nbsp;/&nbsp;')?></li>
                            <li class="article-date highlighted"><?php the_time(get_option('date_format')); ?></li>
                        </ul>
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>  
                    </header> 

                    <div class="post-start clear">

                        <?php if (has_post_thumbnail()) : ?> 
                            <figure class="article-preview-image">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                            </figure>
                        <?php endif; ?>
                    	
                        <?php the_excerpt(); ?>
                        <p><a href="<?php the_permalink(); ?>">Read more &raquo;</a></p>
                    </div>

                </article> 

            <?php endwhile; else : ?> 
                <h1><?php _e('No posts were found!', 'kaylee'); ?></h1> 
            <?php endif; ?> 

                

            <div class="pagination">
                <?php
                next_posts_link();
				previous_posts_link();
				?>
            </div>

        </div><!-- /main-content -->

		<?php get_sidebar( 'blog' ); ?>
        
    </div><!-- /blog-container-->
    <div style="clear: both;"></div>
</div><!-- /content-->

<?php get_template_part( 'footer', 'includes' ); ?>

<?php get_footer(); ?> 
