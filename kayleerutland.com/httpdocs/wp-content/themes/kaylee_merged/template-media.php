<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!--<meta http-equiv="X-UA-Compatible" content="IE=9"/>-->
<title>Kaylee Rutland MusicCatalog</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<link rel="icon" href="<?php echo site_url();?>/wp-content/themes/kaylee_merged/favicon.ico" type="image/x-icon">
<link href='http://fonts.googleapis.com/css?family=Flamenco:300,400|Rokkitt:400,700|Oswald:400,300,700|Shadows+Into+Light+Two|Sacramento|Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Flamenco:300,400|Rokkitt:400,700|Oswald:400,300,700|Shadows+Into+Light+Two|Sacramento|Montserrat:400,700' rel='stylesheet' type='text/css'>

<?php 
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.11.1');
    wp_enqueue_script('jquery'); ?>
    
<?php wp_head(); ?>
<script type="text/javascript" src="http://kayleerutland.com/wp-content/themes/kaylee_merged/assets/scripts/fancybox/jquery.fancybox.js"></script>
<script src="<?php print THEMEROOT; ?>/assets/javascripts/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"  />
<link rel="stylesheet" media="screen" href="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_css/jquery-ui.css"  />
<link rel="stylesheet" media="screen" href="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_css/mainstyles.css"  />
<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/jquery.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/cssua.min.js"></script>
</head>
<?php  
	global 
$custom_layout,
$background_url,
$pageID,
$page;  
$pagename = get_query_var('pagename'); ?>
<?php  
/**
 * Template Name: New Media Page
 * 
 * The template for displaying the Static media page
 *
 * @package WordPress
 * @subpackage Edition
 * @since Edition 1.0
 */
?>
<body>
<!-- NAVBAR -->
<div class="se-pre-con"></div>
<header>
  <div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-header"> <a href="/">
      <h1> Kaylee Rutland</h1>
      </a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    </div>
    <div class="navbar-collapse collapse">
      <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_id' => 'nav' ) ); ?>
    </div>
    <!--/.navbar-collapse --> 
  </div>
  <!-- /.navbar --> 
  
</header>
<div class="container-fluid music_banner"> <img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/banner.jpg" />
  <h1>MusicCatalog</h1>
</div>
<div class="container-fluid catalog_fluid">
  <div class="music_catalog">
    <?php $album_args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'ID',
					'order'            => 'DESC',
					'post_type'        => 'album',
					);
		$albums = get_posts($album_args);
		$newalbms=array();
		
		foreach($albums as $key=>$value){
			
			$songs_ids = get_post_meta( $value->ID, 'kaylee_select', false );
			$values=get_post_meta( $value->ID, 'only_on_private_music_page', false );
			$cat=get_post_meta( $value->ID, 'category', false );
			$itunesUrl = get_post_meta( $value->ID, 'kaylee_album_itunes', false ); 	
			$googlePlayUrl = get_post_meta( $value->ID, 'kaylee_album_google', false ); 
			//$image_id=get_post_meta( $value->ID, 'image', false ); 
			$post_thumbnail_id = get_post_thumbnail_id( $value->ID );
				$url = wp_get_attachment_thumb_url( $post_thumbnail_id);
			//$url = wp_get_attachment_thumb_url( $image_id[0]);
			//print_r($url);
			if($values[0][0]=='yes'){
			$songs_array=array();
			foreach($songs_ids as $song_id) { 
				$song = get_post($song_id);
				$songs_array[]=$song;
			}
			
			$newalbms[$value->ID]=array('id'=>$value->ID,'show'=>$values[0][0],'category'=>$cat[0],'title'=>$value->post_title,'link'=>$value->guid,'songs'=>$songs_array,'itune'=>$itunesUrl[0],'google'=>$googlePlayUrl[0],'image'=>$url);
			}
		}
		/*echo "<pre>";
		print_r($newalbms);
		die;*/
							?>
    <?php  
	foreach($newalbms as $key=>$album) {
		print_r($album);
		if($album['category']=='Unreleased'){ ?>
<div class="catalog_catagery">
<h1><?php echo $album['category'];?></h1>
<ul class="play_list" >
					<?php $i=1;
					//krsort($album['songs']);
					foreach($album['songs'] as $newkey=>$newsong) { 
					$ts=get_the_time('U', $newsong->ID)
					//print_r($newsong);?>
                    <li class="audioplayer" >
                <audio preload="none" id="player<?php echo ($album['id'] + $ts);?>" src="<?php echo $newsong->guid; ?>"></audio>
                    <div class="audioplayer-playpause" lang="player<?php echo ($album['id'] + $ts);?>"> <a href=""></a>
                    <div id="title"><span itemprop="name"><?php echo $newsong->post_title; ?></span>
                    </div>
                    </div>
                    </li>
                    <?php $i++; } ?>
</ul>
</div>
<div class="catalog_catagery release_catagery">
    <?php }else if($album['category']=='Released'){
	?>
      <h2><?php echo $album['category'];?></h2>
      <p class="catagery_tagline" ><?php echo $album['title'];?></p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo $album['image'];?>" />
			<div class="buttons">
				<a href="<?php echo $album['itune'];?>" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="<?php echo $album['google'];?>" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
          <?php $i=1;
					foreach($album['songs'] as $newkey=>$newsong) { 
					$ts=get_the_time('U', $newsong->ID) ?>
          	<li class="audioplayer" >
             <audio preload="none" id="player<?php echo ($album['id'] + $ts);?>" src="<?php echo $newsong->guid; ?>"></audio>
        		<div class="audioplayer-playpause" lang='player<?php echo ($album['id'] + $ts);?>'>
                    <a href=""></a><div id="title"><span itemprop="name"><?php echo $newsong->post_title; ?></span></div> 
                </div>
            </li>
              <?php $i++;} ?>         
          </ul>
        </div>
      </div>
    <?php
	}else{
		?>
         <p class="catagery_tagline" ><?php echo $album['title'];?></p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo $album['image'];?>" />
			<div class="buttons">
				<a href="<?php echo $album['itune'];?>" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="<?php echo $album['google'];?>" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
           <?php $i=1;
					foreach($album['songs'] as $newkey=>$newsong) { 
					$ts=get_the_time('U', $newsong->ID);
					 ?>
          	<li class="audioplayer" >
             <audio preload="none" id="player<?php echo ($album['id'] + $ts);?>" src="<?php echo $newsong->guid; ?>"></audio> 
        		<div class="audioplayer-playpause" lang='player<?php echo ($album['id'] + $ts);?>'>
                    <a href=""></a><div id="title"><span itemprop="name"><?php echo $newsong->post_title; ?></span></div> 
                </div>
            </li>
            <?php $i++;
			} ?>
          </ul>
        </div>
      </div>
        <?php
	}
	} ?>
    </div>
</div>
</div>
<footer>
  <div class="pull-right footer-icons"> <a class="icon icon-youtube" href="http://www.youtube.com/user/kayleerutland?feature=results_main" target="_blank"></a> <a class="icon icon-facebook" href="https://www.facebook.com/KayleeRutlandMusic" target="_blank"></a> <a class="icon icon-twitter" href="https://twitter.com/kayleerutland" target="_blank"></a> <a class="icon icon-instagram" href="http://instagram.com/kayleerutland" target="_blank"></a> <a class="icon icon-pinterest" href="http://www.pinterest.com/kayleerutland/" target="_blank"></a> <a style="padding: 6px 11px 2px 11px;line-height: 32px;font-size: 2em;" class="icon fa fa-vine fa-2x" href="https://vine.co/u/948452025249730560" target="_blank"></a> </div>
  <!-- /#right -->
  
  <div class="pull-left" id="copy-right"> © 2015 Kaylee Rutland Music. All Rights Reserved. </div>
  <a href="/free-download">
  <div class="pull-left" id="home-promotion"> GET YOUR <span style="color: #02d1d1">FREE</span><br>
    <span style="font-weight:500; font-size:32px; line-height:.2em;">DOWNLOAD</span> </div>
  </a> <a target="_blank" href="https://www.nashnextcountry.com/bands/Kaylee%20Rutland">
  <div class="pull-left" id="nash"> <img style="float: left; margin-left: 50px;" src="http://kayleerutland.com/wp-content/themes/kaylee_merged/assets/images/icon-nash.gif" alt="">
    <div style="float: left;display: inline-block;line-height: 1.2em;">BECOME<br>
      <span style="font-size: 35px;">A FAN</span></div>
  </div>
  </a> </footer>
<script type="text/javascript"> 	
 
var this_lang = '';
	$('.audioplayer-playpause').click(function(){ 
		if(this_lang!= ''){
			document.getElementById(this_lang).pause(); 		   
		  	//$("#"+ this_lang).prop("currentTime",0);		 
		}
		if($(this).parent().hasClass('audioplayer-playing')){
			console.log(this_lang);
			document.getElementById(this_lang).pause(); 		   
		  	//$("#"+ this_lang).prop("currentTime",0);
			$(this).parent().removeClass('audioplayer-playing');
		} else {
			$('.audioplayer').removeClass('audioplayer-playing');
			$(this).parent().addClass('audioplayer-playing');
			this_lang = $(this).attr('lang');
			console.log(this_lang);
			document.getElementById(this_lang).play();
		}		
		
		return false;
		
		//$('#'+this_lang).play;
	});
 $('.audioplayer-playpause a').click(function(){
	 $(this).parent('.audioplayer-playpause').trigger('click');
	return false;	 
 });
</script>

</body>
</html>
