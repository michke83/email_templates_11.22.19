<article class="news-item grid-item">
							
  <?php the_post_thumbnail( 'thumb' ); ?>
  
  <div class="grid-content" data-news-id="<?php echo get_the_ID();?>">
    <h2 class="read-more"><?php the_title(); ?></h2> 
    <h4><?php the_time(get_option('date_format')); ?></h4>
    <div class="post-start"><?php the_excerpt(); ?></div>
    
    <div class="row">
    	<div class="share-news col-md-2">
    		<span>Share</span>
    	</div>
    	<div class="share-news col-md-4 addthis_toolbox">
      	 <a addthis:url="<?php the_permalink(); ?>" addthis:title="<?php the_title_attribute(); ?>" class="addthis_button_facebook"><i class="fa fa-facebook fa-2x"></i></a>
				 <a addthis:url="<?php the_permalink(); ?>" addthis:title="<?php the_title_attribute(); ?>" class="addthis_button_twitter"><i class="fa fa-twitter fa-2x"></i></a>
      </div>
      
      <div class="col-md-6 text-right">
      	<a class="read-more" href="">READ MORE >></a>
      </div>
    </div>
  </div><!-- End Grid Content -->

</article><!-- End Grid Item -->