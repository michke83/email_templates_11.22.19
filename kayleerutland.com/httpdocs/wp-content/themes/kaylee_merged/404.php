<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 *
 * @package WordPress
 */

get_header(); ?>

  <div class="content">

		<div class="title-container">
			<h1 class="page-title">Page Not Found</h1>
		</div>

  </div><!-- /content-->

  <?php get_template_part( 'footer', 'includes' ); ?>

  <?php get_footer(); ?>
