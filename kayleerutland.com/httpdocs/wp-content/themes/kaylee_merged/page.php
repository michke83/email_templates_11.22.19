<?php
/**
 * The template for displaying all pages.
 */

get_header(); ?>

    <div class="content no-header">
        <div style="background: white; padding: 5%;" id="blog-container" class="light-bg">   

		    <div id="">
		
				<?php while ( have_posts() ) : the_post(); ?>
				
					<article>
						<?php the_content(); ?>
					</article>
					
				<?php endwhile; // end of the loop. ?>
		
			</div><!-- #content -->
			
        </div>
    </div>
    
    <?php get_template_part( 'footer', 'includes' ); ?>

    <script>
        
        var temp= "<?php print IMAGES; ?>";
        $.backstretch(temp + "/bg_video.jpg");

    </script>

<?php get_footer(); ?>