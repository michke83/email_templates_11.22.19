<?php get_header(); ?> 
    <div class="content">
        <div id="blog-container" class="light-bg">   
            <div class="dark-bg blog-title"><h1 class="shadow"><a href="/blog/"/>blog</a></h1></div>
            
            <div id="main-content">
             
                <?php if (have_posts()) : while(have_posts()) : the_post(); ?> 

                   <article  class="blog-post">

                        <header> 

                            <ul class="article-meta">
                                <li class="article-meta-categories"><?php the_category('&nbsp;/&nbsp;')?></li>
                                <li class="article-date highlighted"><?php the_time(get_option('date_format')); ?></li>
                            </ul>
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>  

                        </header> 

                        <div class="post-start clear">

                            <?php if (has_post_thumbnail()) : ?> 
                                <figure class="article-preview-image">
                                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                                </figure>
                            <?php endif; ?>
                                                    
                        	<?php the_content(); ?>
                        	
                        	<?php comments_template( '', true ); ?>
                        	
                        	
                        </div>

                    </article> 

                <?php endwhile; else : ?> 
                    <h1><?php _e('No posts were found!', 'kaylee'); ?></h1> 
                <?php endif; ?> 
                
                <nav class="nav-single">
					<span class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">' . _x( '&larr;', 'Previous post link', 'twentytwelve' ) . '</span> Previous Post' ); ?></span> 
					<span class="nav-next"><?php next_post_link( '%link', 'Next Post <span class="meta-nav">' . _x( '&rarr;', 'Next post link', 'twentytwelve' ) . '</span>' ); ?></span>
				</nav><!-- .nav-single -->

            </div><!-- /main-content -->

            <?php get_sidebar( 'blog' ); ?>
            
        </div><!-- /blog-container-->
        <div style="clear: both;"></div>
    </div><!-- /content-->
    <?php get_template_part( 'footer', 'includes' ); ?>

    <script>
        
        var temp= "<?php echo IMAGES; ?>";
        $.backstretch(temp + "/bg_events.jpg");

    </script>
    
    <?php get_footer(); ?> 
