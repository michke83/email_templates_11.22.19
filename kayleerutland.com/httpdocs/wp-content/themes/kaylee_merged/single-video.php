<?php get_header(); ?> 
<?php if (have_posts()) : while(have_posts()) : the_post(); ?> 
    <div class="content">
        <div id="blog-container" class="light-bg">   
                
                <div class="dark-bg title-container">
				      	<h1 class="page-title shadow"><?php the_title(); ?></h1>
				    	</div>
				    	
                <div id="main-content" class="no-sidebar">

                   <article  class="blog-post">

                        <div class="post-start clear">
                            <?php the_content(); ?>
                            
                            <?php comments_template( '', true ); ?>
                            
                            
                        </div>

                    </article> 

                <?php endwhile; else : ?> 
                    <h1><?php _e('No posts were found!', 'kaylee'); ?></h1> 
                <?php endif; ?> 
                

            </div><!-- /main-content -->
<div class="clear"></div>

            
        </div><!-- /blog-container-->
    </div><!-- /content-->
    <?php get_template_part( 'footer', 'includes' ); ?>

    <script>
        
        var temp= "<?php echo IMAGES; ?>";
        $.backstretch(temp + "/bg_events.jpg");

    </script>
    
    <?php get_footer(); ?> 
