<?php  
	global 
$custom_layout,
$background_url,
$pageID,
$page;  
$pagename = get_query_var('pagename'); ?>
<?php  
/**
 * Template Name: New Media Page(copy)
 * 
 * The template for displaying the Static media page
 *
 * @package WordPress
 * @subpackage Edition
 * @since Edition 1.0
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Kaylee Rutland MusicCatalog</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="icon" href="<?php echo site_url();?>/wp-content/themes/kaylee_merged/favicon.ico" type="image/x-icon">
<link href='http://fonts.googleapis.com/css?family=Flamenco:300,400|Rokkitt:400,700|Oswald:400,300,700|Shadows+Into+Light+Two|Sacramento|Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Flamenco:300,400|Rokkitt:400,700|Oswald:400,300,700|Shadows+Into+Light+Two|Sacramento|Montserrat:400,700' rel='stylesheet' type='text/css'>

    <?php 
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"), false, '1.11.1');
    wp_enqueue_script('jquery'); ?> 
    
    <?php wp_head(); ?>

    <script src="<?php print THEMEROOT; ?>/assets/javascripts/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>   
<link rel="stylesheet" media="screen" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"  />
<link rel="stylesheet" media="screen" href="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_css/jquery-ui.css"  /> 
<link rel="stylesheet" media="screen" href="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_css/mainstyles.css"  />
<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/jquery.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_js/cssua.min.js"></script> 

</head>
<body>
<!-- NAVBAR -->
<div class="se-pre-con"></div>
<header>

<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="navbar-header">
       <a href="/"><h1> Kaylee Rutland</h1></a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div class="navbar-collapse collapse">  

    <?php wp_nav_menu( array( 'theme_location' => 'header-menu', 'container_id' => 'nav' ) ); ?>   
    </div><!--/.navbar-collapse -->
</div> <!-- /.navbar -->


    
</header> 

<div class="container-fluid music_banner"> <img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/banner.jpg" />  
<h1>MusicCatalog</h1>
</div>
<div class="container-fluid catalog_fluid">
  <div class="music_catalog">
  <?php $album_args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'menu_order',
					'order'            => 'ASC',
					'post_type'        => 'album',
					);
		$albums = get_posts($album_args);
		foreach ($albums as $album) { 
		$values=get_post_meta( $album->ID, 'only_on_private_music_page', false );
		if(!empty($values[0][0])){
											$songs_ids = get_post_meta( $album->ID, 'kaylee_select', false ); 
											$songs_array = array();

											foreach($songs_ids as $song_id) { 
												$song = get_post($song_id);
												$song_order = get_post_meta( $song_id, 'kaylee_media_order', true); 

												$songs_array[$song_order] = $song;
											}
											ksort($songs_array);
		}
		}
		?> 
    <div class="catalog_catagery">
      <h1>Unreleased</h1>
       <ul class="play_list " >
      <li class="audioplayer" >
         <audio preload="none" id="player21" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/MoreOfThat.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player21'>
            	<a href=""></a><div id="title"><span itemprop="name">More of That</span></div> 
            </div>
        </li>
     <li class="audioplayer" >
         <audio preload="none" id="player7" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/UandtheUniverse.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player7'>
            	<a href=""></a><div id="title"><span itemprop="name">U &amp; the Universe</span></div> 
            </div>
        </li> 
      <li class="audioplayer" >
         <audio preload="none" id="player3" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/FeelsLikeMovinOn.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player3'>
            	<a href=""></a><div id="title"><span itemprop="name">Feels Like Movin' On</span></div> 
            </div>
        </li> 
        <li class="audioplayer" >
         <audio preload="none" id="player5" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/ThatSideOfMe.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player5'>
            	<a href=""></a><div id="title"><span itemprop="name">That Side of Me</span></div> 
            </div>
        </li>      
     
        <li class="audioplayer" >
        <audio preload="none" id="player1" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/AllAboutYou.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player1'>
            	<a href=""></a><div id="title"><span itemprop="name">All About You</span></div> 
            </div>
        </li>
        <li class="audioplayer" >
        <audio preload="none" id="player2" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/AlwaysSummer.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player2'>
            	<a href=""></a><div id="title"><span itemprop="name">Always Summer</span></div> 
            </div>
        </li>
       
        <li class="audioplayer" >
         <audio preload="none" id="player4" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/FoundItInYou.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player4'>
            	<a href=""></a><div id="title"><span itemprop="name">Found it in You</span></div> 
            </div>
        </li>
        
        <li class="audioplayer" >
         <audio preload="none" id="player6" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/TurningOffTheWorld.mp3"></audio>
        	<div class="audioplayer-playpause" lang='player6'>
            	<a href=""></a><div id="title"><span itemprop="name">Turning Off the World</span></div> 
            </div>
        </li>
          
      </ul>
    </div>
    <div class="catalog_catagery release_catagery">
      <h2>Released</h2>
      <p class="catagery_tagline" >Good day to get gone</p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/img1.jpg" />
			<div class="buttons">
				<a href="https://itunes.apple.com/us/album/good-day-to-get-gone-ep/id898073108?ign-mpt=uo%3D4" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="https://play.google.com/store/music/album/Kaylee_Rutland_Good_Day_to_Get_Gone?id=Bqo2vqgmfn5uwnstmpoyjrrk3lq&hl=en" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
          	<li class="audioplayer" >
             <audio preload="none" id="player8" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/GoodDayToGetGone.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player8'>
                    <a href=""></a><div id="title"><span itemprop="name">Good Day to Get Gone</span></div> 
                </div>
            </li>
            <li class="audioplayer" >
            <audio preload="none" id="player9" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/EverybodysGotOne.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player9'>
                    <a href=""></a><div id="title"><span itemprop="name">Everybodys Got One</span></div> 
                </div>
            </li>
            <li class="audioplayer" >
            <audio preload="none" id="player10" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/GunShy.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player10'>
                    <a href=""></a><div id="title"><span itemprop="name">Gun Shy</span></div> 
                </div>
            </li>
            <li class="audioplayer" >
             <audio preload="none" id="player11" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/SaveYourHeart.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player11'>
                    <a href=""></a><div id="title"><span itemprop="name">Save Your Heart</span></div> 
                </div>
            </li>
     
            <li class="audioplayer" >
             <audio preload="none" id="player12" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/ThisBigTown.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player12'>
                    <a href=""></a><div id="title"><span itemprop="name">This Big Town</span></div> 
                </div>
            </li>            
          </ul>
        </div>
      </div>
      <p class="catagery_tagline" >Into the circle</p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/img2.jpg" />
			<div class="buttons">
				<a href="https://itunes.apple.com/us/album/into-circle-feat.-jamie-oneal/id667895671" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="https://play.google.com/store/music/album/Kaylee_Rutland_Into_the_Circle_feat_Jamie_O_Neal_C?id=Bcsojno4qahop6hn3propvgezwi&hl=en" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
          	<li class="audioplayer" >
             <audio preload="none" id="player13" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/IntotheCircleft.mp3"></audio> 
        		<div class="audioplayer-playpause" lang='player13'>
                    <a href=""></a><div id="title"><span itemprop="name">Into The Circle</span></div> 
                </div>
            </li>
          </ul>
        </div>
      </div>
      <p class="catagery_tagline" >My man</p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/img3.jpg" />
				<div class="buttons">
				<a href="https://itunes.apple.com/us/album/my-man-single/id742175206" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="https://play.google.com/store/music/album/Kaylee_Rutland_My_Man?id=Buvmh6evjxrymqwycfnvpmyvdvu&hl=en" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
          	<li class="audioplayer" >
              <audio preload="none" id="player14" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/MyMan.mp3"></audio> 
        		<div class="audioplayer-playpause" lang='player14'>
                    <a href=""></a><div id="title"><span itemprop="name">May Man</span></div> 
                </div>
            </li> 
          </ul>
        </div>
      </div>
      <p class="catagery_tagline" >What we'll do for love</p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/img4.jpg" />
			<div class="buttons">
				<a href="https://itunes.apple.com/us/album/what-well-do-for-love-single/id717239549" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="https://play.google.com/store/music/album/Kaylee_Rutland_What_We_ll_Do_for_Love?id=Bq4zfctdqc6xqf7vy26tbmpsnvm&hl=en" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
          
      
          	<li class="audioplayer" >
            <audio preload="none" id="player15" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/WhatWellDoforLove.mp3"></audio> 
        		<div class="audioplayer-playpause" lang='player15'>
                    <a href=""></a><div id="title"><span itemprop="name">What We'll Do for Love</span></div> 
                </div>
            </li>  
          </ul>
        </div>
      </div>
      <p class="catagery_tagline" >Kaylee Rutland</p>
      <div class="row catagery_listsec">
        <div class="col-sm-6 catagery_img">
			<img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/img5.jpg" />
			<div class="buttons">
				<a href="https://itunes.apple.com/us/album/kaylee-rutland-ep/id570854312" target="_blank" id="btn-iTunes" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn_itunesBlack.png" alt=""></a>
				<a href="https://play.google.com/store/music/artist/Kaylee_Rutland?id=Alaexl4jf3e7z2r4sygckda7iiy&hl=en" target="_blank" id="btn-android" itemprop="offer"><img src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/catalog_images/btn-android.png" alt=""></a>		
			</div>
		</div>
        <div class="col-sm-6 audio_list">
          <ul>
          	<li class="audioplayer" >
             <audio preload="none" id="player16" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/BoysWillBeBoys.mp3"></audio> 
        		<div class="audioplayer-playpause" lang='player16'>
                    <a href=""></a><div id="title"><span itemprop="name">Boys Will Be boys</span></div> 
                </div>
            </li>  
      
            <li class="audioplayer" >
            <audio preload="none" id="player17" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/HeyBen.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player17'>
                    <a href=""></a><div id="title"><span itemprop="name">Hey Ben</span></div> 
                </div>
            </li> 
            <li class="audioplayer" ><audio preload="none" id="player18" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/DarkHorse.mp3"></audio> 
       
        		<div class="audioplayer-playpause" lang='player18'>
                    <a href=""></a><div id="title"><span itemprop="name">Dark Horse</span></div> 
                </div>
            </li> 
            <li class="audioplayer" ><audio preload="none" id="player19" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/DaddysGota45.mp3"></audio> 
      
        		<div class="audioplayer-playpause" lang='player19'>
                    <a href=""></a><div id="title"><span itemprop="name">Daddy's Got A 45</span></div> 
                </div>
            </li> 
            <li class="audioplayer" >
            <audio preload="none" id="player20" src="<?php echo site_url(); ?>/wp-content/themes/kaylee_merged/MusicCatalog/MyLastOne.mp3"></audio>
        		<div class="audioplayer-playpause" lang='player20'>
                    <a href=""></a><div id="title"><span itemprop="name">My Last One</span></div> 
                </div>
            </li>  
          </ul>
        </div>
      </div>
    </div>
  </div>
  
</div>
<footer>
    <div class="pull-right footer-icons">
        <a class="icon icon-youtube" href="http://www.youtube.com/user/kayleerutland?feature=results_main" target="_blank"></a>
        <a class="icon icon-facebook" href="https://www.facebook.com/KayleeRutlandMusic" target="_blank"></a>
        <a class="icon icon-twitter" href="https://twitter.com/kayleerutland" target="_blank"></a>
        <a class="icon icon-instagram" href="http://instagram.com/kayleerutland" target="_blank"></a>
        <a class="icon icon-pinterest" href="http://www.pinterest.com/kayleerutland/" target="_blank"></a>
        <a style="padding: 6px 11px 2px 11px;line-height: 32px;font-size: 2em;" class="icon fa fa-vine fa-2x" href="https://vine.co/u/948452025249730560" target="_blank"></a>
    </div><!-- /#right -->

    <div class="pull-left" id="copy-right"> 
    	© 2015 Kaylee Rutland Music. All Rights Reserved.
  	</div>
		<a href="/free-download">
			<div class="pull-left" id="home-promotion">
				GET YOUR <span style="color: #02d1d1">FREE</span><br><span style="font-weight:500; font-size:32px; line-height:.2em;">DOWNLOAD</span>
			</div>
		</a>
		<a target="_blank" href="https://www.nashnextcountry.com/bands/Kaylee%20Rutland">
			<div class="pull-left" id="nash">
				<img style="float: left; margin-left: 50px;" src="http://kayleerutland.com/wp-content/themes/kaylee_merged/assets/images/icon-nash.gif" alt="">
				<div style="float: left;display: inline-block;line-height: 1.2em;">BECOME<br><span style="font-size: 35px;">A FAN</span></div>
			</div>
		</a>
</footer>
 
<script type="text/javascript"> 	
 
var this_lang = '';
	$('.audioplayer-playpause').click(function(){ 
		if(this_lang!= ''){
			document.getElementById(this_lang).pause(); 		   
		  	$("#"+ this_lang).prop("currentTime",0);		 
		}
		if($(this).parent().hasClass('audioplayer-playing')){
			console.log(this_lang);
			document.getElementById(this_lang).pause(); 		   
		  	$("#"+ this_lang).prop("currentTime",0);
			$(this).parent().removeClass('audioplayer-playing');
		} else {
			$('.audioplayer').removeClass('audioplayer-playing');
			$(this).parent().addClass('audioplayer-playing');
			this_lang = $(this).attr('lang');
			console.log(this_lang);
			document.getElementById(this_lang).play();
		}		
		
		
		
		//$('#'+this_lang).play;
	});
 $('.audioplayer-playpause a').click(function(){
	 $(this).parent('.audioplayer-playpause').trigger('click');
	return false;	 
 });
</script>
</body>
</html>
