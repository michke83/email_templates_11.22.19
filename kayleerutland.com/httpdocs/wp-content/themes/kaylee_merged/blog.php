<?php
/* 
    Template Name: Blog Page
*/
?>   

<?php get_header(); ?> 
    <div class="content">
        <div id="blog-container" class="light-bg">   
            <div class="dark-bg blog-title"><h1>blog</h1></div>
            
            <div id="main-content">
             
                <?php if (have_posts()) : while(have_posts()) : the_post(); ?> 

                   <article  class="blog-post">

                        <header> 

                            <div class="article-date highlighted"><?php the_time(get_option('date_format')); ?></div>
                            <div class="article-meta-categories"><?php the_category('&nbsp;/&nbsp;')?></div>
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>  

                        </header> 
                        
                        <?php if (has_post_thumbnail()) : ?> 
                            <figure class="article-preview-image">
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
                            </figure>
                        <?php endif; ?>
                                                    
                        <div class="post-start clear"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas gravida purus orci, ut lacinia turpis dignissim nec. Donec sit amet sapien arcu. Aliquam et ligula enim. Quisque libero dolor, bibendum pharetra euismod et, posuere sit amet purus. Duis gravida turpis orci, quis porttitor odio lacinia at. Praesent auctor congue lorem, vel mollis purus venenatis quis. Proin in imperdiet erat, mollis tempor ipsum. In volutpat nibh vitae arcu iaculis vehicula. Nulla hendrerit felis sed eros tincidunt, ut euismod metus gravida. Integer eget odio magna. Aliquam elementum, metus non ultrices vulputate, massa magna rutrum turpis, ut fermentum magna mi eu justo. Integer non placerat sapien. Ut fermentum laoreet tellus et imperdiet.</br></br>
                        Quisque interdum tortor eget purus ultricies, a viverra justo posuere. Nam ac leo id libero ultrices elementum vel ac nulla. Nullam volutpat lectus sapien, in tincidunt nibh tempor quis. Ut dictum ac nulla eget tincidunt. Morbi sed cursus ipsum. Sed porttitor, diam vitae lacinia consequat, dolor mi porta sapien, quis pretium enim elit et risus. Proin vitae neque euismod, vulputate dolor non, viverra arcu. Sed sed orci lorem. Quisque vestibulum ac nisi accumsan gravida. Proin tincidunt congue fermentum.</br></br>
                        Proin et massa ligula. Aliquam tincidunt feugiat mauris a faucibus. Sed adipiscing est ligula, id lobortis ipsum tempor vitae. Duis congue volutpat nunc ac consectetur. In egestas metus nec nulla imperdiet, eu blandit nibh sagittis. Aenean eget malesuada dui, sed aliquam ipsum. Praesent quis turpis sodales, aliquet arcu nec, placerat risus. Sed gravida, tellus non ultrices mattis, eros metus aliquam ante, nec ullamcorper arcu leo ac risus. Phasellus ac purus orci. Fusce sagittis viverra justo, id aliquet quam facilisis eget. Nam ut pretium enim. Fusce et nisl gravida eros viverra ornare at id felis. Fusce id vestibulum elit, vel ornare orci. Phasellus elementum tempus volutpat. Vestibulum sed metus tempus, congue turpis vel, bibendum nisi. Cras vel tristique ante, sed viverra arcu.</br></br>
                        Etiam commodo elit vel ante lacinia interdum. Etiam consectetur, metus vel dictum facilisis, nulla eros bibendum lorem, ut dapibus urna tortor non erat. Quisque sapien turpis, ultrices ac semper non, ullamcorper vel augue. Ut sit amet pulvinar elit. Donec lacinia est suscipit cursus euismod. Phasellus faucibus mauris a quam mattis, sit amet porta sapien aliquet. Vestibulum justo velit, pellentesque id hendrerit at, suscipit quis sem. Pellentesque euismod, turpis nec ultricies malesuada, erat tortor congue dui, id convallis massa massa gravida libero. Duis pretium justo id leo adipiscing, in fringilla justo sagittis. Fusce sit amet blandit arcu.</p> 
                        </div>

                    </article> 

                <?php endwhile; else : ?> 
                    <h1><?php _e('No posts were found!', 'kaylee'); ?></h1> 
                <?php endif; ?> 

                    

                <div class="pagination">
                    <ul>
                        <li><a href="#">Prev</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">...</a></li>
                        <li><a href="#">3</a></li> 
                        <li><a href="#">Next</a></li>
                    </ul>
                </div>

            </div><!-- /main-content -->

            <div id="blog-aside">
                    <div class="blog-widget">
                        
                        <div class="posts-widget" id="recent-posts">
                            <h2>Recent Posts</h2>
                            <ul>
                                <li><a href="#">Post Title 1</a></li>
                                <li><a href="#">Post Title 2</a></li>
                                <li><a href="#">Post Title 3</a></li>
                            </ul>   
                        </div> 

                        <div class="posts-widget" id="recent-comments">
                            <h2>Recent Comments</h2>
                            <p><i>jess@smth.com: </i>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec eleifend metus <a href="#">[...]</a></p>
                        </div>

                        <div class="posts-widget" id="archives">
                        <h2>Archives</h2>
                            <ul>
                                <li><a href="#">March 2014</a></li>
                                <li><a href="#">April 2014</a></li>
                                <li><a href="#">May 2014</a></li>
                            </ul>  
                        </div> 

                        <div class="posts-widget" id="categories">
                            <h2>Categories</h2>
                            <ul>
                                <li><a href="#">Category 1</a></li>
                                <li><a href="#">Category 2</a></li>
                                <li><a href="#">Category 3</a></li>
                            
                            </ul>  
                        </div>  
                    </div>
                <div class="albums-widget">
                    <img src="http://placehold.it/300x300">
                    <img src="http://placehold.it/300x300">
                </div>
            </div><!-- /blog-aside -->
        </div><!-- /blog-container-->
        <div class="clear"></div>
    </div><!-- /content-->
    
    <?php get_template_part( 'footer', 'includes' ); ?>
    
    
    <script>
        
        var temp= "<?php echo IMAGES; ?>";
        $.backstretch(temp + "/bg_events.jpg");

    </script>
<?php get_footer(); ?> 