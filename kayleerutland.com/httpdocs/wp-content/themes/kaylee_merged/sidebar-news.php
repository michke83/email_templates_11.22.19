<?php
/**
 * The Sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>
<div id="blog-aside">
    <div class="schedule-widget">
    	<?php
		$args = array(
			'posts_per_page' => 3, 
			'post_type' => 'event', 
			'post_status' => 'future', 
            'orderby' => 'post_date',
            'order' => 'ASC',
        );

		$futureEvents = get_posts($args);
        if (count($futureEvents) < 3) {
            //get the latest past events 
            $numEvents = 3 - count($futureEvents); 
            $args = array(
                'posts_per_page' => $numEvents, 
                'post_type' => 'event', 
                'post_status' => 'publish', 
                'orderby' => 'post_date',
                'order' => 'DESC',
            );
            $pastEvents = get_posts($args); 

            //merge the past events and future events in one array
            $allEvents = array_merge($pastEvents, $futureEvents);
            $events = array(); 
            foreach($allEvents as $event) {
                $a = (array)$event; 
                $events[$a['post_date']] = $event; 
            }
            //sort latest events in ascending order; 
            $allEvents = ksort($events);
        }

        else {
            $events = $futureEvents; 
        }

		if($events) {?>

        <h1>Schedule</h1>
		<?php
		foreach($events as $event) { ?>
        <div class="posts-widget">
            <h2>
            	<?php echo get_the_time('m.d.y', $event->ID) . " " . $event->post_title; ?>
        	</h2>
            <div class="highlighted">
                <?php echo $event->post_content; ?>
            </div> 
        </div>
        <?php }

        } ?>
    </div>
<!--     <div class="albums-widget">
    	<?php
    	//Pull images meta fields for sidebar from News and Events page
    	$news_page = get_pages(array(
			'meta_key' => '_wp_page_template',
			'meta_value' => 'page-templates/events.php'
		));
    	$side_images = rwmb_meta( 'kaylee_events_img', 'type=image', $news_page[0]->ID );
		foreach ( $side_images as $image )
		{  
            $imageLink = get_post_meta( $image['ID'], 'kaylee_media_link', true );
		    if ($imageLink)
                echo "<a href='$imageLink' target='blank' ><img src='{$image['url']}' /></a>";
            else 
                echo "<img src='{$image['url']}' />";

		}
    	?>
    </div> -->
</div><!-- /blog-aside -->