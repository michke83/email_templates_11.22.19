jQuery(function($){
	
	// News Page Fancybox - Load post content with AJAX
	$( '.grid' ).on( "click", ".news-item .read-more", function(e) {
	  $.post(
			ajax_object.ajaxurl, 
			{
				action: 'news_fancybox',
				post_id: $(this).parents('.grid-content').data('news-id') //Get id of gallery
			}, function(data) {
						
				// Open content in FancyBox
				$.fancybox({
					type:'ajax',
          autoSize: false,
          width: 700,
          maxWidth: '950%',
	        content: data['content'],
	        padding: 20,
	        helpers: {
				    overlay: {
				      locked: false
				    }
				  },
				  afterShow: function() { // prevent scroll on body when open
					  $("body").css("overflow", "hidden");
				  },
				  afterClose: function() {
					  $("body").css("overflow", "visible");
				  }
		    });
		    		    
		    addthis.toolbox('.addthis_toolbox')
		    
			},'json');
		e.preventDefault();
	});
	
	//Fancybox photo gallery in photos section.
	$('.trigger-gallery').click(function () {
		$.post(ajax_object.ajaxurl, {
			action: 'ajax_action',
			post_id: $(this).attr('id') //Get id of gallery
		}, function(data) {
			// Open content in FancyBox	
			console.log(data['result'])
			$.fancybox( data['result'], { padding : 0, maxWidth: 960, width:960 }); //display fancybox
		},'json');
	});	

	// Fancybox for photo gallery
	$(".various").fancybox({
			maxWidth	: 800,
			maxHeight	: 600,
			fitToView	: false,
			width		: '50%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none',
			helpers : {
	        overlay : {
	            locked : true
	        }
	    }
		});
		
		
		(function ($, F) {
    F.transitions.resizeIn = function() {
        var previous = F.previous,
            current  = F.current,
            startPos = previous.wrap.stop(true).position(),
            endPos   = $.extend({opacity : 1}, current.pos);

        startPos.width  = previous.wrap.width();
        startPos.height = previous.wrap.height();

        previous.wrap.stop(true).trigger('onReset').remove();

        delete endPos.position;

        current.inner.hide();

        current.wrap.css(startPos).animate(endPos, {
            duration : current.nextSpeed,
            easing   : current.nextEasing,
            step     : F.transitions.step,
            complete : function() {
                F._afterZoomIn();

                current.inner.fadeIn("fast");
            }
        });
    };

}(jQuery, jQuery.fancybox));
		
	
});

// Infite scroll for news posts
function newsInfiniteScroll() {	
	console.log("fetching posts");
	$('#loading-spinner').show();
	$.post(
		ajax_object.ajaxurl, 
		{
			action: 'news_pagination',
			numItems: $('article').length
		}, function(data) {
			
			var newsItems = $( data['content'] ).filter('article');
			
	    // Append new blocks
	    $('.grid').append( newsItems ).imagesLoaded(function(){
		    $('.grid').masonry( 'appended', newsItems ); // Have Masonry position new blocks
	    });	

			addthis.toolbox('.addthis_toolbox')
			
			// If there are no posts show, hide the loading icon
			if(!data['showMoreButton']) {
				$('#loading-spinner').remove();
			}
			
		},'json');
}