	//Play audio files	
				/*
	** Audio/Video
	*/
	//If < IE9
	if($("html").hasClass('lt-ie9')) {
	$('audio').mediaelementplayer({
	enablePluginDebug: false,
	audioWidth: 300,
	audioHeight: 30,
	features: ['playpause','progress','duration'],
	pluginPath: 'flash/',
	flashName: 'flashmediaelement.swf',
	silverlightName: 'silverlightmediaelement.xap',
	error: function () {
	alert('there was an error');
	}
	});
	} else {
	//IE9 and above
	$('audio').audioPlayer();

	}
	//Do not play if user paused the audio during their last visit
	/*
	if(readCookie('autoplay') !== 'false') {
	$("#footer audio").get(0).volume = .3;
	$("#footer .audioplayer-playpause").trigger('click');
	}
	$("#footer .audioplayer-playpause").click(function() {
	if($(this).attr('title') == 'Play') {
	//Set cookie to disable autoplay
	createCookie('autoplay','false',7) ;
	}
	if($(this).attr('title') == 'Pause') {
	//Erase cookie to enable autoplay
	eraseCookie('autoplay');
	}
	});
	*/ 