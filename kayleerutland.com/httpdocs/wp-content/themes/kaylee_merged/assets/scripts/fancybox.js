module.exports = function() {

	jQuery(function($){

		//Fancybox photo gallery in photos section.
		$('.trigger-gallery').click(function () {
			console.log($(this).attr('id'));
			$.post(ajax_object.ajaxurl, {
				action: 'ajax_action',
				post_id: $(this).attr('id') //Get id of gallery
			}, function(data) {
				$.fancybox( data['result'], { padding : 0 }); //display fancybox
			},'json');
		});
		
		$(".various").fancybox({
				maxWidth	: 800,
				maxHeight	: 600,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none'
			});
		
	});//End of file

}