<!-- Footer -->
<div class="footer-icons container">
	<div class="pull-right">

	    <a target="_blank" href="http://www.youtube.com/user/kayleerutland?feature=results_main" onclick="trackOutboundLink('http://www.youtube.com/user/kayleerutland?feature=results_main'); return false;">
		    <span class="fa-stack fa-lg">
				  <i class="fa fa-circle fa-stack-2x"></i>
				  <i class="fa fa-youtube fa-stack-1x fa-inverse"></i>
				</span>
	    </a>

	    <a target="_blank" href="https://www.facebook.com/KayleeRutlandMusic" onclick="trackOutboundLink('https://www.facebook.com/KayleeRutlandMusic'); return false;">
		    <span class="fa-stack fa-lg">
				  <i class="fa fa-circle fa-stack-2x"></i>
				  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
				</span>
	    </a>

	    <a target="_blank" href="https://twitter.com/kayleerutland" onclick="trackOutboundLink('https://twitter.com/kayleerutland'); return false;">
		    <span class="fa-stack fa-lg">
				  <i class="fa fa-circle fa-stack-2x"></i>
				  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
				</span>
	    </a>

	    <a target="_blank" href="http://instagram.com/kayleerutland" onclick="trackOutboundLink('http://instagram.com/kayleerutland'); return false;">
		    <span class="fa-stack fa-lg">
				  <i class="fa fa-circle fa-stack-2x"></i>
				  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
				</span>
	    </a>

	    <a target="_blank" href="https://vine.co/u/948452025249730560" onclick="trackOutboundLink('https://vine.co/u/948452025249730560'); return false;">
		    <span class="fa-stack fa-lg">
				  <i class="fa fa-circle fa-stack-2x"></i>
				  <i class="fa fa-vine fa-stack-1x fa-inverse"></i>
				</span>
	    </a>

	    <a target="_blank" href="https://itun.es/us/QUObI" onclick="trackOutboundLink('https://itun.es/us/QUObI'); return false;">
		    <span class="fa-stack fa-lg">
				  <i class="fa fa-circle fa-stack-2x"></i>
				  <i class="fa fa-apple fa-stack-1x fa-inverse"></i>
				</span>
	    </a>

	    <a target="_blank" href="https://open.spotify.com/artist/1VeGEGCgaYtcEe0t61swGr" onclick="trackOutboundLink('https://open.spotify.com/artist/1VeGEGCgaYtcEe0t61swGr'); return false;">
		    <span class="fa-stack fa-lg follow-btn">
				  <!-- <i class="fa fa-circle fa-stack-2x"></i> -->
				  <div class="spotify-div">
				    <i class="fa fa-spotify fa-stack-1x fa-inverse"></i>
				    <p id="follow-text">FOLLOW</p>
				  </div>
				</span>
	    </a>
	</div>
</div>

<footer>
	<p>&copy; <?php echo date('Y'); ?> Kaylee Rutland Music. All Rights Reserved.</p>
</footer><!-- /#footer -->


<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php print THEMEROOT?>/assets/scripts/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="<?php print THEMEROOT?>/assets/javascripts/vendor/jquery.fullPage.min.js"></script>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5465185c08a210bd" async="async"></script>
