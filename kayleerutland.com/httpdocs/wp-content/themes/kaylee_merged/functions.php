<?php
/* Define Constants */
define('THEMEROOT', get_stylesheet_directory_uri());
define('IMAGES', THEMEROOT . '/assets/images');

//Register Menu
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_my_menu' );

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

function process_contact_form() {
    if($_POST['form_contact']) {

        $headers = 'From: kayleerutland.com \r\n';

        if(isset($_POST['contact-concerning'])) {

	    $select1 = $_POST['contact-concerning'];
		    switch ($select1) {
		        case 'general':
		           $email = 'webmaster@kayleerutland.com';

		            break;
		        case 'media-kit':
		           $email = 'kayleerutlandmusic@gmail.com';

		            break;
		        case 'press':
		            $email = 'webmaster@kayleerutland.com';

		            break;
		        default:
		            $email = 'webmaster@kayleerutland.com';
		            break;
		    }
		}

        $message = "Contact form submitted on Kayleerutland.com\r\n\r\n";
        $message .= "Message Concerning: " . $_POST['contact-concerning'] . "\r\n";
        $message .= "Name: " . $_POST['contact-name'] . "\r\n";
        $message .= "Email: " . $_POST['contact-email'] . "\r\n";
        $message .= "Message: " . $_POST['contact-message'] . "\r\n\r\n";

      $mail_sent = mail($email, 'KayleeRutland.com Form Submission', $message, $headers );

        if($mail_sent) {

        	$title = get_the_title();
        	$content = get_the_content();

      	} else {

          	$title = "Error";
          	$content = '<h2>Message failed. Please call our offices for assistance: 972.899.5000</h2>';

        }

        $response = '<h3>Thank you!</h3>';
        echo json_encode($response);
        exit;

    }
    //Process Newsleter Form
    if($_POST['form_newsletter']) {

        $contactName = $_POST['contact-first-name'] . " " .  $_POST['contact-last-name'] ;
        $contact_post = array(
            'post_title'      => $contactName,
            'post_content'    => $_POST['contact-message'],
            'post_type'       => 'contact',
        );
        $new_post = wp_insert_post($contact_post, $wp_error);
        $email = $_POST['contact-email'];
        $optin = '1';
        $type = 'newsletterForm';


        $merge_vars = array('FNAME'=> $_POST['contact-first-name'], 'LNAME' => $_POST['contact-last-name']);
        update_post_meta($new_post, 'kaylee_contactEmail', $email);
        update_post_meta($new_post, 'kaylee_contactNewsletter', $optin);
        update_post_meta($new_post, 'kaylee_contactType', $type);

        include 'helpers/Mailchimp.php';
        $MailChimp = new MailChimp('cfd4f00587480287dbc83e0c6ba37449-us6');
        $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => '6c8927d8d5',
                        'email'             => array('email'=>$email),
                        'double_optin'      => true,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                        'merge_vars'        => Array(
												    'FNAME' => $_POST['contact-first-name'],
												    'LNAME' => $_POST['contact-last-name']
												)
                    ));

        $response = '<h3>Thank You For Subscribing!</h3><p>You should be receiving an email asking you to confirm your subscription shortly.<br/> After you confirm, you\'re all set!<p/> ';
        echo json_encode($response);
        exit;

    }
}
function process_free_download_form() {
  if($_POST['form_free_download']) {
    $contactName = $_POST['contact-first-name'] . " " . $_POST['contact-last-name'] ;
    $contact_post = array(
        'post_title'      => $contactName,
        'post_content'    => $_POST['contact-message'],
        'post_type'       => 'contact',
    );
    $new_post = wp_insert_post($contact_post, $wp_error);
    $email = $_POST['contact-email'];
    $optin = $_POST['contact-newsletter'];
    $type = 'freeDownloadForm';
    update_post_meta($new_post, 'kaylee_contactEmail', $email);
    update_post_meta($new_post, 'kaylee_contactNewsletter', $optin);
    update_post_meta($new_post, 'kaylee_contactType', $type);

    $attachments = array(WP_CONTENT_DIR . '/uploads/2015/03/Found-It-In-You.mp3');

    $headers = 'From: Kaylee Rutland <noreply@kayleerutland.com>' . "\r\n";

    $message = "<html><body><p>Hi " . $_POST['contact-first-name'] . ",";
    $message .="<p>Thank you so much for downloading my single Found It In You! I hope you enjoy the song and can’t wait to hear what you think. For updates on performances in your area, new music, music videos and more check out my website or follow me on Facebook, Twitter and YouTube! Thanks again!<br/><br/>";
    $message .= '<img src="http://staging.ivieinc.com/KayleeSignature.png">';
    $message .= "<p>Kaylee Rutland</p>";
    $message .= "<p>Billboard Country Artist to Watch</p>";
    $message .= "<p>www.KayleeRutland.com</p>";
    $message .= '<a href="http://www.facebook.com/KayleeRutlandMusic">Facebook</a> | <a href="https://twitter.com/kayleerutland">Twitter</a> | <a href="https://www.youtube.com/user/kayleerutland?feature=results_main">Youtube</a>';
    $message .= "</p></body></html>";

    add_filter('wp_mail_content_type',create_function('', 'return "text/html"; '));

    $sent_success = wp_mail($email, 'Your Kaylee Rutland Free Song Is Here', $message, $headers, $attachments );


    // Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
    remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

       include 'helpers/Mailchimp.php';

        if($_POST['contact-newsletter'] == 1) {
        $MailChimp = new MailChimp('cfd4f00587480287dbc83e0c6ba37449-us6');
        $result = $MailChimp->call('lists/subscribe', array(
                        'id'                => '6c8927d8d5',
                        'email'             => array('email'=>$email),
                        'double_optin'      => true,
                        'update_existing'   => true,
                        'replace_interests' => false,
                        'send_welcome'      => false,
                        'merge_vars'        => Array(
												    'FNAME' => $_POST['contact-first-name'],
												    'LNAME' => $_POST['contact-last-name']
												)
                    ));
        } // end if newsletter checked

   }  // end if form is submitted
}

include 'meta-box.php';//include file for metabox plugin
add_theme_support( 'post-thumbnails' ); //add featured images

//Use custom logo for login
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo-login.png);
            padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

function my_login_stylesheet() {
    wp_enqueue_style( 'custom-login', get_template_directory_uri() . '/css/style-login.css' );
}
add_action( 'login_enqueue_scripts', 'my_login_stylesheet' );

/**
 * Add a widget to the dashboard.
 *
 * This function is hooked into the 'wp_dashboard_setup' action below.
 */
function example_add_dashboard_widgets() {

	wp_add_dashboard_widget(
                 'example_dashboard_widget',         // Widget slug.
                 'Kaylee Rutland Quick Links',         // Title.
                 'example_dashboard_widget_function' // Display function.
        );
}
add_action( 'wp_dashboard_setup', 'example_add_dashboard_widgets' );

/**
 * Create the function to output the contents of our Dashboard Widget.
 */
function example_dashboard_widget_function() {

	// Display whatever it is you want to show.
	echo "<a style='color: white;display:inline-block; padding: 10px 20px; background: #492933; border-radius: 5px;margin: 5px;' href='/wp-admin/edit.php'>Edit Blog Posts</a>";
	echo "<a style='color: white;display:inline-block; padding: 10px 20px; background: #492933; border-radius: 5px;margin: 5px;' href='/wp-admin/edit.php?post_type=news'>Edit News</a>";
	echo "<a style='color: white;display:inline-block; padding: 10px 20px; background: #492933; border-radius: 5px;margin: 5px;' href='/wp-admin/edit.php?post_type=event'>Edit Events</a>";
	echo "<a style='color: white;display:inline-block; padding: 10px 20px; background: #492933; border-radius: 5px;margin: 5px;' href='/wp-admin/edit.php?post_type=gallery'>Edit Galleries</a>";
	echo "<a style='color: white;display:inline-block; padding: 10px 20px; background: #492933; border-radius: 5px;margin: 5px;' href='/wp-admin/media-new.php'>Upload Music File</a>";
}


add_filter( 'redirect_canonical', 'my_disable_redirect_canonical' );

function my_disable_redirect_canonical( $redirect_url ) {

	if ( is_singular( 'client' ) )
		$redirect_url = false;

	return $redirect_url;
}


//Register post types

function kaylee_post_types() {

//Register News post type
register_post_type('news', array(	'label' => 'News','description' => 'News','public' => true,'show_ui' => true,'show_in_menu' => true,'menu_position' => 6,'capability_type' => 'post','hierarchical' => false, 'has_archive' => false,'rewrite' => array('slug' => 'news'),'query_var' => true,'supports' => array( 'title', 'editor', 'thumbnail', 'excerpt' ),'exclude_from_search' => 'false', 'labels' => array (
  'name' => 'News',
  'singular_name' => 'News Item',
  'menu_name' => 'News',
  'add_new' => 'Add News Item',
  'add_new_item' => 'Add News Item',
  'edit' => 'Edit',
  'edit_item' => 'Edit News Item',
  'new_item' => 'New News Item',
  'view' => 'View News Item',
  'view_item' => 'View News Item',
  'search_items' => 'Search News Item',
  'not_found' => 'No Info Found',
  'not_found_in_trash' => 'No News In Trash',
  'parent' => '',
),) );

//Register News post type
register_post_type('video', array( 'label' => 'video','description' => 'video','public' => true,'show_ui' => true,'show_in_menu' => true,'menu_position' => 6,'capability_type' => 'post','hierarchical' => false, 'has_archive' => false,'rewrite' => array('slug' => 'video'),'query_var' => true,'supports' => array( 'title', 'editor' ),'exclude_from_search' => 'true', 'labels' => array (
  'name' => 'Self-hosted Videos',
  'singular_name' => 'News video',
  'menu_name' => 'Self-hosted Videos',
  'add_new' => 'Add video',
  'add_new_item' => 'Add video',
  'edit' => 'Edit',
  'edit_item' => 'Edit video',
  'new_item' => 'New video',
  'view' => 'View video',
  'view_item' => 'View video',
  'search_items' => 'Search video',
  'not_found' => 'No Info Found',
  'not_found_in_trash' => 'No videos In Trash',
  'parent' => '',
),) );

//Register Events post type

register_post_type('event', array(	'label' => 'Events','description' => 'Events','public' => true,'show_ui' => true,'show_in_menu' => true,'menu_position' => 10,'capability_type' => 'post','hierarchical' => false,'rewrite' => array('slug' => ''),'query_var' => true,'supports' => array( 'title', 'editor' ),'exclude_from_search' => 'false', 'labels' => array (
  'name' => 'Events',
  'singular_name' => 'Event',
  'menu_name' => 'Events',
  'add_new' => 'Add Event',
  'add_new_item' => 'Add Event',
  'edit' => 'Edit',
  'edit_item' => 'Edit Event',
  'new_item' => 'New Event',
  'view' => 'View Event',
  'view_item' => 'View Event',
  'search_items' => 'Search Event',
  'not_found' => 'No Info Found',
  'not_found_in_trash' => 'No Events In Trash',
  'parent' => '',
),) );

//Register Events post type

register_post_type('contact', array(  'label' => 'Contacts','description' => 'Contacts','public' => true,'show_ui' => true,'show_in_menu' => true,'menu_position' => 10,'capability_type' => 'post','hierarchical' => false,'rewrite' => array('slug' => ''),'query_var' => true,'supports' => array( 'title', 'editor' ),'exclude_from_search' => 'false', 'labels' => array (
  'name' => 'Contacts',
  'singular_name' => 'Contact',
  'menu_name' => 'Contacts',
  'add_new' => 'Add Contact',
  'add_new_item' => 'Add Contact',
  'edit' => 'Edit',
  'edit_item' => 'Edit Contact',
  'new_item' => 'New Contact',
  'view' => 'View Contacts',
  'view_item' => 'View Contact',
  'search_items' => 'Search Contacts',
  'not_found' => 'No Info Found',
  'not_found_in_trash' => 'No Contacts In Trash',
  'parent' => '',
),) );


//Register Gallery post type

register_post_type('gallery', array(	'label' => 'Galleries','description' => 'Galleries','public' => true,'show_ui' => true,'show_in_menu' => true,'menu_position' => 10,'capability_type' => 'post','hierarchical' => false,'rewrite' => array('slug' => ''),'query_var' => true,'supports' => array( 'title', 'editor', 'thumbnail','page-attributes' ),'exclude_from_search' => 'false', 'labels' => array (
  'name' => 'Galleries',
  'singular_name' => 'Gallery',
  'menu_name' => 'Galleries',
  'add_new' => 'Add Gallery',
  'add_new_item' => 'Add Gallery',
  'edit' => 'Edit',
  'edit_item' => 'Edit Gallery',
  'new_item' => 'New Gallery',
  'view' => 'View Gallery',
  'view_item' => 'View Gallery',
  'search_items' => 'Search Gallery',
  'not_found' => 'No Info Found',
  'not_found_in_trash' => 'No Galleries In Trash',
  'parent' => '',
),) );

//Register Album post type

register_post_type('album', array(  'label' => 'Albums','description' => 'Albums','public' => true,'show_ui' => true,'show_in_menu' => true,'menu_position' => 10,'capability_type' => 'post','hierarchical' => false,'rewrite' => array('slug' => ''),'query_var' => true,'supports' => array( 'title', 'editor', 'thumbnail','page-attributes' ),'exclude_from_search' => 'false', 'labels' => array (
  'name' => 'Albums',
  'singular_name' => 'Album',
  'menu_name' => 'Albums',
  'add_new' => 'Add Album',
  'add_new_item' => 'Add Album',
  'edit' => 'Edit',
  'edit_item' => 'Edit Album',
  'new_item' => 'New Album',
  'view' => 'View Album',
  'view_item' => 'View Album',
  'search_items' => 'Search Album',
  'not_found' => 'No Info Found',
  'not_found_in_trash' => 'No Albums In Trash',
  'parent' => '',
),) );
}

add_action( 'init', 'kaylee_post_types' );

/* ==================================================================
           _              ______                _   _
     /\   (_)            |  ____|              | | (_)
    /  \   _  __ ___  __ | |__ _   _ _ __   ___| |_ _  ___  _ __  ___
   / /\ \ | |/ _` \ \/ / |  __| | | | '_ \ / __| __| |/ _ \| '_ \/ __|
  / ____ \| | (_| |>  <  | |  | |_| | | | | (__| |_| | (_) | | | \__ \
 /_/    \_\ |\__,_/_/\_\ |_|   \__,_|_| |_|\___|\__|_|\___/|_| |_|___/
         _/ |
        |__/
================================================================== */

function enqueue_scripts_styles_init() {
	wp_enqueue_script( 'ajax-script', get_stylesheet_directory_uri().'/assets/scripts/main.js', array('jquery'), 1.0 ); // jQuery will be included automatically
	wp_localize_script( 'ajax-script', 'ajax_object', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) ); // setting ajaxurl
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts_styles_init' );
//add_action('init', 'enqueue_scripts_styles_init');

//For Photos section - get photo attachments of gallery from the post id
function ajax_action_stuff() {
		$post_id = $_POST['post_id']; // get variables from ajax post
    $images = rwmb_meta( 'kaylee_gallery', 'type=image', $post_id ); //Get image ids from custom metabox
    $img_array = array(); //Load image urls into array to pass back to fancybox
	foreach ( $images as $image )
	{
		$img_array[] = array(
			'href' => $image['full_url'],
			'title' => $image['caption'],
		);
		
	}
	$return = array('result' => $img_array);
	echo json_encode($return);
	die(); // stop executing script
}
add_action( 'wp_ajax_ajax_action', 'ajax_action_stuff' ); // ajax for logged in users
add_action( 'wp_ajax_nopriv_ajax_action', 'ajax_action_stuff' ); // ajax for not logged in users




/* ================ News Item FancyBox  ================= */

function ajax_news_fancybox() {

	$post = get_post($_POST['post_id']);
	$featured_img = get_the_post_thumbnail($post->ID, 'full', array('class' => 'featured-img img-responsive'));
	$post_date = get_the_date('F, d Y', $post->ID);

	// Build Content
	$content = '<div class="news-content">';
	$content .= $featured_img;
	$content .= '<h2>' . $post->post_title . '</h2>';
	$content .= '<h4>' . $post_date . '</h4>';
	$content .= $post->post_content;
	$content .= '<div class="share-news addthis_toolbox">';
	$content .= '<span>Share</span>';
	$content .= '<a addthis:url="' . get_post_permalink($post->ID) .'" addthis:title="' . $post->post_title . '" class="addthis_button_facebook"><i class="fa fa-facebook fa-2x"></i></a>';
	$content .= '<a addthis:url="' . get_post_permalink($post->ID) .'" addthis:title="' . $post->post_title . '" class="addthis_button_twitter"><i class="fa fa-twitter fa-2x"></i></a>';
	$content .= '</div>';
	$content .= '</div>';

	$return = array('content' =>  $content);
	echo json_encode($return);
	die(); // stop executing script
}
add_action( 'wp_ajax_news_fancybox', 'ajax_news_fancybox' ); // ajax for logged in users
add_action( 'wp_ajax_nopriv_news_fancybox', 'ajax_news_fancybox' ); // ajax for not logged in users




/* ================ News Page Pagination ================= */

function ajax_news_pagination() {

	ob_start();

	// query News posts
	$args = array(
		'posts_per_page' => 10,
		'offset' => $_POST['numItems'],
		'post_type' => 'news',
		'orderby' => 'date',
		'order' => 'DESC'
	);
	$news_items = query_posts($args);

	while (have_posts()) : the_post(); ?>

     <?php get_template_part( 'loop', 'news' ); ?>

  <?php endwhile;
  wp_reset_query();

	// Determine if there are more posts to show - if not then hide Load More button
	$news_object = wp_count_posts( 'news' );
  $news_count = $news_object->publish;

  $showMoreButton = false;
  if($news_count > ($_POST['numItems'] + 10)) {
	  $showMoreButton = true;
  }

  $return = array('content' =>  ob_get_clean(), 'showMoreButton' => $showMoreButton);

	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
      echo json_encode($return);
  }
  else {
      header("Location: ".$_SERVER["HTTP_REFERER"]);
  }

	die(); // stop executing script

}
add_action( 'wp_ajax_news_pagination', 'ajax_news_pagination' ); // ajax for logged in users
add_action( 'wp_ajax_nopriv_news_pagination', 'ajax_news_pagination' ); // ajax for not logged in users

// Excerpt Customization

function new_excerpt_more( $more ) {
  return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

function custom_unixtimesamp ( $post_id ) {
    if ( get_post_type( $post_id ) == 'events' ) {
  $startdate = get_post_meta($post_id, 'event_date_begins', true);

    if($startdate) {
      $dateparts = explode('/', $startdate);
      $newdate1 = strtotime(date('d.m.Y H:i:s', strtotime($dateparts[1].'/'.$dateparts[0].'/'.$dateparts[2])));
      update_post_meta($post_id, 'unixstartdate', $newdate1  );
    }
  }
}
add_action( 'save_post', 'custom_unixtimesamp', 100, 2);

// Add SSL to select pages
function force_ssl($force_ssl, $id = 0) {
	// A list of posts that should be SSL
	$ssl_posts = array(392, 44); //Only for live site

	if(in_array($id, $ssl_posts)) {
		$force_ssl = true;
	}
    return $force_ssl;
}
add_filter('force_ssl' , 'force_ssl', 1, 3);
