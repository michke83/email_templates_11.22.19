<?php
/*=================================================
=================================================
Custom Field Plugin using Meta Box
Documentation: http://www.deluxeblogtips.com/meta-box/getting-started/
=================================================
=================================================*/

$prefix = "kaylee_";

/*=================================================
			Add link to audio library
=================================================*/

global $meta_box_attachment;

$meta_box_attachment = array();
$meta_box_attachment[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'media_link',
	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'File Information', 'rwmb' ),
	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'attachment'),
	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',
	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',
	// Auto save: true, false (default). Optional.
	'autosave' => true,
// List of meta fields
	'fields' => array(
		// TEXT
		array(
			// Field name - Will be used as label
			'name'  => __( 'Link for file', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}media_link",
			// Field description (optional)
			'desc'  => __( 'Insert URL', 'rwmb' ),
			'type'  => 'text',
			// Default value (optional)
			'std'   => __( 'Insert URL', 'rwmb' ),
		),
		
		// TEXT
		array(
			// Field name - Will be used as label
			'name'  => __( 'Album Order (for audio files only)', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}media_order",
			// Field description (optional)
			'desc'  => __( 'Insert Letter, ie. a,b,c', 'rwmb' ),
			'type'  => 'text',
		),
		
		// TEXT - Music Video Link
		array(
			// Field name - Will be used as label
			'name'  => __( 'Link to music video (for audio files only)', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}video_link",
			// Field description (optional)
			'desc'  => __( 'Required format example: https://youtube.com/embed/CChToWWXWkQ?autoplay=1', 'rwmb' ),
			'type'  => 'text',
		),
		
		// WYSIWYG - Lyrics
		array(
			// Field name - Will be used as label
			'name'  => __( 'Song Lyrics', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}lyrics",
			// Field description (optional)
			'desc'  => __( 'Insert song lyrics', 'rwmb' ),
			'type'  => 'wysiwyg',
		),
	)

);

function rw_register_meta_boxes_gallery()
{
	global $meta_box_attachment;
	
	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	foreach ( $meta_box_attachment as $meta )
	{
		new RW_Meta_Box( $meta );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_gallery' );

/*========================================================================
			Add the contacts from the free-download and contact us page
=========================================================================*/

global $meta_box_gallery;

$meta_box_gallery = array();
$meta_box_gallery[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'contacts',
	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'contacts', 'rwmb' ),
	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'free-download', 'free-download-thanks', 'contact', 'contact-thanks'),
	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',
	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',
	// Auto save: true, false (default). Optional.
	'autosave' => true,
// List of meta fields
	'fields' => array(
		// TEXT
		array(
			// Field name - Will be used as label
			'name'  => __( 'Email', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}contactEmail",
			// Field description (optional)
			'desc'  => __( 'Text description', 'rwmb' ),
			'type'  => 'text',
			// Default value (optional)
		),
		
		// TEXT
		array(
			// Field name - Will be used as label
			'name'  => __( 'Newsletter', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}contactNewsletter",
			// Field description (optional)
			'desc'  => __( 'Text description', 'rwmb' ),
			'type'  => 'checkbox',
		),

		// TEXT
		array(
			// Field name - Will be used as label
			'name'  => __( 'Type', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}contactType",
			// Field description (optional)
			'desc'  => __( 'Text description', 'rwmb' ),
			'type'  => 'text',
		),
	)

);

function rw_register_meta_boxes_contacts()
{
	global $meta_box_contacts;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	foreach ( $meta_box_contacts as $meta )
	{
		new RW_Meta_Box( $meta );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_contacts' );



/*=================================================
			Add Video to video page
=================================================*/
global $meta_boxes;
$meta_boxes   = array();
$meta_boxes[] = array(
	'id'     => 'videos',
	'title'  => 'Video Links',
	'pages'  => array( 'page' ),
	'fields' => array(

		// TEXT
			array(
				// Field name - Will be used as label
				'name'  => __( 'Video Links', 'rwmb' ),
				// Field ID, i.e. the meta key
				'id'    => "{$prefix}videos",
				// Field description (optional)
				'desc'  => __( 'ie. https://www.youtube.com/watch?v=CChToWWXWkQ', 'rwmb' ),
				'type'  => 'text',
				// Default value (optional)
				'std'   => __( 'Insert youtube video link here', 'rwmb' ),
				// CLONES: Add to make the field cloneable (i.e. have multiple value)
				'clone' => true,
			),
	),
);

function rw_register_meta_boxes()
{
	global $meta_boxes;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	// Register meta boxes only for some posts/pages
	if ( ! rw_maybe_include() )
		return;

	foreach ( $meta_boxes as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes' );

function rw_maybe_include()
{
	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN )
		return false;

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		return true;

	// Check for post IDs
	$checked_post_IDs = array( );

	if ( isset( $_GET['post'] ) )
		$post_id = $_GET['post'];
	elseif ( isset( $_POST['post_ID'] ) )
		$post_id = $_POST['post_ID'];
	else
		$post_id = false;

	$post_id = (int) $post_id;

	if ( in_array( $post_id, $checked_post_IDs ) )
		return true;

	// Check for page template
	$checked_templates = array( 'page-templates/videos.php' );


	$template = get_post_meta( $post_id, '_wp_page_template', true );
	if ( in_array( $template, $checked_templates ) )
		return true;

	// If no condition matched
	return false;
}




/*=================================================
			Add Images to News and Events Page
=================================================*/
global $meta_boxes_events;
$meta_boxes_events   = array();
$meta_boxes_events[] = array(
	'id'     => 'event_news_img',
	'title'  => 'Side Images',
	'pages'  => array( 'page' ),
	'fields' => array(

		// IMAGE ADVANCED (WP 3.5+)
		array(
			'name'             => __( 'Side Images', 'rwmb' ),
			'id'               => "{$prefix}events_img",
			'type'             => 'image_advanced',
			'max_file_uploads' => 2,
		),	
	),
);

function rw_register_meta_boxes_events()
{
	global $meta_boxes_events;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	// Register meta boxes only for some posts/pages
	if ( ! rw_maybe_include_events() )
		return;

	foreach ( $meta_boxes_events as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_events' );

function rw_maybe_include_events()
{
	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN )
		return false;

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		return true;

	// Check for post IDs
	$checked_post_IDs = array( );

	if ( isset( $_GET['post'] ) )
		$post_id = $_GET['post'];
	elseif ( isset( $_POST['post_ID'] ) )
		$post_id = $_POST['post_ID'];
	else
		$post_id = false;

	$post_id = (int) $post_id;

	if ( in_array( $post_id, $checked_post_IDs ) )
		return true;

	// Check for page template
	$checked_templates = array( 'page-templates/events.php' );

	$template = get_post_meta( $post_id, '_wp_page_template', true );
	if ( in_array( $template, $checked_templates ) )
		return true;

	// If no condition matched
	return false;
}


/*=================================================
			Image Gallery for Photos Page
=================================================*/
global $meta_box_photo_gallery;
$meta_box_photo_gallery = array();

// 1st meta box
$meta_box_photo_gallery[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'photo_gallery',

	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Photo Gallery Images', 'rwmb' ),

	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'gallery'),

	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',

	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',

	// Auto save: true, false (default). Optional.
	'autosave' => true,

// List of meta fields
	'fields' => array(
		// IMAGE ADVANCED (WP 3.5+)
		array(
			'name'             => __( 'Photo Gallery', 'rwmb' ),
			'id'               => "{$prefix}gallery",
			'type'             => 'image_advanced',
		),
	)

);

function rw_register_meta_boxes_photo_gallery()
{
	global $meta_box_photo_gallery;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	foreach ( $meta_box_photo_gallery as $meta )
	{
		new RW_Meta_Box( $meta );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_photo_gallery' );

/*=================================================
			Home Page - Youtube Video and Album
=================================================*/
global $meta_boxes_home;
$meta_boxes_home   = array();
$meta_boxes_home[] = array(
	'id'     => 'home_boxes',
	'title'  => 'Video and Album Image',
	'pages'  => array( 'page' ),
	'fields' => array(

		// TEXT
		array(
			// Field name - Will be used as label
			'name'  => __( 'Video Link', 'rwmb' ),
			// Field ID, i.e. the meta key
			'id'    => "{$prefix}home_video",
			// Field description (optional)
			'desc'  => __( 'ie. https://www.youtube.com/watch?v=CChToWWXWkQ', 'rwmb' ),
			'type'  => 'text',
			// Default value (optional)
			'std'   => __( 'Insert youtube video link here', 'rwmb' ),
		),


		// IMAGE ADVANCED (WP 3.5+)
		array(
			'name'             => __( 'Album Image', 'rwmb' ),
			'id'               => "{$prefix}home_album",
			'type'             => 'image_advanced',
			'max_file_uploads' => 1,
		),	
	),
);

function rw_register_meta_boxes_home()
{
	global $meta_boxes_home;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	// Register meta boxes only for some posts/pages
	if ( ! rw_maybe_include_home() )
		return;

	foreach ( $meta_boxes_home as $meta_box )
	{
		new RW_Meta_Box( $meta_box );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_home' );

function rw_maybe_include_home()
{
	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN )
		return false;

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX )
		return true;

	// Check for post IDs
	$checked_post_IDs = array( );

	if ( isset( $_GET['post'] ) )
		$post_id = $_GET['post'];
	elseif ( isset( $_POST['post_ID'] ) )
		$post_id = $_POST['post_ID'];
	else
		$post_id = false;

	$post_id = (int) $post_id;

	if ( in_array( $post_id, $checked_post_IDs ) )
		return true;

	// Check for page template
	$checked_templates = array( 'page-templates/home.php' );

	$template = get_post_meta( $post_id, '_wp_page_template', true );
	if ( in_array( $template, $checked_templates ) )
		return true;

	// If no condition matched
	return false;
}

//adds select box to the albums page 
global $meta_box_album;

$audio_args = array(
	'posts_per_page'   => -1,
	'orderby'          => 'post_date',
	'order'            => 'DESC',
	'post_type'        => 'attachment',
	'post_mime_type'   => 'audio/mpeg',
);

$audio_files = get_posts($audio_args);

$audio_array = array();
foreach($audio_files as $audio_file) {
	$audio_array[$audio_file->ID] = $audio_file->post_title;
}
$meta_box_album = array();
$meta_box_album[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'album_songs',
	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Link for file', 'rwmb' ),
	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'album'),
	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',
	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',
	// Auto save: true, false (default). Optional.
	'autosave' => true,
// List of meta fields
	'fields' => array(
		//SELECT
		array(
			'name'     => __( 'Select', 'rwmb' ),
			'id'       => "{$prefix}select",
			'type'     => 'select',
			// Array of 'value' => 'Label' pairs for select box
			'options'  => $audio_array,
			// Select multiple values, optional. Default is false.
			'multiple'    => true,
			'std'         => 'value2',
			'placeholder' => __( 'Select an Item', 'rwmb' ),
		),
	)

);

$meta_box_album[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'album_links',
	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Links for songs', 'rwmb' ),
	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'album'),
	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',
	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',
	// Auto save: true, false (default). Optional.
	'autosave' => true,
// List of meta fields
	'fields' => array(
		//iTunes
		array(
			'name'     => __( 'iTunes link', 'rwmb' ),
			'id'       => "{$prefix}album_itunes",
			'type'     => 'text',

		),
		
		//Amazon
		array(
			'name'     => __( 'Amazon link', 'rwmb' ),
			'id'       => "{$prefix}album_amazon",
			'type'     => 'text',

		),

		//Google Play Store
		array(
			'name'     => __( 'Google Play link', 'rwmb' ),
			'id'       => "{$prefix}album_google",
			'type'     => 'text',
			
		),
	)

);

function rw_register_meta_boxes_album()
{
	global $meta_box_album;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	foreach ( $meta_box_album as $meta )
	{
		new RW_Meta_Box( $meta );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_album' );
//! END adds select box to the albums page 
$meta_box_eventplace[] = array(
	// Meta box id, UNIQUE per meta box. Optional since 4.1.5
	'id' => 'event_place',
	// Meta box title - Will appear at the drag and drop handle bar. Required.
	'title' => __( 'Event Details', 'rwmb' ),
	// Post types, accept custom post types as well - DEFAULT is array('post'). Optional.
	'pages' => array( 'event'),
	// Where the meta box appear: normal (default), advanced, side. Optional.
	'context' => 'normal',
	// Order of meta box: high (default), low. Optional.
	'priority' => 'high',
	// Auto save: true, false (default). Optional.
	'autosave' => true,
// List of meta fields
	'fields' => array(
		//Event Venue
		array(
			'name'    => __( 'Event Venue', 'rwmb' ),
			'id'       => "{$prefix}event_venue",
			'type'     => 'text',

		),
		array(
			'name'    => __( 'Event Location', 'rwmb' ),
			'id'       => "{$prefix}event_location",
			'type'     => 'text',

		),
		array(
			'name'    => __( 'Facebook Share Title', 'rwmb' ),
			'id'       => "{$prefix}event_facebook",
			'type'     => 'text',

		),
		array(
			'name'    => __( 'Twitter Share Title', 'rwmb' ),
			'id'       => "{$prefix}event_twitter",
			'type'     => 'text',

		),
	)

);

function rw_register_meta_boxes_eventplace()
{
	global $meta_box_eventplace;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( ! class_exists( 'RW_Meta_Box' ) )
		return;

	foreach ( $meta_box_eventplace as $meta )
	{
		new RW_Meta_Box( $meta );
	}
}

add_action( 'admin_init', 'rw_register_meta_boxes_eventplace' );
?>