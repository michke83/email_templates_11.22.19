<?php
/**
 * Template Name: Photo Gallery
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
        <br><br><br>
        
        <h1>Images</h1>
		<?php
		
		$args = array(
			'posts_per_page'   => -1,
			'post_type'        => 'gallery',
			'post_status'      => 'publish',
		);
		$galleries = get_posts($args);
		foreach($galleries as $gallery) {
			echo '<h1>' . $gallery->post_title . ' ' . $gallery->ID . '</h2>';
			echo '<img class="trigger-gallery" id="' . $gallery->ID . '" src="' . wp_get_attachment_url( get_post_thumbnail_id($gallery->ID) ) . '"/>';
		}
		?>               
		</div>
	</div>
</div>


<?php get_footer(); ?>
