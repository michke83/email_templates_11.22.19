<?php
/**
 * Template Name: Home Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php 
global $post;
$video =  get_post_meta( $post->ID, 'kaylee_home_video', true );
echo $video;
$video_id =  ltrim(strstr($video, '='), '='); // get youtube video id from link
?>

<!-- <iframe width="560" height="315" src="//www.youtube.com/embed/<?php echo $video_id; ?>?autoplay=1&controls=0&volume=0" frameborder="0" allowfullscreen></iframe> -->

<script src="http://www.google.com/jsapi"></script>
<script src="http://ajax.googleapis.com/ajax/libs/swfobject/2.1/swfobject.js"></script>
<div id="youtubeNosound"></div>
<script type="text/javascript">
google.load("swfobject", "2.1");
function onYouTubePlayerReady(playerId) {
youtubePlayer = document.getElementById("myyoutubePlayer");
youtubePlayer.playVideo();
youtubePlayer.mute();
}
var params = { allowScriptAccess: "always" };
var atts = { id: "myyoutubePlayer" };
swfobject.embedSWF("http://www.youtube.com/v/<?php echo $video_id; ?>&feature=plpp_play_all?enablejsapi=1&playerapiid=youtubePlayer&allowFullScreen=true&version=3&loop=1&autohide=1",
"youtubeNosound", "100%", "380", "8", null, null, params, atts)
</script>
	
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
?>