<?php
/**
 * Template Name: testing Page
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">

<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}
	
	//Get audio files
	$args = array(
		'posts_per_page'   => -1,
		'orderby'          => 'post_date',
		'order'            => 'DESC',
		'post_type'        => 'attachment',
		'post_mime_type'   => 'audio/mpeg',
	);
	print_r($args);
	$audio_files = get_posts($args);
		
?>

	<div id="media" class="content-area">
		<div id="content" class="site-content" role="main">
<div id="mediaFrame" class="windowFrame">
    <div class="window">
      <div class="panel" style="min-height: 700px;">
		<div id="theAudio" style="min-height: 400px;">
		<h3>Audio</h3>
		
		
		<?php foreach($audio_files as $file) { ?>
		
		
		<div itemprop="track" itemscope="" itemtype="http://schema.org/MusicRecording" style="height:50px; margin-bottom: 5px;">
		  <h3 style="display: none;"><span itemprop="name"><?php echo $file->post_title; ?></span></h3>
		  <meta itemprop="url" content="https://itunes.apple.com/us/album/daddys-got-a-.45/id570854312?i=570854320&amp;uo=4">
		  <meta itemprop="inAlbum" content="Kaylee Rutland">
		  <meta itemprop="byArtist" content="Kaylee Rutland">
		  <div class="audioplayer"><audio id="audio4" controls="" style="width: 0px; height: 0px; visibility: hidden;">
		    <source src="<?php echo $file->guid; ?>" itemprop="audio">
		  </audio>
		  <div class="audioplayer-playpause" title="Play">
		  </div>
		  	<div style="width: auto; margin-left: 55px;">
		  		1. <span itemprop="name"><?php echo $file->post_title; ?></span>
			</div>
			<div class="audioplayer-bar" style="display: none;">
			<div class="audioplayer-bar-loaded"></div>
			<div class="audioplayer-bar-played" style="width: 0%;"></div></div>
			<a href="<?php echo get_post_meta( $file->ID, 'kaylee_media_link', true ); ?>" target="_blank" class="buyNow" itemprop="offer">Buy now!</a></div>
		  
		  
		</div><!-- End Track -->
		
		<?php } ?>
		
		
		
		</div>
		</div><!-- /panel -->
    </div><!-- /.window -->
</div>



<?php 
global $post;
$videos =  rwmb_meta( 'kaylee_videos', 'type=text' );
print_r($videos);
foreach($videos as $video) {
	$video_id =  ltrim(strstr($video, '='), '=');
	?>

<a class="various fancybox.iframe" href="http://www.youtube.com/embed/<?php echo $video_id; ?>?autoplay=1">
	<img src="http://img.youtube.com/vi/<?php echo $video_id; ?>/0.jpg"/>
</a>
<?php } ?>


		
		</div><!-- #content -->
	</div><!-- #primary -->
</div><!-- #main-content -->

<?php
get_sidebar();
get_footer();
