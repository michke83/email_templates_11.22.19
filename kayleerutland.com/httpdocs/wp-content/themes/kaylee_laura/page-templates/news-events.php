<?php
/**
 * Template Name: News & Events
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="main-content" class="main-content">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

		<h1>News</h1>
    	<?php    	
		// query news posts
		$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$args = array(
			'posts_per_page' => 3, 
			'paged' => $paged, 
			'post_type' => 'news', 
			'orderby' => 'date', 
			'order' => 'DESC' 
		);
		query_posts($args);
		
		if ( have_posts() ) : while (have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
		<?php endwhile;
		next_posts_link();
		previous_posts_link();
		else :
		endif;
        wp_reset_query(); ?>
        
        <h1>Events</h1>
        <?php
		$args = array(
			'posts_per_page' => 3, 
			'post_type' => 'event', 
			'orderby' => 'date', 
			'order' => 'DESC' 
		);
		$events = query_posts($args);
		
		if ( have_posts() ) : while (have_posts()) : the_post(); ?>
				<h1><?php the_title(); ?></h1>
		<?php endwhile;
		else :
		endif;
        wp_reset_query(); ?>
        
        
        <h1>Images</h1>
		<?php
        $images = rwmb_meta( 'kaylee_events_img', 'type=image' );
foreach ( $images as $image )
{
    echo "<a href='{$image['full_url']}' title='{$image['title']}' rel='thickbox'><img src='{$image['url']}' width='{$image['width']}' height='{$image['height']}' alt='{$image['alt']}' /></a>";
}
		 ?>       
        
        
		</div>
	</div>
</div>


<?php get_footer(); ?>
