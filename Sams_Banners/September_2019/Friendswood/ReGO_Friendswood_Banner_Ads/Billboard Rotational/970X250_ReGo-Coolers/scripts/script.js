function startAd() {
  var rotator = document.getElementById("rotator").getElementsByTagName("img"),
      banner = document.getElementById("banner");

  lastIndex = rotator.length - 1,
  currentIndex = 0,
  speed = .5,
  height = banner.offsetHeight,
  rotatorHeight = document.getElementById("rotator").offsetHeight,
  tl = new TimelineLite,
  showIndex = 0,

  tl.to(banner, .6, {
      opacity: 1
  })
  .to(document.getElementById("opener"), speed, {
      //opacity: 0,
      //display: "none",
      y: -rotatorHeight,
      ease: Quad.easeInOut,
      delay: 3.75
  })
  .to(rotator[0], speed, {
      y: 0,
      ease: Quad.easeInOut,
      delay: -speed,
      onComplete: function() {
          currentIndex = 0
      }
  });

  for (var o = 0; lastIndex > o; o++)
    tl.to(rotator[o], speed, {
        y: -height,
        ease: Quad.easeInOut,
        onStart: function() {
            currentIndex += 1
        },
        delay: 3.75
    }),

    tl.to(rotator[o + 1], speed, {
        y: 0,
        ease: Quad.easeInOut,
        delay: -speed
    });

    tl.to(document.getElementById("controls"), .3, {
        opacity: 1,
        delay: .5
    }),

  console.log(tl.duration());

}
