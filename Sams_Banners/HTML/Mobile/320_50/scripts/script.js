function startAd() {
  var rotator = document.getElementById("rotator").getElementsByTagName("img"),
      banner = document.getElementById("banner");

  lastIndex = rotator.length - 1,
  currentIndex = 0,
  speed = .5,
  height = banner.offsetHeight,
  rotatorHeight = document.getElementById("rotator").offsetHeight,
  tl = new TimelineLite,
  tl2 = new TimelineLite,
  showIndex = 0,

  tl2.to(banner, .6, {
      opacity: 1
  });
  tl.to(document.getElementById("opener"), speed, {
      y: -rotatorHeight,
      ease: Quad.easeInOut,
      delay: 1.5
  })
  .to(rotator[0], speed, {
      y: 0,
      ease: Quad.easeInOut,
      delay: -speed,
      onComplete: function() {
          currentIndex = 0
      }
  });

  for (var o = 0; lastIndex > o; o++)
    tl.to(rotator[o], speed, {
        y: -height,
        ease: Quad.easeInOut,
        onStart: function() {
            currentIndex += 1
        },
        //delay: (lastIndex-1==o)?2.5:1.50
		delay: 1.5
    }),

    tl.to(rotator[o + 1], speed, {
        y: 0,
        ease: Quad.easeInOut,
        delay: -speed
    });

    // tl.to(document.getElementById("controls"), .3, {
    //     opacity: 1,
    //     delay: .5
    // });

  //console.log(tl.duration());

  setTimeout(function(){
    tl2.to(document.getElementById("opener"), speed, {
        y: rotatorHeight,
        ease: Quad.easeInOut,
        delay: -speed
    })
    .to(rotator[lastIndex], speed, {
        y: -height,
        ease: Quad.easeInOut,
        delay: speed
    })
    .to(document.getElementById("opener"), speed, {
        y: 0,
        ease: Quad.easeInOut,
        delay: -speed,
        onStart: function() {
            document.getElementById("opener").style = 'display:block;';
        }
    });
    setTimeout(function(){
        tl.restart();
    },1500);
  },tl.duration()*1500);

}
