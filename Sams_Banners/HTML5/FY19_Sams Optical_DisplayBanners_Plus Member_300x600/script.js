function startAd() {
  var rotator = document.getElementById("rotator").getElementsByTagName("img"),
      banner = document.getElementById("banner"),
      next = document.getElementById("btnNext"),
      prev = document.getElementById("btnPrev");

  lastIndex = rotator.length - 1,
  currentIndex = 0,
  speed = .5,
  width = banner.offsetWidth,
  rotatorWidth = document.getElementById("rotator").offsetWidth,
  tl = new TimelineLite,
  showIndex = 0,

  tl.to(banner, .6, {
      opacity: 1
  })
  .to(document.getElementById("opener"), speed, {
      x: -rotatorWidth,
      ease: Quad.easeInOut,
      delay: 2.50,
      onComplete: function() {
          document.getElementById("opener").style = 'display:none';
      }
  })
  .to(rotator[0], speed, {
      x: 0,
      ease: Quad.easeInOut,
      delay: -speed,
      onComplete: function() {
          currentIndex = 0
      }
  });

  for (var o = 0; lastIndex > o; o++)
    tl.to(rotator[o], speed, {
        x: -width,
        ease: Quad.easeInOut,
        onStart: function() {
            currentIndex += 1
        },
        delay: 2.50
    }),

    tl.to(rotator[o + 1], speed, {
        x: 0,
        ease: Quad.easeInOut,
        delay: -speed
    });

    tl.to(document.getElementById("controls"), .3, {
        opacity: 1,
        delay: .5
    }),

  console.log(tl.duration()),

  next.addEventListener("click", e),

  prev.addEventListener("click", t)

  function e(e) {
      showIndex = currentIndex === lastIndex ? 0 : currentIndex + 1, TweenLite.to(rotator[currentIndex], speed, {
          x: -width,
          ease: Quad.easeInOut
      }), TweenLite.set(n[showIndex], {
          x: width
      }), TweenLite.to(n[showIndex], speed, {
          x: 0,
          ease: Quad.easeInOut
      }), currentIndex = currentIndex === lastIndex ? 0 : currentIndex += 1, e.preventDefault()
  }

  function t(e) {
      showIndex = 0 === currentIndex ? lastIndex : currentIndex - 1, TweenLite.to(rotator[currentIndex], speed, {
          x: width,
          ease: Quad.easeInOut
      }), TweenLite.set(n[showIndex], {
          x: -width
      }), TweenLite.to(n[showIndex], speed, {
          x: 0,
          ease: Quad.easeInOut
      }), currentIndex = 0 === currentIndex ? lastIndex : currentIndex -= 1, e.preventDefault()
  }

}
