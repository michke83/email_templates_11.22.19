<?php
/* template name: Contact Thank You Page */

get_header();
$title = get_field("title");
$titledescription = get_field("title_description");
$call_us_address = get_field("call_us_address");
$visit_us_address = get_field("visit_us_address");
$form_title = get_field("form_title");
$tour_shortcode = get_field("form_short_code");
$thank_you_confirmation = get_field("thank_you_confirmation");
if($_POST['gform_submit'] ==  3){ $titledescription= '';}
?>
<!-- pinnacle logo -->
<!--    <div class="container">
      <a href="index.html" class="pinnacle_logo"><img class="fixed-top ml-5" src="<?php echo get_template_directory_uri() ?>/images/PinnacleLogo.png" style="z-index: 1031;" alt="Pinnacle Logo"></a>
    </div>-->
<!-- /pinnacle logo -->
<div id="main">
    <div id="main-container">

        <!-- Contact us -->
        <section class="contact align-content-between">
            <div class="container-fluid mt-5 align-content-between">
                <div class="row align-items-top ">
                    <div class="col-lg-1">
                    </div>
                    <div class="container col-lg-5 text-center order-3 mt-4 mx-0 px-0 mobile_section1">
                        <div class="custom_form">
                           
                            <h2 class="blue-text"> <?php echo $form_title;?></h2>
                            <p><?php echo $thank_you_confirmation;?></p>
                          <?php // echo do_shortcode($tour_shortcode);
                            ?>
           <!--                <img class="img-fluid bookings_image" src="<?php echo get_template_directory_uri() ?>/images/get_in_touch.png" alt="bookings image Photo">-->
                        </div>
                    </div>
                    <div class="col-lg-5 order-2  mt-4">
                        <div class="px-2 container mt-5 my-5">
                            <h2 class="display-5 mb-2 blue-text"><?php echo $title; ?></h2>
                            <?php echo $call_us_address; ?>
                            <?php echo $visit_us_address; ?>
              <!--              <p class="mt-5 blue-text">Call us</p>
                            <p class="text-muted">PHONE: 903-451-4653</p>
                            <p class="text-muted">TOLL FREE: 1-888-340-0622</p>
                            <p class="mt-5 blue-text">Visit us at</p>
                            <p class="text-muted">The Pinnace Golf Club</p>
                            <p class="text-muted">200 Pinnacle Club Drive</p>
                            <p class="text-muted">Mabank, Texas</p>-->
                        </div>
                        
                        <!-- contact accordions -->
                        <?php get_template_part('template-parts/module', 'contact-accordian'); ?>
                        <!-- /contact accordions -->
                    </div>
                </div>
                <div class="col-lg-1">
                </div>
            </div>
    </div>
</section>
<!-- blue section -->
<?php get_template_part('template-parts/module', 'contact-blue'); ?>
<!-- /blue section -->

</div>
</div>


<?php get_footer(); ?>

