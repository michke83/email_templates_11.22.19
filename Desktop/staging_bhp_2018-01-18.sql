# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 10.208.163.76 (MySQL 5.7.21)
# Database: staging_bhp
# Generation Time: 2018-01-18 21:38:59 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table wp_commentmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_commentmeta`;

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_comments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_comments`;

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_comments` WRITE;
/*!40000 ALTER TABLE `wp_comments` DISABLE KEYS */;

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`)
VALUES
	(1,1,'A WordPress Commenter','wapuu@wordpress.example','https://wordpress.org/','','2017-12-12 17:43:38','2017-12-12 17:43:38','Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.',0,'1','','',0,0);

/*!40000 ALTER TABLE `wp_comments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_links
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_links`;

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_options`;

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_options` WRITE;
/*!40000 ALTER TABLE `wp_options` DISABLE KEYS */;

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`)
VALUES
	(1,'siteurl','http://staging.blueheronproperties.com','yes'),
	(2,'home','http://staging.blueheronproperties.com','yes'),
	(3,'blogname','Blue Heron','yes'),
	(4,'blogdescription','','yes'),
	(5,'users_can_register','0','yes'),
	(6,'admin_email','developers@ivieinc.com','yes'),
	(7,'start_of_week','1','yes'),
	(8,'use_balanceTags','0','yes'),
	(9,'use_smilies','1','yes'),
	(10,'require_name_email','1','yes'),
	(11,'comments_notify','1','yes'),
	(12,'posts_per_rss','10','yes'),
	(13,'rss_use_excerpt','0','yes'),
	(14,'mailserver_url','mail.example.com','yes'),
	(15,'mailserver_login','login@example.com','yes'),
	(16,'mailserver_pass','password','yes'),
	(17,'mailserver_port','110','yes'),
	(18,'default_category','1','yes'),
	(19,'default_comment_status','open','yes'),
	(20,'default_ping_status','open','yes'),
	(21,'default_pingback_flag','0','yes'),
	(22,'posts_per_page','10','yes'),
	(23,'date_format','F j, Y','yes'),
	(24,'time_format','g:i a','yes'),
	(25,'links_updated_date_format','F j, Y g:i a','yes'),
	(26,'comment_moderation','0','yes'),
	(27,'moderation_notify','1','yes'),
	(28,'permalink_structure','/%postname%/','yes'),
	(29,'rewrite_rules','a:87:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}','yes'),
	(30,'hack_file','0','yes'),
	(31,'blog_charset','UTF-8','yes'),
	(32,'moderation_keys','','no'),
	(33,'active_plugins','a:3:{i:0;s:29:\"gravityforms/gravityforms.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:30:\"advanced-custom-fields/acf.php\";}','yes'),
	(34,'category_base','','yes'),
	(35,'ping_sites','http://rpc.pingomatic.com/','yes'),
	(36,'comment_max_links','2','yes'),
	(37,'gmt_offset','0','yes'),
	(38,'default_email_category','1','yes'),
	(39,'recently_edited','','no'),
	(40,'template','blue-heron','yes'),
	(41,'stylesheet','blue-heron','yes'),
	(42,'comment_whitelist','1','yes'),
	(43,'blacklist_keys','','no'),
	(44,'comment_registration','0','yes'),
	(45,'html_type','text/html','yes'),
	(46,'use_trackback','0','yes'),
	(47,'default_role','subscriber','yes'),
	(48,'db_version','38590','yes'),
	(49,'uploads_use_yearmonth_folders','1','yes'),
	(50,'upload_path','','yes'),
	(51,'blog_public','0','yes'),
	(52,'default_link_category','2','yes'),
	(53,'show_on_front','posts','yes'),
	(54,'tag_base','','yes'),
	(55,'show_avatars','1','yes'),
	(56,'avatar_rating','G','yes'),
	(57,'upload_url_path','','yes'),
	(58,'thumbnail_size_w','150','yes'),
	(59,'thumbnail_size_h','150','yes'),
	(60,'thumbnail_crop','1','yes'),
	(61,'medium_size_w','300','yes'),
	(62,'medium_size_h','300','yes'),
	(63,'avatar_default','mystery','yes'),
	(64,'large_size_w','1024','yes'),
	(65,'large_size_h','1024','yes'),
	(66,'image_default_link_type','none','yes'),
	(67,'image_default_size','','yes'),
	(68,'image_default_align','','yes'),
	(69,'close_comments_for_old_posts','0','yes'),
	(70,'close_comments_days_old','14','yes'),
	(71,'thread_comments','1','yes'),
	(72,'thread_comments_depth','5','yes'),
	(73,'page_comments','0','yes'),
	(74,'comments_per_page','50','yes'),
	(75,'default_comments_page','newest','yes'),
	(76,'comment_order','asc','yes'),
	(77,'sticky_posts','a:0:{}','yes'),
	(78,'widget_categories','a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(79,'widget_text','a:0:{}','yes'),
	(80,'widget_rss','a:0:{}','yes'),
	(81,'uninstall_plugins','a:0:{}','no'),
	(82,'timezone_string','','yes'),
	(83,'page_for_posts','0','yes'),
	(84,'page_on_front','0','yes'),
	(85,'default_post_format','0','yes'),
	(86,'link_manager_enabled','0','yes'),
	(87,'finished_splitting_shared_terms','1','yes'),
	(88,'site_icon','0','yes'),
	(89,'medium_large_size_w','768','yes'),
	(90,'medium_large_size_h','0','yes'),
	(91,'initial_db_version','38590','yes'),
	(92,'wp_user_roles','a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}','yes'),
	(93,'fresh_site','0','yes'),
	(94,'widget_search','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(95,'widget_recent-posts','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(96,'widget_recent-comments','a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}','yes'),
	(97,'widget_archives','a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}','yes'),
	(98,'widget_meta','a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}','yes'),
	(99,'sidebars_widgets','a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}','yes'),
	(100,'widget_pages','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(101,'widget_calendar','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(102,'widget_media_audio','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(103,'widget_media_image','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(104,'widget_media_gallery','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(105,'widget_media_video','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(106,'nonce_key','yBs?ytNDD6e`]k6;39+u,~V);i2^*Cv]{8~qVFL]^)+IR%sTgUn^,scmI0-`2&W[','no'),
	(107,'nonce_salt','1?ziGbQ=g1?o,pKRL6-H4jyDv#vi-C9P)^N|@*~@&cCzSu97g~elJnNbPZ: Ul6}','no'),
	(108,'widget_tag_cloud','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(109,'widget_nav_menu','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(110,'widget_custom_html','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(111,'cron','a:5:{i:1516211018;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1516211042;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1516224067;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1516238873;a:1:{s:17:\"gravityforms_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}','yes'),
	(112,'theme_mods_twentyseventeen','a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1513124024;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}','yes'),
	(122,'auth_key',':C`zSM)V`pRg>)D[g$lb$bK$7pbmt,Oh[p>=:_]1oUn3DbBXU=P|+,uDV`h#@$.d','no'),
	(123,'auth_salt','o+Y0Jrn.`3D}]N{uzkD!QF]f>M}f6.jz u5.Iwmjta(oks7*@t![yyXVo<]KJxU.','no'),
	(124,'logged_in_key','Q(<}vT(iUCljzd.T9`XJBv#Q0ZXB1LIP^!>.0V<WjBgTa~pHAa[i?uR+2s@u1y|<','no'),
	(125,'logged_in_salt','+AD#DB>cN{#A{ts^Wc>-KSGvUl!xmAZq6~Sg6)KrTm1b4okShN]3p@ICo*sP{piy','no'),
	(141,'can_compress_scripts','1','no'),
	(144,'WPLANG','','yes'),
	(145,'new_admin_email','developers@ivieinc.com','yes'),
	(151,'_transient_twentyseventeen_categories','1','yes'),
	(164,'current_theme','Blue Heron','yes'),
	(165,'theme_mods_bhp','a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1514384449;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),
	(166,'theme_switched','','yes'),
	(170,'recently_activated','a:0:{}','yes'),
	(179,'widget_gform_widget','a:1:{s:12:\"_multiwidget\";i:1;}','yes'),
	(180,'gravityformsaddon_gravityformswebapi_version','1.0','yes'),
	(182,'gform_enable_background_updates','1','yes'),
	(183,'rg_form_version','2.2.5','yes'),
	(189,'acf_version','5.6.7','yes'),
	(192,'acf_pro_license','YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TnpNd05URjhkSGx3WlQxd1pYSnpiMjVoYkh4a1lYUmxQVEl3TVRZdE1ERXRNVGtnTWpFNk5ERTZNREE9IjtzOjM6InVybCI7czozODoiaHR0cDovL3N0YWdpbmcuYmx1ZWhlcm9ucHJvcGVydGllcy5jb20iO30=','yes'),
	(199,'rg_gforms_key','f0218004bb0fab683b64c4eab75db0b2','yes'),
	(200,'gform_enable_noconflict','1','yes'),
	(201,'rg_gforms_enable_akismet','0','yes'),
	(202,'rg_gforms_currency','USD','yes'),
	(203,'gform_enable_toolbar_menu','','yes'),
	(204,'rg_gforms_captcha_public_key','','yes'),
	(205,'rg_gforms_captcha_private_key','','yes'),
	(206,'rg_gforms_message','<!--GFM-->','yes'),
	(208,'gf_is_upgrading','0','yes'),
	(209,'gf_previous_db_version','0','yes'),
	(210,'gf_db_version','2.2.5','yes'),
	(217,'category_children','a:0:{}','yes'),
	(222,'nav_menu_options','a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}','yes'),
	(276,'theme_mods_twentyfifteen','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1514384573;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}','yes'),
	(277,'_site_transient_update_themes','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1516205494;s:7:\"checked\";a:4:{s:10:\"blue-heron\";s:5:\"1.0.0\";s:13:\"twentyfifteen\";s:3:\"1.9\";s:15:\"twentyseventeen\";s:3:\"1.4\";s:13:\"twentysixteen\";s:3:\"1.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}','no'),
	(279,'theme_mods_blue-heron','a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;}','yes'),
	(286,'_site_transient_update_plugins','O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1516205493;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:6:\"4.4.12\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:72:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.4.4.12.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:7:\"default\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";}s:7:\"banners\";a:3:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:7:\"default\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.2.zip\";s:5:\"icons\";a:3:{s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:7:\"default\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";}s:7:\"banners\";a:2:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";s:7:\"default\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}}}','no'),
	(317,'options_header_heading','Luxury without the price tag','no'),
	(318,'_options_header_heading','field_5a43f6d74d754','no'),
	(319,'options_header_link_text','Explore','no'),
	(320,'_options_header_link_text','field_5a43f6ef4d755','no'),
	(321,'options_header_link_url','https://google.com','no'),
	(322,'_options_header_link_url','field_5a43f7124d756','no'),
	(339,'options_facebook_url','https://facebook.com','no'),
	(340,'_options_facebook_url','field_5a43fab53fd7f','no'),
	(341,'options_instagram_url','https://instagram.com','no'),
	(342,'_options_instagram_url','field_5a43faca3fd80','no'),
	(343,'options_vimeo_url','https://vimeo.com','no'),
	(344,'_options_vimeo_url','field_5a43fae13fd81','no'),
	(353,'options_bh_header_logo','36','no'),
	(354,'_options_bh_header_logo','field_5a43fcd290515','no'),
	(355,'options_footer_logo','37','no'),
	(356,'_options_footer_logo','field_5a43fd0090516','no'),
	(357,'options_address','195 Pinnacle Club Place','no'),
	(358,'_options_address','field_5a43fd1f90517','no'),
	(359,'options_city','Mabank','no'),
	(360,'_options_city','field_5a43fd2b90518','no'),
	(361,'options_state','TX','no'),
	(362,'_options_state','field_5a43fd4390519','no'),
	(363,'options_zip','75156','no'),
	(364,'_options_zip','field_5a43fd4e9051a','no'),
	(365,'options_phone_number','903.451.HOME (4663)','no'),
	(366,'_options_phone_number','field_5a43fd6a9051b','no'),
	(367,'options_email','luxury@blueheronproperties.com','no'),
	(368,'_options_email','field_5a43fd7e9051c','no'),
	(369,'options_copyright_text','@ 2018 Blue Heron Properties. All rights reserved. <a href=\"http://staging.blueheronproperties.com/houses/\">Terms and Conditions</a> | <a href=\"http://staging.blueheronproperties.com/houses/\">Privacy Policy</a> | <a href=\"http://staging.blueheronproperties.com/houses/\">Disclaimer</a>','no'),
	(370,'_options_copyright_text','field_5a43fd979051d','no'),
	(388,'options_sub_header_heading','Opportunity awaits','no'),
	(389,'_options_sub_header_heading','field_5a4416c1844cc','no'),
	(390,'options_sub_header_content','Dolor in sirloin laborum lorem ad. Venison ex cow drumstick swine, ipsum consectetur commodo excepteur landjaeger. Incididunt sunt turducken velit sed kevin eu exercitation tempor labore frankfurter ut. Shankle dolore hamburger, ipsum incididunt aliquip kielbasa corned beef dolore. Meatball et jowl, drumstick biltong anim turkey beef ribs sint t-bone jerky nisi. Aliqua excepteur tempor ham hock. Adipisicing sed chicken short loin do rump jowl tempor frankfurter pastrami est. Sirloin turducken non rump tongue leberkas dolore, alcatra adipisicing meatloaf ut voluptate aliqua. Salami turducken non do, dolore doner venison incididunt picanha. Short loin burgdoggen enim, pastrami consectetur boudin in officia et sed biltong salami spare ribs.','no'),
	(391,'_options_sub_header_content','field_5a441705844cd','no'),
	(408,'options_home_page_slider_images_0_slider_images','44','no'),
	(409,'_options_home_page_slider_images_0_slider_images','field_5a44185b447e1','no'),
	(410,'options_home_page_slider_images_1_slider_images','45','no'),
	(411,'_options_home_page_slider_images_1_slider_images','field_5a44185b447e1','no'),
	(412,'options_home_page_slider_images_2_slider_images','46','no'),
	(413,'_options_home_page_slider_images_2_slider_images','field_5a44185b447e1','no'),
	(414,'options_home_page_slider_images','3','no'),
	(415,'_options_home_page_slider_images','field_5a441839447e0','no'),
	(432,'options_image_top_left','56','no'),
	(433,'_options_image_top_left','field_5a441ddcba882','no'),
	(434,'options_image_top_right','57','no'),
	(435,'_options_image_top_right','field_5a441df9ba883','no'),
	(436,'options_image_bottom_left','58','no'),
	(437,'_options_image_bottom_left','field_5a441e25ba884','no'),
	(438,'options_image_bottom_right','59','no'),
	(439,'_options_image_bottom_right','field_5a441e4cba885','no'),
	(440,'options_image_section_header','Sprawling beauty without compromise or equal','no'),
	(441,'_options_image_section_header','field_5a441e75ba886','no'),
	(442,'options_image_section_content','Meatball et jowl, drumstick biltong anim turkey beef ribs sint t-bone jerky nisi. Aliqua excepteur tempor ham hock.\r\nAdipisicing sed chicken short loin do rump jowl tempor frankfurter pastrami est. Sirloin turducken non rump tongue\r\nleberkas dolore, alcatra adipisicing meatloaf ut voluptate aliqua.','no'),
	(443,'_options_image_section_content','field_5a441e8bba887','no'),
	(444,'options_image_section_link_text','SEE THE PROPERTIES','no'),
	(445,'_options_image_section_link_text','field_5a441eadba888','no'),
	(446,'options_image_section_link_url','https://google.com','no'),
	(447,'_options_image_section_link_url','field_5a441ec9ba889','no'),
	(456,'options_image_top_left_header','FAIRWAYS AND GREENS','no'),
	(457,'_options_image_top_left_header','field_5a4421677009c','no'),
	(458,'options_image_top_right_header','THE LANDING','no'),
	(459,'_options_image_top_right_header','field_5a4421a57009d','no'),
	(460,'options_image_bottom_left_header','COVERED BRIDGE COVE','no'),
	(461,'_options_image_bottom_left_header','field_5a4421cc7009e','no'),
	(462,'options_image_bottom_right_header','SAINT ANNES PLACE','no'),
	(463,'_options_image_bottom_right_header','field_5a4421fc7009f','no'),
	(476,'options_bh_possibilities_header','<strong>Explore the Possibilities</strong>\r\n<strong>with Blue Heron</strong>','no'),
	(477,'_options_bh_possibilities_header','field_5a442581f13f7','no'),
	(478,'options_bh_possibilities_link_text','LEARN MORE','no'),
	(479,'_options_bh_possibilities_link_text','field_5a44259bf13f8','no'),
	(480,'options_bh_possibilities_link_url','https://google.com','no'),
	(481,'_options_bh_possibilities_link_url','field_5a4425bef13f9','no'),
	(502,'options_pre_footer_header','Reach out and we\'ll reach back','no'),
	(503,'_options_pre_footer_header','field_5a442b275012b','no'),
	(504,'options_pre_footer_content','Meatball et jowl, drumstick biltong anim turkey beef ribs sint t-bone jerky nisi. Aliqua excepteur tempor ham hock. Adipisicing sed chicken short loin do rump jowl tempor frankfurter pastrami est. Sirloin turducken non rump tongue leberkas dolore, alcatra adipisicing meatloaf ut voluptate aliqua.','no'),
	(505,'_options_pre_footer_content','field_5a442b665012c','no'),
	(514,'options_header_background_image','73','no'),
	(515,'_options_header_background_image','field_5a442f2c9bc33','no'),
	(528,'options_bh_possibilities_background_image','76','no'),
	(529,'_options_bh_possibilities_background_image','field_5a443119d7581','no'),
	(720,'_transient_timeout_gform_update_info','1516208187','no'),
	(721,'_transient_gform_update_info','a:9:{s:12:\"is_valid_key\";b:1;s:6:\"reason\";s:0:\"\";s:7:\"version\";s:5:\"2.2.5\";s:3:\"url\";s:166:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.2.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=TkUZq9ElIqCkl6nY%2F4LwaWQFM3M%3D\";s:15:\"expiration_time\";i:1524632400;s:9:\"offerings\";a:46:{s:12:\"gravityforms\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"2.2.5\";s:14:\"version_latest\";s:8:\"2.2.5.21\";s:3:\"url\";s:166:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.2.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=TkUZq9ElIqCkl6nY%2F4LwaWQFM3M%3D\";s:10:\"url_latest\";s:169:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.2.5.21.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=OSrkcoA2qUTgovxRnYM%2Bur4DGgo%3D\";}s:26:\"gravityformsactivecampaign\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:5:\"1.4.3\";s:3:\"url\";s:189:\"http://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=kgnj45mMPHWKAIgxxblQBbGzo8o%3D\";s:10:\"url_latest\";s:191:\"http://s3.amazonaws.com/gravityforms/addons/activecampaign/gravityformsactivecampaign_1.4.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=cYfqgsMNWej6mpDBzZUmzwJWf7I%3D\";}s:20:\"gravityformsagilecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Q4LYtP4XpIaUoNM377S7UrKgw%2FQ%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/agilecrm/gravityformsagilecrm_1.1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=2v%2Fp5LMw8Tb4D7Cb4u1ZJHaVZ5A%3D\";}s:24:\"gravityformsauthorizenet\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.3\";s:14:\"version_latest\";s:5:\"2.3.4\";s:3:\"url\";s:189:\"http://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=j%2F0JP8sFAT1ril%2FtND3xlWHt2TQ%3D\";s:10:\"url_latest\";s:189:\"http://s3.amazonaws.com/gravityforms/addons/authorizenet/gravityformsauthorizenet_2.3.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=du%2Fy37nCNhhC3VdkuZiuzNIllF0%3D\";}s:18:\"gravityformsaweber\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.5\";s:14:\"version_latest\";s:3:\"2.6\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=zdC3qILyjEhghOOp5EJeWCKSyXs%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/aweber/gravityformsaweber_2.6.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Wl9fgPzNT8Krl5v%2F0gWWV%2FqCviY%3D\";}s:21:\"gravityformsbatchbook\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.1\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=QSNE0xKLAZ492nK4R9lKrKdB8r0%3D\";s:10:\"url_latest\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/batchbook/gravityformsbatchbook_1.2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Rqn%2BGo4yLAATA4vjnJA2wJUEnTM%3D\";}s:18:\"gravityformsbreeze\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=b9QzyQlklLSNgKCrIyRdrTbUsI8%3D\";s:10:\"url_latest\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/breeze/gravityformsbreeze_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=b9QzyQlklLSNgKCrIyRdrTbUsI8%3D\";}s:27:\"gravityformscampaignmonitor\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.5\";s:14:\"version_latest\";s:3:\"3.5\";s:3:\"url\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=ltsGRiP%2FA8xG0r6pQhLsFpONyyc%3D\";s:10:\"url_latest\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/campaignmonitor/gravityformscampaignmonitor_3.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=ltsGRiP%2FA8xG0r6pQhLsFpONyyc%3D\";}s:20:\"gravityformscampfire\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=u0lN%2B1uNMGL1vYSsdmRm9a8IYbc%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/campfire/gravityformscampfire_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=I3LGXMbP71jidSdRVC1SDXQ%2B7M4%3D\";}s:22:\"gravityformscapsulecrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.4\";s:3:\"url\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=atyfS%2F%2BFK5MTScfTaLlcTRBpjDU%3D\";s:10:\"url_latest\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/capsulecrm/gravityformscapsulecrm_1.1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=3wYrlAV4gzvtRs4D33vbbYUMm1M%3D\";}s:26:\"gravityformschainedselects\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:5:\"1.0.5\";s:3:\"url\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.0.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=F7I%2FbobYaa%2BX8WIFQw8DThdadUs%3D\";s:10:\"url_latest\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/chainedselects/gravityformschainedselects_1.0.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=hJkzajXectRwDBih1vWw%2B0VM2KQ%3D\";}s:23:\"gravityformscleverreach\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:5:\"1.3.2\";s:3:\"url\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Y6T%2BzJ0LCpjz3Z2ob2n14lHIgfQ%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/cleverreach/gravityformscleverreach_1.3.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=BJ5yrCdRfBZrzIAhK9kBOR8hX5M%3D\";}s:19:\"gravityformscoupons\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.6\";s:14:\"version_latest\";s:5:\"2.6.1\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.6.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=5n3dcamnqv5rzTXs3nZUdCWq3SE%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/coupons/gravityformscoupons_2.6.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=fdwsE2tB4PeFvWJt%2FWSMWX08MKY%3D\";}s:17:\"gravityformsdebug\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";s:9:\"1.0.beta8\";s:3:\"url\";s:0:\"\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/debug/gravityformsdebug_1.0.beta8.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=1n%2Bp4bRKgHQ2UFlUDYUF06ro%2Fyk%3D\";}s:19:\"gravityformsdropbox\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:5:\"2.0.3\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.0.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=4Jkw%2BN3dnXzlpm5snhzpuiKvRCU%3D\";s:10:\"url_latest\";s:183:\"http://s3.amazonaws.com/gravityforms/addons/dropbox/gravityformsdropbox_2.0.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=WyZAa0%2FL5y1qom5fInZ9i%2FQ4%2F78%3D\";}s:16:\"gravityformsemma\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:3:\"1.2\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=QrbkpJ6SNyjyp9mB%2F7q%2Bugp12qQ%3D\";s:10:\"url_latest\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/emma/gravityformsemma_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=QrbkpJ6SNyjyp9mB%2F7q%2Bugp12qQ%3D\";}s:22:\"gravityformsfreshbooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.5\";s:14:\"version_latest\";s:5:\"2.5.1\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.5.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Utr16QheHQ231eeAdKf8kQTvqnk%3D\";s:10:\"url_latest\";s:187:\"http://s3.amazonaws.com/gravityforms/addons/freshbooks/gravityformsfreshbooks_2.5.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=cQM1P5bNMUP0XKY5l3b%2F7%2BLDPhg%3D\";}s:23:\"gravityformsgetresponse\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.1\";s:3:\"url\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=GExHGa2iDeP0RzG37UOCX%2BNj7dc%3D\";s:10:\"url_latest\";s:187:\"http://s3.amazonaws.com/gravityforms/addons/getresponse/gravityformsgetresponse_1.1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=ryufG99JpU%2Foc5EeYSbKDmPqYH0%3D\";}s:21:\"gravityformsgutenberg\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"1.0-beta-1\";s:14:\"version_latest\";N;s:3:\"url\";s:0:\"\";s:10:\"url_latest\";N;}s:21:\"gravityformshelpscout\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:5:\"1.4.4\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=2NIA6%2F0NfTJE4Xk497mZsGhCduk%3D\";s:10:\"url_latest\";s:187:\"http://s3.amazonaws.com/gravityforms/addons/helpscout/gravityformshelpscout_1.4.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=rxfwXs%2BDXQqcorG%2BR1NPxm%2FxdAM%3D\";}s:20:\"gravityformshighrise\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.2\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=22NR114RYEitp8QsJ3%2BAr4ltAIc%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/highrise/gravityformshighrise_1.2.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=%2BLZfnicXHp8rNUGbHUc1y5FBBqI%3D\";}s:19:\"gravityformshipchat\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.1\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/hipchat/gravityformshipchat_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=GruRgpkNXgFRodqgf2KeLdLyJzI%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/hipchat/gravityformshipchat_1.1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Dw4nfzHRrXk3ho2sb9V18rD1Qj4%3D\";}s:20:\"gravityformsicontact\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.4\";s:3:\"url\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=nDd4%2Blh8KWO8g%2F%2F4I1PT%2FnXVMdE%3D\";s:10:\"url_latest\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/icontact/gravityformsicontact_1.2.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=guYwm6eoGwoZ6GkrbeS3D6E88A8%3D\";}s:19:\"gravityformslogging\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.3\";s:14:\"version_latest\";s:3:\"1.3\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=OeST9OblNAo%2FXVsLXGkmW5JgRkI%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/logging/gravityformslogging_1.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=OeST9OblNAo%2FXVsLXGkmW5JgRkI%3D\";}s:19:\"gravityformsmadmimi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.2\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=dKprob97%2FecyrbYzpM1Upxn59KQ%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/madmimi/gravityformsmadmimi_1.1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=jOTRt%2FWQGpw5p7T%2B8stAyXLPtTs%3D\";}s:21:\"gravityformsmailchimp\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"4.2\";s:14:\"version_latest\";s:5:\"4.2.4\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=BOxI1p1EQP%2BQ9oKkQo51DeydKi0%3D\";s:10:\"url_latest\";s:185:\"http://s3.amazonaws.com/gravityforms/addons/mailchimp/gravityformsmailchimp_4.2.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=u%2B70U3bQQg1CNzY014%2BOx6Lfe2U%3D\";}s:26:\"gravityformspartialentries\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:5:\"1.0.7\";s:3:\"url\";s:189:\"http://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.0.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=nHkYf1XZJCEUr2XTvfedsaZxl5I%3D\";s:10:\"url_latest\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/partialentries/gravityformspartialentries_1.0.7.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=e4K2fENEiV%2BhUBSH46vqA9kW0Wg%3D\";}s:18:\"gravityformspaypal\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.8\";s:14:\"version_latest\";s:5:\"2.8.1\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_2.8.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=ZMxSevZKPmukl9UdRBBpdor1TAU%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/paypal/gravityformspaypal_2.8.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=pbJFYH%2FbA3z5HIAsort1X1Uwi9o%3D\";}s:33:\"gravityformspaypalexpresscheckout\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:0:\"\";s:14:\"version_latest\";N;}s:29:\"gravityformspaypalpaymentspro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.2\";s:14:\"version_latest\";s:5:\"2.2.1\";s:3:\"url\";s:195:\"http://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=0VXVDhUqTiFkEWRQAJ68vQKex04%3D\";s:10:\"url_latest\";s:201:\"http://s3.amazonaws.com/gravityforms/addons/paypalpaymentspro/gravityformspaypalpaymentspro_2.2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=5BbmnWBHkq3ym%2B7IHFLpTJA%2FL9Q%3D\";}s:21:\"gravityformspaypalpro\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"1.7.2\";s:14:\"version_latest\";s:5:\"1.7.2\";s:3:\"url\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.7.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=h61YkBtGjemfkpec5OmayBdUUo8%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/paypalpro/gravityformspaypalpro_1.7.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=h61YkBtGjemfkpec5OmayBdUUo8%3D\";}s:20:\"gravityformspicatcha\";a:3:{s:12:\"is_available\";b:0;s:7:\"version\";s:3:\"2.0\";s:14:\"version_latest\";s:3:\"2.0\";}s:16:\"gravityformspipe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.0\";s:14:\"version_latest\";s:5:\"1.0.1\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.0.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=%2F9XPXnshxAcVX%2Fg7YBKdLoYqfVM%3D\";s:10:\"url_latest\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/pipe/gravityformspipe_1.0.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Gq7gX2PbHjBGqHb6Qo91FW7SVzA%3D\";}s:17:\"gravityformspolls\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.1\";s:14:\"version_latest\";s:5:\"3.1.2\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=mmg2YD0qzfa8hz%2BpvuOl%2BNTl65I%3D\";s:10:\"url_latest\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/polls/gravityformspolls_3.1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=2Drntd5kmi6stT0pPLsFJ2posQY%3D\";}s:16:\"gravityformsquiz\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.1\";s:14:\"version_latest\";s:5:\"3.1.6\";s:3:\"url\";s:169:\"http://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=ieJkd9Mq3SyDV5Bf4f351xgYsmM%3D\";s:10:\"url_latest\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/quiz/gravityformsquiz_3.1.6.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=zUTJxASMpNx4sZqVlCM5gDTsCEM%3D\";}s:19:\"gravityformsrestapi\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:10:\"2.0-beta-2\";s:14:\"version_latest\";s:10:\"2.0-beta-2\";s:3:\"url\";s:186:\"http://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=9PAF%2FQ%2FbTZFUoQPL5gc8FGL1ChA%3D\";s:10:\"url_latest\";s:186:\"http://s3.amazonaws.com/gravityforms/addons/restapi/gravityformsrestapi_2.0-beta-2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=9PAF%2FQ%2FbTZFUoQPL5gc8FGL1ChA%3D\";}s:21:\"gravityformssignature\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.3\";s:14:\"version_latest\";s:5:\"3.3.4\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=jakPiiIVBXCvgntSQQnpOVqhb9s%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/signature/gravityformssignature_3.3.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=deAHl71JApIMUY8hwsnIRh4e8Bc%3D\";}s:17:\"gravityformsslack\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.7\";s:14:\"version_latest\";s:3:\"1.7\";s:3:\"url\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.7.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=5VAsOxPum0eXHQOfA33ZYGU88ac%3D\";s:10:\"url_latest\";s:171:\"http://s3.amazonaws.com/gravityforms/addons/slack/gravityformsslack_1.7.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=5VAsOxPum0eXHQOfA33ZYGU88ac%3D\";}s:18:\"gravityformsstripe\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.3\";s:14:\"version_latest\";s:5:\"2.3.4\";s:3:\"url\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_2.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=bxkOVdPqtD7tbS4OHNSz%2FpNlh7k%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/stripe/gravityformsstripe_2.3.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=Qe9cFx4zYDtdlFfuZrLDkbyI0MA%3D\";}s:18:\"gravityformssurvey\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.2\";s:14:\"version_latest\";s:5:\"3.2.1\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=ZeNM3tTyJ9YYaaGzgtYOtAv%2F%2FLU%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/survey/gravityformssurvey_3.2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=DnDGi1s0msaALJxmVgfXqPYEssY%3D\";}s:18:\"gravityformstrello\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.2\";s:14:\"version_latest\";s:5:\"1.2.1\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=kDjfGbpqMgQhbOOGpFkOrSwH7uU%3D\";s:10:\"url_latest\";s:175:\"http://s3.amazonaws.com/gravityforms/addons/trello/gravityformstrello_1.2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=D2Gc58DbuVNelaojy6iQdH40Xfk%3D\";}s:18:\"gravityformstwilio\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:5:\"2.4.1\";s:14:\"version_latest\";s:5:\"2.4.1\";s:3:\"url\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.4.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=KduMH77PLLgsUSFtIwkT8jDd%2FiY%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/twilio/gravityformstwilio_2.4.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=KduMH77PLLgsUSFtIwkT8jDd%2FiY%3D\";}s:28:\"gravityformsuserregistration\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"3.8\";s:14:\"version_latest\";s:5:\"3.8.2\";s:3:\"url\";s:193:\"http://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_3.8.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=3HBrlu4j0qCBukYSDJtFA5R2cRc%3D\";s:10:\"url_latest\";s:197:\"http://s3.amazonaws.com/gravityforms/addons/userregistration/gravityformsuserregistration_3.8.2.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=S8Nw2WdsH5FVvNOR3Cp3xB%2FkGKk%3D\";}s:20:\"gravityformswebhooks\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.1\";s:14:\"version_latest\";s:5:\"1.1.3\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=5JU4DhZkSLjBWXPoAXjZR%2BuJfJM%3D\";s:10:\"url_latest\";s:181:\"http://s3.amazonaws.com/gravityforms/addons/webhooks/gravityformswebhooks_1.1.3.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=mZGTbVQIrB35P%2BjFmtWheFnzeN8%3D\";}s:18:\"gravityformszapier\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"2.1\";s:14:\"version_latest\";s:5:\"2.1.4\";s:3:\"url\";s:173:\"http://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_2.1.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=bxdgXG4FmYkc7M3vrCmcqXEHJjM%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/zapier/gravityformszapier_2.1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=tkS4r%2Fz7YYsr8IBKmZ3l0WgMXOg%3D\";}s:19:\"gravityformszohocrm\";a:5:{s:12:\"is_available\";b:1;s:7:\"version\";s:3:\"1.4\";s:14:\"version_latest\";s:5:\"1.4.9\";s:3:\"url\";s:179:\"http://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.4.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=MQpOtOGem3YcMyi0OOXX%2BpZhs%2BY%3D\";s:10:\"url_latest\";s:177:\"http://s3.amazonaws.com/gravityforms/addons/zohocrm/gravityformszohocrm_1.4.9.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=QHtEXVF70NKocmcgAlvL5PNzS7I%3D\";}}s:9:\"is_active\";s:1:\"1\";s:14:\"version_latest\";s:8:\"2.2.5.21\";s:10:\"url_latest\";s:169:\"http://s3.amazonaws.com/gravityforms/releases/gravityforms_2.2.5.21.zip?AWSAccessKeyId=1603BBK66770VCSCJSG2&Expires=1516294587&Signature=OSrkcoA2qUTgovxRnYM%2Bur4DGgo%3D\";}','no'),
	(726,'_site_transient_timeout_theme_roots','1516207291','no'),
	(727,'_site_transient_theme_roots','a:4:{s:10:\"blue-heron\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}','no'),
	(729,'_site_transient_update_core','O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.2.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.2.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.2-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.2-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.2\";s:7:\"version\";s:5:\"4.9.2\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1516205493;s:15:\"version_checked\";s:5:\"4.9.2\";s:12:\"translations\";a:0:{}}','no'),
	(730,'auto_core_update_notified','a:4:{s:4:\"type\";s:7:\"success\";s:5:\"email\";s:22:\"developers@ivieinc.com\";s:7:\"version\";s:5:\"4.9.2\";s:9:\"timestamp\";i:1516205493;}','no');

/*!40000 ALTER TABLE `wp_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_postmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_postmeta`;

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_postmeta` WRITE;
/*!40000 ALTER TABLE `wp_postmeta` DISABLE KEYS */;

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`)
VALUES
	(1,2,'_wp_page_template','default'),
	(2,4,'_wp_attached_file','2017/12/wordpress-logo.jpg'),
	(3,4,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:500;s:6:\"height\";i:357;s:4:\"file\";s:26:\"2017/12/wordpress-logo.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"wordpress-logo-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"wordpress-logo-300x214.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:214;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:26:\"wordpress-logo-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(10,8,'_menu_item_type','custom'),
	(11,8,'_menu_item_menu_item_parent','0'),
	(12,8,'_menu_item_object_id','8'),
	(13,8,'_menu_item_object','custom'),
	(14,8,'_menu_item_target',''),
	(15,8,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(16,8,'_menu_item_xfn',''),
	(17,8,'_menu_item_url','http://staging.blueheronproperties.com/'),
	(18,8,'_menu_item_orphaned','1513130695'),
	(19,9,'_menu_item_type','post_type'),
	(20,9,'_menu_item_menu_item_parent','0'),
	(21,9,'_menu_item_object_id','2'),
	(22,9,'_menu_item_object','page'),
	(23,9,'_menu_item_target',''),
	(24,9,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(25,9,'_menu_item_xfn',''),
	(26,9,'_menu_item_url',''),
	(27,9,'_menu_item_orphaned','1513130695'),
	(28,10,'_menu_item_type','post_type'),
	(29,10,'_menu_item_menu_item_parent','0'),
	(30,10,'_menu_item_object_id','2'),
	(31,10,'_menu_item_object','page'),
	(32,10,'_menu_item_target',''),
	(33,10,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(34,10,'_menu_item_xfn',''),
	(35,10,'_menu_item_url',''),
	(39,14,'_edit_last','1'),
	(40,14,'_edit_lock','1514403343:1'),
	(41,16,'_menu_item_type','post_type'),
	(42,16,'_menu_item_menu_item_parent','0'),
	(43,16,'_menu_item_object_id','14'),
	(44,16,'_menu_item_object','page'),
	(45,16,'_menu_item_target',''),
	(46,16,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(47,16,'_menu_item_xfn',''),
	(48,16,'_menu_item_url',''),
	(50,14,'_wp_page_template','full-width.php'),
	(51,18,'_edit_last','1'),
	(52,18,'_edit_lock','1514418091:1'),
	(53,22,'_edit_last','1'),
	(54,22,'_edit_lock','1514404532:1'),
	(55,26,'_edit_last','1'),
	(56,26,'_edit_lock','1514411571:1'),
	(57,36,'_wp_attached_file','2017/12/header-logo.jpg'),
	(58,36,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:152;s:6:\"height\";i:111;s:4:\"file\";s:23:\"2017/12/header-logo.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"header-logo-150x111.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:111;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(59,37,'_wp_attached_file','2017/12/footer-logo.jpg'),
	(60,37,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:75;s:6:\"height\";i:114;s:4:\"file\";s:23:\"2017/12/footer-logo.jpg\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(61,38,'_edit_last','1'),
	(62,38,'_edit_lock','1514411915:1'),
	(63,41,'_edit_last','1'),
	(64,41,'_edit_lock','1514413346:1'),
	(65,44,'_wp_attached_file','2017/12/slider1.jpg'),
	(66,44,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:721;s:4:\"file\";s:19:\"2017/12/slider1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"slider1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"slider1-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"slider1-768x396.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:396;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"slider1-1024x527.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:527;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(67,45,'_wp_attached_file','2017/12/slider2.jpg'),
	(68,45,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:720;s:4:\"file\";s:19:\"2017/12/slider2.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"slider2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"slider2-300x154.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"slider2-768x395.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:395;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"slider2-1024x527.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:527;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(69,46,'_wp_attached_file','2017/12/slider3.jpg'),
	(70,46,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:720;s:4:\"file\";s:19:\"2017/12/slider3.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"slider3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"slider3-300x154.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:154;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:19:\"slider3-768x395.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:395;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"slider3-1024x527.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:527;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(71,47,'_edit_last','1'),
	(72,47,'_edit_lock','1514415330:1'),
	(73,56,'_wp_attached_file','2017/12/houseonthegreen.jpg'),
	(74,56,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:290;s:6:\"height\";i:249;s:4:\"file\";s:27:\"2017/12/houseonthegreen.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"houseonthegreen-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(75,57,'_wp_attached_file','2017/12/house2.jpg'),
	(76,57,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:295;s:6:\"height\";i:248;s:4:\"file\";s:18:\"2017/12/house2.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"house2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(77,58,'_wp_attached_file','2017/12/dock.jpg'),
	(78,58,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:292;s:6:\"height\";i:253;s:4:\"file\";s:16:\"2017/12/dock.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"dock-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(79,59,'_wp_attached_file','2017/12/house3.jpg'),
	(80,59,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:294;s:6:\"height\";i:248;s:4:\"file\";s:18:\"2017/12/house3.jpg\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"house3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(81,64,'_edit_last','1'),
	(82,64,'_edit_lock','1514419006:1'),
	(83,68,'_edit_last','1'),
	(84,68,'_edit_lock','1514417744:1'),
	(87,73,'_wp_attached_file','2017/12/new-header-bg.jpg'),
	(88,73,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:810;s:4:\"file\";s:25:\"2017/12/new-header-bg.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"new-header-bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"new-header-bg-300x174.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:174;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"new-header-bg-768x444.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:444;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"new-header-bg-1024x592.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:592;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(91,76,'_wp_attached_file','2017/12/new-fishing-bg.jpg'),
	(92,76,'_wp_attachment_metadata','a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:429;s:4:\"file\";s:26:\"2017/12/new-fishing-bg.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"new-fishing-bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"new-fishing-bg-300x92.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:92;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"new-fishing-bg-768x235.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:235;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"new-fishing-bg-1024x314.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:314;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
	(93,2,'_edit_lock','1514473903:1'),
	(94,2,'_edit_last','1'),
	(95,79,'_edit_last','1'),
	(96,79,'_wp_page_template','default'),
	(97,79,'_edit_lock','1515441413:1'),
	(98,81,'_menu_item_type','post_type'),
	(99,81,'_menu_item_menu_item_parent','0'),
	(100,81,'_menu_item_object_id','79'),
	(101,81,'_menu_item_object','page'),
	(102,81,'_menu_item_target',''),
	(103,81,'_menu_item_classes','a:1:{i:0;s:0:\"\";}'),
	(104,81,'_menu_item_xfn',''),
	(105,81,'_menu_item_url','');

/*!40000 ALTER TABLE `wp_postmeta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_posts`;

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_posts` WRITE;
/*!40000 ALTER TABLE `wp_posts` DISABLE KEYS */;

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`)
VALUES
	(1,1,'2017-12-12 17:43:38','2017-12-12 17:43:38','Welcome to WordPress. This is your first post. Edit or delete it, then start writing!','Hello world!','','publish','open','open','','hello-world','','','2017-12-12 17:43:38','2017-12-12 17:43:38','',0,'http://staging.blueheronproperties.com/?p=1',0,'post','',1),
	(2,1,'2017-12-12 17:43:38','2017-12-12 17:43:38','[gravityform id=\"1\" title=\"false\" description=\"false\"]\r\n\r\n&nbsp;\r\n\r\nThis is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"http://staging.blueheronproperties.com/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','publish','closed','open','','sample-page','','','2017-12-28 15:11:46','2017-12-28 15:11:46','',0,'http://staging.blueheronproperties.com/?page_id=2',0,'page','',0),
	(4,1,'2017-12-12 23:08:38','2017-12-12 23:08:38','','wordpress-logo','','inherit','open','closed','','wordpress-logo','','','2017-12-12 23:08:38','2017-12-12 23:08:38','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/wordpress-logo.jpg',0,'attachment','image/jpeg',0),
	(8,1,'2017-12-13 02:04:55','0000-00-00 00:00:00','','Home','','draft','closed','closed','','','','','2017-12-13 02:04:55','0000-00-00 00:00:00','',0,'http://staging.blueheronproperties.com/?p=8',1,'nav_menu_item','',0),
	(9,1,'2017-12-13 02:04:55','0000-00-00 00:00:00',' ','','','draft','closed','closed','','','','','2017-12-13 02:04:55','0000-00-00 00:00:00','',0,'http://staging.blueheronproperties.com/?p=9',1,'nav_menu_item','',0),
	(10,1,'2017-12-13 02:05:39','2017-12-13 02:05:39',' ','','','publish','closed','closed','','10','','','2018-01-11 15:17:24','2018-01-11 15:17:24','',0,'http://staging.blueheronproperties.com/?p=10',2,'nav_menu_item','',0),
	(14,1,'2017-12-27 14:44:38','2017-12-27 14:44:38','Irure jowl fugiat cupim porchetta. Pastrami fugiat ad chuck, andouille in irure picanha incididunt nostrud occaecat bresaola ham labore. Doner sed ham kevin magna shoulder porchetta ea pork ut. Ham fatback cillum non ex sunt frankfurter laborum picanha in. Veniam capicola quis, kevin venison velit frankfurter dolore irure in non nostrud sed. Ham et nostrud enim veniam, cow ut. Labore tempor biltong shank, ut venison irure incididunt meatloaf in ullamco tongue.\r\n\r\nSirloin nisi shoulder tenderloin meatball eiusmod pancetta meatloaf. Culpa short ribs corned beef venison pork loin, esse deserunt ground round. Aliqua in occaecat sint. Beef bresaola boudin turkey elit. Short loin sint fatback, tri-tip bresaola pig magna laborum ut kevin. Sed tri-tip nulla sausage. Beef ribs voluptate cow cupim consectetur et enim commodo short loin corned beef sunt.\r\n\r\nCorned beef fugiat lorem eu tempor, ut shank frankfurter salami spare ribs quis do fatback in biltong. Chuck nulla ipsum ham hock ut do nostrud duis laborum. Ut flank non aliquip. Picanha fatback fugiat, ham ea jowl tail. Shankle lorem brisket reprehenderit aliquip adipisicing consequat.\r\n\r\nOccaecat strip steak biltong tempor, swine ad magna cupim sint ullamco t-bone duis commodo. Non meatloaf culpa voluptate capicola in tail mollit sunt jerky duis nisi aliquip aute. Hamburger ground round filet mignon flank, irure tenderloin nisi commodo turkey consequat. Short ribs dolore meatball consequat adipisicing rump ex.\r\n\r\nLorem exercitation laboris cupim kevin buffalo salami fatback. Filet mignon laboris doner, est ham kevin ipsum ex rump anim t-bone consequat venison ut spare ribs. Meatloaf exercitation elit, pig tri-tip sunt shank magna prosciutto porchetta biltong enim. Porchetta aute pork chop bacon. Ham drumstick shankle pork belly rump, tempor esse.','Houses','','publish','closed','closed','','houses','','','2017-12-27 18:15:32','2017-12-27 18:15:32','',0,'http://staging.blueheronproperties.com/?page_id=14',0,'page','',0),
	(15,1,'2017-12-27 14:44:38','2017-12-27 14:44:38','','Houses','','inherit','closed','closed','','14-revision-v1','','','2017-12-27 14:44:38','2017-12-27 14:44:38','',14,'http://staging.blueheronproperties.com/14-revision-v1/',0,'revision','',0),
	(16,1,'2017-12-27 14:44:51','2017-12-27 14:44:51',' ','','','publish','closed','closed','','16','','','2018-01-11 15:17:24','2018-01-11 15:17:24','',0,'http://staging.blueheronproperties.com/?p=16',1,'nav_menu_item','',0),
	(17,1,'2017-12-27 14:55:45','2017-12-27 14:55:45','Irure jowl fugiat cupim porchetta. Pastrami fugiat ad chuck, andouille in irure picanha incididunt nostrud occaecat bresaola ham labore. Doner sed ham kevin magna shoulder porchetta ea pork ut. Ham fatback cillum non ex sunt frankfurter laborum picanha in. Veniam capicola quis, kevin venison velit frankfurter dolore irure in non nostrud sed. Ham et nostrud enim veniam, cow ut. Labore tempor biltong shank, ut venison irure incididunt meatloaf in ullamco tongue.\r\n\r\nSirloin nisi shoulder tenderloin meatball eiusmod pancetta meatloaf. Culpa short ribs corned beef venison pork loin, esse deserunt ground round. Aliqua in occaecat sint. Beef bresaola boudin turkey elit. Short loin sint fatback, tri-tip bresaola pig magna laborum ut kevin. Sed tri-tip nulla sausage. Beef ribs voluptate cow cupim consectetur et enim commodo short loin corned beef sunt.\r\n\r\nCorned beef fugiat lorem eu tempor, ut shank frankfurter salami spare ribs quis do fatback in biltong. Chuck nulla ipsum ham hock ut do nostrud duis laborum. Ut flank non aliquip. Picanha fatback fugiat, ham ea jowl tail. Shankle lorem brisket reprehenderit aliquip adipisicing consequat.\r\n\r\nOccaecat strip steak biltong tempor, swine ad magna cupim sint ullamco t-bone duis commodo. Non meatloaf culpa voluptate capicola in tail mollit sunt jerky duis nisi aliquip aute. Hamburger ground round filet mignon flank, irure tenderloin nisi commodo turkey consequat. Short ribs dolore meatball consequat adipisicing rump ex.\r\n\r\nLorem exercitation laboris cupim kevin buffalo salami fatback. Filet mignon laboris doner, est ham kevin ipsum ex rump anim t-bone consequat venison ut spare ribs. Meatloaf exercitation elit, pig tri-tip sunt shank magna prosciutto porchetta biltong enim. Porchetta aute pork chop bacon. Ham drumstick shankle pork belly rump, tempor esse.','Houses','','inherit','closed','closed','','14-revision-v1','','','2017-12-27 14:55:45','2017-12-27 14:55:45','',14,'http://staging.blueheronproperties.com/14-revision-v1/',0,'revision','',0),
	(18,1,'2017-12-27 19:40:17','2017-12-27 19:40:17','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:21:\"acf-options-bh-header\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Header Section','bh-header-section','publish','closed','closed','','group_5a43f6d091039','','','2017-12-27 23:39:47','2017-12-27 23:39:47','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=18',0,'acf-field-group','',0),
	(19,1,'2017-12-27 19:40:17','2017-12-27 19:40:17','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:44:\"Please enter the text for the Header Heading\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Header Heading','header_heading','publish','closed','closed','','field_5a43f6d74d754','','','2017-12-27 19:40:17','2017-12-27 19:40:17','',18,'http://staging.blueheronproperties.com/?post_type=acf-field&p=19',0,'acf-field','',0),
	(20,1,'2017-12-27 19:40:17','2017-12-27 19:40:17','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:41:\"Please enter the Text for the Header Link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Header Link Text','header_link_text','publish','closed','closed','','field_5a43f6ef4d755','','','2017-12-27 19:40:17','2017-12-27 19:40:17','',18,'http://staging.blueheronproperties.com/?post_type=acf-field&p=20',1,'acf-field','',0),
	(21,1,'2017-12-27 19:40:17','2017-12-27 19:40:17','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:40:\"Please enter the URL for the Header Link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Header Link URL','header_link_url','publish','closed','closed','','field_5a43f7124d756','','','2017-12-27 19:40:17','2017-12-27 19:40:17','',18,'http://staging.blueheronproperties.com/?post_type=acf-field&p=21',2,'acf-field','',0),
	(22,1,'2017-12-27 19:56:37','2017-12-27 19:56:37','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:27:\"acf-options-bh-social-media\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Social Media','bh-social-media','publish','closed','closed','','group_5a43faa89c373','','','2017-12-27 19:57:51','2017-12-27 19:57:51','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=22',0,'acf-field-group','',0),
	(23,1,'2017-12-27 19:56:37','2017-12-27 19:56:37','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:29:\"Please enter the Facebook URL\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Facebook URL','facebook_url','publish','closed','closed','','field_5a43fab53fd7f','','','2017-12-27 19:56:37','2017-12-27 19:56:37','',22,'http://staging.blueheronproperties.com/?post_type=acf-field&p=23',0,'acf-field','',0),
	(24,1,'2017-12-27 19:56:37','2017-12-27 19:56:37','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:30:\"Please enter the Instagram URL\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Instagram URL','instagram_url','publish','closed','closed','','field_5a43faca3fd80','','','2017-12-27 19:56:37','2017-12-27 19:56:37','',22,'http://staging.blueheronproperties.com/?post_type=acf-field&p=24',1,'acf-field','',0),
	(25,1,'2017-12-27 19:56:37','2017-12-27 19:56:37','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:26:\"Please enter the Vimeo URL\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Vimeo URL','vimeo_url','publish','closed','closed','','field_5a43fae13fd81','','','2017-12-27 19:56:37','2017-12-27 19:56:37','',22,'http://staging.blueheronproperties.com/?post_type=acf-field&p=25',2,'acf-field','',0),
	(26,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Theme Settings','bh-theme-settings','publish','closed','closed','','group_5a43fcc94ab40','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=26',0,'acf-field-group','',0),
	(27,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:31:\"Please upload Header Logo image\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','BH Header Logo','bh_header_logo','publish','closed','closed','','field_5a43fcd290515','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=27',0,'acf-field','',0),
	(28,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:31:\"Please upload Footer Logo image\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Footer Logo','footer_logo','publish','closed','closed','','field_5a43fd0090516','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=28',1,'acf-field','',0),
	(29,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:27:\"Please enter Street Address\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Address','address','publish','closed','closed','','field_5a43fd1f90517','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=29',2,'acf-field','',0),
	(30,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:17:\"Please enter City\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','City','city','publish','closed','closed','','field_5a43fd2b90518','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=30',3,'acf-field','',0),
	(31,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:22:\"Please enter the State\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','State','state','publish','closed','closed','','field_5a43fd4390519','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=31',4,'acf-field','',0),
	(32,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:25:\"Please enter the Zip code\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Zip','zip','publish','closed','closed','','field_5a43fd4e9051a','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=32',5,'acf-field','',0),
	(33,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:29:\"Please enter the phone number\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Phone Number','phone_number','publish','closed','closed','','field_5a43fd6a9051b','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=33',6,'acf-field','',0),
	(34,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:30:\"Please enter the Email address\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}','Email','email','publish','closed','closed','','field_5a43fd7e9051c','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=34',7,'acf-field','',0),
	(35,1,'2017-12-27 20:08:12','2017-12-27 20:08:12','a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:72:\"Please enter the Copyright text which appears at the bottom of the page.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}','Copyright Text','copyright_text','publish','closed','closed','','field_5a43fd979051d','','','2017-12-27 20:08:12','2017-12-27 20:08:12','',26,'http://staging.blueheronproperties.com/?post_type=acf-field&p=35',8,'acf-field','',0),
	(36,1,'2017-12-27 20:08:49','2017-12-27 20:08:49','','header-logo','','inherit','open','closed','','header-logo','','','2017-12-27 20:08:51','2017-12-27 20:08:51','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/header-logo.jpg',0,'attachment','image/jpeg',0),
	(37,1,'2017-12-27 20:08:59','2017-12-27 20:08:59','','footer-logo','','inherit','open','closed','','footer-logo','','','2017-12-27 20:08:59','2017-12-27 20:08:59','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/footer-logo.jpg',0,'attachment','image/jpeg',0),
	(38,1,'2017-12-27 21:57:25','2017-12-27 21:57:25','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:25:\"acf-options-bh-sub-header\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Sub Header Section','bh-sub-header-section','publish','closed','closed','','group_5a4416bd3c413','','','2017-12-27 21:57:25','2017-12-27 21:57:25','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=38',0,'acf-field-group','',0),
	(39,1,'2017-12-27 21:57:25','2017-12-27 21:57:25','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:51:\"Please enter the Heading for the Sub Header Section\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Sub Header Heading','sub_header_heading','publish','closed','closed','','field_5a4416c1844cc','','','2017-12-27 21:57:25','2017-12-27 21:57:25','',38,'http://staging.blueheronproperties.com/?post_type=acf-field&p=39',0,'acf-field','',0),
	(40,1,'2017-12-27 21:57:25','2017-12-27 21:57:25','a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:59:\"Please enter the content for the Sub Header Content Section\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}','Sub Header Content','sub_header_content','publish','closed','closed','','field_5a441705844cd','','','2017-12-27 21:57:25','2017-12-27 21:57:25','',38,'http://staging.blueheronproperties.com/?post_type=acf-field&p=40',1,'acf-field','',0),
	(41,1,'2017-12-27 22:02:44','2017-12-27 22:02:44','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:31:\"acf-options-bh-home-page-slider\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Home Page Slider','bh-home-page-slider','publish','closed','closed','','group_5a44183149eb0','','','2017-12-27 22:03:37','2017-12-27 22:03:37','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=41',0,'acf-field-group','',0),
	(42,1,'2017-12-27 22:02:44','2017-12-27 22:02:44','a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}','Home Page Slider Images','home_page_slider_images','publish','closed','closed','','field_5a441839447e0','','','2017-12-27 22:02:44','2017-12-27 22:02:44','',41,'http://staging.blueheronproperties.com/?post_type=acf-field&p=42',0,'acf-field','',0),
	(43,1,'2017-12-27 22:02:44','2017-12-27 22:02:44','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:44:\"Please upload image for the Home page slider\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Slider Images','slider_images','publish','closed','closed','','field_5a44185b447e1','','','2017-12-27 22:02:44','2017-12-27 22:02:44','',42,'http://staging.blueheronproperties.com/?post_type=acf-field&p=43',0,'acf-field','',0),
	(44,1,'2017-12-27 22:05:10','2017-12-27 22:05:10','','slider1','','inherit','open','closed','','slider1','','','2017-12-27 22:05:13','2017-12-27 22:05:13','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/slider1.jpg',0,'attachment','image/jpeg',0),
	(45,1,'2017-12-27 22:05:30','2017-12-27 22:05:30','','slider2','','inherit','open','closed','','slider2','','','2017-12-27 22:05:41','2017-12-27 22:05:41','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/slider2.jpg',0,'attachment','image/jpeg',0),
	(46,1,'2017-12-27 22:05:54','2017-12-27 22:05:54','','slider3','','inherit','open','closed','','slider3','','','2017-12-27 22:06:10','2017-12-27 22:06:10','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/slider3.jpg',0,'attachment','image/jpeg',0),
	(47,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:28:\"acf-options-bh-image-section\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Image Section','bh-image-section','publish','closed','closed','','group_5a441dd811c3a','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=47',0,'acf-field-group','',0),
	(48,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:55:\"Please upload the Image for the Top Left Image Section.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image Top Left','image_top_left','publish','closed','closed','','field_5a441ddcba882','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=48',1,'acf-field','',0),
	(49,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:56:\"Please upload the Image for the Top Right Image Section.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image Top Right','image_top_right','publish','closed','closed','','field_5a441df9ba883','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=49',3,'acf-field','',0),
	(50,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:58:\"Please upload the Image for the Bottom Left Image Section.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image Bottom Left','image_bottom_left','publish','closed','closed','','field_5a441e25ba884','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=50',5,'acf-field','',0),
	(51,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:58:\"Please upload the Image for the Bottom Left Image Section.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Image Bottom Right','image_bottom_right','publish','closed','closed','','field_5a441e4cba885','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=51',7,'acf-field','',0),
	(52,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:42:\"Please enter text for Image Section Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Image Section Header','image_section_header','publish','closed','closed','','field_5a441e75ba886','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=52',8,'acf-field','',0),
	(53,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:38:\"Please enter content for Image Section\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}','Image Section Content','image_section_content','publish','closed','closed','','field_5a441e8bba887','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=53',9,'acf-field','',0),
	(54,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:44:\"Please enter text for the Image Section Link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Image Section Link Text','image_section_link_text','publish','closed','closed','','field_5a441eadba888','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=54',10,'acf-field','',0),
	(55,1,'2017-12-27 22:30:19','2017-12-27 22:30:19','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:47:\"Please enter the URL for the Image Section Link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','Image Section Link URL','image_section_link_url','publish','closed','closed','','field_5a441ec9ba889','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=55',11,'acf-field','',0),
	(56,1,'2017-12-27 22:31:09','2017-12-27 22:31:09','','houseonthegreen','','inherit','open','closed','','houseonthegreen','','','2017-12-27 22:31:21','2017-12-27 22:31:21','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/houseonthegreen.jpg',0,'attachment','image/jpeg',0),
	(57,1,'2017-12-27 22:31:40','2017-12-27 22:31:40','','house2','','inherit','open','closed','','house2','','','2017-12-27 22:31:43','2017-12-27 22:31:43','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/house2.jpg',0,'attachment','image/jpeg',0),
	(58,1,'2017-12-27 22:32:05','2017-12-27 22:32:05','','dock','','inherit','open','closed','','dock','','','2017-12-27 22:32:07','2017-12-27 22:32:07','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/dock.jpg',0,'attachment','image/jpeg',0),
	(59,1,'2017-12-27 22:32:26','2017-12-27 22:32:26','','house3','','inherit','open','closed','','house3','','','2017-12-27 22:32:30','2017-12-27 22:32:30','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/house3.jpg',0,'attachment','image/jpeg',0),
	(60,1,'2017-12-27 22:44:11','2017-12-27 22:44:11','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:51:\"Please enter the text for the Image Top Left Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Image Top Left Header','image_top_left_header','publish','closed','closed','','field_5a4421677009c','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&p=60',0,'acf-field','',0),
	(61,1,'2017-12-27 22:44:11','2017-12-27 22:44:11','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:52:\"Please enter the text for the Image Top Right Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Image Top Right Header','image_top_right_header','publish','closed','closed','','field_5a4421a57009d','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&p=61',2,'acf-field','',0),
	(62,1,'2017-12-27 22:44:11','2017-12-27 22:44:11','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:54:\"Please enter the text for the Image Bottom Left Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Image Bottom Left Header','image_bottom_left_header','publish','closed','closed','','field_5a4421cc7009e','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&p=62',4,'acf-field','',0),
	(63,1,'2017-12-27 22:44:11','2017-12-27 22:44:11','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:55:\"Please enter the text for the Image Bottom Right Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Image Bottom Right Header','image_bottom_right_header','publish','closed','closed','','field_5a4421fc7009f','','','2017-12-27 22:44:11','2017-12-27 22:44:11','',47,'http://staging.blueheronproperties.com/?post_type=acf-field&p=63',6,'acf-field','',0),
	(64,1,'2017-12-27 22:59:49','2017-12-27 22:59:49','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:35:\"acf-options-bh-possibilites-section\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Possibilites Section','bh-possibilites-section','publish','closed','closed','','group_5a442572a76a8','','','2017-12-27 23:58:09','2017-12-27 23:58:09','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=64',0,'acf-field-group','',0),
	(65,1,'2017-12-27 22:59:49','2017-12-27 22:59:49','a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:49:\"Please enter text for the BH Possibilities Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:13:\"default_value\";s:0:\"\";s:5:\"delay\";i:0;}','BH Possibilities Header','bh_possibilities_header','publish','closed','closed','','field_5a442581f13f7','','','2017-12-27 23:17:30','2017-12-27 23:17:30','',64,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=65',0,'acf-field','',0),
	(66,1,'2017-12-27 22:59:49','2017-12-27 22:59:49','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:51:\"Please enter the text for the BH Possibilities Link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','BH Possibilities Link Text','bh_possibilities_link_text','publish','closed','closed','','field_5a44259bf13f8','','','2017-12-27 22:59:49','2017-12-27 22:59:49','',64,'http://staging.blueheronproperties.com/?post_type=acf-field&p=66',1,'acf-field','',0),
	(67,1,'2017-12-27 22:59:49','2017-12-27 22:59:49','a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:50:\"Please enter the URL for the BH Possibilities Link\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}','BH Possibilities Link URL','bh_possibilities_link_url','publish','closed','closed','','field_5a4425bef13f9','','','2017-12-27 22:59:49','2017-12-27 22:59:49','',64,'http://staging.blueheronproperties.com/?post_type=acf-field&p=67',2,'acf-field','',0),
	(68,1,'2017-12-27 23:25:32','2017-12-27 23:25:32','a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:33:\"acf-options-bh-pre-footer-section\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}','BH Pre Footer Section','bh-pre-footer-section','publish','closed','closed','','group_5a442b1db8dd4','','','2017-12-27 23:25:32','2017-12-27 23:25:32','',0,'http://staging.blueheronproperties.com/?post_type=acf-field-group&#038;p=68',0,'acf-field-group','',0),
	(69,1,'2017-12-27 23:25:32','2017-12-27 23:25:32','a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:47:\"Please enter the text for the Pre Footer Header\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}','Pre Footer Header','pre_footer_header','publish','closed','closed','','field_5a442b275012b','','','2017-12-27 23:25:32','2017-12-27 23:25:32','',68,'http://staging.blueheronproperties.com/?post_type=acf-field&p=69',0,'acf-field','',0),
	(70,1,'2017-12-27 23:25:32','2017-12-27 23:25:32','a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:51:\"Please enter the content for the Pre Footer Section\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}','Pre Footer Content','pre_footer_content','publish','closed','closed','','field_5a442b665012c','','','2017-12-27 23:25:32','2017-12-27 23:25:32','',68,'http://staging.blueheronproperties.com/?post_type=acf-field&p=70',1,'acf-field','',0),
	(71,1,'2017-12-27 23:39:47','2017-12-27 23:39:47','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','Header Background Image','header_background_image','publish','closed','closed','','field_5a442f2c9bc33','','','2017-12-27 23:39:47','2017-12-27 23:39:47','',18,'http://staging.blueheronproperties.com/?post_type=acf-field&p=71',3,'acf-field','',0),
	(73,1,'2017-12-27 23:44:27','2017-12-27 23:44:27','','new-header-bg','','inherit','open','closed','','new-header-bg','','','2017-12-27 23:44:39','2017-12-27 23:44:39','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/new-header-bg.jpg',0,'attachment','image/jpeg',0),
	(74,1,'2017-12-27 23:48:42','2017-12-27 23:48:42','a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:49:\"Please upload the Possibilities Background Image.\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:9:\"thumbnail\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}','BH Possibilities Background Image','bh_possibilities_background_image','publish','closed','closed','','field_5a443119d7581','','','2017-12-27 23:58:09','2017-12-27 23:58:09','',64,'http://staging.blueheronproperties.com/?post_type=acf-field&#038;p=74',3,'acf-field','',0),
	(76,1,'2017-12-27 23:53:19','2017-12-27 23:53:19','','new-fishing-bg','','inherit','open','closed','','new-fishing-bg','','','2017-12-27 23:53:32','2017-12-27 23:53:32','',0,'http://staging.blueheronproperties.com/wp-content/uploads/2017/12/new-fishing-bg.jpg',0,'attachment','image/jpeg',0),
	(77,1,'2017-12-28 15:11:46','2017-12-28 15:11:46','[gravityform id=\"1\" title=\"false\" description=\"false\"]\r\n\r\n&nbsp;\r\n\r\nThis is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\r\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like piña coladas. (And gettin\' caught in the rain.)</blockquote>\r\n...or something like this:\r\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\r\nAs a new WordPress user, you should go to <a href=\"http://staging.blueheronproperties.com/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!','Sample Page','','inherit','closed','closed','','2-revision-v1','','','2017-12-28 15:11:46','2017-12-28 15:11:46','',2,'http://staging.blueheronproperties.com/2-revision-v1/',0,'revision','',0),
	(79,1,'2018-01-08 19:54:06','2018-01-08 19:54:06','test','test','','publish','closed','closed','','test','','','2018-01-08 19:54:06','2018-01-08 19:54:06','',0,'http://staging.blueheronproperties.com/?page_id=79',0,'page','',0),
	(80,1,'2018-01-08 19:54:06','2018-01-08 19:54:06','test','test','','inherit','closed','closed','','79-revision-v1','','','2018-01-08 19:54:06','2018-01-08 19:54:06','',79,'http://staging.blueheronproperties.com/79-revision-v1/',0,'revision','',0),
	(81,1,'2018-01-08 19:54:19','2018-01-08 19:54:19',' ','','','publish','closed','closed','','81','','','2018-01-11 15:17:24','2018-01-11 15:17:24','',0,'http://staging.blueheronproperties.com/?p=81',3,'nav_menu_item','',0);

/*!40000 ALTER TABLE `wp_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_rg_form
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_form`;

CREATE TABLE `wp_rg_form` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_trash` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_rg_form` WRITE;
/*!40000 ALTER TABLE `wp_rg_form` DISABLE KEYS */;

INSERT INTO `wp_rg_form` (`id`, `title`, `date_created`, `is_active`, `is_trash`)
VALUES
	(1,'Contact Form','2017-12-13 01:36:24',1,0);

/*!40000 ALTER TABLE `wp_rg_form` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_rg_form_meta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_form_meta`;

CREATE TABLE `wp_rg_form_meta` (
  `form_id` mediumint(8) unsigned NOT NULL,
  `display_meta` longtext COLLATE utf8mb4_unicode_ci,
  `entries_grid_meta` longtext COLLATE utf8mb4_unicode_ci,
  `confirmations` longtext COLLATE utf8mb4_unicode_ci,
  `notifications` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_rg_form_meta` WRITE;
/*!40000 ALTER TABLE `wp_rg_form_meta` DISABLE KEYS */;

INSERT INTO `wp_rg_form_meta` (`form_id`, `display_meta`, `entries_grid_meta`, `confirmations`, `notifications`)
VALUES
	(1,'{\"title\":\"Contact Form\",\"description\":\"Contact Form\",\"labelPlacement\":\"left_label\",\"descriptionPlacement\":\"below\",\"button\":{\"type\":\"text\",\"text\":\"Submit\",\"imageUrl\":\"\"},\"fields\":[{\"type\":\"text\",\"id\":1,\"label\":\"FIRST AND LAST NAME\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"large\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"bhinput\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"email\",\"id\":3,\"label\":\"EMAIL\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"large\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"emailConfirmEnabled\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"phone\",\"id\":4,\"label\":\"PHONE\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"large\",\"errorMessage\":\"\",\"inputs\":null,\"phoneFormat\":\"standard\",\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"form_id\":\"\",\"productField\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"select\",\"id\":5,\"label\":\"PROPERTY INTEREST\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"large\",\"errorMessage\":\"\",\"inputs\":null,\"choices\":[{\"text\":\"FAIRWAYS AND GREENS\",\"value\":\"FAIRWAYS AND GREENS\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"HOUSES\",\"value\":\"HOUSES\",\"isSelected\":false,\"price\":\"\"},{\"text\":\"FEATURES\",\"value\":\"FEATURES\",\"isSelected\":false,\"price\":\"\"}],\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePrice\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"},{\"type\":\"text\",\"id\":7,\"label\":\"COMMENTS\",\"adminLabel\":\"\",\"isRequired\":false,\"size\":\"large\",\"errorMessage\":\"\",\"inputs\":null,\"formId\":1,\"description\":\"\",\"allowsPrepopulate\":false,\"inputMask\":false,\"inputMaskValue\":\"\",\"inputType\":\"\",\"labelPlacement\":\"\",\"descriptionPlacement\":\"\",\"subLabelPlacement\":\"\",\"placeholder\":\"\",\"cssClass\":\"\",\"inputName\":\"\",\"visibility\":\"visible\",\"noDuplicates\":false,\"defaultValue\":\"\",\"choices\":\"\",\"conditionalLogic\":\"\",\"productField\":\"\",\"enablePasswordInput\":\"\",\"maxLength\":\"\",\"multipleFiles\":false,\"maxFiles\":\"\",\"calculationFormula\":\"\",\"calculationRounding\":\"\",\"enableCalculation\":\"\",\"disableQuantity\":false,\"displayAllCategories\":false,\"useRichTextEditor\":false,\"pageNumber\":1,\"displayOnly\":\"\"}],\"version\":\"2.2.5\",\"id\":1,\"useCurrentUserAsAuthor\":true,\"postContentTemplateEnabled\":false,\"postTitleTemplateEnabled\":false,\"postTitleTemplate\":\"\",\"postContentTemplate\":\"\",\"lastPageButton\":null,\"pagination\":null,\"firstPageCssClass\":null,\"subLabelPlacement\":\"below\",\"cssClass\":\"\",\"enableHoneypot\":false,\"enableAnimation\":false,\"save\":{\"enabled\":false,\"button\":{\"type\":\"link\",\"text\":\"Save and Continue Later\"}},\"limitEntries\":false,\"limitEntriesCount\":\"\",\"limitEntriesPeriod\":\"\",\"limitEntriesMessage\":\"\",\"scheduleForm\":false,\"scheduleStart\":\"\",\"scheduleStartHour\":\"\",\"scheduleStartMinute\":\"\",\"scheduleStartAmpm\":\"\",\"scheduleEnd\":\"\",\"scheduleEndHour\":\"\",\"scheduleEndMinute\":\"\",\"scheduleEndAmpm\":\"\",\"schedulePendingMessage\":\"\",\"scheduleMessage\":\"\",\"requireLogin\":false,\"requireLoginMessage\":\"\"}','','{\"5a308418e2cdd\":{\"id\":\"5a308418e2cdd\",\"name\":\"Default Confirmation\",\"isDefault\":true,\"type\":\"message\",\"message\":\"Thanks for contacting us! We will get in touch with you shortly.\",\"url\":\"\",\"pageId\":\"\",\"queryString\":\"\"}}','{\"5a308418e2696\":{\"id\":\"5a308418e2696\",\"to\":\"{admin_email}\",\"name\":\"Admin Notification\",\"event\":\"form_submission\",\"toType\":\"email\",\"subject\":\"New submission from {form_title}\",\"message\":\"{all_fields}\"}}');

/*!40000 ALTER TABLE `wp_rg_form_meta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_rg_form_view
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_form_view`;

CREATE TABLE `wp_rg_form_view` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` char(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `count` mediumint(8) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `date_created` (`date_created`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_rg_form_view` WRITE;
/*!40000 ALTER TABLE `wp_rg_form_view` DISABLE KEYS */;

INSERT INTO `wp_rg_form_view` (`id`, `form_id`, `date_created`, `ip`, `count`)
VALUES
	(1,1,'2018-01-17 16:15:04','',2);

/*!40000 ALTER TABLE `wp_rg_form_view` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_rg_incomplete_submissions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_incomplete_submissions`;

CREATE TABLE `wp_rg_incomplete_submissions` (
  `uuid` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `form_id` mediumint(8) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `ip` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `submission` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `form_id` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_rg_lead
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_lead`;

CREATE TABLE `wp_rg_lead` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL,
  `post_id` bigint(20) unsigned DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `is_starred` tinyint(1) NOT NULL DEFAULT '0',
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  `ip` varchar(39) COLLATE utf8mb4_unicode_ci NOT NULL,
  `source_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_agent` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `currency` varchar(5) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_date` datetime DEFAULT NULL,
  `payment_amount` decimal(19,2) DEFAULT NULL,
  `payment_method` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `transaction_id` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_fulfilled` tinyint(1) DEFAULT NULL,
  `created_by` bigint(20) unsigned DEFAULT NULL,
  `transaction_type` tinyint(1) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_rg_lead_detail
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_lead_detail`;

CREATE TABLE `wp_rg_lead_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` int(10) unsigned NOT NULL,
  `form_id` mediumint(8) unsigned NOT NULL,
  `field_number` float NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `form_id` (`form_id`),
  KEY `lead_id` (`lead_id`),
  KEY `lead_field_number` (`lead_id`,`field_number`),
  KEY `lead_field_value` (`value`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_rg_lead_detail_long
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_lead_detail_long`;

CREATE TABLE `wp_rg_lead_detail_long` (
  `lead_detail_id` bigint(20) unsigned NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lead_detail_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_rg_lead_meta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_lead_meta`;

CREATE TABLE `wp_rg_lead_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `form_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lead_id` bigint(20) unsigned NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `meta_key` (`meta_key`(191)),
  KEY `lead_id` (`lead_id`),
  KEY `form_id_meta_key` (`form_id`,`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_rg_lead_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_rg_lead_notes`;

CREATE TABLE `wp_rg_lead_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lead_id` int(10) unsigned NOT NULL,
  `user_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci,
  `note_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lead_id` (`lead_id`),
  KEY `lead_user_key` (`lead_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_term_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_term_relationships`;

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_term_relationships` WRITE;
/*!40000 ALTER TABLE `wp_term_relationships` DISABLE KEYS */;

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`)
VALUES
	(1,1,0),
	(10,2,0),
	(16,2,0),
	(81,2,0);

/*!40000 ALTER TABLE `wp_term_relationships` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_term_taxonomy
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_term_taxonomy`;

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_term_taxonomy` WRITE;
/*!40000 ALTER TABLE `wp_term_taxonomy` DISABLE KEYS */;

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`)
VALUES
	(1,1,'category','',0,1),
	(2,2,'nav_menu','',0,3);

/*!40000 ALTER TABLE `wp_term_taxonomy` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_termmeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_termmeta`;

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table wp_terms
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_terms`;

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_terms` WRITE;
/*!40000 ALTER TABLE `wp_terms` DISABLE KEYS */;

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`)
VALUES
	(1,'Uncategorized','uncategorized',0),
	(2,'mainmenu','mainmenu',0);

/*!40000 ALTER TABLE `wp_terms` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_usermeta
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_usermeta`;

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_usermeta` WRITE;
/*!40000 ALTER TABLE `wp_usermeta` DISABLE KEYS */;

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`)
VALUES
	(1,1,'nickname','digivie'),
	(2,1,'first_name',''),
	(3,1,'last_name',''),
	(4,1,'description',''),
	(5,1,'rich_editing','true'),
	(6,1,'syntax_highlighting','true'),
	(7,1,'comment_shortcuts','false'),
	(8,1,'admin_color','fresh'),
	(9,1,'use_ssl','0'),
	(10,1,'show_admin_bar_front','false'),
	(11,1,'locale',''),
	(12,1,'wp_capabilities','a:1:{s:13:\"administrator\";b:1;}'),
	(13,1,'wp_user_level','10'),
	(14,1,'dismissed_wp_pointers',''),
	(15,1,'show_welcome_panel','1'),
	(17,1,'wp_dashboard_quick_press_last_post_id','78'),
	(18,1,'community-events-location','a:1:{s:2:\"ip\";s:11:\"10.27.167.0\";}'),
	(19,1,'session_tokens','a:1:{s:64:\"19de0afed6378decd65f748481112c8817f8e95cb16d7acbdd583b8d01d78e21\";a:4:{s:10:\"expiration\";i:1515806701;s:2:\"ip\";s:12:\"10.27.167.10\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36\";s:5:\"login\";i:1515633901;}}'),
	(20,1,'managenav-menuscolumnshidden','a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
	(21,1,'metaboxhidden_nav-menus','a:1:{i:0;s:12:\"add-post_tag\";}'),
	(22,1,'nav_menu_recently_edited','2'),
	(23,1,'meta-box-order_toplevel_page_theme-general-settings','a:2:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:23:\"acf-group_5a43fcc94ab40\";}'),
	(24,1,'screen_layout_toplevel_page_theme-general-settings','2'),
	(25,1,'wp_user-settings','libraryContent=browse&editor=tinymce'),
	(26,1,'wp_user-settings-time','1514405427'),
	(27,1,'acf_user_settings','a:0:{}'),
	(28,1,'closedpostboxes_acf-field-group','a:0:{}'),
	(29,1,'metaboxhidden_acf-field-group','a:1:{i:0;s:7:\"slugdiv\";}');

/*!40000 ALTER TABLE `wp_usermeta` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table wp_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `wp_users`;

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `wp_users` WRITE;
/*!40000 ALTER TABLE `wp_users` DISABLE KEYS */;

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`)
VALUES
	(1,'digivie','$P$B6rv.CV6J6nCukaaZRddOqU3xcSbLO/','digivie','developers@ivieinc.com','','2017-12-12 17:43:38','',0,'digivie');

/*!40000 ALTER TABLE `wp_users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
