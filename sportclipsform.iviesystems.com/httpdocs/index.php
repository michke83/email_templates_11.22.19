<?php
error_reporting(0);
// grab URL parameters data from short form
$first_name = strip_tags(trim($_GET['first_name']));
$last_name = strip_tags(trim($_GET['last_name']));
$email = strip_tags(trim($_GET['email']));
$phone = strip_tags(trim($_GET['phone']));
$phone = preg_replace('/[^0-9]+/', '', $phone);
$phone = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
$city = strip_tags(trim($_GET['city']));
$state = strip_tags(trim($_GET['state']));
$zip = strip_tags(trim($_GET['zip']));

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <title>Sport Clips Long Form</title>
      <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"> -->
      <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"> -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" text="type/css" rel="stylesheet" />
      <link href="https://fonts.googleapis.com/css?family=Bitter" rel='stylesheet' type='text/css'>
      <link href="css/style.css" rel="stylesheet" text="type/css">
  </head>
  <body>
      <header class="container mt-0">
        <div class="d-flex justify-content-between flex-wrap bd-highlight mb-3">


          <div class="header-logo">
            <img src="img/scjobs-logo.png" class="logo img-fluid d-inline-block " />
            <!-- <h1 class="jobs-header d-inline-block pl-3 ">JOBS</h1> -->
          </div>

          <div class="mt-2 d-none d-md-inline search-item">
            <form class="form-inline">
              <div class="input-group">
                <input id="searchbox" type="text" class="form-control " placeholder="Search" aria-label="Search" aria-describedby="searchicon">
                <i class="fa fa-search" aria-hidden="true"></i>
              </div>
            </form>
          </div>
          <div class="d-none d-md-inline">
            <ul class="social-media-top">
              <li><a href=""><i class="fa fa-facebook fa-lg" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-twitter fa-lg" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-instagram fa-lg" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-youtube fa-lg" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-google-plus fa-lg" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
    </header>
        <nav class="navbar navbar-light navbar-toggleable-md">
          <!-- <a href="#" class="navbar-brand"></a> -->
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav mx-auto">
              <a class="nav-item nav-link active px-5" href="#">Home <span class="sr-only">(current)</span></a>
              <a class="nav-item nav-link px-5" href="#">Our Story</a>
              <a class="nav-item nav-link px-5" href="#">Stylists</a>
              <a class="nav-item nav-link px-5" href="#">Contests & Scholarships</a>
              <a class="nav-item nav-link px-5" href="#">Events</a>
              <a class="nav-item nav-link px-5" href="#">Blog</a>
            </div>
          </div>
        </nav>

    </div>

      <section id="header" class="image-header ">
        <div class="container-fluid">
        <div class="row">
          <div class="col">
            <h1 id="heading" class="text-light pl-4 pl-md-5 ml-lg-5">
              <?php
if (!empty($first_name)) {
    echo 'Hey ' . $first_name . '! ...Interesting! Tell us more!';
} else {
    echo '...Interesting! Tell us more!';
}
?>
              </h1>
          </div>
        </div>
      </div>
      </section>
    <div class="container">
      <section class="content">
        <div class="row">
          <div class="d-block d-lg-none d-sm-none container career-text">
              <p>
              A career with Sport Clips features a great
              enviroment, great pay, great tips and
              best clients in the business.
            </p>
            <p>
              We're interviewing motivated, positive, reliable,
              upbeat Salon Managers and Licensed Stylists to
              help grow and be one of our winning team members.
              You should be an outgoing and career-oriented
              hairstylist who has a current license and passion
              for doing men and boys hair cuts.
            </p>
          </div>
          <div class="col-lg-7 push-lg-5 col-sm-12">
            <form action="" method="post">
              <div class="form-group">
                <label for="first_name"><strong>First Name *</strong></label>
                <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo $first_name; ?>">
              </div>
              <div class="form-group">
                <label for="last_name"><strong>Last Name *</strong></label>
                <input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $last_name; ?>">
              </div>
              <div class="form-group">
                <label for="email"><strong>Email Address *</strong></label>
                <input type="email" class="form-control" name="email" id="email" value="<?php echo $email; ?>">
              </div>
              <div class="form-group">
                <label for="email2"><strong>Confirm Email Address *</strong></label>
                <input type="email" class="form-control" name="email2" id="email2">
              </div>
              <div class="form-group">
                <label for="phone"><strong>Phone *</strong></label>
                <input type="text" class="form-control" name="phone" id="phone" value="<?php echo $phone; ?>">
              </div>
              <div class="form-row inline-fields">
                <div class="form-group">
                  <label for="city"><strong>City *</strong></label>
                  <input type="text" class="form-control" name="city" id="city" value="<?php echo $city; ?>">
                </div>
                <div class="form-group">
                  <label for="state"><strong>State *</strong></label>
                  <select id="state" name="state" class="form-control">
                    <option value="<?php echo $state; ?>"><?php echo $state; ?></option>
                    <option value="Alabama">Alabama</option>
                    <option value="Alaska">Alaska</option>
                    <option value="Arizona">Arizona</option>
                    <option value="Arkansas">Arkansas</option>
                    <option value="California">California</option>
                    <option value="Colorado">Colorado</option>
                    <option value="Connecticut">Connecticut</option>
                    <option value="Delaware">Delaware</option>
                    <option value="Florida">Florida</option>
                    <option value="Georgia">Georgia</option>
                    <option value="Hawaii">Hawaii</option>
                    <option value="Idaho">Idaho</option>
                    <option value="Illinois">Illinois</option>
                    <option value="Indiana">Indiana</option>
                    <option value="Iowa">Iowa</option>
                    <option value="Kansas">Kansas</option>
                    <option value="Kentucky">Kentucky</option>
                    <option value="Louisiana">Louisiana</option>
                    <option value="Maine">Maine</option>
                    <option value="Maryland">Maryland</option>
                    <option value="Massachusetts">Massachusetts</option>
                    <option value="Michigan">Michigan</option>
                    <option value="Minnesota">Minnesota</option>
                    <option value="Mississippi">Mississippi</option>
                    <option value="Missouri">Missouri</option>
                    <option value="Montana">Montana</option>
                    <option value="Nebraska">Nebraska</option>
                    <option value="Nevada">Nevada</option>
                    <option value="New Hampshire">New Hampshire</option>
                    <option value="New Jersey">New Jersey</option>
                    <option value="New Mexico">New Mexico</option>
                    <option value="New York">New York</option>
                    <option value="North Carolina">North Carolina</option>
                    <option value="North Dakota">North Dakota</option>
                    <option value="Ohio">Ohio</option>
                    <option value="Oklahoma">Oklahoma</option>
                    <option value="Oregon">Oregon</option>
                    <option value="Pennsylvania">Pennsylvania</option>
                    <option value="Rhode Island">Rhode Island</option>
                    <option value="South Carolina">South Carolina</option>
                    <option value="South Dakota">South Dakota</option>
                    <option value="Tennessee">Tennessee</option>
                    <option value="Texas">Texas</option>
                    <option value="Utah">Utah</option>
                    <option value="Vermont">Vermont</option>
                    <option value="Virginia">Virginia</option>
                    <option value="Washington">Washington</option>
                    <option value="West Virginia">West Virginia</option>
                    <option value="Wisconsin">Wisconsin</option>
                    <option value="Wyoming">Wyoming</option>
                    <option value="District of Columbia">District of Columbia</option>
                  </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group">
                  <label for="zip"><strong>Zip *</strong></label>
                  <input type="text" class="form-control" name="zip" id="zip" value="<?php echo $zip; ?>">
                </div>
              </div>
              <div class="form-row">
                <label for="opportunities" class="font-italic">(Miles shown below is approximate distance from your zip code.)</label>
                <br>
                <div class="">
                <div class="form-group">
                  <label for="primary_loc"><strong>Primary location</strong></label>
                  <select id="primary_loc" name="primary_loc" class="form-control">
                    <option value="<?php echo $primary_loc; ?>"><?php echo $primary_loc; ?></option>
                    <option value="Location 1<">Location 1</option>
                    <option value="Location 1<">Location 2</option>
                    <option value="Location 1<">Location 3</option>
                  </select>
                </div>
                <div class="form-group">
                  <label for="secondary_loc"><strong>Secondary location</strong></label>
                  <select id="secondary_loc" name="secondary_loc" class="form-control">
                    <option value="<?php echo $secondary_loc; ?>"><?php echo $secondary_loc; ?></option>
                    <option value="Alabama">Alabama</option>
                    <option value="Alaska">Alaska</option>
                    <option value="District of Columbia">District of Columbia</option>
                  </select>
                </div>
              </div>
            </div>  
              <br />
              <div class="form-group">
                <label for="opportunities"><strong>What opportunities are you interested in at Sport Clips?</strong></label>
                <br>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="opportunities" id="opportunities" value="Management"> Management
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="opportunities" id="opportunities" value="Stylist/Barber"> Stylist/Barber
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="opportunities" id="opportunities" value="Receptionist"> Receptionist
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="opportunities" id="opportunities" value="Educator(Coach)"> Educator (Coach)
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label for="resume"><strong>Upload your resume</strong></label>
                <input type="file" class="form-control-file spcheck" name="resume" id="resume">
                <span class=" text-muted">(.docx, .doc, .odt, .rtf, .pdf, .txt)</span>
              </div>

              <div class="form-group">
                <label for="licensed"><strong>Are you a licensed cosmotologist or barber?</strong></label>
                <br>
                <div class="form-check form-check-inline form-check-inline-b ">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="radio" name="licensed" id="licensed" value="Yes"> Yes
                  </label>
                </div>
                <div class="form-check form-check-inline form-check-inline-b">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="radio" name="licensed" id="licensed" value="No"> No
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label for="worktype"><strong>What type of work are you looking for?</strong></label>
                <br>
                <div class="form-check form-check-inline form-check-inline-b">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="radio" name="worktype" id="worktype" value="Full-time"> Full-time
                  </label>
                </div>
                <div class="form-check form-check-inline form-check-inline-b">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="radio" name="worktype" id="worktype" value="Part-time"> Part-time
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label for="skills"><strong>Additional skills or specialties (seperate each with a comma)</strong></label>
                <textarea class="form-control" name="skills" id="skills" rows="3"></textarea>
              </div>

              <div class="form-group">
                <label for="portfolio"><strong>Link to portfolio or social media profile (optional)</strong></label>
                <textarea class="form-control" name="portfolio" id="portfolio" rows="3"></textarea>
              </div>

              <div class="form-group">
                <label for="contact"><strong>How may we contact you (preferred)?</strong></label>
                <br>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="contact" id="contact" value="Phone"> Phone
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="contact" id="contact" value="Email"> Email
                  </label>
                </div>
                <div class="form-check form-check-inline">
                  <label class="form-check-label text-muted">
                    <input class="form-check-input" type="checkbox" name="contact" id="contact" value="Text Message"> Text Message
                  </label>
                </div>
              </div>

              <div class="form-group">
                <label for="referral"><strong>How did you hear about Sport Clips?</strong></label>
                <select class="form-control" name="referral" id="referral">
                  <option value="Google">Google</option>
                  <option value="Indeed">Indeed</option>
                  <option value="Craigslist">Craigslist</option>
                  <option value="Facebook">Facebook</option>
                  <option value="Google">5</option>
                </select>
              </div>

              <div class="form-check">
                <label class="form-check-label">
                  <input class="form-check-input" type="checkbox" checked value="">
                  Opt-in to receive info about Sport Clips career opportunities via Internet/mobile.
                </label>
              </div>

              <button type="submit" class="btn btn-sportclip">Submit</button>
            </form>
          </div>
          <div class="col-lg-5 pull-lg-7 col-sm-12">
           <div class="d-none d-lg-block d-sm-block career-text-2">
            <p>
              A career with Sport Clips features a great
              environment, great pay, great tips and
              best clients in the business.
            </p>
            <p>
              We're interviewing motivated, positive, reliable,
              upbeat Salon Managers and Licensed Stylists to
              help grow and be one of our winning team members.
              You should be an outgoing and career-oriented
              hairstylist who has a current license and passion
              for doing men and boys hair cuts.
            </p>
          </div>
            <hr />
            <h2 id="gdheader" class="sc-red">Don't just take it from us,
              let our people tell it..."</h2>

            <div class="d-flex justify-content-left pb-5 event-box">
              <div class="gdWidget"><a href="https://www.glassdoor.com/api/api.htm?version=1&action=employer-single-review&t.s=w-m&t.a=c&format=300x280&reviewId=19052736" target="_gd">Sport Clips Reviews</a> | <a href="https://www.glassdoor.com/api/api.htm?version=1&action=employer-jobs&t.s=w-m&t.a=c&reviewId=19052736" target="_gd">Sport Clips Jobs</a> | <a href="https://www.glassdoor.com/Reviews/index.htm?t.s=w-m&t.a=c" target="_gd">Company review</a> from employees</div><script src="https://www.glassdoor.com/static/js/api/widget/v1.js"></script>
            </div>
            <div class="d-flex justify-content-left pb-5 event-box">
              <div class="gdWidget"><a href="https://www.glassdoor.com/api/api.htm?version=1&action=employer-single-review&t.s=w-m&t.a=c&format=300x280&reviewId=18932364" target="_gd">Sport Clips Reviews</a> | <a href="https://www.glassdoor.com/api/api.htm?version=1&action=employer-jobs&t.s=w-m&t.a=c&reviewId=18932364" target="_gd">Sport Clips Jobs</a> | <a href="https://www.glassdoor.com/Reviews/index.htm?t.s=w-m&t.a=c" target="_gd">Company review</a> from employees</div><script src="https://www.glassdoor.com/static/js/api/widget/v1.js"></script>
            </div>
            <div class="d-flex justify-content-left pb-5 event-box">
              <div class="gdWidget"><a href="https://www.glassdoor.com/api/api.htm?version=1&action=employer-single-review&t.s=w-m&t.a=c&format=300x280&reviewId=19052074" target="_gd">Sport Clips Reviews</a> | <a href="https://www.glassdoor.com/api/api.htm?version=1&action=employer-jobs&t.s=w-m&t.a=c&reviewId=19052074" target="_gd">Sport Clips Jobs</a> | <a href="https://www.glassdoor.com/Reviews/index.htm?t.s=w-m&t.a=c" target="_gd">Company review</a> from employees</div><script src="https://www.glassdoor.com/static/js/api/widget/v1.js"></script>
            </div>


          </div>
        </section>
      </div>

      <footer>
        <div class="col">
            <div class="footer-logo">
                <img src="img/sportclips-footer-logo.png" class="img-fluid mx-auto d-block"  />
            </div>
        </div>
        <div class="col">
            <ul class="social-media d-flex justify-content-center">
              <li><a href=""><i class="fa fa-facebook fa-3x" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-twitter fa-3x" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-instagram fa-3x" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-youtube fa-3x" aria-hidden="true"></i></a></li>
              <li><a href=""><i class="fa fa-google-plus fa-3x" aria-hidden="true"></i></a></li>
            </ul>
        </div>
      </footer>

    </div><!-- End Container -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
  </body>
</html>
