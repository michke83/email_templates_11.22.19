 

Click here (*|ARCHIVE|*) if you are having trouble viewing this email. 

https://professional.heart.org/professional/EducationMeetings/MeetingsLiveCME/ScientificSessions/UCM_316932_Registration-Housing-Scientific-Sessions.jsp 

 There will be many can't-miss sessions at Scientific Sessions 2019 in Philadelphia, PA this November 16-18. 

 Click through the programming communities below and view the line-up of the most contemporary topics and controversies in your field. 

  * Acute Coronary Syndromes (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Acute%20Coronary%20Syndromes/1) 
  * Cardio-Oncology (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Cardio-Oncology/1) 
  * Cardiometabolic Health and Diabetes (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Cardiometabolic%20Health%20and%20Diabetes/1) 
  * Congenital Heart Disease & Pediatric Cardiology (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Congenital%20Heart%20Disease%20and%20Pediatric%20Cardiology/1) 
  * Electrophysiology & Arrhythmias (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Electrophysiology%20and%20Arrhythmias/1) 
  * Epidemiology, Big Data and Precision Medicine (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Epidemiology,%20Big%20Data%20and%20Precision%20Medicine/1) 
  * Go Red For Women and Special Populations (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=GFRW%20and%20Special%20Populations/1) 
  * Heart Failure & Cardiomyopathies (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Heart%20Failure%20and%20Cardiomyopathies/1) 
  * Hypertension and Nephrology (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Hypertension%20and%20Nephrology/1) 
  * Imaging & Nuclear Medicine (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Imaging%20and%20Nuclear%20Medicine/1) 
  * Interventional Treatments (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Interventional%20Treatments/1) 
  * Lifestyle & Behavioral Medicine (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Lifestyle%20$_26%20Behavioral%20Medicine/1) 
  * Nursing Research and Clinical (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Nursing%20Research%20and%20Clinical/1) 
  * Prevention Health and Wellness (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Prevention,%20Health%20and%20Wellness/1) 
  * Pulmonary Hypertension & Critical Care (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Pulmonary%20Hypertension%20$_26%20Critical%20Care/1) 
  * Stroke & Neuroscience (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Stroke%20$_26%20Neuroscience/1) 
  * Surgery & Anesthesia (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Surgery%20$_26%20Anesthesia/1) 
  * Quality of Care (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Quality%20of%20Care/1) 
  * Vascular Disease and Thrombosis (https://www.abstractsonline.com/pp8/#!/7891/sessions/@sessionCategory=Vascular%20Disease%20and%20Thrombosis/1) 

 _View all programming for your specialty and educational interest on the Scientific Sessions 2019 Online Program Planner!_ 

 https://www.abstractsonline.com/pp8/#!/7891 https://professional.heart.org/professional/EducationMeetings/MeetingsLiveCME/ScientificSessions/UCM_316932_Registration-Housing-Scientific-Sessions.jsp 

 http://scientificsessions.org 

============================================================

 ** (https://twitter.com/ahameetings)

 ** (https://www.facebook.com/ahameetings/)

 ** (https://www.linkedin.com/company/american-heart-association?trk=company_search)

 ** (https://www.instagram.com/american_heart/)

 ** (https://www.youtube.com/user/AHASessions)

 ** DONATE (https://donatenow.heart.org/index.html?x=1&utm_source=search&utm_medium=organic&utm_campaign=Donations)
| ** FORWARD THIS MESSAGE (*|FORWARD|*)

You are receiving this email because you have previously opted in to receiving communications from the American Heart Association and/or Scientific Sessions.

To be removed from this email/newsletter list, please use the link below and follow the instructions.
** Remove my address from only this email/newsletter list. (*|UPDATE_PROFILE|*)

You will be removed from the email/newsletter within 24 hours.
** Privacy Policy (http://www.heart.org/HEARTORG/General/Privacy-Policy_UCM_300371_Article.jsp#.VwLQTWQrJGE)
 | ** Ethics Policy (http://www.heart.org/HEARTORG/General/Ethics-Policy_UCM_300430_Article.jsp#.VwLQtWQrJGE)
 | ** Conflict of Interest Policy (http://www.heart.org/HEARTORG/General/Conflict-of-Interest-Policy_UCM_300435_Article.jsp#.VwLQ6WQrJGE)

To unsubscribe via postal mail, please contact us at:
*|LIST:ADDRESS|* - ATTN: Email Subscriptions Group
Copyright (C) *|CURRENT_YEAR|* *|LIST:COMPANY|* All rights reserved.

   

 