<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'localwp');

/** MySQL database username */
define('DB_USER', 'wpuser');

/** MySQL database password */
define('DB_PASSWORD', 'rDIN9FreAZUFxJW1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vmW~Cg]g.7mt7_v;vIYu]+oG-B_reBkZ;ObvNfj6<+jy=U8MSKaM}:Mvu/7G>bB!');
define('SECURE_AUTH_KEY',  'Lb1*?aSr)0&[7m?g<Hb*s@LOu&R48J_cXDo!rANePYA}y_8MJYqTK:!4|lM Zqs3');
define('LOGGED_IN_KEY',    'Hj;.Z/@:+0l)K!htu@l,E7L><U71JNph`|=1;Ua*$n1TIxTM8Z9,1rwFUZ)J-ryF');
define('NONCE_KEY',        '5XbNsKd{OSCfcZnmj336Rd3hYw.ZnQ@/;{)fU!}c,x9Ydfs.bF~%cDF],v(u0j.J');
define('AUTH_SALT',        '6AqG66B9H$Kz#1v2ng?/ed[`Uxw9 ~BJU&+QOkTH-)~vpg~[!Hoafu94TNWudg|[');
define('SECURE_AUTH_SALT', 'L(j#q?#VQ{Ii>I)*`!jsdT`I)hv3_&&^oPEq+dl:zp$o8MBQ;2Y4Dk*LEb:Sj(jE');
define('LOGGED_IN_SALT',   's>[K*(J^9_yA0|tE +bYz.U_u1m,2NaAL>I}wE3_G}Bt09u -SG=@(fYFU!D5z:d');
define('NONCE_SALT',       'G2<_TOL:XIZnWrY]BIv]s,<7a(RQNm>B18DO8@OzZaiSz+I/#+AeS<`lodi[Xwf{');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'localwp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
