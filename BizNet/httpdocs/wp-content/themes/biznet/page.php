<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Biznet
 */

get_header();

global $post;
$page_id = $post->ID;

// if (has_post_thumbnail( $page_id ) ) :
//
// 	the_post_thumbnail();
//
// endif;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			while ( have_posts() ) : the_post();
			?>
			<?php
				$subheading = get_field( 'page_sub_headings' );
			?>
	      <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	        <header class="entry-header">
						<h3><?php echo $subheading; ?></h3>
	          <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	        </header><!-- .entry-header -->
	      </section>

				<?php

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;
				// Get Custom Page Footer Options While in the Loop
				$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
				$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
				$show_footer_signup 			= get_field( 'show_footer_signup' );
				$show_prefooter 				= get_field( 'show_prefooter' );
			endwhile; // End of the loop.
			?>
			<?php
			// logic conditions to show Page Footer Options
			if( $show_testimonials_clients == 1 )
			{
				get_template_part( 'template-parts/testimonials-clients' );
			}

			if( $show_testimonials_employees == 1)
			{
				get_template_part( 'template-parts/testimonials-employees' );
			}

			if( $show_prefooter ==1)
			{
				get_template_part( 'template-parts/prefooter' );
			}

			if( $show_footer_signup == 1)
			{
				get_template_part( 'template-parts/footersignup' );
			}
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
//get_sidebar();
get_footer();
