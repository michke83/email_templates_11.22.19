<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Biznet
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="grid || grid--large" style="align-items: center;">

				<div class="flex-half || page-content">
					<h1 class="page-title ta-center"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'biznet' ); ?></h1>
					<p class="ta-center"><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'biznet' ); ?></p>
				</div><!-- .page-content -->
				<div class="flex-half || page-content">
					<?php
						get_search_form();
					?>
				</div>
			</section><!-- .error-404 -->
			<!-- PRE FOOTER TEXT SECTION -->
			<?php get_template_part( 'template-parts/prefooter' ); ?>

			<!-- FOOTER SIGN UP SECTION -->
			<?php get_template_part( 'template-parts/footersignup' ); ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
