<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Biznet
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<!--

                                `````/oooooooooooo++oooooooooooo/`````
                         `````.ooooooo+++++++++++``+++++++++++ooooooo.`````
                       `./oooooo+/////`                      `/////+oooooo/.`
                    ---:ooooo/::-                                  -::/ooooo:---
                  --ooooo/:::                                          :::/ooooo--
                .:+oo+---`                              -::::::::.        `---+oo+:.
            `///+ooo..                             /////ossssss+-`            ..ooo+///`
            .ooooo-.                         -+++++sssssssssso.`                .-ooooo.
          `++ooo-`                         .oossssssssssssssso                    `-ooo++`
         +oooo:                         ooosssssssssssssssssssooo-                  `:ooo++
       /ooo+/                       ``ossssssssssssssssssssssssssssss.```              /+ooo/
     ``/oo+                    .....+sssssssssssssssssssssssssssssssssss+.`              +oo/``
   `./ooo/:                  .-sssssssssssssssssssssssssssssssssssssssssss/.`            :/ooo/.`
   -ooo/:                  .-sssssssssssssssssssssssssssssssssssssssssssssss/-`            :/ooo-
 `-/ooo.                   +ssssssssssssssssssssssssssssssssssssssssssssssssss/:            .ooo/-`
 `ooooo.                   +ssssssssssssssssssssssssssssssssssssssssssssssssssss            .ooooo`
 `ooo:.`                   +ssssssssssssssssssssssssssssssssssssssssssssssssssss/:          `.:ooo`
/+ooo-                     +sssssssssssssssssssssssssssssssssss+.......sssssssssso            -ooo+/
ooo/`                      +sssssssssssssssssssssssssssssssssss/       ```:sssssso             `/ooo
ooo:                     :sssssssssssssssssssssssssssssssssssss/            /sssso              :ooo
ooo:                  `:sssssssssssssssssssssssssssssssssssssssss:```       :ossso              :ooo
ooo:                 `ssso+++ssssssso+sssssssssssssssssssssssssssssss..       osso              :ooo
ooo:                -:so/.   /+sssss/-/////+sssssssssssssssssssssssss+/       :/so              :ooo
ooo:                sss/       ///////`    .///////sssssssssso///////           so              :ooo
ooo:              -:so:-                           :::+sssssso:::::::::         :-              :ooo
ooo:              osso                         -//////+ssssssssssssssss///-                     :ooo
ooo:              osso                   `/////osso.................-osssso/.                   :ooo
ooo:              osso                /++osss/.....                  ..sssss-                   :ooo
ooo:              os``            -ooossso```                          `-sss-                   :ooo
ooo:              oss+          .sssssss                                -sss-                   :ooo
ooo/`             osso     ``ossssoo.                                 `sssss-                  `/ooo
/+ooo-            osso   `.+sssso+-                                  ..ssso+.                 -ooo+/
 `ooo:.`          osso-. :sssss/:                                  .-sssss:                 `.:ooo`
 `ooooo.          ossss+-+sss/:                                  .-osssso/.                 .ooooo`
 `-/ooo.          -:sssssss/:                                `:::+sss:::-                   .ooo/-`
   -ooo/:           -:sssss.                              :///sssss:-                      :/ooo-
   `./ooo/:          `sssss++                         -+++sssss+...`                     :/ooo/.`
     ``/oo+          `sssssss+++++-            /++++++osss-....`                         +oo/``
       /ooo+/        `sss:```osssssoooooooooooosssss+`````                             /+ooo/
         ++ooo:`     `sssss.                                                        `:ooo++
          `++ooo-`   `oosss-```                                                   `-ooo++`
            .ooooo-.   -sssssso...`                                             .-ooooo.
            `///+ooo.. .///ossssss:                                           ..ooo+///`
                .:+oo+---` -::/sss:                                       `---+oo+:.
                  --ooooo/:::  :::.                                    :::/ooooo--
                    ---:ooooo/::-                                  -::/ooooo:---
                       `./oooooo+/////`                      `/////+oooooo/.`
                         `````.ooooooo+++++++++++``+++++++++++ooooooo.`````
                               `````/oooooooooooo++oooooooooooo/`````
-->
<!-- FAVICON -->
<link rel="apple-touch-icon" sizes="57x57" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo THEME_DIRECTORY ?>/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo THEME_DIRECTORY ?>/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo THEME_DIRECTORY ?>/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo THEME_DIRECTORY ?>/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo THEME_DIRECTORY ?>/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo THEME_DIRECTORY ?>/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PZJKPBZ');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PZJKPBZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="page" class="site">
	<a class="skip-link screen-reader-text sr-only" href="#content"><?php esc_html_e( 'Skip to content', 'biznet' ); ?></a>

	<header id="masthead" class="site-header || grid || grid--vertical" role="banner">
		<section class="top-bar">
			<div class="grid || h-social">
				<a target="_blank" href="<?php echo the_field( 'facebook_url', 'options' )?>">
					<img src="<?php echo THEME_DIRECTORY ?>/img/h-facebook.png" />
				</a>
				<a target="_blank" href="<?php echo the_field( 'linkedin_url', 'options' )?>">
					<img src="<?php echo THEME_DIRECTORY ?>/img/h-linkedin.png" />
				</a>
				<a target="_blank" href="<?php echo the_field( 'youtube_url', 'options' )?>">
					<img src="<?php echo THEME_DIRECTORY ?>/img/h-youtube.png" />
				</a>
				<a target="_blank" href="<?php echo the_field( 'instagram_url', 'options' )?>">
					<img src="<?php echo THEME_DIRECTORY ?>/img/h-instagram.png" />
				</a>
			</div>
			<?php wp_nav_menu( array( 'theme_location' => 'top-bar', 'menu_id' => 'top-bar' ) ); ?>
		</section>

		<section class="grid || main-nav || js-fixed-header">
			<div class="site-branding || column--light">

				<a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><img class="img-responsive" src="<?php the_field( 'site_logo', 'option' ); ?>" /></a>

			</div><!-- .site-branding -->

			<nav class="mobile-navigation || column--heavy" role="navigation">
				<button class="menu-toggle" id="js-menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="hamburger" id="js-hamburger"><span class="sr-only"><?php esc_html_e( 'Primary Menu', 'biznet' ); ?></span></span></button>
				<div class="mobile-nav || sr-only" id="js-mobile-nav">

				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'mainmenu' ) ); ?>

				<?php wp_nav_menu( array( 'theme_location' => 'top-bar', 'menu_id' => 'top-bar' ) ); ?>

				</div>
			</nav><!-- #site-navigation -->

			<nav id="site-navigation" class="main-navigation || column--heavy" role="navigation">
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'mainmenu' ) ); ?>

				<div class="header-free-trial">
					<a class="btn || btn--free-trial" href="<?php echo the_field( 'subscribe_form_button_url', 'options' ); ?>"><?php echo the_field( 'subscribe_form_button_text', 'options' ); ?>
					</a>
				</div>
			</nav><!-- #site-navigation -->
		</section>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
