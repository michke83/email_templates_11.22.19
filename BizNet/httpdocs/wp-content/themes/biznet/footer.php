<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Biznet
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

		<section class="footer-links" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/footerpattern.png'), linear-gradient(to right, #3360ae 0%,#1f447f 100%)">
			<div class="grid || grid--medium || footer-nav">
				<?php
					$footer_column_one 		= get_field( 'footer_column_one', 'options' );
					$footer_column_two 		= get_field( 'footer_column_two', 'options' );
					$footer_column_three 	= get_field( 'footer_column_three', 'options' );
					$footer_column_four 	= get_field( 'footer_column_four', 'options' );
					$footer_column_five 	= get_field( 'footer_column_five', 'options' );
				?>
				<!-- FOOTER ONE -->
				<div class="footer-link-section">
					<?php echo $footer_column_one; ?>

					<?php get_sidebar(); ?>
				</div>
				<!-- FOOTER TWO -->
				<div class="footer-link-section">
					<?php echo $footer_column_two; ?>
				</div>
				<!-- FOOTER THREE -->
				<div class="footer-link-section">
					<?php echo $footer_column_three; ?>
				</div>
				<!-- FOOTER FOUR -->
				<div class="footer-link-section">
					<?php echo $footer_column_four; ?>
				</div>
				<!-- FOOTER FIVE -->
				<div class="footer-link-section">
					<?php echo $footer_column_five; ?>
				</div>
			</div>

			<div class="footer-social || grid">
				<div class="footer-line">
				</div>
				<div class="wrapper-small || grid">
					<a target="_blank" href="<?php echo the_field( 'facebook_url', 'options' )?>">
						<img src="<?php echo THEME_DIRECTORY ?>/img/f-facebook.png" />
					</a>
					<div class="footer-line">
					</div>
					<a target="_blank" href="<?php echo the_field( 'linkedin_url', 'options' )?>">
						<img src="<?php echo THEME_DIRECTORY ?>/img/f-linkedin.png" />
					</a>
					<div class="footer-line">
					</div>
					<a target="_blank" href="<?php echo the_field( 'youtube_url', 'options' )?>">
						<img src="<?php echo THEME_DIRECTORY ?>/img/f-youtube.png" />
					</a>
					<div class="footer-line">
					</div>
					<a target="_blank" href="<?php echo the_field( 'instagram_url', 'options' )?>">
						<img src="<?php echo THEME_DIRECTORY ?>/img/f-instagram.png" />
					</a>
				</div>
				<div class="footer-line">
				</div>
			</div>
		</section>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
