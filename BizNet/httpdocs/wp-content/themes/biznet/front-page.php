<?php
/**
 * Front Page Template
 *
 * @package Biznet
 */

get_header(); ?>

	<div id="primary" class="content-area || full-width">
		<main id="main" class="site-main || full-width" role="main">
			<!-- MAIN IMAGE SECTION -->
			<section class="hero-img" style="background-image: url('<?php the_field( 'main_homepage_image', 'options' ); ?>')">
				<img class="excel-logo" src="<?php echo THEME_DIRECTORY ?>/img/excellogo.png" />
				<div class="hero-content">
					<div class="grid || grid--vertical || wrapper-large">
						<?php
							$homepage_main_image_textarea = get_field( 'homepage_main_image_textarea', 'options' );
							echo $homepage_main_image_textarea;
						?>
						<div class="ta-center">
							<a class="btn || btn--secondary" href="<?php the_field( 'homepage_left_cta_button_url', 'options'); ?>"><?php the_field( 'homepage_left_cta_button_text', 'options' ); ?></a>
							<a class="btn || btn--secondary" href="<?php the_field( 'homepage_right_cta_button_url', 'options'); ?>"><?php the_field( 'homepage_right_cta_button_text', 'options' ); ?></a>
						</div>
					</div>
				</div>
			</section>
			<!-- VIDEO SECTION -->
			<section class="grid || grid--xl">
				<div class="video-textarea">
				<?php
					$homepage_video_section_textarea = get_field( 'homepage_video_section_textarea', 'options' );
					echo $homepage_video_section_textarea;
				?>
				<a class="btn" href="<?php the_field( 'homepage_video_button_url', 'options'); ?>"><?php the_field( 'homepage_video_button_text', 'options' ); ?></a>
				</div>
				<div class="homepage-video">
					<img class="fr-dialogmodal-open || js-fr-dialogmodal-open" aria-controls="modal" src="<?php the_field( 'homepage_video_image', 'options'); ?>" />
					<div class="fr-dialogmodal || js-fr-dialogmodal" id="modal">
					  <div class="fr-dialogmodal-modal || js-fr-dialogmodal-modal" aria-labelledby="modal-title">
							<button class="close-button || btn || btn--secondary || js-fr-dialogmodal-close">X</button>
							<iframe src="<?php the_field( 'homepage_video_url', 'options' ); ?>" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					  </div>
					</div>
				</div>
			</section>
			<!-- PRODUCTS SECTION -->
			<section class="grid || grid--vertical || ta-center || home-products || parallax" style="background-image: url('<?php the_field( 'homepage_biz_logos_section_background_image', 'options' ); ?>')">
				<?php
					$bizconnector_textarea 	= get_field( 'bizconnector_textarea', 'options' );
					$bizconnector_page_url 	= get_field( 'bizconnector_page_url', 'options' );
					$bizinsight_textarea 		= get_field( 'bizinsight_textarea', 'options' );
					$bizinsight_page_url 		= get_field( 'bizinsight_page_url', 'options' );
					$bizbroadcast_textarea 	= get_field( 'bizbroadcast_textarea', 'options' );
					$bizbroadcast_page_url 	= get_field( 'bizbroadcast_page_url', 'options' );
				?>
				<div class="home-svg">
					<svg class="biz-line" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="5px" height="80px"
						 viewBox="0 0 3.6 133.3" enable-background="new 0 0 3.6 133.3" xml:space="preserve">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#F7ED4E" d="M0,0h3.6v133.3H0V0z"/>
					</svg>
					<div class="biz-small">
						<a href="<?php echo $bizconnector_page_url; ?>"><img class="sm-center || svg-img" src="<?php echo THEME_DIRECTORY ?>/img/bizconnector.png" /></a>
						<?php
							echo $bizconnector_textarea;
						?>
					</div>
					<svg class="biz-line" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="5px" height="80px"
						 viewBox="0 0 3.6 133.3" enable-background="new 0 0 3.6 133.3" xml:space="preserve">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#F7ED4E" d="M0,0h3.6v133.3H0V0z"/>
					</svg>
				</div>
				<div class="home-svg">
					<svg class="biz-line" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="5px" height="80px"
						 viewBox="0 0 3.6 133.3" enable-background="new 0 0 3.6 133.3" xml:space="preserve">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#2cb1eb" d="M0,0h3.6v133.3H0V0z"/>
					</svg>
					<div>
						<a href="<?php echo $bizinsight_page_url; ?>"><img class="sm-center || svg-img" src="<?php echo THEME_DIRECTORY ?>/img/bizinsight.png" /></a>
						<?php
							echo $bizinsight_textarea;
						?>
					</div>
					<svg class="biz-line" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="5px" height="80px"
						 viewBox="0 0 3.6 133.3" enable-background="new 0 0 3.6 133.3" xml:space="preserve">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#2cb1eb" d="M0,0h3.6v133.3H0V0z"/>
					</svg>
				</div>
				<div class="home-svg">
					<svg class="biz-line" class="biz-line" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="5px" height="80px"
						 viewBox="0 0 3.6 133.3" enable-background="new 0 0 3.6 133.3" xml:space="preserve">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#70be3d" d="M0,0h3.6v133.3H0V0z"/>
					</svg>
					<div class="biz-small">
						<a href="<?php echo $bizbroadcast_page_url; ?>"><img class="sm-center || svg-img" src="<?php echo THEME_DIRECTORY ?>/img/bizbroadcast.png" /></a>
						<?php
							echo $bizbroadcast_textarea;
						?>
					</div>
					<svg class="biz-line" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="5px" height="80px"
						 viewBox="0 0 3.6 133.3" enable-background="new 0 0 3.6 133.3" xml:space="preserve">
					<path fill-rule="evenodd" clip-rule="evenodd" fill="#70be3d" d="M0,0h3.6v133.3H0V0z"/>
					</svg>
				</div>
			</section>

			<!-- COMPATIBLE SYSTEMS SECTION -->
			<section class="home-compatible">
				<div>
				<?php
					$compatible_systems_textarea = get_field( 'compatible_systems_textarea', 'options' );

					echo $compatible_systems_textarea;
				?>
				</div>
				<!-- LOGO SLIDER -->
				<div class="compatible-slider">
				<!-- QUERY COMPATIBLE SYSTEMS LOGO CPT -->
					<?php
					global $post;

			    $args = array( 'post_type' => 'comp_systems', 'posts_per_page' => 50 );

			    $loop = new WP_Query( $args );

			  	while ( $loop->have_posts() ) : $loop->the_post();

					?>
					<div>
						<div class="compatible-img">
							<a href="<?php the_field( "compatible_system_lp_url" ); ?>"><img src="<?php the_field( "comp_system_image" ); ?>" /></a>
						</div>
					</div>


					<?php endwhile; // end of the loop. ?>
				</div>
			</section>

			<!-- TESTIMONIALS SECTION -->
			<?php get_template_part( 'template-parts/testimonials-clients' ); ?>

			<!-- PRE FOOTER TEXT SECTION -->
			<?php get_template_part( 'template-parts/prefooter' ); ?>

			<!-- FOOTER SIGN UP SECTION -->
			<?php get_template_part( 'template-parts/footersignup' ); ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
