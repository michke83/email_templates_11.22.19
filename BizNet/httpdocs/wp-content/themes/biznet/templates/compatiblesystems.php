<?php
/**
*
* Template Name: Compatibile Systems
*
**/

	get_header();
?>
<?php
	global $post;
	$page_id = $post->ID;

  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
                  <h3><?php echo $subheading; ?></h3>
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  		          </header><!-- .entry-header -->
              </section>

		          <div class="entry-content">
		            <?php
		              the_content();
		            ?>
		          </div><!-- .entry-content -->

							<?php
							global $wp_query;
							$product_post_id = 11910;
							$imgurl = get_field( 'product_logo', $product_post_id );
							$productdescription = get_field( 'product_description', $product_post_id );
							$productbuttonurl = get_field( 'product_button_url', $product_post_id );
							$productbuttontext = get_field( 'product_button_text', $product_post_id );
							$monitorimage = get_field( 'product_monitor_image', $product_post_id );
							?>


              <div class="grid || grid--large || product-right">

								<div class="flex-half">
									<img class="product-img-left || animated" src="<?php echo $monitorimage; ?>" />
								</div>

                <div class="flex-half || product-container">
                  <h2 class="small-header">CONNECT</h2>
                  <aside>
                    <img class="product-logo" src="<?php echo $imgurl; ?>" />
                  </aside>
                  <?php echo $productdescription;
                  ?>
                  <!-- <a class="btn" href="<?php echo $productbuttonurl; ?>">
                    <?php echo $productbuttontext;  ?>
                  </a> -->
                </div>
              </div>
							<?php
							wp_reset_query();
							?>

		        </article><!-- #post-## -->
			<?php
			// Get Custom Page Footer Options While in the Loop
			$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
			$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
			$show_footer_signup 			= get_field( 'show_footer_signup' );
			$show_prefooter 				= get_field( 'show_prefooter' );
			// End the loop.
			endwhile;

			// End If
			endif;
			?>

			<!-- QUERY COMPATIBLE SYSTEMS LOGO CPT -->
			<section class="grid || grid--small || compatible-systems">
      <?php
				  global $post;
			    $args = array( 'post_type' => 'comp_systems',
												 'posts_per_page' => -1 );
			    $loop = new WP_Query( $args );

			  	while ( $loop->have_posts() ) : $loop->the_post();

					 ?>
            	<div class="grid-boxes">
                <img src="<?php the_field( "comp_system_image" ); ?>" />

								<div class="compatible-links">
	                <a href="<?php the_field( 'compatible_system_lp_url' ); ?>">
										<a class="btn || btn--secondary" href="<?php the_field( 'compatible_system_lp_url' ); ?>">More Info</a>
										<a class="btn || btn--secondary" href="<?php the_field( 'compatible_system_buy_now_url' ); ?>">Buy Now</a>
									</a>
								</div>
              </div>

			  <?php endwhile; // end of the loop. ?>

			</section>
			<?php
			// logic conditions to show Page Footer Options
			if( $show_testimonials_clients == 1 )
			{
				get_template_part( 'template-parts/testimonials-clients' );
			}

			if( $show_testimonials_employees == 1)
			{
				get_template_part( 'template-parts/testimonials-employees' );
			}

			if( $show_prefooter ==1)
			{
				get_template_part( 'template-parts/prefooter' );
			}

			if( $show_footer_signup == 1)
			{
				get_template_part( 'template-parts/footersignup' );
			}
			?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
