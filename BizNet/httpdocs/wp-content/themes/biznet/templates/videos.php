<?php
/**
*
* Template Name: Videos
*
**/

  get_header();

  global $post;
	$page_id = $post->ID;
  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php
            $subheading = get_field( 'page_sub_headings' );
          ?>
          <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
	          <header class="entry-header">
              <h3><?php echo $subheading; ?></h3>
	            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	          </header><!-- .entry-header -->
          </section>

					<section>
	          <div class="entry-content || ta-center">
	            <?php
	              the_content();

	              wp_link_pages( array(
	                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'biznet' ),
	                'after'  => '</div>',
	              ) );
	            ?>
	          </div><!-- .entry-content -->
					</section>
        </article><!-- #post-## -->

			<?php
      // Get Custom Page Footer Options While in the Loop
      $show_testimonials_clients    = get_field( 'show_testimonials_clients' );
      $show_testimonials_employees  = get_field( 'show_testimonials_employees' );
      $show_footer_signup       = get_field( 'show_footer_signup' );
      $show_prefooter         = get_field( 'show_prefooter' );

      $vimeo_portofolios_rows = get_field('vimeo_portfolios');
			// End the loop.
			endwhile;
			?>

			<section>
			<!-- QUERY COURSES CPT -->
      <?php
			  global $post;
		    $args = array( 'post_type' => 'videos', 'posts_per_page' => 3, 'orderby' => 'video_date', 'order'   => 'ASC', );
		    $loop = new WP_Query( $args );
				 ?>
        <div class="grid || grid--large || grid--wrap">
          <?php   while ( $loop->have_posts() ) : $loop->the_post(); ?>
              <div class="ta-center">
                  <iframe style="height: auto;" src="<?php the_field( 'video_url' ); ?>" width="350" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
              </div>
          <?php endwhile; // end of the loop. ?>
        </div>

        <div class="grid || grid--large || grid--wrap || product-left" style="padding-top: 2rem; margin-top: 2rem;">
          <?php
          if($vimeo_portofolios_rows)
          {
            foreach($vimeo_portofolios_rows as $vimeo_portofolios_rows)
          	{
              echo '<div class="ta-center || grid || grid--vertical">' . $vimeo_portofolios_rows['portfolio_title'] . ' <div><a class="btn" href="' . $vimeo_portofolios_rows['portfolio_url'] . '"> LEARN MORE</a></div></div>';
            }
          }
          ?>
        </div>
			</section>
      <?php
      // logic conditions to show Page Footer Options
      if( $show_testimonials_clients == 1 )
      {
        get_template_part( 'template-parts/testimonials-clients' );
      }

      if( $show_testimonials_employees == 1)
      {
        get_template_part( 'template-parts/testimonials-employees' );
      }

      if( $show_prefooter ==1)
      {
        get_template_part( 'template-parts/prefooter' );
      }

      if( $show_footer_signup == 1)
      {
        get_template_part( 'template-parts/footersignup' );
      }
      ?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
