<?php
/**
*
* Template Name: Careers
*
**/

  get_header();

  global $post;
	$page_id = $post->ID;
  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
      <?php
        $subheading = get_field( 'page_sub_headings' );
      ?>
      <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
        <header class="entry-header">
          <h3><?php echo $subheading; ?></h3>
          <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
      </section>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>

	          <section class="entry-content || ta-center || wrapper-large">
	            <?php
	              the_content();
	            ?>
      			<!-- QUERY COURSES CPT -->
      			</section>

      			<?php
            // Get Custom Page Footer Options While in the Loop
            $show_testimonials_clients    = get_field( 'show_testimonials_clients' );
            $show_testimonials_employees  = get_field( 'show_testimonials_employees' );
            $show_footer_signup           = get_field( 'show_footer_signup' );
            $show_prefooter               = get_field( 'show_prefooter' );
			    // End the loop.
			endwhile;
			?>
      <?php
          global $post;
          $args = array( 'post_type' => 'jobs' );
          $loop = new WP_Query( $args );
          if ( $loop->have_posts() )
          {
            $i = 0;
            while ( $loop->have_posts() ) : $loop->the_post();
            $i++;
             ?>
              <div class="product-left || wrapper-large">
                <h3>
                  <a href="">
                    <?php the_field( 'job_title' ); ?>
                  </a>
                </h3>
                <div class="fr-accordion || js-fr-accordion">
                  <div id="accordion-header-<?php echo $i; ?>" class="fr-accordion__header js-fr-accordion__header">
                    <img class="plus" src="<?php echo THEME_DIRECTORY ?>/img/plusblue.png" /><img class="minus" src="<?php echo THEME_DIRECTORY ?>/img/minusblue.png" />&nbsp;Click to view job description
                  </div>
                  <div id="accordion-panel-<?php echo $i; ?>" class="fr-accordion__panel js-fr-accordion__panel">
                    <div class="fr-accordion__inner">
                      <?php the_field( 'job_description' ); ?>
                    </div>
                  </div>
                </div>
              </div>
            <?php
            endwhile; // end of the loop.
            ?>
            <div class="wrapper-large">
            <?php
            gravity_form( 2, $display_title = true, $display_description = true, $display_inactive = false, $field_values = null, $ajax = false, $echo = true );
            ?>
            </div>
          <?php
          }
          else
          {
            ?>
            <div class="grid || grid--large || product-left">
              <div class="flex-half || product-container">
                There are currently no current job openings.
              </div>
            </div>
          <?php
          }
          ?>

      <?php
      // logic conditions to show Page Footer Options
      if( $show_testimonials_clients == 1 )
      {
        get_template_part( 'template-parts/testimonials-clients' );
      }

      if( $show_testimonials_employees == 1)
      {
        get_template_part( 'template-parts/testimonials-employees' );
      }

      if( $show_prefooter ==1)
      {
        get_template_part( 'template-parts/prefooter' );
      }

      if( $show_footer_signup == 1)
      {
        get_template_part( 'template-parts/footersignup' );
      }
      ?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
