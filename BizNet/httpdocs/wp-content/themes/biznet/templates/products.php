<?php
/**
*
* Template Name: Products
*
**/

  get_header();

  global $post;
  $page_id = $post->ID;
  ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
              <?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
                  <h3><?php echo $subheading; ?></h3>
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  		          </header><!-- .entry-header -->
              </section>

		          <section class="entry-content || ta-center || wrapper-large">
		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		            <?php
		              the_content();

		              wp_link_pages( array(
		                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'biznet' ),
		                'after'  => '</div>',
		              ) );
		            ?>
		          </section><!-- .entry-content -->

		          <?php if ( get_edit_post_link() ) : ?>
		            <footer class="entry-footer">
		              <?php
		                edit_post_link(
		                  sprintf(
		                    /* translators: %s: Name of current post */
		                    esc_html__( 'Edit %s', 'biznet' ),
		                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
		                  ),
		                  '<span class="edit-link">',
		                  '</span>'
		                );
		              ?>
		            </footer><!-- .entry-footer -->
		          <?php endif; ?>
		        </article><!-- #post-## -->


			<?php
			// Get Custom Page Footer Options While in the Loop
			$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
			$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
			$show_footer_signup 			= get_field( 'show_footer_signup' );
			$show_prefooter 				= get_field( 'show_prefooter' );
			// End the loop.
			endwhile;
			?>

			<section>
			<!-- QUERY COMPATIBLE SYSTEMS LOGO CPT -->
      <?php
				  global $post;
			    $args = array( 'post_type' => 'products' );
			    $loop = new WP_Query( $args );

          // need to create counter to alternate Products
			    $number = 0;

			  	while ( $loop->have_posts() ) : $loop->the_post();

			  	// if number even
			  	if ($number % 2 == 0)
			  	{
					 ?>
              <div class="grid || grid--large || product-left">

                <div class="flex-half || product-container">
                  <h2 class="small-header"><?php the_field( "green_heading_above_each_product_logo" ); ?></h2>
                  <aside>
                    <img class="product-logo" src="<?php the_field( "product_logo" ); ?>" />
                  </aside>
                  <?php
                  the_field( 'product_description' );
                  echo '<br>';
                  ?>
                  <a class="btn" href="<?php the_field( 'product_button_url' ); ?>">
                    <?php the_field( 'product_button_text' );  ?>
                  </a>
                </div>

                <div class="flex-half">
                  <img class="product-img-right || animated" src="<?php  the_field( "product_monitor_image" ); ?>" />
                </div>
              </div>
				 <?php
				}
				// number is odd
				else
				{
				 ?>
            <div class="grid || grid--large || product-right">
              <div class="flex-half">
                <img class="product-img-left || animated" src="<?php the_field( "product_monitor_image" ); ?>" />
              </div>

              <div class="flex-half || product-container">
                <h2 class="small-header"><?php the_field( "green_heading_above_each_product_logo" ); ?></h2>
                <aside>
                  <img class="product-logo" src="<?php the_field( "product_logo" ); ?>" />
                </aside>
                <?php
                the_field( 'product_description' );
                echo '<br>';
                ?>
                <a class="btn" href="<?php the_field( 'product_button_url' ); ?>">
                  <?php the_field( 'product_button_text' );  ?>
                </a>
              </div>
            </div>
				<?php
				}
				// increment number counter by one
				$number++
				?>

			  <?php endwhile; // end of the loop. ?>

			</section>
			<?php
			// logic conditions to show Page Footer Options
			if( $show_testimonials_clients == 1 )
			{
				get_template_part( 'template-parts/testimonials-clients' );
			}

			if( $show_testimonials_employees == 1)
			{
				get_template_part( 'template-parts/testimonials-employees' );
			}

			if( $show_prefooter ==1)
			{
				get_template_part( 'template-parts/prefooter' );
			}

      if( $show_footer_signup == 1)
			{
				get_template_part( 'template-parts/footersignup' );
			}
			?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
