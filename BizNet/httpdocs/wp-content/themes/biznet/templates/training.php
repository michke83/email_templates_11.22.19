<?php
/**
*
* Template Name: Training
*
**/

  get_header();

  global $post;
	$page_id = $post->ID;
  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
									<h3><?php echo $subheading; ?></h3>
  		          </header><!-- .entry-header -->
              </section>

		          <div class="entry-content || ta-center">
		            <?php
		              the_content();
		            ?>
		          </div><!-- .entry-content -->

		        </article><!-- #post-## -->

			<?php
      // Get Custom Page Footer Options While in the Loop
      $show_testimonials_clients    = get_field( 'show_testimonials_clients' );
      $show_testimonials_employees  = get_field( 'show_testimonials_employees' );
      $show_footer_signup       = get_field( 'show_footer_signup' );
      $show_prefooter         = get_field( 'show_prefooter' );

      // standard pricing values
      $standard_training_title = get_field( 'standard_training_title' );
      $standard_training_price = get_field( 'standard_training_price' );
      $standard_training_price_text = get_field( 'standard_training_price_text' );
      $standard_training_price_button_url = get_field( 'standard_training_price_button_url' );
      $standard_training_price_button_text = get_field( 'standard_training_price_button_text' );
      $standard_features_rows = get_field('standard_training_price_bundle_features');

      // premium pricing values
      $premium_training_title = get_field( 'premium_training_title' );
      $premium_training_price = get_field( 'premium_training_price' );
      $premium_training_price_text = get_field( 'premium_training_price_text' );
      $premium_training_price_button_url = get_field( 'premium_training_price_button_url' );
      $premium_training_price_button_text = get_field( 'premium_training_price_button_text' );
      $premium_features_rows = get_field('premium_training_price_bundle_features');

      // premium plus pricing values
      $premium_plus_training_title = get_field( 'premium_plus_training_title' );
      $premium_plus_training_price = get_field( 'premium_plus_training_price' );
      $premium_plus_training_price_text = get_field( 'premium_plus_training_price_text' );
      $premium_plus_training_price_button_url = get_field( 'premium_plus_training_price_button_url' );
      $premium_plus_training_price_button_text = get_field( 'premium_plus_training_price_button_text' );
      $premium_plus_features_rows = get_field('premium_plus_training_price_bundle_features');

			// End the loop.
			endwhile;
			?>

			<section class="fr-accordion || js-fr-accordion">
        <h3 class="ta-center">COURSES</h3>
			<!-- QUERY COURSES CPT -->
          <div class="grid || grid--medium">
            <div class="flex-half">
                <p class="ta-center">
                <strong>Available BizInsight V5 Training Courses:</strong><br />
                *New courses added on a quarterly basis.</p>
                <h1 class="ta-center">BizInsight 5</h1>
                <?php
    				        global $post;
                    $args = array( 'post_type' => 'courses', 'meta_key' => 'course_type', 'meta_value'	=> 'bizinsight5' );
    			          $loop = new WP_Query( $args );
                    $i = 0;
                    while ( $loop->have_posts() ) : $loop->the_post();
                    $i++;
    					   ?>
                 <div class="ta-center">
                   <h2 class="small-header"><?php the_field( 'course_title' ); ?></h2>
                   <h4>Course Description</h4>
                   <p>
                     <?php
                         the_field( 'course_description', false, false );
                         echo '<br>';
                     ?>
                   </p>
                      <div id="accordion-header-<?php echo $i; ?>" class="fr-accordion__header js-fr-accordion__header">
                        <div style="margin: 0 auto;">
                          <img class="plus" src="<?php echo THEME_DIRECTORY ?>/img/plusblue.png" />
                          <img class="minus" src="<?php echo THEME_DIRECTORY ?>/img/minusblue.png" />
                          <h4>Course Outline</h4>
                        </div>
                      </div>
                      <div id="accordion-panel-<?php echo $i; ?>" class="fr-accordion__panel js-fr-accordion__panel">
                        <p class="fr-accordion__inner">
                          <?php
                              the_field( 'course_outline', false, false );
                              echo '<br>';
                          ?>
                        </p>
                      </div>
                    </div>
                <?php endwhile; // end of the loop. ?>
            </div>

            <div class="flex-half">
                <p class="ta-center">
                <strong>Available BizInsight V7 Training Courses:</strong><br />
                *New courses added on a quarterly basis.</p>
                <h1 class="ta-center">BizInsight 7</h1>
                  <?php
                    global $post;
                    $args = array( 'post_type' => 'courses', 'meta_key' => 'course_type', 'meta_value'	=> 'bizinsight7' );
                    $loop = new WP_Query( $args );
                    $x = 10;
                    while ( $loop->have_posts() ) : $loop->the_post();
                    $x++;
                  ?>
                  <div class="ta-center">
                    <h2 class="small-header"><?php the_field( 'course_title' ); ?></h2>
                    <h4>Course Description</h4>
                    <p>
                      <?php
                          the_field( 'course_description', false, false );
                          echo '<br>';
                      ?>
                    </p>
                      <div id="accordion-header-<?php echo $x; ?>" class="fr-accordion__header js-fr-accordion__header">
                        <div style="margin: 0 auto;">
                          <img class="plus" src="<?php echo THEME_DIRECTORY ?>/img/plusblue.png" />
                          <img class="minus" src="<?php echo THEME_DIRECTORY ?>/img/minusblue.png" />
                          <h4>Course Outline</h4>
                        </div>
                      </div>
                      <div id="accordion-panel-<?php echo $x; ?>" class="fr-accordion__panel js-fr-accordion__panel">
                        <p class="fr-accordion__inner">
                          <?php
                              the_field( 'course_outline', false, false );
                              echo '<br>';
                          ?>
                        </p>
                      </div>
                    </div>
                  <?php endwhile; // end of the loop. ?>
                </div>
              </div>
			</section>
      <br />
      <!-- Pricing Table -->
      <!-- STANDARD BUNDLE -->
      <section class="grid || parallax || bundles" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/compatibleparallax.jpg')">
        <div class="ta-center || grid || grid--vertical">
					<div class="bundle">
	          <h4><?php echo $standard_training_title; ?></h4>
						<div class="bundle-circle">
		          <h5><?php echo $standard_training_price; ?>/YR</h5>
		          <p><?php echo $standard_training_price_text; ?></p>
						</div>
	          <a class="btn" href="<?php echo $standard_training_price_button_url; ?>">
	            <?php echo $standard_training_price_button_text; ?>
	          </a>
						<p><?php the_field( 'cs_starter_bundle_below_button_text' ); ?></p>
	          <!-- FEATURES -->
            <?php
            if($standard_features_rows)
            {
            	echo '<ul>';
            	foreach($standard_features_rows as $standard_features_row)
            	{
            		echo '<li>' . $standard_features_row['standard_features'] . '</li>';
            	}
            	echo '</ul>';
            }
            ?>
	        </div>
				</div>
        <!-- PREMIUM BUNDLE -->
        <div class="ta-center || grid || grid--vertical">
	        <div class="bundle">
	          <h4><?php echo $premium_training_title; ?></h4>
						<div class="bundle-circle">
		          <h5><?php echo $premium_training_price; ?>/YR</h5>
		          <p><?php echo $premium_training_price_text; ?></p>
						</div>
	          <a class="btn" href="<?php echo $premium_training_price_button_url; ?>">
	            <?php echo $premium_training_price_button_text; ?>
	          </a>
	          <!-- FEATURES -->
            <?php
            if($premium_features_rows)
            {
            	echo '<ul>';
            	foreach($premium_features_rows as $premium_features_row)
            	{
            		echo '<li>' . $premium_features_row['premium_features'] . '</li>';
            	}
            	echo '</ul>';
            }
            ?>
	          </div>
					</div>

          <!-- PREMIUM PLUS BUNDLE -->
	        <div class="ta-center || grid || grid--vertical">
	          <div class="bundle">
	          	<h4><?php echo $premium_plus_training_title; ?></h4>
							<div class="bundle-circle">
		            <h5><?php echo $premium_plus_training_price; ?>/YR</h5>
		            <p><?php echo $premium_plus_training_price_text; ?></p>
							</div>
	            <a class="btn" href="<?php echo $premium_plus_training_price_button_url; ?>">
	             <?php echo $premium_plus_training_price_button_text; ?>
	            </a>
	            <!-- FEATURES -->
              <?php
              if($premium_plus_features_rows)
              {
              	echo '<ul>';
              	foreach($premium_plus_features_rows as $premium_plus_features_row)
              	{
              		echo '<li>' . $premium_plus_features_row['premium_plus_features'] . '</li>';
              	}
              	echo '</ul>';
              }
              ?>
	          </div>
					</div>
        </section>
      <?php
      // logic conditions to show Page Footer Options
      if( $show_testimonials_clients == 1 )
      {
        get_template_part( 'template-parts/testimonials-clients' );
      }

      if( $show_testimonials_employees == 1)
      {
        get_template_part( 'template-parts/testimonials-employees' );
      }

      if( $show_prefooter ==1)
      {
        get_template_part( 'template-parts/prefooter' );
      }

      if( $show_footer_signup == 1)
      {
        get_template_part( 'template-parts/footersignup' );
      }
      ?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
