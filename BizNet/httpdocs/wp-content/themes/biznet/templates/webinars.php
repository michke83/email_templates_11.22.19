<?php
/**
*
* Template Name: Webinars
*
**/

  get_header();

  global $post;
	$page_id = $post->ID;
  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
      <?php
        $subheading = get_field( 'page_sub_headings' );
      ?>
      <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
        <header class="entry-header">
          <h3><?php echo $subheading; ?></h3>
          <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        </header><!-- .entry-header -->
      </section>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>

	          <section class="entry-content || ta-center || wrapper-large">
	            <?php
	              the_content();
	            ?>
	          </section><!-- .entry-content -->

			<?php
      // Get Custom Page Footer Options While in the Loop
      $show_testimonials_clients    = get_field( 'show_testimonials_clients' );
      $show_testimonials_employees  = get_field( 'show_testimonials_employees' );
      $show_footer_signup       = get_field( 'show_footer_signup' );
      $show_prefooter         = get_field( 'show_prefooter' );
			// End the loop.
			endwhile;
			?>

			<section class="wrapper-large">
			<!-- QUERY COURSES CPT -->
      <?php
				  global $post;
			    $args = array( 'post_type' => 'webinars', 'order' => 'asc' );
			    $loop = new WP_Query( $args );

          while ( $loop->have_posts() ) : $loop->the_post();
					 ?>
              <div class="grid || grid--large || schedule">
                <div class="flex-half || grid">
                  <div class="flex-half">
                    Date: <?php the_field( 'webinar_date' ); ?>
                    Time: <?php the_field( 'webinar_start_time' ); ?> - <?php the_field( 'webinar_end_time' ); ?>
                  </div>
                  <div class="flex-half">
                    <a href="<?php the_field( 'webinar_link_url' ); ?>">
                      <?php the_field( 'webinar_title' ); ?>
                    </a>
                  </div>
                </div>

                <div class="flex-half">
                  <?php the_field( 'webinar_description' ); ?>
                </div>
              </div>

          <?php endwhile; // end of the loop. ?>

			</section>
      <?php
      // logic conditions to show Page Footer Options
      if( $show_testimonials_clients == 1 )
      {
        get_template_part( 'template-parts/testimonials-clients' );
      }

      if( $show_testimonials_employees == 1)
      {
        get_template_part( 'template-parts/testimonials-employees' );
      }

      if( $show_prefooter ==1)
      {
        get_template_part( 'template-parts/prefooter' );
      }

      if( $show_footer_signup == 1)
      {
        get_template_part( 'template-parts/footersignup' );
      }
      ?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
