<?php
/**
*
* Template Name: Contact
*
**/

	get_header();
?>
<?php
	global $post;
	$page_id = $post->ID;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
                  <h3><?php echo $subheading; ?></h3>
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  		          </header><!-- .entry-header -->
              </section>

							<section class="grid || grid--medium || contact-grid">
			          <div class="entry-content || column--heavy">
			            <?php
			              the_content();

			              wp_link_pages( array(
			                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'biznet' ),
			                'after'  => '</div>',
			              ) );
			            ?>
			          </div><!-- .entry-content -->

								<div class="column--light || address-card">
									<?php
									$biz_address 			= get_field( 'biznet_street_address', 'options' );
									$biz_city 				= get_field( 'biznet_city', 'options' );
									$biz_state 				= get_field( 'biznet_state', 'options' );
									$biz_zip 					= get_field( 'biznet_zip_code', 'options' );
									$biz_info_email 	= get_field( 'biznet_info_email', 'options' );
									$biz_phone_number = get_field( 'biznet_phone_number', 'options' );
									$biz_fax_number 	= get_field( 'biznet_fax_number', 'options' );
									$biz_office_pic   = get_field ( 'office_image', 'options' );
									?>
									<div>
										<img src="<?php echo $biz_office_pic; ?>" width="200" />
									</div>
									<div class="ta-center">
									<h2 class="small-header">HEADQUARTERS</h2>
									<h3>DALLAS, TX</h3>

										<?php
										echo $biz_address;
										echo '<br />';
										echo $biz_city;
										echo '<br />';
										echo $biz_state;
										echo '<br />';
										echo $biz_zip;
										echo '<br />';
										echo $biz_info_email;
										echo '<br />';
										echo $biz_phone_number;
										echo '<br />';
										echo $biz_fax_number;
										echo '<br />';
										?>
									</div>
								</div>
							</section>
						</article><!-- #post-## -->

			<?php
			// Get Custom Page Footer Options While in the Loop
			$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
			$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
			$show_footer_signup 			= get_field( 'show_footer_signup' );
			$show_prefooter 				= get_field( 'show_prefooter' );
			// End the loop.
			endwhile;

			// End If
			endif;
			?>

			<div class="grid grid--medium">
				<!-- GOOGLE MAP -->
				<div class="grid">
						<div id="googleMap" style="height:400px;"></div>
				</div>
			</div>
			<?php
			// logic conditions to show Page Footer Options
			if( $show_testimonials_clients == 1 )
			{
				get_template_part( 'template-parts/testimonials-clients' );
			}

			if( $show_testimonials_employees == 1)
			{
				get_template_part( 'template-parts/testimonials-employees' );
			}

			if( $show_prefooter ==1)
			{
				get_template_part( 'template-parts/prefooter' );
			}

			if( $show_footer_signup == 1)
			{
				get_template_part( 'template-parts/footersignup' );
			}
			?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
