<?php
/**
*
* Template Name: Events
*
**/

	get_header();
?>
<?php
	global $post;
	$page_id = $post->ID;
  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
                  <h3><?php echo $subheading; ?></h3>
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  		          </header><!-- .entry-header -->
              </section>

		          <div class="entry-content">
		            <?php
		              the_content();
		            ?>
		          </div><!-- .entry-content -->

		        </article><!-- #post-## -->
			<?php
			// Get Custom Page Footer Options While in the Loop
			$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
			$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
			$show_footer_signup 			= get_field( 'show_footer_signup' );
			$show_prefooter 				= get_field( 'show_prefooter' );
			// End the loop.
			endwhile;

			// End If
			endif;
			?>

      <div class="grid grid--large">
				<div class="events-left">
						<?php
            $events_top_left_image  = get_field( 'top_left_box_image' );
            $events_top_left_text		= get_field( 'top_left_box_text' );
            $events_top_left_url		= get_field( 'top_left_box_url_link' );
            ?>
            <a href="<?php echo $events_top_left_url; ?>">
							<div class="events">
	              <img src="<?php echo $events_top_left_image; ?>" />
								<div class="events-header">
	              	<h3><?php echo $events_top_left_text; ?></h3>
								</div>
							</div>
            </a>
				</div>

        <div class="events-right">
						<?php
            $events_top_right_image = get_field( 'top_right_box_image' );
            $events_top_right_text	= get_field( 'top_right_box_text' );
            $events_top_right_url		= get_field( 'top_right_box_url_link' );
            ?>
            <a href="<?php echo $events_top_right_url; ?>">
							<div class="events">
	              <img src="<?php echo $events_top_right_image; ?>" />
								<div class="events-header">
	              	<h3><?php echo $events_top_right_text; ?></h3>
								</div>
							</div>
            </a>
				</div>
			</div>

      <div class="grid grid--large">
				<div class="events-left">
						<?php
            $events_bottom_left_image = get_field( 'bottom_left_box_image' );
            $events_bottom_left_text	= get_field( 'bottom_left_box_text' );
            $events_bottom_left_url		= get_field( 'bottom_left_box_url_link' );
            ?>
            <a href="<?php echo $events_bottom_left_url; ?>">
							<div class="events">
              	<img src="<?php echo $events_bottom_left_image; ?>" />
								<div class="events-header">
              		<h3><?php echo $events_bottom_left_text; ?></h3>
								</div>
							</div>
            </a>
				</div>

        <div class="events-right">
						<?php
            $events_bottom_right_image = get_field( 'bottom_right_box_image' );
            $events_bottom_right_text	= get_field( 'bottom_right_box_text' );
            $events_bottom_right_url		= get_field( 'bottom_right_box_url_link' );
            ?>
            <a href="<?php echo $events_bottom_right_url; ?>">
							<div class="events">
	              <img src="<?php echo $events_bottom_right_image; ?>" />
								<div class="events-header">
	              	<h3><?php echo $events_bottom_right_text; ?></h3>
								</div>
							</div>
            </a>
				</div>
			</div>
			<?php
			// logic conditions to show Page Footer Options
			if( $show_testimonials_clients == 1 )
			{
				get_template_part( 'template-parts/testimonials-clients' );
			}

			if( $show_testimonials_employees == 1)
			{
				get_template_part( 'template-parts/testimonials-employees' );
			}

			if( $show_prefooter ==1)
			{
				get_template_part( 'template-parts/prefooter' );
			}

			if( $show_footer_signup == 1)
			{
				get_template_part( 'template-parts/footersignup' );
			}
			?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
