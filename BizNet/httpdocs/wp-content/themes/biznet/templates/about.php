<?php
/**
*
* Template Name: About
*
**/

	get_header();
?>
<?php
	global $post;
	$page_id = $post->ID;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
                  <h3><?php echo $subheading; ?></h3>
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  		          </header><!-- .entry-header -->
              </section>

							<section class="about-entry" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/aboutparallax.jpg')">
			          <div class="entry-content">
			            <?php
			              the_content();

			              wp_link_pages( array(
			                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'biznet' ),
			                'after'  => '</div>',
			              ) );
			            ?>
			          </div><!-- .entry-content -->
							</section>

		          <?php if ( get_edit_post_link() ) : ?>
		            <footer class="entry-footer">
		              <?php
		                edit_post_link(
		                  sprintf(
		                    /* translators: %s: Name of current post */
		                    esc_html__( 'Edit %s', 'biznet' ),
		                    the_title( '<span class="screen-reader-text">"', '"</span>', false )
		                  ),
		                  '<span class="edit-link">',
		                  '</span>'
		                );
		              ?>
		            </footer><!-- .entry-footer -->
		          <?php endif; ?>
		        </article><!-- #post-## -->


			<?php
			// Get Custom Page Footer Options While in the Loop
			$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
			$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
			$show_footer_signup 			= get_field( 'show_footer_signup' );
			$show_prefooter 				= get_field( 'show_prefooter' );

			// End the loop.
			endwhile;

			// End If
			endif;
			?>

			<section class="team">
				<div class="ta-center">
					<h2 class="small-header">WHO WE ARE</h2>
					<h3>Meet Our Team</h3>
				</div>

				<div class="team-slider">
					<?php
					global $post;

					$args = array( 'post_type' => 'team_members', 'order' => 'ASC', );

					$loop = new WP_Query( $args );

					while ( $loop->have_posts() ) : $loop->the_post(); ?>

					<div class="team-members">
						<div class="team-image">
							<img src="<?php the_field( "team_member_image" ); ?>" />

							<div class="team-text">
								<?php
									echo '<h5>';
									the_field( 'team_member_name' );
									echo '</h5>';
									echo '<h6>';
									the_field( 'team_member_title' );
									echo '</h6>';
									echo '<br>';
									the_field( 'team_member_bio' );
								?>
							</div>
						</div>
					</div>

					<?php endwhile; // end of the loop. ?>
				</div>
			</section>

			<section class="culture" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/aboutconfetti.jpg')">
				<div class="ta-center">
				<?php

					$title = get_field( 'culture_section_title', 'options' );
					echo '<h2 class="small-header">';
					echo $title;
					echo '</h2>';
					echo '<h3>';
					the_field( 'culture_section_sub_title', 'options' );
				?>
					</h3>
				</div>
				<?php $button_url = get_field( 'culture_section_button_url', 'options' ); ?>
				<div class="grid || grid--vertical || culture-grid">
					<div class="culture-description || flex-half || ta-center">
						<?php the_field( 'culture_section_description', 'options' ); ?>
						<a class="btn || btn--quatrinary" href="<?php echo $button_url; ?>">
						<?php
						the_field( 'culture_section_button_text', 'options' );
						?>
						</a>
					</div>

					<?php if( have_rows('culture_reasons', 'options') ):

					$i = 0;?>

					<ul class="culture-list || fr-accordion || js-fr-accordion || flex-half">

					<?php while( have_rows('culture_reasons', 'options') ): the_row();

					$i++;?>

							<li id="accordion-header-<?php echo $i; ?>" class="fr-accordion__header js-fr-accordion__header"><img class="plus" src="<?php echo THEME_DIRECTORY ?>/img/plussign.png" /><img class="minus" src="<?php echo THEME_DIRECTORY ?>/img/minus.png" /><h4><?php the_sub_field('culture_reason_title', 'options'); ?></h4>
							</li>
							<li id="accordion-panel-<?php echo $i; ?>" class="fr-accordion__panel js-fr-accordion__panel">
								<p class="fr-accordion__inner">
										<?php the_sub_field('culture_reason_text', 'options'); ?>
								</p>
							</li>

					<?php endwhile; ?>

					</ul>
				</div>

			</section>

			<?php endif; ?>

			<div class="grid || grid--medium">
				<!-- GOOGLE MAP -->
				<div style="flex-basis: 50%">
						<div id="googleMap" style="height: 100%;"></div>
				</div>
				<div class="grid || flex-half">
					<div class="grid || grid--vertical || ta-center || address-card">
						<h2 class="small-header">HEADQUARTERS</h2>
						<h3>DALLAS, TX</h3>
						<?php
						$biz_address 			= get_field( 'biznet_street_address', 'options' );
						$biz_city 				= get_field( 'biznet_city', 'options' );
						$biz_state 				= get_field( 'biznet_state', 'options' );
						$biz_zip 					= get_field( 'biznet_zip_code', 'options' );
						$biz_info_email 	= get_field( 'biznet_info_email', 'options' );
						$biz_phone_number = get_field( 'biznet_phone_number', 'options' );
						$biz_fax_number 	= get_field( 'biznet_fax_number', 'options' );
						$biz_office_pic   = get_field ( 'office_image', 'options' );
						echo $biz_address;
						echo '<br />';
						echo $biz_city;
						echo '<br />';
						echo $biz_state;
						echo '<br />';
						echo $biz_zip;
						echo '<br />';
						echo $biz_info_email;
						echo '<br />';
						echo $biz_phone_number;
						echo '<br />';
						echo $biz_fax_number;
						echo '<br />';
						?>
					</div>
					<div class="biz-img" style="background-image: url('<?php echo $biz_office_pic; ?>')">
					</div>
				</div>
			</div>
			<?php
			// logic conditions to show Page Footer Options
			if( $show_testimonials_clients == 1 )
			{
				get_template_part( 'template-parts/testimonials-clients' );
			}

			if( $show_testimonials_employees == 1)
			{
				get_template_part( 'template-parts/testimonials-employees' );
			}

			if( $show_prefooter ==1)
			{
				get_template_part( 'template-parts/prefooter' );
			}

			if( $show_footer_signup == 1)
			{
				get_template_part( 'template-parts/footersignup' );
			}
			?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
