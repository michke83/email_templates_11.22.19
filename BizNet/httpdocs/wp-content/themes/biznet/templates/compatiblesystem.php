<?php
/**
*
* Template Name: Compatibile System - Single
*
**/

	get_header();
?>
<?php
	global $post;
	$page_id = $post->ID;
  ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();
        ?>
		        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php
                $subheading = get_field( 'page_sub_headings' );
              ?>
              <section class="page-hero" style="background-image: url('<?php the_post_thumbnail_url(); ?>');">
  		          <header class="entry-header">
									<h3><?php echo $subheading; ?></h3>
  		            <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
  		          </header><!-- .entry-header -->
              </section>

		          <div class="entry-content || ta-center">
		            <?php
		              the_content();
		            ?>
		          </div><!-- .entry-content -->

		        </article><!-- #post-## -->
			<?php
			// Get Custom Page Footer Options While in the Loop
			$show_testimonials_clients 		= get_field( 'show_testimonials_clients' );
			$show_testimonials_employees 	= get_field( 'show_testimonials_employees' );
			$show_footer_signup 			= get_field( 'show_footer_signup' );
			$show_prefooter 				= get_field( 'show_prefooter' );
			$cs_have_pricing = get_field( 'cs_have_pricing' );

			// standard pricing values
      $standard_training_price = get_field( 'standard_training_price' );
      $standard_training_price_text = get_field( 'standard_training_price_text' );
      $standard_training_price_button_url = get_field( 'standard_training_price_button_url' );
      $standard_training_price_button_text = get_field( 'standard_training_price_button_text' );
      $standard_features_rows = get_field('standard_training_price_bundle_features');

      // premium pricing values
      $premium_training_price = get_field( 'premium_training_price' );
      $premium_training_price_text = get_field( 'premium_training_price_text' );
      $premium_training_price_button_url = get_field( 'premium_training_price_button_url' );
      $premium_training_price_button_text = get_field( 'premium_training_price_button_text' );
      $premium_features_rows = get_field('premium_training_price_bundle_features');

      // premium plus pricing values
      $premium_plus_training_price = get_field( 'premium_plus_training_price' );
      $premium_plus_training_price_text = get_field( 'premium_plus_training_price_text' );
      $premium_plus_training_price_button_url = get_field( 'premium_plus_training_price_button_url' );
      $premium_plus_training_price_button_text = get_field( 'premium_plus_training_price_button_text' );
      $premium_plus_features_rows = get_field('premium_plus_training_price_bundle_features');

			// End the loop.
			endwhile;

			// End If
			endif;
			?>
			<?php
			if ($cs_have_pricing == 'yes_pricing')
			{
				?>
      <!-- Pricing Table -->
      <!-- STARTER BUNDLE -->
      <section class="grid || grid--large || parallax || bundles" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/compatibleparallax.jpg')">
        <div class="ta-center || grid || grid--vertical">
					<div class="bundle">
	          <h4>STARTER BUNDLE</h4>
						<div class="bundle-circle">
		          <h5><?php the_field( 'cs_starter_bundle_monthly_price' ); ?>/MO</h5>
		          <p><?php the_field( 'cs_starter_bundle_price_text' ); ?></p>
						</div>
	          <a class="btn" href="<?php the_field( 'cs_starter_bundle_button_url' ); ?>">
	            <?php the_field( 'cs_starter_bundle_button_text' ); ?>
	          </a>
						<p><?php the_field( 'cs_starter_bundle_below_button_text' ); ?></p>
	          <!-- FEATURES -->
	          <?php if( have_rows('cs_starter_bundle_features') ): ?>
	                <ul>
	                <?php while( have_rows('cs_starter_bundle_features') ): the_row(); ?>
	                    <li><?php the_sub_field('starter_features'); ?></li>
	                <?php endwhile; ?>
	                </ul>
	            <?php endif; ?>
	        </div>
				</div>
        <!-- EXECUTIVE BUNDLE -->
        <div class="ta-center || grid || grid--vertical">
	        <div class="bundle">
	          <h4>EXECUTIVE BUNDLE</h4>
						<div class="bundle-circle">
		          <h5><?php the_field( 'cs_executive_bundle_monthly_price' ); ?>/MO</h5>
		          <p><?php the_field( 'cs_executive_bundle_price_text' ); ?></p>
						</div>
	          <a class="btn" href="<?php the_field( 'cs_executive_bundle_button_url' ); ?>">
	            <?php the_field( 'cs_executive_bundle_button_text' ); ?>
	          </a>
						<p><?php the_field( 'cs_executive_bundle_below_button_text' ); ?></p>
	          <!-- FEATURES -->
	          <?php if( have_rows('cs_executive_bundle_features') ): ?>
	            <ul>
	            <?php while( have_rows('cs_executive_bundle_features') ): the_row(); ?>
	                <li><?php the_sub_field('executive_features'); ?></li>
	            <?php endwhile; ?>
	            </ul>
	            <?php endif; ?>
	          </div>
					</div>

          <!-- Biz Broadcast Bundle -->
	        <div class="ta-center || grid || grid--vertical">
	          <div class="bundle">
	          	<h4>BIZBROADCAST</h4>
							<div class="bundle-circle">
		            <h5><?php the_field( 'biz_broadcast_monthly_price' ); ?>/MO</h5>
		            <p><?php the_field( 'biz_broadcast_price_text' ); ?></p>
							</div>
	            <a class="btn" href="<?php the_field( 'biz_broadcast_button_url' ); ?>">
	              <?php the_field( 'biz_broadcast_button_text' ); ?>
	            </a>

	            <!-- FEATURES -->
	            <?php if( have_rows('biz_broadcast_bundle_features') ): ?>
	                  <ul>
	                  <?php while( have_rows('biz_broadcast_bundle_features') ): the_row(); ?>
	                      <li><?php the_sub_field('biz_broadcast_features'); ?></li>
	                  <?php endwhile; ?>
	                  </ul>
	              <?php endif; ?>
	            <p><?php the_field( 'biz_broadcast_bundle_below_button_text' ); ?></p>
	          </div>
					</div>
        </section>
				<?php
				}
				?>
				<section>
						<?php get_template_part( 'template-parts/enterprise' ); ?>

						<?php get_template_part( 'template-parts/biztraining' ); ?>
				</section>
				<!-- Pricing Table -->
	      <!-- STANDARD BUNDLE -->
	      <section class="grid || parallax || bundles" style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/compatibleparallax.jpg')">
	        <div class="ta-center || grid || grid--vertical">
						<div class="bundle">
		          <h4>STANDARD</h4>
							<div class="bundle-circle">
			          <h5><?php echo $standard_training_price; ?>/YR</h5>
			          <p><?php echo $standard_training_price_text; ?></p>
							</div>
		          <a class="btn" href="<?php echo $standard_training_price_button_url; ?>">
		            <?php echo $standard_training_price_button_text; ?>
		          </a>
							<p><?php the_field( 'cs_starter_bundle_below_button_text' ); ?></p>
		          <!-- FEATURES -->
	            <?php
	            if($standard_features_rows)
	            {
	            	echo '<ul>';
	            	foreach($standard_features_rows as $standard_features_row)
	            	{
	            		echo '<li>' . $standard_features_row['standard_features'] . '</li>';
	            	}
	            	echo '</ul>';
	            }
	            ?>
		        </div>
					</div>
	        <!-- PREMIUM BUNDLE -->
	        <div class="ta-center || grid || grid--vertical">
		        <div class="bundle">
		          <h4>PREMIUM</h4>
							<div class="bundle-circle">
			          <h5><?php echo $premium_training_price; ?>/YR</h5>
			          <p><?php echo $premium_training_price_text; ?></p>
							</div>
		          <a class="btn" href="<?php echo $premium_training_price_button_url; ?>">
		            <?php echo $premium_training_price_button_text; ?>
		          </a>
		          <!-- FEATURES -->
	            <?php
	            if($premium_features_rows)
	            {
	            	echo '<ul>';
	            	foreach($premium_features_rows as $premium_features_row)
	            	{
	            		echo '<li>' . $premium_features_row['premium_features'] . '</li>';
	            	}
	            	echo '</ul>';
	            }
	            ?>
		          </div>
						</div>

	          <!-- PREMIUM PLUS BUNDLE -->
		        <div class="ta-center || grid || grid--vertical">
		          <div class="bundle">
		          	<h4>PREMIUM PLUS</h4>
								<div class="bundle-circle">
			            <h5><?php echo $premium_plus_training_price; ?>/YR</h5>
			            <p><?php echo $premium_plus_training_price_text; ?></p>
								</div>
		            <a class="btn" href="<?php echo $premium_plus_training_price_button_url; ?>">
		             <?php echo $premium_plus_training_price_button_text; ?>
		            </a>
		            <!-- FEATURES -->
	              <?php
	              if($premium_plus_features_rows)
	              {
	              	echo '<ul>';
	              	foreach($premium_plus_features_rows as $premium_plus_features_row)
	              	{
	              		echo '<li>' . $premium_plus_features_row['premium_plus_features'] . '</li>';
	              	}
	              	echo '</ul>';
	              }
	              ?>
		          </div>
						</div>
	        </section>
				<?php
				// logic conditions to show Page Footer Options
				if( $show_testimonials_clients == 1 )
				{
					get_template_part( 'template-parts/testimonials-clients' );
				}

				if( $show_testimonials_employees == 1)
				{
					get_template_part( 'template-parts/testimonials-employees' );
				}

				if( $show_prefooter ==1)
				{
					get_template_part( 'template-parts/prefooter' );
				}

				if( $show_footer_signup == 1)
				{
					get_template_part( 'template-parts/footersignup' );
				}
				?>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
