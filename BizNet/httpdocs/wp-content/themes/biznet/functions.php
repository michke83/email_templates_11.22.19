<?php
/**
 * Biznet functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Biznet
 */
/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/
if( !defined( 'THEME_DIRECTORY' ) ) {
 	define( 'THEME_DIRECTORY', get_template_directory_uri() );
}
if( !defined( 'THEME_DIR_PATH' ) ) {
 	define( 'THEME_DIR_PATH', get_template_directory() );
}
if(!defined('THEME_DIR_FILE')) {
	define('THEME_DIR_FILE', __FILE__);
}
if(!defined('SITE_URL')) {
  define('SITE_URL', get_site_url() );
}

if ( ! function_exists( 'biznet_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function biznet_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Biznet, use a find and replace
	 * to change 'biznet' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'biznet', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'biznet' ),
		'top-bar' => esc_html__( 'Top Bar', 'biznet' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'biznet_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );
}
endif;
add_action( 'after_setup_theme', 'biznet_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function biznet_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'biznet_content_width', 640 );
}
add_action( 'after_setup_theme', 'biznet_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function biznet_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'biznet' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'biznet' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'biznet_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function biznet_scripts() {
  wp_enqueue_style( 'biznet-style', get_stylesheet_uri() );

	wp_enqueue_style( 'slick-style', get_template_directory_uri() . '/css/slick.css' );

	wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/css/slick-theme.css' );

	wp_enqueue_style( 'modal-style', get_template_directory_uri() . '/css/modal.css' );

	wp_enqueue_style( 'smalt-style', get_template_directory_uri() . '/css/smalt.css' );

	wp_enqueue_style( 'animate-style', get_template_directory_uri() . '/css/animate.css' );

	wp_enqueue_script( 'biznet-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_style( 'biznet-custom-style', get_template_directory_uri() . '/css/styles.css' );
}

function biznet_footer_scripts() {

  wp_enqueue_script( 'biznet-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20151215', true );

  wp_enqueue_script( 'slick-scripts', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.7', true );

  wp_enqueue_script( 'modal-scripts', get_template_directory_uri() . '/js/modal.min.js', array('jquery'), '1.7', true );

  wp_enqueue_script('accordion-script', get_template_directory_uri() . '/js/accordion.min.js');

  wp_enqueue_script( 'waypoint-scripts', get_template_directory_uri() . '/js/waypoint.js', array('jquery'), '1.7', true );

  wp_enqueue_script( 'custom-scripts', get_template_directory_uri() . '/js/custom.js', array('jquery'), '20151215', true );

  // JS stuff for Google Maps API on About Page
  if ( is_page('about-us') OR (is_page('contact') ) ) {

    // enqueue gmaps js file
    wp_enqueue_script('pw-script', get_template_directory_uri() . '/js/gmaps.js');

    // get ACF fields from Options page settings
    $bizlat = get_field( 'biznet_latitude', 'options');
    $bizlong = get_field( 'biznet_longitude', 'options');
    $bizzoom = get_field( 'google_maps_zoom_number', 'options');
    $bizicon = get_field( 'google_maps_custom_icon', 'options');
    $bizapikey = get_field( 'google_maps_api_key', 'biznet' );

    // localize script and pass PHP variables to JS gmaps file
  	wp_localize_script('pw-script', 'pw_script_vars', array(
  			'latitude'   => __( $bizlat, 'biznet' ),
  			'longitude'  => __( $bizlong, 'biznet' ),
        'zoom'       => __( $bizzoom, 'biznet' ),
        'icon'       => __( $bizicon, 'biznet' )
  		)
  	);

    // call Google Maps JS Library
    wp_enqueue_script( 'biznet-gmaps-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBqwNmNAh7CqVk3ce1log3auBkExpRR9WQ&callback=initMap', array( 'jquery' ), true );
  }
}
add_action( 'wp_enqueue_scripts', 'biznet_scripts' );
add_action( 'wp_footer', 'biznet_footer_scripts' );



/**
* ACF Theme Options settings
**/
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Home Page Settings',
		'menu_title'	=> 'Home Page',
		'parent_slug'	=> 'theme-general-settings',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));

  acf_add_options_sub_page(array(
		'page_title' 	=> 'About Page Settings',
		'menu_title'	=> 'About Page',
		'parent_slug'	=> 'theme-general-settings',
	));

}

/**
*
* Register Custom Post Types
*
**/

/* Compatiable Systems CPT */
function comp_systems_post_types() {

	$labels = array(
		'name'                  => _x( 'Compatible Systems', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Compatible Systems', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Compatible Systems', 'biznet' ),
		'name_admin_bar'        => __( 'Compatible Systems', 'biznet' ),
		'archives'              => __( 'Logos Archives', 'biznet' ),
		'attributes'            => __( 'Logos Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Comp Systems:', 'biznet' ),
		'all_items'             => __( 'All Compatible Systems', 'biznet' ),
		'add_new_item'          => __( 'Add New Compatible Systems', 'biznet' ),
		'add_new'               => __( 'Add Compatible Systems', 'biznet' ),
		'new_item'              => __( 'New Compatible Systems', 'biznet' ),
		'edit_item'             => __( 'Edit Compatible Systems', 'biznet' ),
		'update_item'           => __( 'Update Compatible Systems', 'biznet' ),
		'view_item'             => __( 'View Compatible Systems', 'biznet' ),
		'view_items'            => __( 'View Compatible Systems', 'biznet' ),
		'search_items'          => __( 'Search Compatible Systems', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Compatible Systems list', 'biznet' ),
		'items_list_navigation' => __( 'Compatible Systems list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Compatible Systems', 'biznet' ),
		'description'           => __( 'Compatible Systems Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-format-image',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'comp_systems', $args );

}
add_action( 'init', 'comp_systems_post_types', 0 );

/* Case Studies CPT */
function case_studies_post_types() {

	$labels = array(
		'name'                  => _x( 'Case Studies', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Case Studies', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Case Studies', 'biznet' ),
		'name_admin_bar'        => __( 'Case Studies', 'biznet' ),
		'archives'              => __( 'Case Studies Archives', 'biznet' ),
		'attributes'            => __( 'Case Studies Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Case Studies:', 'biznet' ),
		'all_items'             => __( 'All Case Studies', 'biznet' ),
		'add_new_item'          => __( 'Add New Case Studies', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Case Studies', 'biznet' ),
		'edit_item'             => __( 'Edit Case Studies', 'biznet' ),
		'update_item'           => __( 'Update Case Studies', 'biznet' ),
		'view_item'             => __( 'View Case Studies', 'biznet' ),
		'view_items'            => __( 'View Case Studies', 'biznet' ),
		'search_items'          => __( 'Search Case Studies', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Case Studies list', 'biznet' ),
		'items_list_navigation' => __( 'Case Studies list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Case Studies', 'biznet' ),
		'description'           => __( 'Case Studies Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-portfolio',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'case_studies', $args );

}
add_action( 'init', 'case_studies_post_types', 0 );

/* Webinars CPT */
function webinars_post_types() {

	$labels = array(
		'name'                  => _x( 'Webinars', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Webinars', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Webinars', 'biznet' ),
		'name_admin_bar'        => __( 'Webinars', 'biznet' ),
		'archives'              => __( 'Webinars Archives', 'biznet' ),
		'attributes'            => __( 'Webinars Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Webinars:', 'biznet' ),
		'all_items'             => __( 'All Webinars', 'biznet' ),
		'add_new_item'          => __( 'Add New Webinars', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Webinars', 'biznet' ),
		'edit_item'             => __( 'Edit Webinars', 'biznet' ),
		'update_item'           => __( 'Update Webinars', 'biznet' ),
		'view_item'             => __( 'View Webinars', 'biznet' ),
		'view_items'            => __( 'View Webinars', 'biznet' ),
		'search_items'          => __( 'Search Webinars', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Webinars list', 'biznet' ),
		'items_list_navigation' => __( 'Webinars list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Webinars', 'biznet' ),
		'description'           => __( 'Webinars Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-welcome-view-site',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'webinars', $args );

}
add_action( 'init', 'webinars_post_types', 0 );

/* Q&A Sessions CPT */
function sessions_post_types() {

	$labels = array(
		'name'                  => _x( 'Sessions', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Sessions', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Sessions', 'biznet' ),
		'name_admin_bar'        => __( 'Sessions', 'biznet' ),
		'archives'              => __( 'Sessions Archives', 'biznet' ),
		'attributes'            => __( 'Sessions Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Sessions:', 'biznet' ),
		'all_items'             => __( 'All Sessions', 'biznet' ),
		'add_new_item'          => __( 'Add New Sessions', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Sessions', 'biznet' ),
		'edit_item'             => __( 'Edit Sessions', 'biznet' ),
		'update_item'           => __( 'Update Sessions', 'biznet' ),
		'view_item'             => __( 'View Sessions', 'biznet' ),
		'view_items'            => __( 'View Sessions', 'biznet' ),
		'search_items'          => __( 'Search Sessions', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Sessions list', 'biznet' ),
		'items_list_navigation' => __( 'Sessions list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Sessions', 'biznet' ),
		'description'           => __( 'Sessions Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-megaphone',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'sessions', $args );

}
add_action( 'init', 'sessions_post_types', 0 );

/* Testimonials CPT */
function testimonials_post_types() {

	$labels = array(
		'name'                  => _x( 'Testimonials', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Testimonials', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Testimonials', 'biznet' ),
		'name_admin_bar'        => __( 'Testimonials', 'biznet' ),
		'archives'              => __( 'Testimonials Archives', 'biznet' ),
		'attributes'            => __( 'Testimonials Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Testimonials:', 'biznet' ),
		'all_items'             => __( 'All Testimonials', 'biznet' ),
		'add_new_item'          => __( 'Add New Testimonials', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Testimonials', 'biznet' ),
		'edit_item'             => __( 'Edit Testimonials', 'biznet' ),
		'update_item'           => __( 'Update Testimonials', 'biznet' ),
		'view_item'             => __( 'View Testimonials', 'biznet' ),
		'view_items'            => __( 'View Testimonials', 'biznet' ),
		'search_items'          => __( 'Search Testimonials', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Testimonials list', 'biznet' ),
		'items_list_navigation' => __( 'Testimonials list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Testimonials', 'biznet' ),
		'description'           => __( 'Testimonials Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-testimonial',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'testimonials', $args );

}
add_action( 'init', 'testimonials_post_types', 0 );

/* Courses CPT */
function courses_post_types() {

	$labels = array(
		'name'                  => _x( 'Courses', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Courses', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Courses', 'biznet' ),
		'name_admin_bar'        => __( 'Courses', 'biznet' ),
		'archives'              => __( 'Courses Archives', 'biznet' ),
		'attributes'            => __( 'Courses Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Courses:', 'biznet' ),
		'all_items'             => __( 'All Courses', 'biznet' ),
		'add_new_item'          => __( 'Add New Courses', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Courses', 'biznet' ),
		'edit_item'             => __( 'Edit Courses', 'biznet' ),
		'update_item'           => __( 'Update Courses', 'biznet' ),
		'view_item'             => __( 'View Courses', 'biznet' ),
		'view_items'            => __( 'View Courses', 'biznet' ),
		'search_items'          => __( 'Search Courses', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Courses list', 'biznet' ),
		'items_list_navigation' => __( 'Courses list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Courses', 'biznet' ),
		'description'           => __( 'Courses Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-welcome-learn-more',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'courses', $args );

}
add_action( 'init', 'courses_post_types', 0 );

/* Team Members CPT */
function team_members_post_types() {

	$labels = array(
		'name'                  => _x( 'Team Members', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Team Member', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Team Members', 'biznet' ),
		'name_admin_bar'        => __( 'Team Members', 'biznet' ),
		'archives'              => __( 'Team Members Archives', 'biznet' ),
		'attributes'            => __( 'Team Members Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Team Members:', 'biznet' ),
		'all_items'             => __( 'All Team Members', 'biznet' ),
		'add_new_item'          => __( 'Add New Team Members', 'biznet' ),
		'add_new'               => __( 'Add Team Members', 'biznet' ),
		'new_item'              => __( 'New Team Members', 'biznet' ),
		'edit_item'             => __( 'Edit Team Members', 'biznet' ),
		'update_item'           => __( 'Update Team Members', 'biznet' ),
		'view_item'             => __( 'View Team Members', 'biznet' ),
		'view_items'            => __( 'View Team Members', 'biznet' ),
		'search_items'          => __( 'Search Team Members', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Team Members list', 'biznet' ),
		'items_list_navigation' => __( 'Team Members list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Team Members', 'biznet' ),
		'description'           => __( 'Team Members Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-groups',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'team_members', $args );

}
add_action( 'init', 'team_members_post_types', 0 );

/* Tradeshows CPT */
function tradeshows_post_types() {

	$labels = array(
		'name'                  => _x( 'Tradeshows', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Tradeshows', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Tradeshows', 'biznet' ),
		'name_admin_bar'        => __( 'Tradeshows', 'biznet' ),
		'archives'              => __( 'Tradeshows Archives', 'biznet' ),
		'attributes'            => __( 'Tradeshows Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Tradeshows:', 'biznet' ),
		'all_items'             => __( 'All Tradeshows', 'biznet' ),
		'add_new_item'          => __( 'Add New Tradeshows', 'biznet' ),
		'add_new'               => __( 'Add Tradeshows', 'biznet' ),
		'new_item'              => __( 'New Tradeshows', 'biznet' ),
		'edit_item'             => __( 'Edit Tradeshows', 'biznet' ),
		'update_item'           => __( 'Update Tradeshows', 'biznet' ),
		'view_item'             => __( 'View Tradeshows', 'biznet' ),
		'view_items'            => __( 'View Tradeshows', 'biznet' ),
		'search_items'          => __( 'Search Tradeshows', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Tradeshows list', 'biznet' ),
		'items_list_navigation' => __( 'Tradeshows list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Tradeshows', 'biznet' ),
		'description'           => __( 'Tradeshows Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-lightbulb',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'tradeshows', $args );

}
add_action( 'init', 'tradeshows_post_types', 0 );

/* Products CPT */
function products_post_types() {

	$labels = array(
		'name'                  => _x( 'Products', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Products', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Products', 'biznet' ),
		'name_admin_bar'        => __( 'Products', 'biznet' ),
		'archives'              => __( 'Products Archives', 'biznet' ),
		'attributes'            => __( 'Products Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Products:', 'biznet' ),
		'all_items'             => __( 'All Products', 'biznet' ),
		'add_new_item'          => __( 'Add New Products', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Products', 'biznet' ),
		'edit_item'             => __( 'Edit Products', 'biznet' ),
		'update_item'           => __( 'Update Products', 'biznet' ),
		'view_item'             => __( 'View Products', 'biznet' ),
		'view_items'            => __( 'View Products', 'biznet' ),
		'search_items'          => __( 'Search Products', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Products list', 'biznet' ),
		'items_list_navigation' => __( 'Products list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Products', 'biznet' ),
		'description'           => __( 'Products Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-store',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'products', $args );

}
add_action( 'init', 'products_post_types', 0 );

/* Videos CPT */
function videos_post_types() {

	$labels = array(
		'name'                  => _x( 'Videos', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Videos', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Videos', 'biznet' ),
		'name_admin_bar'        => __( 'Videos', 'biznet' ),
		'archives'              => __( 'Videos Archives', 'biznet' ),
		'attributes'            => __( 'Videos Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Videos:', 'biznet' ),
		'all_items'             => __( 'All Videos', 'biznet' ),
		'add_new_item'          => __( 'Add New Videos', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Videos', 'biznet' ),
		'edit_item'             => __( 'Edit Videos', 'biznet' ),
		'update_item'           => __( 'Update Videos', 'biznet' ),
		'view_item'             => __( 'View Videos', 'biznet' ),
		'view_items'            => __( 'View Videos', 'biznet' ),
		'search_items'          => __( 'Search Videos', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Videos list', 'biznet' ),
		'items_list_navigation' => __( 'Videos list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Videos', 'biznet' ),
		'description'           => __( 'Videos Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-format-video',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'videos', $args );

}
add_action( 'init', 'videos_post_types', 0 );

/* Jobs CPT */
function jobs_post_types() {

	$labels = array(
		'name'                  => _x( 'Jobs', 'Post Type General Name', 'biznet' ),
		'singular_name'         => _x( 'Jobs', 'Post Type Singular Name', 'biznet' ),
		'menu_name'             => __( 'Jobs', 'biznet' ),
		'name_admin_bar'        => __( 'Jobs', 'biznet' ),
		'archives'              => __( 'Jobs Archives', 'biznet' ),
		'attributes'            => __( 'Jobs Attributes', 'biznet' ),
		'parent_item_colon'     => __( 'Parent Jobs:', 'biznet' ),
		'all_items'             => __( 'All Jobs', 'biznet' ),
		'add_new_item'          => __( 'Add New Jobs', 'biznet' ),
		'add_new'               => __( 'Add New', 'biznet' ),
		'new_item'              => __( 'New Jobs', 'biznet' ),
		'edit_item'             => __( 'Edit Jobs', 'biznet' ),
		'update_item'           => __( 'Update Jobs', 'biznet' ),
		'view_item'             => __( 'View Jobs', 'biznet' ),
		'view_items'            => __( 'View Jobs', 'biznet' ),
		'search_items'          => __( 'Search Jobs', 'biznet' ),
		'not_found'             => __( 'Not found', 'biznet' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'biznet' ),
		'featured_image'        => __( 'Featured Image', 'biznet' ),
		'set_featured_image'    => __( 'Set featured image', 'biznet' ),
		'remove_featured_image' => __( 'Remove featured image', 'biznet' ),
		'use_featured_image'    => __( 'Use as featured image', 'biznet' ),
		'insert_into_item'      => __( 'Insert into item', 'biznet' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'biznet' ),
		'items_list'            => __( 'Jobs list', 'biznet' ),
		'items_list_navigation' => __( 'Jobs list navigation', 'biznet' ),
		'filter_items_list'     => __( 'Filter items list', 'biznet' ),
	);
	$args = array(
		'label'                 => __( 'Jobs', 'biznet' ),
		'description'           => __( 'Jobs Post Types.', 'biznet' ),
		'labels'                => $labels,
		'supports'              => array(),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 20,
    'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'jobs', $args );

}
add_action( 'init', 'jobs_post_types', 0 );
// custom HR link for Searching Jobs
add_action('admin_menu', 'hr_jobs_search_menu');
function hr_jobs_search_menu() {
    add_submenu_page('edit.php?post_type=jobs', 'HR JOB SEARCH', 'HR JOB SEARCH', 'manage_options', 'admin.php?page=CF7DBPluginSubmissions');
}
// Code for Custom Gravity Forms Dropdown list for Jobs Apply Form
add_filter( 'gform_pre_render_2', 'populate_posts' );
add_filter( 'gform_pre_validation_2', 'populate_posts' );
add_filter( 'gform_pre_submission_filter_2', 'populate_posts' );
add_filter( 'gform_admin_pre_render_2', 'populate_posts' );
function populate_posts( $form ) {

    foreach ( $form['fields'] as &$field ) {

        if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-posts' ) === false ) {
            continue;
        }

        $posts = get_posts( 'post_type=jobs&post_status=publish' );

        $choices = array();

        foreach ( $posts as $post ) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
        }

        $field->placeholder = 'Select a Job';
        $field->choices = $choices;
    }
    return $form;
}

// custom Gravity Form drop down list for a list of ERP Systems
add_filter( 'gform_pre_render', 'populate_erp_systems' );
add_filter( 'gform_pre_validation', 'populate_erp_systems' );
add_filter( 'gform_admin_pre_render', 'populate_erp_systems' );
add_filter( 'gform_pre_submission_filter', 'populate_erp_systems' );

function populate_erp_systems( $form ) {

    foreach ( $form['fields'] as &$field )
		{
        if ( $field->type != 'select' || strpos( $field->cssClass, 'erp-system-dropdown' ) === false )
				{
            continue;
        }

				$posts = get_posts( 'post_type=comp_systems&post_status=publish' );

        $choices = array();

        foreach ( $posts as $post ) {
            $choices[] = array( 'text' => $post->post_title, 'value' => $post->post_title );
        }

        $field->placeholder = 'Select a System';
        $field->choices = $choices;
    }
    return $form;
}

// custom Gravity Form drop down list for a list of Q&A Sessions
add_filter( 'gform_pre_render_4', 'populate_qa_session' );
add_filter( 'gform_pre_validation_4', 'populate_qa_session' );
add_filter( 'gform_admin_pre_render_4', 'populate_qa_session' );
add_filter( 'gform_pre_submission_filter_4', 'populate_qa_session' );

function populate_qa_session( $form ) {

    foreach ( $form['fields'] as &$field )
		{
        if ( $field->type != 'select' || strpos( $field->cssClass, 'qa-session-dropdown' ) === false )
				{
            continue;
        }
        global $wpdb;
        $sessiondates = $wpdb->get_results(
        	"
        	SELECT *
        	FROM $wpdb->posts
        	WHERE post_type = 'sessions'
        		AND post_status = 'publish'
        	"
        );

        $choices = array();

        foreach ( $sessiondates as $sessiondate ) {
            $choices[] = array( 'text' => $sessiondate->post_title, 'value' => $sessiondate->post_title );
        }

        $field->placeholder = 'Select Session';
        $field->choices = $choices;
    }
    return $form;
}

/**
 * Limit Post Excerpt Characters
 */
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';
