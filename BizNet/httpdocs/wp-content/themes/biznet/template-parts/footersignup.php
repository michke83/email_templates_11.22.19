<section class="footer-signup || grid || grid--medium">
  <div class="newsletter-signup">
    <?php echo gravity_form( 1, false, false, false, '', true, 12 ); ?>
  </div>
  <div class="free-trial">
    <?php
      echo get_field( 'subscribe_form_text', 'options' );
    ?>
    <a class="btn || btn--tertiary" href="<?php echo the_field( 'subscribe_form_button_url', 'options' ); ?>"><?php echo the_field( 'subscribe_form_button_text', 'options' ); ?>
    </a>
</section>
