<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Biznet
 */

?>

<article id="post-<?php the_ID(); ?>" class="wrapper" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( sprintf( '<a href="%s" rel="bookmark"><h3 class="entry-title">', esc_url( get_permalink() ) ), '</h3></a>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php biznet_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-summary">
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->

	<footer class="entry-footer">
		<?php biznet_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
