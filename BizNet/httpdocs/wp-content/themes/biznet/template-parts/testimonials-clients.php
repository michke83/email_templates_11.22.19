<section class="ta-center || testimonials || home-products || parallax" style="background-image: url('<?php the_field( 'homepage_case_studies_image', 'options' ); ?>');">

  <div class="testimonials-slider || slick-slider">
  <!-- QUERY CASE STUDIES CPT -->
  <?php
  global $post;

  $args = array( 'post_type' => 'testimonials', 'meta_key' => 'testimonial_type', 'meta_value'	=> 'user' );

  $loop = new WP_Query( $args );

  while ( $loop->have_posts() ) : $loop->the_post();

  ?>

    <div>
      <div class="testimonial-text">
        <img class="quotes" src="<?php echo THEME_DIRECTORY ?>/img/leftquotes.png" />
        <p style="display: inline;"><span class="text-quotes">"</span><?php the_field( 'testimonial', false, false ); ?><span class="text-quotes">"</span></p>
        <img class="quotes" src="<?php echo THEME_DIRECTORY ?>/img/rightquotes.png" />
        <br>
      </div>
      <div class="testimonial-meta">
        <img class="testimonial-icon" src="<?php the_field( 'testimonial_logo' ); ?>" />
        <div class="ta-left">
          <?php the_field( 'testimonial_person' ); ?>
          <br>
          <?php the_field( 'testimonial_person_title' ); ?>
        </div>
      </div>
    </div>
  <?php endwhile; // end of the loop. ?>
  </div>

  <!-- <a class="btn" href="<?php the_field( 'homepage_case_studies_button_url', 'options'); ?>"><?php the_field( 'homepage_case_studies_button_text', 'options' ); ?></a> -->
</section>
