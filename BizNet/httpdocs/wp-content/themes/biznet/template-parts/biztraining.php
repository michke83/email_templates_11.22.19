<div class="ta-center || wrapper-large">
  <h3>BizNet Training</h3>
  <p>
    A world-class system needs world-class training. Train Up on BizInsight.
    Get help with common problems, tips on Excel, and learn about best-in-class report building.
  </p>
  <a class="btn" href="/training">TRAINING</a>
</div>
