<!-- PRE FOOTER TEXT SECTION -->
<section class="home-prefooter">
  <div class="ta-center || wrapper-large">
  <?php
    $pre_footer_text_section_heading = get_field( 'pre_footer_text_section_heading', 'options' );
    $pre_footer_text_section_left = get_field( 'pre_footer_text_section_left', 'options' );
    $pre_footer_text_section_middle = get_field( 'pre_footer_text_section_middle', 'options' );
    $pre_footer_text_section_right = get_field( 'pre_footer_text_section_right', 'options' );

    echo '<h2>';
    echo $pre_footer_text_section_heading;
    echo '</h2>';

    echo '<div class="ta-left || grid || grid--medium">';
    echo '<div class="prefooter-grid">';
    echo $pre_footer_text_section_left;
    echo '</div>';
    echo '<div class="prefooter-grid">';
    echo $pre_footer_text_section_middle;
    echo '</div>';
    echo '<div class="prefooter-grid">';
    echo $pre_footer_text_section_right;
    echo '</div>';
    echo '</div>';
  ?>

  </div>
</section>
