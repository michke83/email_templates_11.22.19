<?php
/**
 * Template part for displaying SINGLE posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Biznet
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <header class="entry-header || wrapper">

  	<?php
  	if ( is_single() ) :
  		the_title( '<h3 class="entry-title">', '</h3>' );
  	else :
  		the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
  	endif;
  	 ?>
  </header><!-- .entry-header -->

	<div class="entry-content || wrapper">
		<?php if (has_post_thumbnail( $post->ID ) ): ?>
		  <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
      <img src="<?php echo $image[0]; ?>"  />
		<?php endif; ?>


    <?php
    if ( 'post' === get_post_type() ) : ?>
      <div class="entry-meta">
        <?php biznet_posted_on(); ?> | <?php biznet_entry_footer(); ?>
      </div><!-- .entry-meta -->
    <?php
    endif;
    ?>
    <?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'biznet' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'biznet' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->


</article><!-- #post-## -->
