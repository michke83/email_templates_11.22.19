<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Biznet
 */

get_header();

global $post;
$page_id = $post->ID;
$url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$end = end((explode('/', rtrim($url, '/'))));
?>
<section style="background-image: url('<?php echo THEME_DIRECTORY ?>/img/bizblog.jpg')">
	<div class="wrapper-large || ta-center">
		<?php
		if ( $end == 'bizblog' )
		{
			the_field( 'blog_page_intro_text', 'options' );
		}
		?>
	</div>
</section>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php
		get_sidebar();

		if ( have_posts() ) :

			if ( is_home() && ! is_front_page() ) : ?>
				<header class="sr-only">
					<h1 class="page-title sr-only"><?php single_post_title(); ?></h1>
				</header>

			<?php
			endif;
			?>
			<section class="grid || grid--medium || blog-posts">

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();



				/*
				 * Include the Post-Format-specific template for the content.
				 * If you want to override this in a child theme, then include a file
				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
				 */
				get_template_part( 'template-parts/content', get_post_format() );


			// Get Custom Page Footer Options While in the Loop

			endwhile;

			?>
			</section>

			<section class="page-number__container || grid">
			<?php
				global $wp_query;

				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'prev_next' => false,
					'total' => $wp_query->max_num_pages,
					'type' => 'list'
				) );
				?>
			</section>
			<?php

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>
		<!-- PRE FOOTER TEXT SECTION -->
		<section class="home-prefooter">
		  <div class="ta-center || wrapper-large">
		  <?php
		    $pre_footer_text_section_heading = get_field( 'pre_footer_text_section_heading', 'options' );
		    $pre_footer_text_section_left = get_field( 'pre_footer_text_section_left', 'options' );
		    $pre_footer_text_section_middle = get_field( 'pre_footer_text_section_middle', 'options' );
		    $pre_footer_text_section_right = get_field( 'pre_footer_text_section_right', 'options' );

		    echo '<h2>';
		    echo $pre_footer_text_section_heading;
		    echo '</h2>';

		    echo '<div class="ta-left || grid || grid--medium">';
		    echo '<div class="prefooter-grid">';
		    echo $pre_footer_text_section_left;
		    echo '</div>';
		    echo '<div class="prefooter-grid">';
		    echo $pre_footer_text_section_middle;
		    echo '</div>';
		    echo '<div class="prefooter-grid">';
		    echo $pre_footer_text_section_right;
		    echo '</div>';
		    echo '</div>';
		  ?>

		  </div>
		</section>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
