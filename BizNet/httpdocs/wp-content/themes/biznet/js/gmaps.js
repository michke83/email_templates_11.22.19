
  function initMap() {

    var zoom = parseInt(pw_script_vars.zoom);
    var glat = parseFloat(pw_script_vars.latitude);
    var glong = parseFloat(pw_script_vars.longitude);
    var customIcon = pw_script_vars.icon;

    var biznetCoords = { lat: glat, lng: glong };

    var map = new google.maps.Map(document.getElementById( 'googleMap' ), {
        zoom: zoom,
        center: biznetCoords,
        styles:
        [
          {
              "featureType": "landscape",
              "elementType": "geometry",
              "stylers": [
                  {
                      "color": "#d7ebf9"
                  }
              ]
          },
          {
              "featureType": "administrative",
              "elementType": "geometry",
              "stylers": [
                  {
                      "weight": 0.6
                  },
                  {
                      "color": "#1a3541"
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": "0"
                  },
                  {
                      "lightness": "0"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "simplified"
                  },
                  {
                      "saturation": "-60"
                  },
                  {
                      "lightness": "-20"
                  }
              ]
          }
      ]

      });
      var marker = new google.maps.Marker({
        position: biznetCoords,
				icon: customIcon,
        map: map
      });
    }
