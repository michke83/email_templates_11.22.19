/// Sticky header

jQuery(function() {
    jQuery(window).scroll(sticky_relocate);
    sticky_relocate();
});

function sticky_relocate() {
  var window_top = jQuery(window).scrollTop();

  if (window_top > div_top) {
      jQuery('.js-fixed-header').addClass('stick');
  } else if (window_top < div_top) {
      jQuery('.js-fixed-header').removeClass('stick');
  }
}

/// Mobile menu

var menuToggle = document.getElementById('js-menu-toggle'),
mobileMenu = document.getElementById('js-mobile-nav'),
hamburger = document.getElementById('js-hamburger');

jQuery(menuToggle).on('click', function(){
  jQuery(mobileMenu).toggleClass("sr-only");
  jQuery(mobileMenu).toggleClass("is-open");
  jQuery(hamburger).toggleClass("hamburger-top");
});

// Compatible systems slider - homepage

jQuery(document).on('ready', function(){

  var slickHeight = jQuery('.slick-track').height();
  jQuery('.slick-slide').css('height', slickHeight + 'px' );

  jQuery('.compatible-slider').slick({
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    dots: false,
    infinite: true,
    nextArrow: '<div class="chevron-container || slick-right"><img class="chevron-right" src="https://biznetsoftware.com/wp-content/themes/biznet/img/arrowright.png" /><span class="sr-only">Next</span></div>',
    prevArrow: '<div class="chevron-container"><img src="https://biznetsoftware.com/wp-content/themes/biznet/img/arrowleft.png" /><span class="sr-only">Previous</span></div>',
    slidesToShow: 6,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }]
  });

  /// Testimonials slider - full site

  jQuery('.testimonials-slider').slick({
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: false,
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
  });

  /// Team slider

  jQuery('.team-slider').slick({
    autoplay: true,
    autoplaySpeed: 8000,
    arrows: false,
    dots: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 1
        }
      }]
  });
});

/// Video modal - homepage

var myModal = Frdialogmodal(),
div_top = jQuery('.js-fixed-header').offset().top;

/// Waypoint Animations

jQuery('.product-img-right').each(function() {
  jQuery(this).waypoint(function() {
    jQuery(this).addClass('slideInRight');
  }, { offset: '110%'});
});

jQuery('.product-img-left').each(function() {
  jQuery(this).waypoint(function() {
    jQuery(this).addClass('slideInLeft');
  }, { offset: '110%'});
});

// Frend accordion - About page

var myAccordion = Fraccordion({
  // Boolean - If set to false, each accordion instance will only allow a single panel to be open at a time
	multiselectable: true,
  firstPanelsOpenByDefault: false,
});
