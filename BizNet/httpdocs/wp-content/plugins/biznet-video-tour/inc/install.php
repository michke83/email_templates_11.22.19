<?php
/**
 *  Install Function
 *
 *  Runs on plugin install
 *
 *  @package bvt
 **/

if ( ! function_exists( 'bvt_install' ) ) {
	/**
	 *  Installs plugin and creates db version option
	 */
	function bvt_install() {
		global $wpdb;
		global $bvt_db_version;

		add_option( 'bvt_db_version', $bvt_db_version );
		// clear permalinks !
		flush_rewrite_rules();
	}
}

register_activation_hook( BVT_PLUGIN_FILE, 'bvt_install' );
