<?php
/**
 * Template Name: Demo Tour
 */
get_header();
?>
<style>
h3 {
  font-size: 1.8rem;
  margin: 0;
}
.checkbox__label {
  color: #797777;
  font-size: inherit;
  font-weight: normal;
}
.checkbox {
  text-align: left;
}
.checkbox::before {
  border-radius: 3px;
  border: 1px solid #ccc;
  -webkit-box-shadow: inset 0px 1px 0px 0px rgba(0,0,0,0.3);
  -moz-box-shadow: inset 0px 1px 0px 0px rgba(0,0,0,0.3);
  box-shadow: inset 0px 1px 0px 0px rgba(0,0,0,0.3);
}
select {
  border: 1px solid #ccc;
  font-size: 1.5rem;
}
.arrow-div {
  position: relative;
}
.arrow-div:after {
  content: '>';
  font: 17px "Consolas", monospace;
  color: #333;
  -webkit-transform: rotate(90deg);
  -moz-transform: rotate(90deg);
  -ms-transform: rotate(90deg);
  transform: rotate(90deg);
  right: 15px;
  /*Adjust for position however you want*/

  top: 73%;
  padding: 0 0 2px;
  /*left line */

  position: absolute;
  pointer-events: none;
}
.checkbox input[type=checkbox]:checked+.checkbox__label::after {
  background-color: #1e437f;
  color: #fff;
  font-size: 12px;
  font-family: 'Source Sans Pro', sans-serif;
}
.video {
  margin: 0 auto;
}

@media screen and (min-width: 800px) {
  h3 {
    text-align: center;
  }
  .arrow-div:after {
    right: 26.5%;
  }
  select {
    margin: 0 auto;
    width: 50%;
  }
  #video-topics {
    margin: 0 auto;
    width: 50%;
  }
}
#demo-header {
  width: 100%;
  background-color: #3360ae;
  color: #fff;
  padding: 100px 0 100px 0;
}
#tour-form {
  padding: 10px 20px 10px 20px !important;
}
</style>
<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">
    <div id="demo-header">
      <h2 class="ta-center">Biznet Demo Tour</h2>
    </div>

    <section class="wrapper-large">
      <div class="grid || grid--vertical">
        <!-- text content from page editor -->
        <div class="grid-center">
          <?php
          while ( have_posts() ) : the_post();
            the_content();
          endwhile; // End of the loop.
          ?>
        </div>
        <form id="tour-form" name="tour-form" method="post" action="">
          <?php if ( have_rows( 'question_answer_video' ) ) : ?>
            <?php
            while ( have_rows( 'question_answer_video' ) ) :
      	       the_row();
            ?>
            <?php
      	     $qvalue = get_sub_field_object( 'question' );
      	     $qname = $qvalue['name'];
             ?>
             <div class="arrow-div" id="div-<?php echo esc_attr( $qname ); ?>">
    	       <h3><?php the_sub_field( 'question' ); ?></h3>
             <?php if ( have_rows( 'answer' ) ) : ?>
    		         <select class="form-select" id="<?php echo esc_attr( $qname ); ?>" name="<?php echo esc_attr( $qname ); ?>">
      		         <option value="">please choose an answer...</option>
      		         <?php
      		           while ( have_rows( 'answer' ) ) :
      			         the_row();
      		         ?>
      		         <option value="<?php the_sub_field( 'answer_text' ); ?>">
      		             <?php the_sub_field( 'answer_text' ); ?>
      		         </option>
      		         <?php endwhile; ?>
    		         </select>
                 <span class="dropdown-arrow"></span>
    	       <?php endif; ?>
    	      </div>
    	      <br/>
            <?php endwhile; ?>
          <?php endif; ?>
          <div id="video-topics">
  	       <h3>I would like more information about:</h3>
  	        <?php if ( have_rows( 'video_topics' ) ) : ?>
          	<?php
          	while ( have_rows( 'video_topics' ) ) :
          		the_row();
          		$vqvalue = get_sub_field_object( 'video_topic' );
          		$vqname = $vqvalue['name'];
          	?>
            <label class="checkbox">
  	         <input type="checkbox" id="<?php echo esc_attr( $vqname ); ?>" name="videotopics[]" value="<?php echo esc_attr( $vqname ); ?>" />
             <span class="checkbox__label"><?php the_sub_field( 'video_topic' ); ?></span>
            </label>
  	         <br/>
  	        <?php endwhile; ?>
  	       <?php endif; ?>
            <input type="submit" id="sgtSubmit" name="sgtSubmit" value="Submit" />
          </div>
        </form>

        <div class="grid || grid--medium || grid--wrap">
           <?php
           if ( 'POST' === $_SERVER['REQUEST_METHOD'] ) {
             ?>
            <style>
            #tour-form {
              display: none;
            }
            .grid--wrap {
              justify-content: space-between;
            }
            .video {
              flex: initial;
            }
            .video h3 {
              text-align: left;
            }
            </style>
            <?php
            if ( isset( $_POST['question_answer_video_0_question'] ) ) {
              $question_answer_video_0_question = sanitize_text_field( wp_unslash( $_POST['question_answer_video_0_question'] ) );
            }
            if ( isset( $_POST['question_answer_video_1_question'] ) ) {
              $question_answer_video_1_question = sanitize_text_field( wp_unslash( $_POST['question_answer_video_1_question'] ) );
            }
            // create new array to hold the video topics checkboxes from the form input.
            $video_topics = [];
            if ( isset( $_POST['videotopics'] ) && is_array( $_POST['videotopics'] ) ) {
              $video_topics = wp_unslash( $_POST['videotopics'] );
            }
            if ( have_rows( 'question_answer_video' ) ) :
              while ( have_rows( 'question_answer_video' ) ) :
                  the_row();
                if ( have_rows( 'answer' ) ) :
                  while ( have_rows( 'answer' ) ) :
                    the_row();
                    $answers = array();
                    $answers = get_sub_field_object( 'answer_text' );
                    $answers_name = $answers['name'];
                    $answers_name = explode( ' ', $answers_name );
                    $answer_value = $answers['value'];
                    $answer_value = explode( ' ', $answer_value );

                    $video_urls = array();
                    $video_urls = get_sub_field_object( 'video_for_answer' );
                    $video_urls_name = $video_urls['name'];
                    $video_urls_name = explode( ' ', $video_urls_name );
                    $video_urls_value = $video_urls['value'];
                    $video_urls_value = explode( ' ', $video_urls_value );

                    $final_answers = array();
                    $final_answers  = array_combine( $answers_name, $answer_value );

                    $final_video = array();
                    $final_video  = array_combine( $video_urls_name, $video_urls_value );

                    $final_final = array();
                    $final_final  = array_combine( $answer_value, $video_urls_value );
                    ?>

                    <?php
                    foreach ( $final_final as $key => $value ) {
                      if ( $question_answer_video_0_question === $key ) {
                        // get the video title
                        $video_id = end(explode('/', $value));
                        $urlvideotitle = json_decode(file_get_contents("http://vimeo.com/api/v2/video/" . $video_id . ".json"));
                        foreach ($urlvideotitle as $uvtitle) {
                          $vimeo_title =  $uvtitle->title;
                        }

                        ?>
                        <div class="video">
                        <h3><?php echo $vimeo_title; ?></h3>
                        <iframe src="<?php echo esc_url( $value ); ?>" width="450" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>

                        <?php
                      }
                      if ( $question_answer_video_1_question === $key ) {
                        // get the video title
                        $video_id = end(explode('/', $value));
                        $urlvideotitle = json_decode(file_get_contents("http://vimeo.com/api/v2/video/" . $video_id . ".json"));
                        foreach ($urlvideotitle as $uvtitle) {
                          $vimeo_title =  $uvtitle->title;
                        }
                        ?>
                        <div class="video">
                          <h3><?php echo $vimeo_title; ?></h3>
                        <iframe src="<?php echo esc_url( $value ); ?>" width="450" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>

                        <?php
                      }
                    }
                  endwhile;
                  endif;
                endwhile;
            endif;

            if ( have_rows( 'video_topics' ) ) :
              while ( have_rows( 'video_topics' ) ) :
                the_row();

                $vid_top = array();
                $vid_top = get_sub_field_object( 'video_topic' );
                $vt_names = $vid_top['name'];
                $vt_names = explode( ' ', $vt_names );

                $vid_url = array();
                $vid_url = get_sub_field_object( 'video_url' );
                $vus_urls = $vid_url['value'];
                $vus_urls = explode( ' ', $vus_urls );

                $final_video_topics = array();
                $final_video_topics  = array_combine( $vt_names, $vus_urls );

                foreach ( $video_topics as $the_chosen ) {
                  foreach ( $final_video_topics as $key => $value ) {
                    if ( $the_chosen == $key ) {
                      // get the video title
                      $video_id = end(explode('/', $value));
                      $urlvideotitle = json_decode(file_get_contents("http://vimeo.com/api/v2/video/" . $video_id . ".json"));
                      foreach ($urlvideotitle as $uvtitle) {
                        $vimeo_title =  $uvtitle->title;
                      }
                        ?>
                          <div class="video">
                            <h3><?php echo $vimeo_title; ?></h3>
                          <iframe src="<?php echo esc_url( $value ); ?>" width="450" height="250" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                          </div>

                      <?php
                    }
                  }
                }
                endwhile;
            endif;
          }
          ?>
        </div>
      </div>
    </section>
  </main><!-- #main -->
</div><!-- #primary -->
<?php
get_footer();
?>
