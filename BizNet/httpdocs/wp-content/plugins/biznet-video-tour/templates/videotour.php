<?php
/**
 * Template Name: Video Tour
 *
 * Custom template for Video Tour Page
 *
 * This is the custom template that displays the Biznet
 * Self Guided Video Tour page. This plugin needs ACF plugin
 * that attaches the custom fields to this template.
 *
 *  PHP version 7.0
 *
 *  @category   Plugin
 *  @package    WordPress
 *  @subpackage Bvt
 *  @author     Darren Ladner <darren.ladner@ivieinc.com>
 *  @license    https://codex.wordpress.org WordPress
 *  @version    GIT:1.0.0
 *  @link       https://codex.wordpress.org
 */

  get_header();
?>
<?php
if ( isset( $_SERVER['REQUEST_METHOD'] ) && check_admin_referer( 'biznet_video_tour', 'biznet_video_tour_nonce' ) ) {
	if ( 'POST' === $_SERVER['REQUEST_METHOD'] ) { // start Post.
		if ( isset( $_POST['question_answer_video_0_question'] ) ) {
			$question_answer_video_0_question = sanitize_text_field( wp_unslash( $_POST['question_answer_video_0_question'] ) );
		}
		if ( isset( $_POST['question_answer_video_1_question'] ) ) {
			$question_answer_video_1_question = sanitize_text_field( wp_unslash( $_POST['question_answer_video_1_question'] ) );
		}
		// create new array to hold the video topics checkboxes from the form input.
		$video_topics = [];
		if ( isset( $_POST['videotopics'] ) && is_array( $_POST['videotopics'] ) ) {
			$video_topics = wp_unslash( $_POST['videotopics'] );
		}
		if ( have_rows( 'question_answer_video' ) ) :
			while ( have_rows( 'question_answer_video' ) ) :
				  the_row();
				if ( have_rows( 'answer' ) ) :
					while ( have_rows( 'answer' ) ) :
						the_row();
						$answers = array();
						$answers = get_sub_field_object( 'answer_text' );
						$answers_name = $answers['name'];
						$answers_name = explode( ' ', $answers_name );
						$answer_value = $answers['value'];
						$answer_value = explode( ' ', $answer_value );

						$video_urls = array();
						$video_urls = get_sub_field_object( 'video_for_answer' );
						$video_urls_name = $video_urls['name'];
						$video_urls_name = explode( ' ', $video_urls_name );
						$video_urls_value = $video_urls['value'];
						$video_urls_value = explode( ' ', $video_urls_value );

						$final_answers = array();
						$final_answers  = array_combine( $answers_name, $answer_value );

						$final_video = array();
						$final_video  = array_combine( $video_urls_name, $video_urls_value );

						$final_final = array();
						$final_final  = array_combine( $answer_value, $video_urls_value );

						foreach ( $final_final as $key => $value ) {
							if ( $question_answer_video_0_question === $key ) {
								echo 'VIDEO URL: ' . esc_url( $value );
							}
							if ( $question_answer_video_1_question === $key ) {
								echo 'VIDEO URL: ' . esc_url( $value );
							}
						}
						echo '<br/>';
					endwhile;
					endif;
			  endwhile;
		endif;

		if ( have_rows( 'video_topics' ) ) :
			while ( have_rows( 'video_topics' ) ) :
				the_row();

				$vid_top = array();
				$vid_top = get_sub_field_object( 'video_topic' );
				$vt_names = $vid_top['name'];
				$vt_names = explode( ' ', $vt_names );

				$vid_url = array();
				$vid_url = get_sub_field_object( 'video_url' );
				$vus_urls = $vid_url['value'];
				$vus_urls = explode( ' ', $vus_urls );

				$final_video_topics = array();
				$final_video_topics  = array_combine( $vt_names, $vus_urls );

				foreach ( $video_topics as $the_chosen ) {
					foreach ( $final_video_topics as $key => $value ) {
						if ( $the_chosen == $key ) {
							echo 'VIDEO URL: ' . esc_url( $value ) . '<br/>';
						}
					}
				}
			  endwhile;
		endif;
	}
}
?>
<form id="tour-form" name="tour-form" method="post" action="">
<?php if ( have_rows( 'question_answer_video' ) ) : ?>

<?php
while ( have_rows( 'question_answer_video' ) ) :
	the_row();
?>

<?php
	wp_nonce_field( 'biznet_video_tour', 'biznet_video_tour_nonce' );
	$qvalue = get_sub_field_object( 'question' );
	$qname = $qvalue['name'];
?>
	<div id="div-<?php echo esc_attr( $qname ); ?>">
	<?php
		the_sub_field( 'question' );
	?>
	<?php if ( have_rows( 'answer' ) ) : ?>
		<select id="<?php echo esc_attr( $qname ); ?>" name="<?php echo esc_attr( $qname ); ?>">
		<option value="">please choose an anwser...</option>
		<?php
		while ( have_rows( 'answer' ) ) :
			the_row();
		?>

		<option value="<?php the_sub_field( 'answer_text' ); ?>">
		<?php the_sub_field( 'answer_text' ); ?>
		</option>

		<?php endwhile; ?>

		</select>

	<?php endif; ?>
	</div>
	<br/>
<?php endwhile; ?>

<?php endif; ?>
	<div id="video-topics">
	I would like more information about:<br/>
	<?php if ( have_rows( 'video_topics' ) ) : ?>
	<?php
	while ( have_rows( 'video_topics' ) ) :
		the_row();
		$vqvalue = get_sub_field_object( 'video_topic' );
		$vqname = $vqvalue['name'];
	?>
	<input type="checkbox" id="<?php echo esc_attr( $vqname ); ?>" name="videotopics[]" value="<?php echo esc_attr( $vqname ); ?>" />
	<?php
		the_sub_field( 'video_topic' );
	?>
	<br/>
	<?php endwhile; ?>
	<?php endif; ?>
	<input type="submit" id="sgtSubmit" name="sgtSubmit" value="Submit" />
	</div>
</form>


?>

<?php
get_footer();
?>
