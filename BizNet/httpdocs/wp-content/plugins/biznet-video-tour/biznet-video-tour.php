<?php
/**
 * Plugin Name: Biznet Video Tour
 * Plugin URI: http://ivieinc.com
 * Description: Biznet Self Guided Video Tour Custom Plugin
 * Author: Darren Ladner
 * Author URI: http://buzzshift.com
 * Textdomain: bvt
 * Version: 1.0.0
 *
 *  @package bvt
 **/

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
|--------------------------------------------------------------------------
| CONSTANTS
|--------------------------------------------------------------------------
*/

// plugin folder url.
if ( ! defined( 'BVT_PLUGIN_URL' ) ) {
	define( 'BVT_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
}
// plugin folder path.
if ( ! defined( 'BVT_PLUGIN_DIR' ) ) {
	define( 'BVT_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}
// plugin root file.
if ( ! defined( 'BVT_PLUGIN_FILE' ) ) {
	define( 'BVT_PLUGIN_FILE', __FILE__ );
}

/*
|--------------------------------------------------------------------------
| INCLUDES
|--------------------------------------------------------------------------
*/

require_once BVT_PLUGIN_DIR . 'inc/install.php';
require_once BVT_PLUGIN_DIR . 'inc/class-bvtpagetemplater.php';

// action hook to load and create the BVT Page Templater Class.
add_action( 'plugins_loaded', array( 'BVTPageTemplater', 'get_instance' ) );

/**
 * Enqueue scripts and styles.
 */
function sgt_scripts() {
	wp_enqueue_script( 'customjs', BVT_PLUGIN_URL . '/js/custom.js', array( 'jquery' ), true );
}
add_action( 'wp_enqueue_scripts', 'sgt_scripts' );
