<?php
/**
 * Uninstall File
 *
 * Runs on plugin uninstall.
 *
 *  @package bvt
 **/

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	die;
}
