�ªZ<?php exit; ?>a:1:{s:7:"content";O:8:"stdClass":24:{s:2:"ID";i:13974;s:11:"post_author";s:2:"10";s:9:"post_date";s:19:"2017-06-22 18:49:05";s:13:"post_date_gmt";s:19:"2017-06-22 18:49:05";s:12:"post_content";s:3962:"<h1 style="text-align: center;">FREQUENTLY ASKED QUESTIONS</h1>
<h3 style="text-align: center;">Find answers to our most common questions below.</h3>
<strong>Q:</strong> Can I purchase BizInsight 7 without a connector?
<strong>A:</strong> Yes. BizInsight 7 is a stand-alone product, however we do additionally offer a number of pre-built connectors for specific ERPs we have relationships with. You can <a href="https://www.biznetsoftware.com/compatible-systems/">see if we have your ERP here</a>.

<strong>Q:</strong> Can I connect my QuickBooks data to BizInsight 7?
<strong>A:</strong> Yes, depending on which version you have, and not without a connector and extra steps to get your data into Excel. For more information, please contact your sales representative to discuss your options.

<strong>Q:</strong> Can I purchase BizBroadcast as a standalone product?
<strong>A:</strong> Yes.

<strong>Q:</strong> How many users are allowed per license?
<strong>A:</strong> One.

<strong>Q:</strong> Will I receive maintenance and support?
<strong>A:</strong> Yes, support and maintenance is included.

<strong>Q:</strong> Do I have to be connected to an ERP to use your products?
<strong>A:</strong> No, you can purchase BizInsight 7 on its own, however reports that you generate will not be through a live data connection, meaning the data within the report will only be as new as when the report was run. We can show you how to refresh your data in Excel.

<strong>Q:</strong> Can I buy products online?
<strong>A:</strong> You can buy some of our products online. Some products require you to speak with a sales representative. A “Buy Now” button indicates you can purchase the product via the website.

<strong>Q:</strong> Will my license automatically renew?
<strong>A:</strong> Yes.

<strong>Q:</strong> Who benefits from using your products?
<strong>A:</strong> Any person in any industry that build or analyzes reports using Microsoft Excel.

<strong>Q:</strong> Where can I go online for technical questions?
<strong>A:</strong> Current customers who have a current support subscription can <a href="http://biznetsupport.net/csammons/">open a support ticket</a> here. Those who are not yet customers but have a technical question can <a href="https://www.biznetsoftware.com/contact/">contact us here</a>.

<strong>Q:</strong> What kind of training is provided with a purchase?
<strong>A:</strong> The Getting Started Workbook is provided with all purchases, but <a href="https://www.biznetsoftware.com/training/">additional training is available</a> at a cost.

<strong>Q:</strong> How long do your products take to install?
<strong>A:</strong> Our Version 5 products typically take half of a day for implementation, whereas our Version 7 products take less than five minutes in a click-through installation.

<strong>Q:</strong> What is required to cancel a subscription?
<strong>A:</strong> Please contact your Sales Representative.

<strong>Q:</strong> Can I get a demo before I commit to this?
<strong>A:</strong> Absolutely. Simply <a href="https://www.biznetsoftware.com/schedule-a-demo/">fill out this form</a> and a member of our sales team will contact you to schedule a demonstration.

<strong>Q:</strong> Are the licenses transferrable?
<strong>A:</strong> Yes.

<strong>Q:</strong> If I create a report with BizInsight can I send it to someone who doesn’t have BizInsight?
<strong>A:</strong> Yes, if the report is saved as a PDF or Excel values only. However, if the person receiving the reports wants to be able to drill-down into the data, they will need a license to do so. The good news is there are now multiple ways to do this in BizInsight 7, and we’d be happy to discuss your options.

<strong>Still have answers? We're ready to help. Call, email, or fill out our <a href="https://www.biznetsoftware.com/contact/">Contact Us</a> form to speak to the professionals. </strong>";s:10:"post_title";s:3:"FAQ";s:12:"post_excerpt";s:0:"";s:11:"post_status";s:7:"publish";s:14:"comment_status";s:6:"closed";s:11:"ping_status";s:6:"closed";s:13:"post_password";s:0:"";s:9:"post_name";s:3:"faq";s:7:"to_ping";s:0:"";s:6:"pinged";s:0:"";s:13:"post_modified";s:19:"2018-03-09 15:57:36";s:17:"post_modified_gmt";s:19:"2018-03-09 15:57:36";s:21:"post_content_filtered";s:0:"";s:11:"post_parent";i:0;s:4:"guid";s:45:"https://www.biznetsoftware.com/?page_id=13974";s:10:"menu_order";i:0;s:9:"post_type";s:4:"page";s:14:"post_mime_type";s:0:"";s:13:"comment_count";s:1:"0";s:6:"filter";s:3:"raw";}}