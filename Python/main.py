# import sklearn
import sys
# print ("Hello World!")


# string2 = {}
# print(type(string2))


# Python collection (Arrays)
# There are 4 types of collections in python:
# 1. list
# 2. tuple
# 3. set
# 4. dictionary

# List:
l1 = [2, 343, 5, 34]
# print("My first list is here:", l1)
# l1.append(342)
# print(l1)
# l1.pop()
# print(l1)
# l1.pop(2)
# print(l1)
# l1.remove(343)
# print(l1)
# l1.clear()
# print(l1)

# Tuples
# t1 = (1,2,2,3)
# print("my tuple is ", t1)
# print("the value of at index 1 is ", t1[1])
#t1[1] = 87 #this does not work
# print("the value of at index 1 is ", t1[1])


# Sets:

# myset = {1,2,343,53,1,2,1,21,1,2}
# print(myset)
# will only print each number once and in order. Will not print duplicates

# Dictionary in python

# myDictionary = {
#     "harry": "good boy",
#     "mac": "bad boy",
#     "sam": "boy",
#     "mark": 324
# }
# print(myDictionary["harry"])
# print(myDictionary.get("mark"))
# myDictionary["mark"] = 34
# print(myDictionary["mark"])


# If Else Conditionals In Python:

# a = 34
# b = 3
# c = 12

# d = input("enter a value \n")
# print(type(d))
# d=int(d)
# print(type(d))
# print("the value you entered is",d)
# change types with str(), float(), int()

# if(d>a):
#     print("d is greater than a")
# elif(d==a):
#     print("d is equal to a")
# else:
#     print("d is not greater than a")


# Loops in Python:

# while loops
# i = 0 

# while(i<10):
#     print("Hello")
#     i = i+1
# print("Your loop has ended")
    
# for loops:

# for i in range(0,12):
    # print(i)

fruits = ["grapes", "water melons", "oranges", "apples", 34]
# for item in fruits:
#     # if item == "oranges":
#     #     print("oranges found")
#     #     break
#     if item == "oranges":
#         print("oranges found")
#         continue
#     print(item)

# def printList(a):
#     for item in fruits:
#         if item == "oranges":
#             print("oranges found")
#             continue
#         print("this item is",item)



# printList(fruits)

# name = "MK"

# print(id(name))



# i = range(1,10000)
#     # print(i)
# print(sys.getsizeof(i))



a = (51117//21)

print(a)



