// ========================================================================================
// Multiples of 3 and 5
// Problem 1 
// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

// Find the sum of all the multiples of 3 or 5 below 1000.

// function SumBelow(){
//   var sum = 0
//   for(let i = 3;i<1000;i++){
//     if(i % 3 == 0){
//       sum = sum + i
//       continue
//     }
//     if(i % 5 == 0){
//       sum = sum + i
//     }
//   }
//   console.log(sum)
// }
// SumBelow()


// Answer:
// 233168
// Completed on Fri, 9 Nov 2018, 17:24

// ========================================================================================

// Even Fibonacci numbers
// Problem 2 
// Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:

// 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

// By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.

// function EvenFib(){
//   var previous = 0 
//   var current = 1 
//   var sum = 0
//   var next
//   // let i = 10
//   while(sum <= 4000000){
//     next = previous + current
//     previous = current
//     current = next
//     if(current <= 4000000){
//       if(current % 2 == 0){
//         sum = sum + current

//       console.log(sum)
//       // i--
//       }
//     }
//   }
// }
// EvenFib()

// Answer:
// 4613732
// Completed on Fri, 9 Nov 2018, 18:14

// ========================================================================================

// Largest prime factor
// Problem 3 
// The prime factors of 13195 are 5, 7, 13 and 29.

// What is the largest prime factor of the number 600851475143 ?

// function PrimeFactor(a){
  // var factorArray = []
  // var largestFact = 0
  // var newA = Math.sqrt(a)
  // for(i = 2; i <= newA; i++){    
  //   if(a % i == 0){
      // factorArray.push(i)
      // var isPrime = true
      // for(j = 2; j < i; j++){
      //   if(i % j == 0){
      //     isPrime = false
      //     break
      //   }
      // }
      // if (isPrime){
      //   largestFact = i
        // console.log(largestFact)
  //     }
  //   }      
  // }
  // console.log(factorArray)
//   console.log(largestFact)
// }

// PrimeFactor(600851475143)

// Answer:
// 6857
// Completed on Mon, 12 Nov 2018, 20:48


// ========================================================================================

// Largest palindrome product
// Problem 4 
// A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

// Find the largest palindrome made from the product of two 3-digit numbers.


// function Palindrome(){
//   var c = 0
//   var d = 0
//   var palins = []
//     for(a = 999; a >99; a--){
//       for(i = 999; i > 99; i--){
//         c = a*i    
//         // console.log(c)
//         c = c + ""
//         d = c.split("").reverse().join("")
//         // console.log(d)

//         if(c == d){
//           palins.push(d)
//           // console.log(d)
//           // console.log(a)
//           // console.log(i)
//         }
//       }
//     }
//   return Math.max(...palins)
//     // return false
// }
// Palindrome()


// Answer:
// 906609
// Completed on Tue, 13 Nov 2018, 14:54


// ========================================================================================

Smallest multiple
Problem 5 
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

function SmallestDivisible(){
  for(i = 20; i >= 20; i++){
    if (i % 1 == 0){
      if(i % 2 == 0){
        if(i % 3 == 0){
          if(i % 4 == 0){
            if(i % 5 == 0){
              if(i % 6 == 0){
                if(i % 7 == 0){
                  if(i % 8 == 0){
                    if(i % 9 == 0){
                      if(i % 10 == 0){
                        if(i % 11 == 0){
                          if(i % 12 == 0){
                            if(i % 13 == 0){
                              if(i % 14 == 0){
                                if(i % 15 == 0){
                                  if(i % 16 == 0){
                                    if(i % 17 == 0){
                                      if(i % 18 == 0){
                                        if(i % 19 == 0){
                                          if(i % 20 == 0){
                                            return i
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      
    }
    
  }
}

SmallestDivisible()


function SmallestDivisible(){
  var found = true
  for(i = 20; i <= 300000000; i++){
    for(j = 1; found && j <= 10; j++){
      found = found && (i % j == 0)
    }
      if(found){
        return i
      }
  }
}

SmallestDivisible()

























