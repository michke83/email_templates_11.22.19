'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var minify = require('gulp-minify');
var concat = require('gulp-concat');
var uglifycss = require('gulp-uglifycss');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
  return gulp.src('./sass/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

gulp.task('styles', function () {
  gulp.src([
  		'./css/bootstrap.min.css',
  		'./css/jquery-ui.css',
  		'./js/slick/slick.css',
                './js/slick/slick-theme.css',
                './css/font-awesome.min.css',
                './css/animate.min.css',
                './css/main.css',
  	])
  	.pipe(concat('main.min.css'))
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest('./css/'));
}); 

gulp.task('styles:watch', function () {
  gulp.watch('./css/main.css', ['styles']);
});

gulp.task('scripts', function() {
  gulp.src([
                './js/slick.min.js',
  		'./js/bootstrap.min.js',
                './js/jquery.wow.min.js',
                './js/script.js'
                
     	])
    .pipe(concat('script.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js/'))
});
