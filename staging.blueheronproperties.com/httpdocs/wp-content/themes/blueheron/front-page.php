<?php
/* template name: Front Page */
get_header();
?>
<div id="main">
    <div id="main-container">
        <div id="page-front" class="header-footer-gap">
            <?php get_template_part('template-parts/template', 'full-width'); ?>
        </div>
    </div>
</div>


<?php get_footer(); ?>