
jQuery(document).ready(function () {
    _body = jQuery('body');
    wow = new WOW();
    _window = jQuery(window);
    _bodyhtml = jQuery('body, html');
    _responsivemenu = jQuery('#responsive-main-menu');
    _menuitemchildren = jQuery('.menu-item-has-children');
    _selectmenutime = jQuery('select[name="menu-time"]');
    _addselection = jQuery('.addselecticon');

    /*add elements*/
    _body.append('<a href="#?" id="toTop" style="display:none;"><i class="fa fa-angle-up"></i></a>');
    _menuitemchildren.append('<span class="ddmenuitem"><i class="fa fa-angle-down"></i></span>')
    _selectmenutime.before('<a class="ddmenuitem ddselect"><i class="fa fa-angle-down"></i></a>');
    _addselection.before('<a class="ddmenuitem ddselect"><i class="fa fa-angle-down"></i></a>');

    hash = window.location.hash;
    //alert(hash);


    /* Fix scroll menu */
    _window.scroll(function () {
        _this = jQuery(this);
        _ctabar = jQuery("#header");
        _totop = jQuery("#toTop");
        if (_this.scrollTop() > 300) {
            _totop.fadeIn(300);
        } else {
            _totop.fadeOut(300);
        }


    });
    // nav bar add class and remove class script//
    jQuery(window).scroll(function () {

        if (jQuery(window).width() > 767) {

            if (jQuery(".page-template-front-page").length > 0) {
                if (jQuery(this).scrollTop() > 100) {
                    jQuery('.bcb_navbar').addClass("scroled");
                    jQuery('.logo-img').addClass("scrolltop-img");
                } else {
                    jQuery('.bcb_navbar').removeClass("scroled");
                    jQuery('.logo-img').removeClass("scrolltop-img");
                }
            }


        }

        if (jQuery(window).width() > 1024) {

            // banner text scrolltop//
            var scroll_diff = 200;
            if (jQuery(window).width() < 992) {
                scroll_diff = 250;
            }
            var scrollPos = jQuery(window).scrollTop();
            var banner_height = jQuery('.bcb_header-sec-banner').height() - scroll_diff;

            jQuery('.bcb_header-content').css({
                transform: 'translateY(' + (scrollPos / 2) + 'px)',
                opacity: 1 - (scrollPos / banner_height)
            });
        }

    });
    jQuery('window').on('touchmove', function (event) {
        //Prevent the window from being scrolled.
        event.preventDefault();

        //Do something like call window.scrollTo to mimic the scrolling
        //request the user made.
    });
    /* Fix scroll menu */

    /* Menu hover function */
    jQuery('#main-menu ul li.menu-item-has-children').hover(function () {
        jQuery(this).find('b').removeClass('caret');
        jQuery(this).find('b').addClass('caret-up');
    }, function () {
        jQuery(this).find('b').removeClass('caret-up');
        jQuery(this).find('b').addClass('caret');

    });
    /* Menu hover function */


    setHeight();

    _body.delegate('#responsive-main-menu', 'click', function (e) {
        _this = jQuery(this);
        _this.toggleClass('cross');
        _this.next().slideToggle().toggleClass('mobile-menus-list');
    });

    jQuery('#main-menu ul li.menu-item-has-children > span').click(function () {
        jQuery(this).parent().siblings().find('ul').slideUp();
        jQuery(this).prev('ul').stop(true, false, true).slideToggle();
        return false;
    });

    /*** mobile logo click event ***/
    jQuery(".center-logo").click(function () {
        if (jQuery(window).width() < 767) {
            location.href = script_vars.home_url;
        }
    })
    /*** mobile logo click event ***/

    /*** Pages loader ***/
    ////
    jQuery(window).load(function () {
        jQuery('.inner-loader').fadeOut("slow");
    })
    /*** Pages loader ***/


    /*Back to top*/
    jQuery("#toTop").click(function (e) {
        scrollposition = 0;
        _bodyhtml.animate({scrollTop: scrollposition}, 'slow');
        e.preventDefault();
    });
    /*Back to top*/

    /* animate */
    wow.init();
    /* animate */





});



function setHeight() {
    var window_height = jQuery(window).height();
    var site_footer = jQuery(".site-footer ").height();
    var site_header = jQuery(".nav-primary ").height();
    var temHeight = window_height - (site_header + site_footer);
    jQuery('.header-footer-gap').css('min-height', temHeight + 'px');
    // jQuery('.bcb_homebanner').css('min-height', window_height + 'px');
    //jQuery('#bcb_banner_img').css('min-height', window_height + 'vh');
    console.log(window_height)
}



jQuery(document).resize(function () {
    setHeight();

});


jQuery(document).ready(function () {

    setTimeout(function () {
        jQuery('.scroll_bottom_arrow').show();
    }, 3000);
    /*** Brans slick slider ***/
    jQuery('.bh-slick-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: false,
        dots: false,
        autoplaySpeed: 1000,
        arrows: false,
        responsive: [
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                }
            }

        ]
    });
    /*** Brans slick slider ***/

    jQuery('#toggle').click(function () {
        jQuery(this).toggleClass('active');
        jQuery('#overlay').toggleClass('open');
    });

});


/****************New Js ******/
jQuery(".arrow_button").click(function () {
    scroll_to_section("#about-us");
});

jQuery(".show_more").click(function () {
    jQuery(this).css("display", "none");
});
jQuery(document).ready(function () {
//jQuery(".button_container.active").click(function () {
    setTimeout(function () {

    }, 3000);
//})

    jQuery(".button_container").click(function () {
        if (jQuery(this).hasClass("active")) {
            jQuery('html,body').css({'overflow': 'hidden', 'height': '100%'});
        } else {
            jQuery(' html,body').css({'overflow': 'auto', 'height': 'auto'});
        }
    });
});

jQuery(document).ready(function () {
function swap_images() {
   var img_left = jQuery('.current_slide img').attr('src');
   var heading_left = jQuery('.current_slide .sb-heading').html();
   var desc_left = jQuery('.current_slide .sb-content').html();

   var img_right = jQuery('.next_slide img').attr('src');
   var heading_right = jQuery('.next_slide .sb-heading').html();
   var desc_right = jQuery('.next_slide .sb-content').html();


   jQuery('.current_slide img').attr('src', img_right);
   jQuery('.current_slide .sb-heading').html(heading_right);
   jQuery('.current_slide .sb-content').html(desc_right);

   jQuery('.next_slide img').attr('src', img_left);
   jQuery('.next_slide .sb-heading').html(heading_left);
   jQuery('.next_slide .sb-content').html(desc_left);

   jQuery("#home-carousel2-heading").html(jQuery('.current_slide .sb-heading').html());
   jQuery("#home-carousel2-content").html(jQuery('.current_slide .sb-content').html());
}

function calculate_dimensions(){
   jQuery('.current_slide').css('width', jQuery('#bh_slider_right_content').width() * 0.62 + 'px');
   jQuery('.next_slide').css('width', jQuery('#bh_slider_right_content').width() * 0.32 + 'px');
   var small_height = jQuery('.current_slide img').height();
   if (small_height > jQuery('.next_slide img').height()) {
       small_height = jQuery('.next_slide img').height();
   }
   jQuery('#bh_slider_right_content').css('height', small_height + 'px');
   jQuery('#bh_slider_right_content li,.slide-button-prev,.slide-button-next').on('click', function () {
       swap_images();
   })
}

jQuery(document).ready(function () {
   if (jQuery(window).width() > 767) {
       calculate_dimensions();
   }
   /*
    jQuery('#bh_slider_right_content li').on("swipeleft", function () {
    swap_images();
    });
    jQuery('#bh_slider_right_content li').on("swiperight", function () {
    swap_images();
    }); */

   jQuery(".properties-tab-img").on("click", function () {
       tab_id = jQuery(this).attr("data-tab-id");
       console.log(tab_id);
       jQuery(".properties-tab-content").removeClass("active show");
       jQuery("#" + tab_id).addClass("active show");
   });

   jQuery(".properties_show_more").on("click", function () {
       tab_id = jQuery(this).attr("data-tab-id");
       console.log(tab_id);
       //jQuery(".properties-mobile-content").removeClass("active show");
       jQuery("#" + tab_id).addClass("active show");
   });

   jQuery(".vertical-pagination-menu").on("click", function () {
       //jQuery(".vertical-pagination-menu").removeClass("active");
       //jQuery(this).addClass("active");
       target = jQuery(this).attr("data-section-id");
       var scroll_top = jQuery("#" + target).offset().top;
       jQuery("html, body").animate({
           scrollTop: scroll_top
       }, 800);
   });

   jQuery(".scroll_to_link").on("click", function () {
       target = jQuery(this).attr("data-section-id");
       var scroll_top = jQuery("#" + target).offset().top;
       jQuery("html, body").animate({
           scrollTop: scroll_top
       }, 800);
   });

   jQuery(window).on('resize',function(){
       if(jQuery(this).width() < 767){
           jQuery('.current_slide').css('width', '');
           jQuery('.next_slide').css('width', '');
       }else{
           calculate_dimensions();
       }    
   });

   //jQuery("#vertical-pagination").scrollspy();
});
    ;
    /*
     jQuery('#bh_slider_right_content li').on("swipeleft", function () {
     swap_images();
     });
     jQuery('#bh_slider_right_content li').on("swiperight", function () {
     swap_images();
     }); */

    jQuery(".properties-tab-img").on("click", function () {
        tab_id = jQuery(this).attr("data-tab-id");
        console.log(tab_id);
        jQuery(".properties-tab-content").removeClass("active show");
        jQuery("#" + tab_id).addClass("active show");
    });

    jQuery(".properties_show_more").on("click", function () {
        tab_id = jQuery(this).attr("data-tab-id");
        console.log(tab_id);
        //jQuery(".properties-mobile-content").removeClass("active show");
        jQuery("#" + tab_id).addClass("active show");
    });

    jQuery(".vertical-pagination-menu").on("click", function () {
        //jQuery(".vertical-pagination-menu").removeClass("active");
        //jQuery(this).addClass("active");
        target = jQuery(this).attr("data-section-id");
        var scroll_top = jQuery("#" + target).offset().top;
        jQuery("html, body").animate({
            scrollTop: scroll_top
        }, 800); 
    });

    jQuery(".scroll_to_link").on("click", function () {
        target = jQuery(this).attr("data-section-id");
        var scroll_top = jQuery("#" + target).offset().top;
        jQuery("html, body").animate({
            scrollTop: scroll_top
        }, 800);
    });

    //jQuery("#vertical-pagination").scrollspy();
});



jQuery(window).on('scroll', function () {
    var stp = jQuery(this).scrollTop();
    var diff = 999999999999999999;
    var sec_id = '';
    jQuery('.panel').each(function () {
        var stv = jQuery(this).offset().top;

        var tdiff = Math.abs(stv - stp);
        var temp_sec_id = jQuery(this).attr('id');
        if (tdiff < diff) {
            diff = tdiff;
            sec_id = temp_sec_id;
        }
    });
    jQuery('#vertical-pagination').find('a').removeClass('active')
    jQuery('#vertical-pagination').find('a[data-section-id="' + sec_id + '"]').addClass('active')
})