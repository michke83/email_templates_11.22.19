<div class="bh_thumb_text">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <div class="grid">

                    <?php
                    if (have_rows('properties')):
                        $i = 1;
                        while (have_rows('properties')): the_row();


                            $title = get_sub_field("title");
                            $description_heading = get_sub_field("description_heading");
                            $description_content = get_sub_field("description_content");
                            $img = get_sub_field("image");
                            $properties_txt = get_sub_field("properties_text");
                            $link = get_sub_field("link");
                            $id = get_sub_field("id");
                            ?>
                            <div class="cell <?php echo ($i % 2 == 0) ? 'cell_even' : ''; ?>">
                                <div class="cell-title">
                                    <h4><?php echo $title; ?></h4>
                                </div>
        <!--                                <a class = "properties-tab-img" data-tab-id = "<?php echo $id; ?>">
                                    <img src="<?php // echo $img;   ?>" alt="<?php // echo $id;   ?>" class="img-fluid d-none d-sm-block d-md-block d-lg-block">
                                </a>-->

                                <a class = "properties-tab-img" data-tab-id = "<?php echo $id; ?>">
                                    <div class="bh-img-bg d-none d-sm-block d-md-block d-lg-block" style="background-image:url('<?php echo $img ?>');"></div>
                                </a>

                                <!--                                <a>
                                                                    <img src="<?php // echo $img;   ?>" alt="<?php // echo $id;   ?>" class="img-fluid d-block d-sm-none d-md-none d-lg-none">
                                                                </a> -->

                                <a class = "properties-tab-img" data-tab-id = "<?php echo $id; ?>">
                                    <div class="bh-img-bg d-block d-sm-none d-md-none d-lg-none" style="background-image:url('<?php echo $img ?>');"></div>
                                </a>

                            </div>



                            <div class="thumbnail_content d-block d-sm-none d-md-none d-lg-none">
                                <div class="entry-header">
                                    <h3><?php echo $description_heading; ?></h3>
                                </div>
                                <div class="show_more properties_show_more" href="#mobile-<?php echo $id; ?>" data-tab-id = "<?php echo "mobile-" . $id; ?>">
                                    <h4>SHOW MORE <i class="fa fa-chevron-circle-down" aria-hidden="true"></i></h4>
                                </div>
                                <div class="thumbnail_properties_link primary">
                                    <p><a target="_blank" href="<?php echo ($link) ? $link : '#'; ?>"><?php echo ($properties_txt) ? $properties_txt : 'SEE THE  PROPERTIES'; ?></a></p>
                                </div>

                            </div>

                            <div class="d-block d-sm-none d-md-none d-lg-none">
                                <div class="thumbnail_content tab-content">
                                    <div class="tab-pane fade in properties-mobile-content" id="mobile-<?php echo $id; ?>">
                                        <div class="entry-content">
                                            <?php echo $description_content; ?>
                                        </div>
                                        <div class="thumbnail_properties_link d-none d-sm-block d-md-block d-lg-block">
                                            <p><a href="<?php echo ($link) ? $link : '#'; ?>"><?php echo ($properties_txt) ? $properties_txt : 'SEE THE  PROPERTIES'; ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div> 

                            <?php
                            $i++;
                        endwhile;
                        ?>
                    <?php endif;
                    ?>



                </div>
            </div>

            <div class="col-md-5 d-none d-sm-block d-md-block d-lg-block">
                <div class="thumbnail-align">
                    <div class="thumbnail_content tab-content">
                        <?php
                        if (have_rows('properties')):
                            $j = 1;
                            while (have_rows('properties')): the_row();


                                $title = get_sub_field("title");
                                $description_heading = get_sub_field("description_heading");
                                $description_content = get_sub_field("description_content");
                                $img = get_sub_field("image");
                                $properties_txt = get_sub_field("properties_text");
                                $link = get_sub_field("link");
                                $id = get_sub_field("id");
                                ?>
                                <div class="properties-tab-content tab-pane fade in <?php echo ($j == 1) ? 'active show' : ''; ?>" id="<?php echo $id; ?>">
                                    <div class="entry-header">
                                        <h3><?php echo $description_heading; ?></h3>
                                    </div>
                                    <div class="entry-content">
                                        <?php echo $description_content; ?>
                                    </div>
                                    <div class="thumbnail_properties_link">
                                        <p><a href="<?php echo ($link) ? $link : '#'; ?>"><?php echo ($properties_txt) ? $properties_txt : 'SEE THE  PROPERTIES'; ?></a></p>
                                    </div>
                                </div>

                                <?php
                                $j++;
                            endwhile;
                            ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
