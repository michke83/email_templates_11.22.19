<div class="service_thumbnails">
    <div class="container">
        <div class="row">
            <?php
            if (have_rows('si_images')):
                while (have_rows('si_images')): the_row();

                    $link = get_sub_field("link");
                    $img = get_sub_field("image");
                    ?>
                    <div class="col-md-4 col-sm-4">
                        <div class="thumbnail">
        <!--                    <a href="<?php //echo ($link) ? $link : '#';  ?>" target="_blank">
                                <img src="<?php //echo $img ?>" alt="Service Image" class="img-fluid">
                            </a>-->
                            <a href="<?php echo ($link) ? $link : '#'; ?>" target="_blank">
                                <div class="thumbnail-bg" style="background-image: url('<?php echo $img ?>')"></div>
                            </a>
                        </div>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>

        </div>
    </div>
</div>