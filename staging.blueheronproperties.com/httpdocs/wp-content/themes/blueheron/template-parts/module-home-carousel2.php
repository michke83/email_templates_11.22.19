<?php
$carousel_img1 = get_field("sb_section_img1");
$carousel_title1 = get_field("sb_section_title1");
$carousel_desc1 = get_field("sb_section_desc1");

$carousel_img2 = get_field("sb_section_img2");
$carousel_title2 = get_field("sb_section_title2");
$carousel_desc2 = get_field("sb_section_desc2");
?>

<div class="bh-textslider d-none d-sm-block d-md-block d-lg-block">
    <div class="slide-button-prev"></div>
    <div class="slide-button-next"></div>
    <div class="row">
        <div class="col-md-5" id = 'bh_slider2_content'>
            <div class="bh-textslider_content">
                <div class="entry-header" >
                    <h3 id = "home-carousel2-heading"><?php echo $carousel_title1; ?></h3>
                </div>
                <div class="entry-content" id = "home-carousel2-content">

                    <?php echo $carousel_desc1; ?>
                    <!--
                    <p>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                    </p> -->
                </div>
            </div>
        </div>
        <div class="col-md-7 pad-left0 pad-right0" id="bh_slider_right_content">

            <div class="bh-slick-slider2" >
                <ul>

                    <li class="current_slide" id = "current_slide">
                        <img src="<?php echo $carousel_img1; ?>">
                        <div class = "sb-heading" style = "display:none;"><?php echo $carousel_title1; ?></div>
                        <div class = "sb-content" style = "display:none;"><?php echo $carousel_desc1; ?></div>
                    </li>

                    <li class="next_slide" id = "next_slide">
                        <img src="<?php echo $carousel_img2; ?>">
                        <div class = "sb-heading" style = "display:none;"><?php echo $carousel_title2; ?></div>
                        <div class = "sb-content" style = "display:none;"><?php echo $carousel_desc2; ?></div>
                    </li>
                    <!--
                    <li class="next_slide canSlide" id = "next_slide" >
                        <img src="http://202.63.105.89/blueheron/wp-content/uploads/2018/01/fishingpoleondock.png" alt="home">
                    </li> -->

                </ul>

            </div>
        </div>
    </div>
</div>

<div class="bh-textslider d-block d-sm-none d-md-none d-lg-none">
    <div class="row">
        <div class="col-md-5" id = 'bh_slider2_content'>

        </div>
        <div class="col-md-12" id="bh_slider_right_content">

            <div class="bh-slick-slider2" >
                <ul>

                    <li class="current_slide" id = "current_slide">
                        <img src="<?php echo $carousel_img1; ?>">
                        <div class = "sb-heading" style = "display:none;"><?php echo $carousel_title1; ?></div>
                        <div class = "sb-content" style = "display:none;"><?php echo $carousel_desc1; ?></div>
                        <div class="bh-textslider_content">
                            <div class="entry-header" >
                                <h3 id = "home-carousel2-heading"><?php echo $carousel_title1; ?></h3>
                            </div>
                            <div class="entry-content" id = "home-carousel2-content">

                                <?php echo $carousel_desc1; ?>
                                <!--
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                                </p> -->
                            </div>
                        </div>
                    </li>

                    <li class="next_slide" id = "next_slide">
                        <img src="<?php echo $carousel_img2; ?>">
                        <div class = "sb-heading" style = "display:none;"><?php echo $carousel_title2; ?></div>
                        <div class = "sb-content" style = "display:none;"><?php echo $carousel_desc2; ?></div>
                        <div class="bh-textslider_content">
                            <div class="entry-header" >
                                <h3 id = "home-carousel2-heading"><?php echo $carousel_title1; ?></h3>
                            </div>
                            <div class="entry-content" id = "home-carousel2-content">

                                <?php echo $carousel_desc1; ?>
                                <!--
                                <p>
                                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum
                                </p> -->
                            </div>
                        </div>
                    </li>
                    <!--
                    <li class="next_slide canSlide" id = "next_slide" >
                        <img src="http://202.63.105.89/blueheron/wp-content/uploads/2018/01/fishingpoleondock.png" alt="home">
                    </li> -->

                </ul>

            </div>
        </div>
    </div>
</div>