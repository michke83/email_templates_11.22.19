<?php
$title = get_field("wtoes_title");
$desc = get_field("wtoes_description");
$ser_list1 = get_field("wtoes_services1");
$ser_list2 = get_field("wtoes_services2");
?>

<div class="service_section_2_container">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="entry-header">
                    <h3><?php echo $title; ?></h3>
                </div>
                <div class="entry-content">
                    <?php echo $desc; ?>

                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="services_menu">
                    <?php echo $ser_list1; ?>
                   
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="services_menu">
                    <?php echo $ser_list2; ?>
                   
                </div>
            </div>
        </div>
    </div>
</div>