<?php
$map_img  = get_field("map_image");
?>

<div class="lots_map">
    <div class="lots-img-sec">
        <a href = "<?php echo ($map_img) ? $map_img : '#'; ?>" target = "_blank">
            <img src="<?php echo ($map_img) ? $map_img : get_template_directory_uri().'/images/BH-Map-Draft.jpg'; ?>" class="img-fluid" alt="map" style="width: 100%;display: block;"/>
        </a>
    </div>
</div>