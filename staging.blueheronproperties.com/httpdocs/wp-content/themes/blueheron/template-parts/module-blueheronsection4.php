<?php
$title = get_field("ep_title","option");
$more_text = get_field("ep_more_text","option");
$more_link = get_field("ep_more_link","option");
$bg_img = get_field("ep_background_image","option");
?>
<div class="possibilities" style = "background-image: url(<?php echo ($bg_img) ? $bg_img : get_template_directory_uri().'/images/manfishingshot.png'; ?>);">
    <div class="overlay"></div>
    <div class="container">
        <div class="sec4-section">
            <div class="entry-header">
                <h3><?php echo ($title) ? $title : "Explore ther Possibilities with Blue Heron."; ?></h3>
            </div>
            <div class="entry-content">
                <p><a href="<?php echo ($more_link) ? $more_link: "#"; ?>"><?php echo ($more_text) ? $more_text: "LEARN MORE"; ?></a></p>
            </div>
        </div>
    </div>
</div>