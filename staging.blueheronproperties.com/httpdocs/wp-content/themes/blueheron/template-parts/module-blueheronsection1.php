
<?php
$title = get_field("mid_sec_title");
$desc  = get_field("mid_sec_description");
$explore_text = get_field("mid_sec_explore_text");
$go_to_section = get_field("mid_sec_go_to_section");
$catalogue_brochure = get_field("cataloguebrochure")
?>
<div class="container">
    <div class="sec1-section">
        <div class="entry-header">
            <h2><?php echo $title; ?></h2>
            <hr class="line">
        </div>
        <div class="entry-header">
            <?php echo $catalogue_brochure; ?>
        </div>
        <div class="entry-content">
            <?php echo $desc;?>
            <p><a class = 'scroll_to_link' data-section-id = "<?php echo $go_to_section;?>"><?php echo $explore_text; ?></a></p>
            
        </div>
    </div>
</div>