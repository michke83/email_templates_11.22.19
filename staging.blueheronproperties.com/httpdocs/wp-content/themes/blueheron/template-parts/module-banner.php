<?php if(is_front_page()): ?>


<div class="bh_homeslider">
    <div class="bh-slick-slider">
        <?php
            
        if (have_rows('hs_slider')):
            while (have_rows('hs_slider')): the_row(); 
                    
                   
                $title = get_sub_field("title");
                $bg_img   = get_sub_field("background_image");
            ?>
                <div class="hero hero-1" style = "background-image: url(<?php echo ($bg_img) ? $bg_img : get_template_directory_uri().'/images/houseimage.png'; ?>);"  >
                    <div class="overlay"></div>
                    <div class="slider_title">
                        <h1><?php echo $title;?></h1>
                        <p><a class = 'scroll_to_link' data-section-id = "section_1">EXPLORE</a></p>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
</div>
<?php else : ?>
<?php
$title = get_field("hdr_bnr_title");
$bg_img = get_field("hdr_bnr_background_image");
?>
<div class="inner_baner_img" style = "background-image: url(<?php echo ($bg_img) ? $bg_img : get_template_directory_uri().'/images/manfishingshot.png'; ?>);">
    <div class="overlay"></div>
    <div class="container">
        <div class="banner-title">
            <div class="entry-header">
                <h3><?php echo ($title) ? $title : "Explore ther Possibilities with Blue Heron."; ?></h3>
            </div>
            
        </div>
    </div>
</div>
<?php 
endif;
?>