<?php
$logo = get_field("bh_logo","option");

?>
<div class="bh-menu-section">
    <div>
        <div class="bh-menu-container">
            <div class="header-logo <?php echo (!is_front_page()) ? 'inner-hdr-logo' : '';?>">
                <a href = "<?php echo home_url(); ?>"><img src="<?php echo ($logo) ? $logo : get_template_directory_uri().'images/logo-white.png'; ?>" alt="header-logo"/></a>
            </div>
            <div class="bh_social_icons">
                <ul>
                    <?php
                    if (have_rows('si_list','option')):
                    while (have_rows('si_list','option')): the_row();
                        $name = get_sub_field("name",'option');
                        $url  = get_sub_field("url",'option');
                    ?>
                    <li><a target="_blank" href="<?php echo ($url) ? $url : '#' ?>"><i class="fa fa-<?php echo $name;?>" aria-hidden="true"></i></a></li>
                    <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="button_container" id="toggle">
                <span class="top"></span>
                <span class="middle"></span>
                <span class="bottom"></span>
            </div>

            <div class="overlay" id="overlay">
                <nav class="overlay-menu">
                    
                <?php
                $menuitems = return_wp_clean_menu_items(wp_get_nav_menu_items('Main Menu'));
                global $post;
                $currentmenu = get_the_ID();
                $ctr = 1;
                if ($menuitems) {
                    ?>
                    <ul>
                        <?php
                        foreach ($menuitems as $menu) {
                            $activemenu = $menu['menu']->object_id == $currentmenu ? 'current-menu-item' : '';
                            $liclass = return_wp_menu_attr($menu['menu'], 'li') . $activemenu;
                            $liclass .= isset($menu['sub-menu']) ? ' menu-item-has-children' : '';
                            $linkattr = return_wp_menu_attr($menu['menu'], 'link');
                            ?>         

                            <li  class="<?php echo $liclass; ?>">
                                <?php 
                                
                                $menu_link = $menu['menu']->url;
                               
                                
                                ?>
                               
                                <a href="<?php echo $menu_link; ?>" class ="<?php echo ($activemenu == 'current-menu-item') ? 'current_menu': '';?>" >
                                    <?php echo esc_attr($menu['menu']->title); ?>
                                    <?php if (isset($menu['sub-menu'])) : ?>
                                    <span class="sf-top-icon"><b class="caret"></b></span>
                                    <?php endif; ?>         
                                </a>

                                <?php if (isset($menu['sub-menu'])) { ?>
                                    <ul class="sub-menu">
                                        <?php
                                        foreach ($menu['sub-menu'] as $submenu) {
                                            $activesubmenu = $submenu->object_id == $currentmenu ? ' current-menu-item' : '';
                                            $liclass = return_wp_menu_attr($submenu, 'li') . $activesubmenu;
                                            $linkattr = return_wp_menu_attr($submenu, 'link');
                                            ?>
                                            <li id="menu-item-<?php echo esc_attr($submenu->ID); ?>" class="<?php echo $liclass; ?>">
                                                <a <?php echo $linkattr; ?>><?php echo esc_attr($submenu->title); ?></a>
                                            </li>                        
                                        <?php }
                                        ?>
                                    </ul>

                                    <?php
                                }
                                $ctr++;
                                ?>
                                </li>
                            <?php
                            }
                            ?>
                            
                        
                    </ul>
                <?php }
                ?>
                   
                </nav>
            </div>
        </div>
    </div>
</div>