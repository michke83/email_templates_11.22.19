
<header class="bh_homebanner panel" id="bh_headerbanner" >
    <?php get_template_part('template-parts/module', 'banner'); ?>
</header>
<section class="section_1 panel" id="section_1" >
    <?php get_template_part('template-parts/module', 'blueheronsection1'); ?>
</section>
<section class="section_2 panel" id="section_2" >
    <?php get_template_part('template-parts/module', 'home-carousel2'); ?>
</section>

<section class="section_3 panel" id="section_3" >
    <?php get_template_part('template-parts/module', 'blueheronsection3'); ?>
</section>


