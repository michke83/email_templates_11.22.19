<?php
/* template name: Template Lots */
get_header();
?>
<header class="bh_homebanner inner_bh_banner" id="bh_headerbanner">
    <?php get_template_part('template-parts/module', 'banner'); ?>
</header>
<section class="section_1" id="section_1">
    <?php get_template_part('template-parts/module', 'blueheronsection1'); ?>
</section>
<section class="section_map" id='map-section'>
    <?php get_template_part('template-parts/module', 'lots-map'); ?>
</section>

<?php get_footer(); ?>
