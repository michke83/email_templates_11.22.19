<?php
/* template name: Template concierge */
get_header();
?>
<header class="bh_homebanner inner_bh_banner" id="bh_headerbanner">
    <?php get_template_part('template-parts/module', 'banner'); ?>
</header>
<section class="section_1 concierge_desc" id="section_1">
    <?php get_template_part('template-parts/module', 'blueheronsection1'); ?>
</section>

<section class="service_section_2" id="service_section_2">
    <?php get_template_part('template-parts/module', 'servicesection2'); ?>
</section>

<section class="service_section_3" id="service_section_3">
    <?php get_template_part('template-parts/module', 'servicesection3'); ?>
</section>
<?php get_footer(); ?>
