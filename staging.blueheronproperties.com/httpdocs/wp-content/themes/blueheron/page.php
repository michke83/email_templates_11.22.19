<?php get_header(); ?>

<header class="bh_homebanner inner_bh_banner" id="bh_headerbanner">
    <?php get_template_part('template-parts/module', 'banner'); ?>
</header>

<section class="default-temp" id="default-temp">

	    <div class="default-temp-sec">
	    	<div class="container">
      	  		<?php get_template_part('template-parts/content'); ?>	
        	</div>
    </div>

</section>

<?php get_footer(); ?>