<!DOCTYPE html>


<head>

    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <link href="https://fonts.googleapis.com/css?family=Montserrat:200,200i,300,300i,400,400i,500,500i,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Domine:400,700" rel="stylesheet">

    <?php
    $faviconsrclink_opt = wp_get_attachment_image_src(get_field('bh_favicon', 'option'), 'full');
    $faviconsrc = !empty($faviconsrclink_opt) ? $faviconsrclink_opt[0] : get_stylesheet_directory_uri() . '/images/favicon.ico';
    ?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo $faviconsrc; ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo $faviconsrc; ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $faviconsrc; ?>">
    <meta name="theme-color" content="#ffffff ">
    <?php wp_head(); ?>
    <?php $header_analytics = get_field("header_analytics","option");
    if($header_analytics !=''){
        echo $header_analytics;
    }
    ?>
 </head>
<?php $body_classes = array(); ?>
<body <?php body_class($body_classes); ?> > 
 <?php $body_analytics = get_field("body_analytics","option");
    if($body_analytics !=''){
        echo $body_analytics;
    }
    ?>
    <?php get_template_part('template-parts/module', 'menu'); ?>
    <?php
    $home_sections = array("bh_headerbanner", "section_1", "section_2", "section_3", "section_4", "bh_contact");
    $i = 0;
    ?>
    <?php if (is_front_page()): ?>
        <div class="vertical-pagination" id="vertical-pagination">
            <ul class="pagination">
                <?php foreach ($home_sections as $home_section): ?>
                    <?php $i++; ?>
                    <li><a class="vertical-pagination-menu <?php echo ($i == 1) ? 'active' : ''; ?>" data-section-id = "<?php echo $home_section; ?>"></a></li>
                    <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>



