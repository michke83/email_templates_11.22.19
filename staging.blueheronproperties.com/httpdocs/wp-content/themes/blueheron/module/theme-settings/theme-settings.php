<?php
$parent['menu_slug'] = 'site-general-settings';


if (function_exists('acf_add_options_page')) {


    /* Site Configurations */
    if (function_exists('acf_add_options_page')) {

        acf_add_options_sub_page(array(
            'page_title' => 'Site Configurations',
            'menu_title' => 'Site Configurations',
            'menu_slug' => 'site-configuration-settings',
            'capability' => 'edit_posts',
            'redirect' => false,
            'parent_slug' => $parent['menu_slug']
        ));
    }

    /* Social Media Links */
    if (function_exists('acf_add_options_page')) {

        acf_add_options_sub_page(array(
            'page_title' => 'Social Media Links',
            'menu_title' => 'Social Media Links',
            'menu_slug' => 'social-media-settings',
            'capability' => 'edit_posts',
            'redirect' => false,
            'parent_slug' => $parent['menu_slug']
        ));
    }


    /* Faqs */
    if (function_exists('acf_add_options_page')) {

        acf_add_options_sub_page(array(
            'page_title' => 'Explore Possibilties',
            'menu_title' => 'Explore Possibilties',
            'menu_slug' => 'explore',
            'capability' => 'edit_posts',
            'redirect' => false,
            'parent_slug' => $parent['menu_slug']
        ));
    }
    
    /* Contact Settings */
    if (function_exists('acf_add_options_page')) {

        acf_add_options_sub_page(array(
            'page_title' => 'Contact Settings',
            'menu_title' => 'Contact Settings Menu',
            'menu_slug' => 'contact_settings',
            'capability' => 'edit_posts',
            'redirect' => false,
            'parent_slug' => $parent['menu_slug']
        ));
    }
    if (function_exists('acf_add_options_page')) {

        acf_add_options_sub_page(array(
            'page_title' => 'Analytics Settings',
            'menu_title' => 'Analytics Settings Menu',
            'menu_slug' => 'analytics_settings',
            'capability' => 'edit_posts',
            'redirect' => false,
            'parent_slug' => $parent['menu_slug']
        ));
    }
}

function add_menus() {
    add_menu_page('Site Settings', 'Site Settings', 'manage_options', 'site-general-settings', 'submenu_callback', '', 3);
}

add_action('admin_menu', 'add_menus');

function submenu_callback() {

    $args = array(
        'post_type' => 'acf-field-group',
        'order_by' => 'name',
        'posts_per_page' => -1
    );

    $config = array(
      
        'brands' => array(
            'icon' => 'dashicons-groups',
            'disabled' => true
        ),
    );


    $field_groups = new WP_Query($args);
    
    //echo "<pre>";print_r($field_groups);exit;

    if (!empty($field_groups->posts)) {
        ?>
        <div class='wrap rd-site-settings'>
            <h2>Settings</h2>

            <div class="theme-browser rendered">
                <div class="themes wp-clearfix">
        <?php
        foreach ($field_groups->posts as $field_group) {
            $title = $field_group->post_title;
            $post_content = maybe_unserialize($field_group->post_content);
            $post_link_slug = isset($post_content['location'][0][0]['value']) ? $post_content['location'][0][0]['value'] : '';
            $settings_link = admin_url() . 'admin.php?page=' . $post_link_slug;
            $icon = isset($config[$field_group->post_excerpt]) ? $config[$field_group->post_excerpt]['icon'] : 'dashicons-admin-generic';
            $disable = isset($config[$field_group->post_excerpt]) ? $config[$field_group->post_excerpt]['disabled'] : false;
            ?>	
                        <?php if ((!$disable && $post_link_slug != 'default') &&  $post_content['location'][0][0]['param'] == "options_page") { ?>

                            <a href="<?php echo $settings_link; ?>">
                                <span class="dashicons <?php echo $icon; ?>"></span>
                            <?php echo $title; ?>
                            </a>

                        <?php } ?>

                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

                    <?php
                }
            }
            