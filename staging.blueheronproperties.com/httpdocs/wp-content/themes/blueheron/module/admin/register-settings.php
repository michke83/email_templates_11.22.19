<?php

/* theme setup */
add_action('after_setup_theme', 'register_script_setup', 15);

function register_script_setup() {
    add_action('wp_enqueue_scripts', 'bh_add_style', 99);
    add_action('wp_enqueue_scripts', 'bh_add_scripts', 99);
}

/* ADD STYLE SHEET FOR ADMIN AND FRONT PAGE */
if (!function_exists('bh_add_style')) {
    function bh_add_style() {
        if (!is_admin()) {

            if (WP_MINIFY) {
                wp_enqueue_style('maincss', get_template_directory_uri() . '/css/main.min.css', false, '1.1');
            } else {

                wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', false, '1.1');
                wp_register_style('jquery-ui', get_template_directory_uri() . '/css/jquery-ui.css', false, 1.1);
                wp_enqueue_style('jquery-ui');

                
                wp_enqueue_style('slick_css', get_template_directory_uri() . '/js/slick/slick.css', array(), 1.1);
                wp_enqueue_style('slick-theme', get_template_directory_uri() . '/js/slick/slick-theme.css', false, 1.1);
                wp_enqueue_style('fa-icons', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.7.0');
                wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.min.css', false, '1.1');
                
               // wp_enqueue_style('jquery-mobile', get_template_directory_uri() . '/js/jquery.mobile-1.4.5.min.css', array(), 1.1);
                
                wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', false, '1.1');
            }
        }
    }
}

/* ADD SCRIPT FRONT PAGE */

if (!function_exists('bh_add_scripts')) {
    function bh_add_scripts() {

        global $post;

        if (!is_admin()) {


            $is_home = '';
            if (is_front_page()) {
                $is_home = 1;
            }
            $local_vars = array(
                "home_url" => home_url(),
                "is_home" => $is_home
            );

            if (WP_MINIFY) {
                wp_register_script('site_scripts', get_template_directory_uri() . '/js/script.min.js', array('jquery'), '1.1', true);
                wp_localize_script('site_scripts', 'script_vars', $local_vars);
                wp_enqueue_script('site_scripts');
            } else {

                wp_register_script('slick', get_template_directory_uri() . '/js/slick/slick.min.js', array('jquery'), '1.1', true);
                wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.1', true);
                wp_register_script('wow', get_template_directory_uri() . '/js/jquery.wow.min.js', array('jquery'), '1.1', true);
                wp_register_script('script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.1', true);
                
                wp_enqueue_script('bootstrap');
                wp_enqueue_script('slick');
                wp_enqueue_script('wow');
               
                wp_localize_script('script', 'script_vars', $local_vars);
                wp_enqueue_script('script');
            }
        }
    }
}
