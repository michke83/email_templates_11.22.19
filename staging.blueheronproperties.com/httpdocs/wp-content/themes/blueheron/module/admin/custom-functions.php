<?php

/* function to return properly structured menu object */

if (!function_exists('return_wp_clean_menu_items')) {
    function return_wp_clean_menu_items($menuargs) {
        $menuarr = array();
        $submenuargs = $menuargs;

        if ($menuargs) {
            $ctr = 0;

            foreach ($menuargs as $menu):

                if ($menu->menu_item_parent == 0) {
                    $menuarr[$ctr]['menu'] = $menu;
                    $ctr1 = 0;

                    foreach ($menuargs as $submenu):

                        if ($submenu->menu_item_parent == $menu->ID) {
                            $menuarr[$ctr]['sub-menu'][$ctr1] = $submenu;
                            $ctr1++;
                        }

                    endforeach;

                    $ctr++;
                }

            endforeach;
        }

        return $menuarr;
    }
}

if (!function_exists('return_wp_menu_attr')) {
    function return_wp_menu_attr($args, $mode = 'li') {

        if ($mode == 'li') {
            $liclass = 'menu-item';
            $classattr = '';
            $liclass .=!empty($args->type) ? ' menu-item-type-' . esc_attr($args->type) : '';
            $liclass .=!empty($args->object) ? ' menu-item-object-' . esc_attr($args->object) : '';
            $liclass .=!empty($args->ID) ? ' menu-item-' . esc_attr($args->ID) : '';


            if (isset($args->classes)) {
                foreach ($args->classes as $key => $val) {
                    $classattr = explode('-slide-', $val);
                    $dataslide = ($classattr[0] === 'data') ? '" data-slide="' . end($classattr) . '' : null;
                    $liclass .= ' ' . esc_attr($val);
                }
            }

            $liclass .= ($dataslide != null) ? ' ' . $dataslide : null;
            return $liclass;
        } else if ($mode == 'link') {

            $linkattr = !empty($args->attr_title) ? ' title="' . esc_attr($args->attr_title) . '"' : '';
            $linkattr .=!empty($args->target) ? ' target="' . esc_attr($args->target) . '"' : '';
            $linkattr .=!empty($args->xfn) ? ' rel="' . esc_attr($args->xfn) . '"' : '';
            $linkattr .=!empty($args->url) ? ' href="' . esc_attr($args->url) . '"' : '';

            return $linkattr;
        }

        return '';
    }
}

/* list pages */
if (!function_exists('get_post_top_ancestor_id')) {

    /**
     * Gets the id of the topmost ancestor of the current page. Returns the current
     * page's id if there is no parent.
     * 
     * @uses object $post
     * @return int 
     */
    function get_post_top_ancestor_id() {

        global $post;

        if (is_page()) {
            if ($post->post_parent) {
                $ancestors = array_reverse(get_post_ancestors($post->ID));
                return $ancestors[0];
            }

            return $post->ID;
        }
    }

}

add_action('wp_footer', 'bh_footer_script');

if (!function_exists('bh_footer_script')) {

    function bh_footer_script() {
        $gacodecontent = get_field('google_analytics_code', 'option') ? get_field('google_analytics_code', 'option') : '';
        echo $gacodecontent;
    }
}


