<?php if(!is_page_template( 'page-templates/page-contact.php' )):?>
    <section class="section_4 panel" id="section_4">
        <?php get_template_part('template-parts/module', 'blueheronsection4'); ?>
    </section>
<?php endif; ?>
<section class="bh_contact panel" id="bh_contact">
    <?php get_template_part('template-parts/module', 'contact'); ?>
</section>
<?php if(is_page_template('page-templates/page-contact.php' )):?>
<section class="section_map" id='map-section'>
    <?php get_template_part('template-parts/module', 'contactlots-map'); ?>
</section>
<?php endif; ?>
<?php
$footer_logo = get_field("bh_footer_logo", "option");
$footer_address = get_field("bh_address", "option");

$copyright = get_field("bh_copyright_content", "option");
?>
<footer>
    <div class="footer-section">
        <div class="footersec1">
            <div class="footer_logo">
                <img src="<?php echo ($footer_logo) ? $footer_logo : get_template_directory_uri() . 'images/logo-black.png'; ?>" alt="footer-logo"/>
            </div>
            <div class="footer-address">
                <?php echo $footer_address; ?>

            </div>
            <div class="footer-socialicons">
                <ul>
                    <?php
                    if (have_rows('si_list', 'option')):
                        while (have_rows('si_list', 'option')): the_row();
                            $name = get_sub_field("name", 'option');
                            $url = get_sub_field("url", 'option');
                            ?>
                            <li><a target="_blank" href="<?php echo ($url) ? $url : '#' ?>"><i class="fa fa-<?php echo $name; ?>" aria-hidden="true"></i></a></li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        
        <?php
        $footer_logo2 = get_field("pinnacle_logo", "option");
        $footer_address2 = get_field("pinnacle_address", "option");

        ?>
        
        <div class="footersec2">
            <div class="footer_logo">
                <img src="<?php echo ($footer_logo2) ? $footer_logo2 : get_template_directory_uri() . 'images/logo-black.png'; ?>" alt="footer-logo"/>
            </div>
            <div class="footer-address">
                <?php echo $footer_address2; ?>

            </div>
            <div class="footer-socialicons">
                <ul>
                    <?php
                    if (have_rows('pinnacle_social_icon_list', 'option')):
                        while (have_rows('pinnacle_social_icon_list', 'option')): the_row();
                            $name = get_sub_field("name", 'option');
                            $url = get_sub_field("url", 'option');
                            ?>
                            <li><a target="_blank" href="<?php echo ($url) ? $url : '#' ?>"><i class="fa fa-<?php echo $name; ?>" aria-hidden="true"></i></a></li>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="termsconditions">
            <?php echo $copyright; ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>